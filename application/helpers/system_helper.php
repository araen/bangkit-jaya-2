<?php
function base_assets() {
    return APPPATH."assets/";
}

function get_bulan_all($full=true){
    if($full){
        $bulan=array(
            '01'=>'Januari',
            '02'=>'Pebruari',
            '03'=>'Maret',
            '04'=>'April',
            '05'=>'Mei',
            '06'=>'Juni',
            '07'=>'Juli',
            '08'=>'Agustus',
            '09'=>'September',
            '10'=>'Oktober',
            '11'=>'November',
            '12'=>'Desember'
        );
    } else {
        $bulan=array(
            '01'=>'Jan',
            '02'=>'Peb',
            '03'=>'Mar',
            '04'=>'Apr',
            '05'=>'Mei',
            '06'=>'Jun',
            '07'=>'Jul',
            '08'=>'Agus',
            '09'=>'Sept',
            '10'=>'Okt',
            '11'=>'Nov',
            '12'=>'Des'
        );
    }
    
    return $bulan;
}

function get_hari_all($full = true){
    if ($full) {
        return array(
            '1'=>'Senin',
            '2'=>'Selasa',
            '3'=>'Rabu',
            '4'=>'Kamis',
            '5'=>"Jum'at",
            '6'=>'Sabtu',
            '7'=>'Minggu'
            );
    } else {
        return array(
            '1'=>'Sen',
            '2'=>'Sel',
            '3'=>'Rab',
            '4'=>'Kam',
            '5'=>"Jum",
            '6'=>'Sab',
            '7'=>'Ming'
            );    
    }
}

function dateToIndo($date){
	$BulanIndo = array("Januari", "Februari", "Maret",
					   "April", "Mei", "Juni",
					   "Juli", "Agustus", "September",
					   "Oktober", "November", "Desember");

	$tahun = substr($date, 0, 4);
	$bulan = substr($date, 5, 2);
	$tgl   = substr($date, 8, 2);
	$jam   = substr($date, 11, 8);
	
	$result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun." ".$jam;		
	return($result);
}

function dateIndo($date=0,$day=false,$time=false,$format=0){ // 0=01 Januari 1970,1 = 01/01/1970
    $bulan=get_bulan_all(false);
    $hari=get_hari_all();
    $d=date('d',strtotime($date));
    $m=date('m',strtotime($date));
    $y=date('Y',strtotime($date));
    $n=date('N',strtotime($date));
    $t=date('H:i',strtotime($date));
    if($day){
        switch($format){
            case 0 : $date = $hari[$n].", ".$d." ".$bulan[$m]." ".$y." ";
            break;
            case 1 : $date = "$hari[$n], $d/$m/$y ";
        }
    }else{
        $date = $d." ".$bulan[$m]." ".$y." ";
    }    
    if($time)
        $date .= $t;
        
    return $date;
}

function terbilang( $num ,$dec=4){
    $stext = array(
        "Nol",
        "Satu",
        "Dua",
        "Tiga",
        "Empat",
        "Lima",
        "Enam",
        "Tujuh",
        "Delapan",
        "Sembilan",
        "Sepuluh",
        "Sebelas"
    );
    $say  = array(
        "Ribu",
        "Juta",
        "Milyar",
        "Triliun",
        "Biliun", // remember limitation of float
        "--apaan---" ///setelah biliun namanya apa?
    );
    $w = "";

    if ($num <0 ) {
        $w  = "Minus ";
        //make positive
        $num *= -1;
    }

    $snum = number_format($num,$dec,",",".");
    //die($snum);
    $strnum =  explode(".",substr($snum,0,strrpos($snum,",")));
    //parse decimalnya
    $koma = substr($snum,strrpos($snum,",")+1);

    $isone = substr($num,0,1)  ==1;
    if (count($strnum)==1) {
        $num = $strnum[0];
        switch (strlen($num)) {
            case 1:
            case 2:
                if (!isset($stext[$strnum[0]])){
                    if($num<19){
                        $w .=$stext[substr($num,1)]." Belas";
                    }else{
                        $w .= $stext[substr($num,0,1)]." Puluh ".
                            (intval(substr($num,1))==0 ? "" : $stext[substr($num,1)]);
                    }
                }else{
                    $w .= $stext[$strnum[0]];
                }
                break;
            case 3:
                $w .=  ($isone ? "Seratus" : terbilang(substr($num,0,1)) .
                    " Ratus").
                    " ".(intval(substr($num,1))==0 ? "" : terbilang(substr($num,1)));
                break;
            case 4:
                $w .=  ($isone ? "Seribu" : terbilang(substr($num,0,1)) .
                    " Ribu").
                    " ".(intval(substr($num,1))==0 ? "" : terbilang(substr($num,1)));
                break;
            default:
                break;
        }
    }else{
        $text = $say[count($strnum)-2];
        $w = ($isone && strlen($strnum[0])==1 && count($strnum) <=3? "Se".strtolower($text) : terbilang($strnum[0]).' '.$text);
        array_shift($strnum);
        $i =count($strnum)-2;
        foreach ($strnum as $k=>$v) {
            if (intval($v)) {
                $w.= ' '.terbilang($v).' '.($i >=0 ? $say[$i] : "");
            }
            $i--;
        }
    }
    $w = trim($w);
    if ($dec = intval($koma)) {
        $w .= " Koma ". terbilang($koma);
    }
    return trim($w);
}

function send_mail($param)
{
    $ci = get_instance();   
    $ci->load->library('email');
    $ci->load->model('model_setting','setting');
    $option = $ci->setting->get_setting();
    
    $config['protocol']     = 'smtp';  
    $config['smtp_host']    = $option['smtphost'];  
    $config['smtp_port']    = (int)$option['smtpport'];  
    $config['smtp_timeout'] = $option['smtptimeout'];  
    $config['smtp_user']    = $option['smtpuser']; 
    $config['smtp_pass']    = $option['smtppass'];
    $config['mailtype']     = 'html'; 
    $config['wordwrap']     = TRUE;
    $config['charset']      = 'utf-8';
    $config['validation']   = TRUE;    
    $ci->email->initialize($config);
    $ci->email->set_newline("\r\n");
    $ci->email->from($option['smtphost'],$param['name']);
    $ci->email->to($option['email_contact']); 
    $ci->email->subject($param['subject']);
    $ci->email->message($param['message']);    
    if($ci->email->send())
        return 'success';
    else
        return $ci->email->print_debugger();
}

function sendEmail($param) {
        
    $ci = get_instance();
    $ci->load->library('mailer'); 
    $option = $ci->setting->get_setting(); 

    $mail = $ci->mailer->getMailer();

    $mail->IsSMTP();
    $mail->SMTPDebug = 0;

    $mail->Host = $option['smtphost'];
    $mail->Port = (int)$option['smtpport']; 
    $mail->Username = $option['smtpuser'];  
    $mail->Password = $option['smtppass'];
    $mail->From = $param['from']; 
    $mail->FromName = $param['name']; 
    $mail->Subject = $param['subject'];
    $mail->IsHTML(true);
    $mail->Body = $param['message'];
    $mail->AddAddress($option['email_contact']);
    
    if(!$mail->Send()) {
        $error = 'Mail error: '.$mail->ErrorInfo; 
        return false;
    }
    return true;
}

function send_mail1($param)
{
    /*$ci = get_instance();   
    $ci->load->library('email');

    $config['mailtype']     = 'html'; 
    $config['wordwrap']     = TRUE;
    $config['charset']      = 'utf-8';
    $config['validation']   = TRUE;    
    $ci->email->initialize($config);
    $ci->email->set_newline("\r\n");
    $ci->email->from($param['from'],$param['name']);
    $ci->email->to($param['to']); 
    $ci->email->subject($param['subject']);
    $ci->email->message($param['message']);    
    if($ci->email->send())
        return 'success';
    else
        return $ci->email->print_debugger();*/
    $to         = implode(',',$param['to']);    
    $subject    = $param['subject'];
    $body_mail .= $param['message'];
    $headers    = "From: $param[name]<$param[from]>\r\n";
    $headers   .= "MIME-Version: 1.0\n";
    $headers   .= "Content-Type: text/html; charset=iso-8859-1";
    mail($to, $subject, $body_mail, $headers);
}

function str_rand($length=0)
{
    $alphaNumeric = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
    return substr(str_shuffle($alphaNumeric), 0, $length);    
}

function romawi($n){
    $hasil = '';
    $iromawi = array('','I','II','III','IV','V','VI','VII','VIII','IX','X',20=>'XX',30=>'XXX',40=>'XL',50=>'L',
    60=>'LX',70=>'LXX',80=>'LXXX',90=>'XC',100=>'C',200=>'CC',300=>'CCC',400=>'CD',500=>'D',600=>'DC',700=>'DCC',
    800=>'DCCC',900=>'CM',1000=>'M',2000=>'MM',3000=>'MMM');
    if(array_key_exists($n,$iromawi)){
        $hasil = $iromawi[$n];
    }elseif($n >= 11 && $n <= 99){
        $i = $n % 10;
        $hasil = $iromawi[$n-$i] . romawi($n % 10);
    }elseif($n >= 101 && $n <= 999){
        $i = $n % 100;
        $hasil = $iromawi[$n-$i] . romawi($n % 100);
    }else{
        $i = $n % 1000;
        $hasil = $iromawi[$n-$i] . romawi($n % 1000);
    }
    return $hasil;
}

function diffTime($time1,$time2,$type='d'){
    $diff = $time1 - $time2;
    $arrDiff['d'] = floor($diff/86400);
    $sisa = $diff % 86400;
    $arrDiff['h'] = floor($sisa/3600);
    $sisa = $sisa % 3600;
    $arrDiff['m'] = floor($sisa/60);
    
    return $arrDiff[$type]; 
}

function fStrSecure($str)
{
    return str_replace("'","",$str);
}

function fPermalink($string)
{
    $url = preg_replace('~[^\\pL0-9_]+~u', '-', str_replace(" ","-",$string));
    $url = trim($url, "-");
    $url = iconv("utf-8", "us-ascii//TRANSLIT", $url);
    $url = strtolower($url);
    $url = preg_replace('~[^-a-z0-9_]+~', '', $url);
    return $url;
}

function fPaging($page,$limit,$total,$attribute=array())
{
    $total = $total;
    $totpage=0;
    $strPage = '<select ';
    
    foreach ($attribute as $key=>$value)
    {
        $strPage .= "$key='$value' ";    
    }
    
    $strPage .= ">";
    
    if (($total%$limit) == 0)
        $totpage=$total/$limit;
    else
        $totpage=(int)($total/$limit) + 1;
    
    $strPage .= "<option value='1' ".(1 == $page?'selected="selected"':'')." >&nbsp;1</option>";
    
    for( $i = 2 ; $i <= $totpage; $i++ )
    {
        $strPage .= "<option value='$i' ".($i == $page?'selected="selected"':'')." >&nbsp;$i</option>";
    }
    $strPage .= '</select>';
    
    return $strPage;
}

function fSelect($name='', $attribute=array(), $option=array(), $data='',$firstRow=false)
{
    $html = "<select id='$name' name='$name' ";
    
    if (isset($attribute))
    {
        foreach ($attribute as $key=>$value)
        {
            $html .= "$key = '$value' ";
        }
    }
    
    $html .= ">";
    
    if(is_bool($firstRow) and $firstRow==true)
    {
        $html .= "<option>-- pilih --</option>";    
    }
    elseif(is_string($firstRow))
    {
        $html .= "<option>$firstRow</option>";        
    }
    
    if (isset($option))
    {
        foreach ($option as $key=>$value)
        {
            $selected = ($key == $data)?"selected='selected'":"";
            
            $html .= "<option value='$key' $selected>$value</option>";
        }
    }
    
    $html .= "</select>";
    
    return $html;    
}

/*function fSelect($name='', $attribute=array(), $option=array(), $data='',$firstRow=false)
{
    $html = "<select id='$name' name='$name' ";
    
    if (isset($attribute))
    {
        foreach ($attribute as $key=>$value)
        {
            $html .= "$key = '$value' ";
        }
    }
    
    $html .= ">";
    
    if(is_bool($firstRow) and $firstRow==true)
    {
        $html .= "<option>-- pilih --</option>";    
    }
    elseif(is_string($firstRow))
    {
        $html .= "<option>$firstRow</option>";        
    }
    
    if (isset($option))
    {
        foreach ($option as $key=>$value)
        {
            $selected = ($key == $data)?"selected='selected'":"";
            
            $html .= "<option value='$key' $selected>$value</option>";
        }
    }
    
    $html .= "</select>";
    
    return $html;    
}*/

function fInputText($name='',$attribute=array(),$data=null)
{
    $html = "<input id='$name' name='$name' type='text' ";
        
    foreach ($attribute as $key=>$value)
    {
        $html .= "$key = '$value' ";
    }   
    if (!is_null($data) and $data <> '')
    {
        $html .= "value = '$data'";
    }
    $html .= " />";
    
    return $html;
}

function fInputHidden($name='',$data=null)
{
    $html = "<input id='$name' name='$name' type='hidden' ";
        
        if (!is_null($data) and $data <> '')
        {
            $html .= "value = '$data' ";
        }
        
    $html .= " />";
    
    return $html;
}

function fInputRadio($name='',$attribute=array(),$data=null)
{
    $html = "<input id='$name' name='$name' type='radio' ";
        
    foreach ($attribute as $key=>$value)
    {
        $html .= "$key = '$value' ";
        
        if (!is_null($data) and $data <> '')
        {
            if ($value == $data)
            {
                $html .= "checked = 'checked'";
            }
        }
    }
    
    $html .= " />";
    
    return $html;
}

function fInputCheck($name='',$attribute=array(),$data=null)
{
    $html = "<input id='$name' name='$name' type='checkbox' ";
        
    foreach ($attribute as $key=>$value)
    {
        $html .= "$key = '$value' ";
        
        if (!is_null($data) and $data <> '')
        {
            if ($value == $data)
            {
                $html .= "checked = 'checked'";
            }
        }
    }
    
    $html .= " />";
    
    return $html;
}

function fTextArea($name='',$attribute=array(),$data=null)
{
    $html = "<textarea id='$name' name='$name' ";
        
    foreach ($attribute as $key=>$value)
    {
        $html .= "$key = '$value' ";
    }
    
    $html .= '>';
    
    if (!is_null($data) and $data <> '')
    {
        $html .= $data;
    }
    
    $html .= "</textarea>";
    
    return $html;
}

function fHours($hr)
{
    for($i=12;$i<=120;$i+12)
    {
        if($i < 24)
        {
            $txtName = ' jam';
        }
        else                     
        {
            $txtName = ' hari';
        }
        $arrReturn[$i] = ($i==12)?$i.$txtName:(($i/12)/2).$txtName;
    }
    
    if($hr)
        return $arrReturn[$hr];
    else
        return $arrReturn;
}

function fCurrency($number,$dec = 0)
{
    return number_format($number,$dec,'.',',');
}

function fNumber($number) 
{
	//list($num, $des) = explode(",",$number);
	return str_replace(",","",$number);
}

function fStrRand($length=0,$lower=true)
{
    $alphaNumeric = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    if($lower){
        $alphaNumeric .= "abcdefghijklmnopqrstuvwxyz";    
    }
    $alphaNumeric.="123456789";
    return substr(str_shuffle($alphaNumeric), 0, $length);    
}

function dGetDate($date) 
{
	$strDate = explode('-',$date);
	return $strDate[2].'-'.$strDate[1].'-'.$strDate[0];
}

function orderStatus($status)
{
    switch($status){
        case 0 : echo 'Dibatalkan';
        break;
        case 1 : echo 'Order';
        break;
        case 2 : echo 'Konfirmasi';
        break;
        case 3 : echo 'Proses';
        break;
        case 4 : echo 'Selesai';
        break;
    }
}

function isOpname() {
    $ci = &get_instance();
    $ci->load->model("model_status", "status");
    
    $ostatus = $ci->status->get_value(1);
    
    return $ostatus['OSTATUSVALUE'];
}

function debug($data) {
    echo "<pre>";
    if ( is_array($data) )
        print_r($data);
    else
        echo $data;
    echo "</pre>";
    die();
}

function setHeaderFormat($format,$namafile){
	switch($format){
		case 'xls';
		case 'xls' :
			ob_clean();
			header("Content-Type: application/msexcel");
			header('Content-Disposition: attachment; filename="'.$namafile.'.xls"');
			break;
		default : header("Content-Type: text/html");
	}
}

function rekonversi($data = array(), $pcode, $number) {
    $conv1 = $data[$pcode]['CONV1'];
    $conv2 = $data[$pcode]['CONV2'];
    $conv3 = $data[$pcode]['CONV3'];
    
    $result['QTY1'] = 0;
    $result['QTY2'] = 0;
    $result['QTY3'] = 0;
    
    if ( $number == 0 )
        return $result;
    
    $real1 = $number / ($conv2 * $conv3);
    $num1 = floor($real1);
    
    $real2 = ($real1 - $num1) * $conv2;
    $num2 = floor($real2);
    
    $real3 = ($real2 - $num2) * $conv3;
    $num3 = floor($real3);
    
    $result['QTY1'] = $num1;
    $result['QTY2'] = $num2;
    $result['QTY3'] = $num3;
    
    return $result;
}

function uploadfile($path, $files, $upname) {
    $ci = & get_instance();
    
    if ($files[$upname]['error'] == 0) 
    {
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'xls|xlsx';
        
        $ci->load->library('upload', $config);
        
        if ( ! $ci->upload->do_upload($upname) ) 
        {
            debug($ci->upload->display_errors());    
        } 
        else 
        {
            $data = $ci->upload->data();
            
            return $data;
        }
    }    
}

function excelreader($file) {
    $ci = & get_instance();
    $ci->load->library('phpexcel');
    $ci->load->library('phpexcel/iofactory');
    
    try {
        $inputFileType = IOFactory::identify($file['full_path']);
        $objReader = IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file['full_path']);
    } 
    catch(Exception $e) 
    {
        die('Error loading file '.$file['file_name'].' : '.$e->getMessage());
    }
    
    $sheet = $objPHPExcel->getSheet(0)->toArray(null,true,true,true); ;

    return $sheet;    
}
