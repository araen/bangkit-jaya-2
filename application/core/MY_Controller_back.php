<?php
class MY_Controller extends CI_Controller {
    protected $controller;
    protected $cls_model;
    protected $data = array();
    
    function __construct()
    {
        parent::__construct();
        
        $this->load->model($this->cls_model);
        $model = $this->cls_model;
        $this->model = $this->$model;
        
        //$this->load_setting();
        
        if ($this->config->item('debug_db')){
            $sections = array(
                'queries' => TRUE,
                'config'  => FALSE,
                'benchmarks' => FALSE,
                'controller_info' => FALSE,
                'http_headers' => FALSE,
                'uri_string' => FALSE,
                'query_toggle_count' => FALSE,
            );
            $this->output->set_profiler_sections($sections);
            $this->output->enable_profiler(TRUE);
        }
        $this->data['cpanel_dir'] = $this->config->item('cpanel_dir');
    }
    
    public function view($view,$return = false)
    {
        if($return)
        {
            return $this->load->view($view,$this->data,$return);
        }
        else
        {
            $this->load->view($view,$this->data);    
        }
    }
    
    function delete($id)
    {
        $this->model->delete($id);
        redirect($this->controller);        
    }
    
    private function load_setting() {
        $this->load->model('model_setting','setting');
        $this->data['setting'] = $this->setting->get_setting();
    }
    
    public function curl($url, $fields = array()) {
        $fields_string = '';
        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
            rtrim($fields_string,'&');
        
        if (!function_exists('curl_exec')) {
            return false;
        }
        
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        curl_setopt($ch,CURLOPT_POST,count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
        curl_exec($ch);
        curl_close($ch);
    }
    
    function kcaptcha() {
        $this->load->library('kcaptcha');
        $captcha = new KCAPTCHA(); 
    }
}
?>
