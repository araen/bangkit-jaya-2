<?php
class MY_Model extends CI_Model {
    public $last_query = "";
    protected $str_table = "";
    protected $strSQL = "";
	protected $strID = "";
    protected $arr_param = array(
                    'page'      => 1,
                    'limit'     => 20,
                    'order'     => '',
                    'filter'    => '',
                    'q'         => ''
                );
    
    function __construct()
    {
        parent::__construct();
        
        $this->load->database();
    }
    
    function get_row($param=array(),$select = "*")
    {
        $arr_prm = array(
                        'page'  => 1,
                        'limit' => 1,
                        'filter'=> @$param['filter'],
                    );
        $return = array();
        $arr_return = $this->get_array_all($arr_prm,$select);

        if($arr_return){
            foreach($arr_return[0] as $key=>$value)
            {
                $return[$key] = $value;
            }
        }
        return $return;
    }
    
    function get_rows($param=array(),$key="ID",$value=array(),$first=false)
    {
        $return = array();
        
        $arr_return = $this->get_array_all($param);
        
        if($first)
            $return[''] = '';
        
        for ($i=0; $i<count($arr_return); $i++)
        {
            if (is_array($value))
            {
                $return[$arr_return[$i][$key]] = $arr_return[$i];    
            }
            else
            {
                $return[$arr_return[$i][$key]] = $arr_return[$i][$value];
            }  
        }
        
        return $return;
    }
    
    function get_all($param=array(), $select="*"){       
        if( empty($this->strSQL) ) 
        {
            $this->strSQL= "SELECT $select 
                            FROM {$this->str_table}";
        }
        foreach ($param as $key=>$value)
        {
            $this->arr_param[$key] = $value;
        }
        
        $arrToReturn['data'] = $this->list_array();
        $arrToReturn['total'] = $this->list_total();
        $this->db->close();
        
        return $arrToReturn;
    }
    
    function get_array_all($param=array(), $select="*"){       
        if( empty($this->strSQL) ) 
        {
            $this->strSQL= "SELECT $select FROM {$this->str_table}";
        } 

        foreach ($param as $key=>$value)
        {
            $this->arr_param[$key] = $value;
        }
        
        $arrToReturn = $this->list_array_all();;
        $this->db->close();
        
        return $arrToReturn;
    }
    
    function list_total() {
        $return = $this->db->query($this->strSQL)->num_rows();
        $this->db->close();
        return $return;
    }
    
    function list_array() {
        //seleksi kondisi query
        $str_condition = '';
        //jika menggunakan filter
        $str_condition = (($this->arr_param['filter']!='') ? " WHERE " . $this->arr_param['filter'] : '');
        //jika kata kunci ada (search berlaku), maka cari kata kunci tersebut pada semua field
        $str_search = '';
        if($this->arr_param['q']!=''){
            $str_condition .= (strpos($str_condition,'WHERE')===FALSE)?' WHERE ':' AND ';
            $bln_is_first=true;
            $queFields = $this->db->query($this->strSQL);
            foreach ($queFields->list_fields() as $field) {
                $arrCondition[] = "UPPER($field) LIKE '%".strtoupper($this->arr_param['q'])."%'";
            } 
            $str_condition .="(".implode(' OR ',$arrCondition).")";
        }
        //paging
        $int_start = ($this->arr_param['page']-1)*$this->arr_param['limit'];
        $int_limit = $this->arr_param['limit'];
        //rebuild main query, u/ dikondisikan
        $this->strSQL = "SELECT t00.* FROM (" . $this->strSQL . ") t00 " . $str_condition;
        
        $strSQLData = "SELECT t01.* FROM (" . $this->strSQL . " ";
		
		if(!empty($this->arr_param['order'] ))
		$strSQLData .= "ORDER BY " . $this->arr_param['order'] ." ";
        
		$strSQLData .= " ) t01 LIMIT $int_start,$int_limit";

        $return = $this->db->query($strSQLData)->result_array();
        
        $this->last_query = $this->db->last_query();
                
        $this->db->close();
        return $return;
    }
    
    function list_array_all() {
        //seleksi kondisi query
        $str_condition = '';
        //jika menggunakan filter
        $str_condition = (($this->arr_param['filter']!='') ? " WHERE " . $this->arr_param['filter'] : '');
        //jika kata kunci ada (search berlaku), maka cari kata kunci tersebut pada semua field
        $str_search = '';
        if($this->arr_param['q']!='') {
            $str_condition .= (strpos($str_condition,'WHERE')===FALSE)?' WHERE ':' AND ';
            $bln_is_first=true;
            $queFields = $this->db->query($this->strSQL);
            foreach ($queFields->list_fields() as $field) {
                $arrCondition[] = "UPPER($field) LIKE '%".strtoupper($this->arr_param['q'])."%'";
            } 
            $str_condition .="(".implode(' OR ',$arrCondition).")";
        }
        //paging
        $int_start = ($this->arr_param['page']-1)*$this->arr_param['limit'];
        $int_limit = $int_start+$this->arr_param['limit'];
        //rebuild main query, u/ dikondisikan
        $this->strSQL = "SELECT t00.* FROM (" . $this->strSQL . ") t00 " . $str_condition;
        
        $strSQLData = "SELECT t01.* FROM (" . $this->strSQL . " ";
		
		if(!empty($this->arr_param['order'] ))
		$strSQLData .= "ORDER BY " . $this->arr_param['order'] ." ";
        
		$strSQLData .= " ) t01";

        $return = $this->db->query($strSQLData)->result_array();
        
        $this->last_query = $this->db->last_query();
        
        $this->db->close();
        return $return;
    }
    
    function add($data){
        //$this->db->trans_start();
        foreach($data as $key=>$value){
            $field[] = $key;
            $rec[] = fStrSecure($value);
            
            $this->db->set($key,fStrSecure($value));
        }
        $this->db->insert($this->str_table);
        
        $this->last_query = $this->db->last_query();

        $return = $this->db->select_max($this->strID,$this->strID)
                    ->get($this->str_table)
                    ->row_array();
        //$this->db->trans_complete();       
        $this->db->close();
        
        return $return[$this->strID];
    }
    
    function edit($param,$data, $strKey = 'ID'){
        $this->db->trans_start();
        foreach($data as $key=>$value){
             $this->db->set($key,fStrSecure($value));
        }
        if(is_array($param))
        {
            foreach($param as $k=>$v)
            {
                $this->db->where($k,$v);
            }
        }
        else
        {
            $this->db->where($this->strID,$param);
        }
        $this->db->update($this->str_table);
        
        $this->last_query = $this->db->last_query();
        
        $this->db->trans_complete();
        $return = $this->db->trans_status();
        $this->db->close();
        
        return $return;
    }
    
    function delete($param, $strkey = 'ID'){
        $this->load->database();
        
        $this->db->trans_start();
        if(is_array($param))
        {
            foreach($param as $key=>$value)
            {
                $this->db->where($key,$value);
            }
        }
        else
        {
			$this->db->where($this->strID,$param);
        }
        $ok = $this->db->delete($this->str_table);
        
        $this->last_query = $this->db->last_query();
        
        $this->db->trans_complete();
        
        $return = $this->db->trans_status();
        
        $this->db->close();
        return $return;
    }
    
    function reset_query() {
        $this->arr_param = array();
        //$this->strSQL = null;
    }
	
	// mengubah format bilangan
	function formatNumber($num,$dec=0) {
		return number_format($num,$dec,',','.');
	}
}
?>
