<?php
class MY_Controller extends CI_Controller {
    protected $controller;
    protected $cls;
    protected $cls_model;
    protected $menu = array();
    protected $data = array();
    protected $hidemenu = array();
    
    function __construct()
    {
        parent::__construct();
        
        $this->load->model($this->cls_model);
        $model = $this->cls_model;
        $this->model = $this->$model;
        
        if(empty($this->menu)) {
            $idrole = $this->session->userdata('id_role');
            
            $this->menu = $this->auth->get_menu($idrole);   
        }
        
        $this->hidemenu = array();
        
        $this->htmlmenu = $this->_setMenu($this->menu);
        
        if ($this->config->item('debug_db')){
            $sections = array(
                'queries' => TRUE,
                'config'  => FALSE,
                'benchmarks' => FALSE,
                'controller_info' => FALSE,
                'http_headers' => FALSE,
                'uri_string' => FALSE,
                'query_toggle_count' => FALSE,
            );
            $this->output->set_profiler_sections($sections);
            $this->output->enable_profiler(TRUE);
        }
        
        $this->data = $this->auth->get_access($this->cls);
        
        $this->load_setting();
        
        $this->cls = $this->uri->segment(1);
        $this->controller = base_url().$this->uri->segment(1);
        
        $this->data['cls'] = $this->controller;
        $this->data['cpanel_dir'] = $this->config->item('cpanel_dir');
    }
    
    public function view($view,$return = false)
    {
        $this->data['transaction_message'] = $this->load->view('admin/_trans_message','',true);
        
        if($return)
        {
            return $this->load->view($view,$this->data,$return);
        }
        else
        {
            $this->load->view($view,$this->data);    
        }
    }
    
    function delete($id)
    {
        $status = $this->model->delete($id);

        if($status)
            $this->session->set_flashdata('success', 'Proses delete data berhasil');
        else
            $this->session->set_flashdata('error', 'Proses delete data gagal');
        redirect($this->controller);        
    }
    
    private function load_setting() {
        $this->load->model('model_setting','setting');
        $this->data['setting'] = $this->setting->get_setting();
    }
    
    public function curl($url, $fields = array()) {
        $fields_string = '';
        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
            rtrim($fields_string,'&');
        
        if (!function_exists('curl_exec')) {
            return false;
        }
        
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        curl_setopt($ch,CURLOPT_POST,count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
        curl_exec($ch);
        curl_close($ch);
    }
    
    function kcaptcha() {
        $this->load->library('kcaptcha');
        $captcha = new KCAPTCHA(); 
    }
    
    function _setMenu($arrMenu = array(),$level=1, $first = true)
    {
        $html = "";
        
        switch($level){
            case 1 :
                $html .= "<ul id='mainmenu' class='sf-menu'>";       
                break;
            case 2 :
                $html .= "<ul class='sf-menu'>";
                break;  
        }
        
        if($first)
            $html .= "<li><a href='".site_url($this->config->item('app_path')."main")."'>Home</a></li>";

        if(isset($arrMenu))
        {
            foreach($arrMenu as $key => $value)
            {
                if( !in_array($key, $this->hidemenu) ) {
                    if( !empty($arrMenu[$key]['SUB']) and $value['ISACTIVE'] == 1)
                    {
                        $html .= "<li><a href='javascript:void(0)'>$value[NAME]</a>";
                        $html .= $this->_setMenu($value['SUB'],$value['LEVEL']+1,false);
                        $html .= "</li>";
                    }
                    elseif($value['ISACTIVE'] == 1 and $value['ISREAD'] == 1)
                    {
                        $html .= "<li><a href='".site_url($this->config->item('app_path'))."$value[URL]'>$value[NAME]</a></li>";    
                    } 
                }      
            }    
        }
        $html .= "</ul>";
        
        return $html;            
    }
    
    function trans_message($status, $type='add', $addmsg='') 
    {
        switch ( $type ) 
        {
            case 'add' : $msg = 'tambah';break;        
            case 'edit' : $msg = 'edit';break;        
            case 'delete' : $msg = 'hapus';break;        
        }
        
        if($status)
            $this->session->set_flashdata('success', 'Proses '.$msg.' data berhasil'.(!empty($addmsg) ? ", $addmsg" : ""));
        else
            $this->session->set_flashdata('error', 'Proses '.$msg.' data gagal'.(!empty($addmsg) ? ", $addmsg" : ""));
    }
}
?>
