<?php
class Pajak{
	
function grab($url,$cache_file,$cache_time){
    if(file_exists($cache_file)&&filesize($cache_file)>10)
    {
        $mtime = filemtime($cache_file);
        $age = time() - $mtime;
        if ($cache_time > $age){
        $grab = $this->fetchcachefile($cache_file);
        }else{
           $grab = $this->fetchurl($url);
           $this->createcachefile($cache_file,$grab); 
        } 
    }
    else
    {
        $grab = $this->fetchurl($url);
        //$this->createcachefile($cache_file,$grab);
    }
    return $grab;
}
	
function extract_string($str_tag='', $data, $int_index=0)
{
    $result=explode($str_tag, $data);
    return $result[$int_index];
}

function fetchurl($url){
    if(function_exists('curl_init')){
     $data = curl_init();
     curl_setopt($data, CURLOPT_RETURNTRANSFER, 1);
     curl_setopt($data, CURLOPT_URL, $url);
     $hasil = curl_exec($data);
     curl_close($data);
     return $hasil;
    }else{
        return false;
    }
}

function fetchopen($url){
     ini_set ('allow_url_fopen', '1' );
     ini_set ('auto_detect_line_endings', 'Off') ;
     ini_set ('default_socket_timeout', '30' );
     ini_set ('max_execution_time', '0' );
     $f = fopen ($url, 'r' );
     $doc = '';
     while (! feof ($f) ){
     $doc = $doc . fgets($f, 3072) ;
     }

     if ($doc != '')
     {
        $a = strpos($doc, $html_start) ;
        $b = strpos($doc, $html_end) - $a ;
        $show = substr($doc, $a, $b) ;
     }else $show = "";
     return $show; 
}

function fetchcachefile ($cacheFile)
{
    $fp = fopen($cacheFile, "r");
    $cresults = fread($fp, filesize($cacheFile));
    fclose($fp);
    return $cresults;
}

function createcachefile($cachefile,$data){
    if(is_writable($cachefile)){
    $fp = fopen($cachefile, "w");
    fwrite ($fp, $data);
    fclose($fp);
    return true;
    }else{
        return false;
    }
}

function overwritecache($cachefile,$data){
    $fp = fopen($cacheFile, "r");
    createcachefile($fp,$data);
}
}
?>
