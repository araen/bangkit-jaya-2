<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promo extends MY_Controller {
    protected $cls_model = "model_promo";
    protected $controller;
	protected $view = "admin/promo/";
    
    function __construct()
    {
        parent::__construct();
        $this->controller = $this->config->item('base_url')."promo/"; 
    }
    
    public function index()
    {
        if($this->session->userdata('login') == TRUE){
            if(isset($_POST['page'])){
                $param = array(
                    'page'   => $_POST['page'],
                    'limit'  => 10,
                    'order'  => 'IDPROMO ASC',
                    'filter' => "",
                    'q'      => $_POST['q']
                );
            }
            else
            {
                $param = array(
                    'page'   => 1,
                    'limit'  => 10,
                    'order'  => 'IDPROMO ASC',
                    'filter' => "", 
                    'q'      => "",
                );        
            }

            $this->data['page_title'] = "Promo";
            $this->data['q'] = @$param['q'];
            $this->data['page'] = @$param['page'];
            $this->data['limit'] = @$param['limit'];
            $this->data['rows'] = $this->model->get_all($param);
            $this->data['content'] = parent::view($this->view.'grid',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    function form($idpromo=null)
    {
        $this->load->model('model_produk','produk');
		$this->load->model('model_brand','brand');
        $this->load->model('model_supplier','supplier');
        $this->load->model('model_jnsoutlet', 'jnsoutlet');
		$this->load->model('model_outlet_promo','outlet_promo');
        
        if($this->session->userdata('login') == TRUE)
        {
            $this->data['page_title'] = "Detail Produk";
            if($idpromo)
            {
                $this->data['row'] = $this->model->get_row(array('filter'=>"idpromo=$idpromo"));
                $this->data['outletpromo'] = $this->outlet_promo->get_array_all(array('filter' => "IDPROMO = '$idpromo'"));
                $this->data['jnsoutlet'] = $this->jnsoutlet->get_rows(array(), 'ATTCODE', 'ATTNAME');
            }
            
            if(isset($_POST['data']))
            {
                $data = $_POST['data'];
                $data['startdate'] = dGetDate($data['startdate']);
                $data['duedate'] = dGetDate($data['duedate']);
                $data['isitemdisc'] = (isset($data['isitemdisc'])) ? $data['isitemdisc'] : '';
                if($idpromo)
                {
                    $status = $this->model->edit($idpromo,$data);                 

                    $this->trans_message($status, 'edit');
                } else {
                    $idpromo = $this->model->add($data);
                    
                    $this->trans_message($idpromo, 'add');
                }
                
                if(isset($_POST['ottcode'])) {
                    $record['IDPROMO'] = $idpromo;    
                    $record['ATTCODE'] = $_POST['ottcode'];
                    
                    $this->outlet_promo->add($record);
                }
                
                redirect($this->controller."form/$idpromo");
            }
            
            $this->data['idpromo'] = $idpromo;
			$this->data['content'] = parent::view($this->view.'form',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");    
        }        
    }
    
    function template() {
        $this->load->helper('download');

        $data = file_get_contents(base_assets()."template/produk.xlsx");
        force_download('produk.xlsx',$data);
    }
    
    function delete_outlet_type($idpromo, $ott) {
        $this->load->model('model_outlet_promo','outlet_promo');
        
        $this->outlet_promo->delete(array('IDPROMO' => $idpromo, 'ATTCODE' => $ott));
        
        redirect($this->controller."form/$idpromo");    
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
