<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Autobill extends MY_Controller {
    protected $cls_model = "model_pembayaran";
    protected $controller;
	protected $view = "admin/autobill/";
    
    function __construct()
    {
        parent::__construct();
    }
    
    public function index($slsno = '')
    {
        $this->load->model('model_penjualan','penjualan');
        $this->load->model('model_penjualan_detail','detail');
        $this->load->model('model_setting','setting');
        $this->load->model('model_invoice','invoice');
        $this->load->model('model_sales','sales');
        $this->load->model('model_outlet','outlet');
        $this->load->model('model_stock','stock');
        
        if($this->session->userdata('login') == TRUE){
            if(isset($_POST['page'])){
                $param = array(
                    'page'   => $_POST['page'],
                    'limit'  => 10,
                    'order'  => 'ORDERNO DESC',
                    'filter' => "",
                    'q'      => $_POST['q']
                );
            }
            else
            {
                if ( !empty($slsno) ) {
                    $a_inv = $this->session->userdata('invoice');
                    $s_filter = ($this->session->userdata('invoice')) ? "AND INVNO IN('".implode("','",$a_inv)."')" : "";
                    
                    $filter = "SLSNO = '$slsno' $s_filter AND (TAGIHAN - PAYMENT) = 0";
                } else {
                    $filter = " (TAGIHAN - PAYMENT) <> 0 ";
                    $filter .= !empty($_POST['startdate']) ? "AND INVDATE BETWEEN '".dGetDate($_POST['startdate'])."' 
                                AND '".dGetDate($_POST['enddate'])."'" : "";
                }
                
                $param = array(
                    'page'   => 1,
                    'limit'  => 10,
                    'order'  => '(TAGIHAN - PAYMENT) DESC',
                    'filter' => $filter, 
                    'q'      => "",
                ); 
                
                if( empty($slsno) )
                    $this->session->unset_userdata('invoice');       
            }
            
            $setting = $this->setting->get_setting();
            
            if(isset($_POST['proses'])) {
                $a_inv = array();
                
                foreach ( $_POST['invoice'] as $key => $value ) {
                    $a_inv[] = $value;
                    $record['INVNO'] = $value;
                    $record['PAYDATE'] = date('Y-m-d');
                    $record['AMOUNT'] = $_POST['tagihan'][$key];

                    $status = $this->model->add($record);
                }
                
                $this->trans_message($status,'add');
                
                $this->session->set_userdata('invoice', $a_inv);
                
                redirect($this->controller.'/index/'.$_POST['sales']);
            }
            
            $this->data['slsno'] = $_POST['sales'];
            $this->data['preview'] = !empty($slsno) ? true : false;
            $this->data['startdate'] = $_POST['startdate'];
            $this->data['enddate'] = $_POST['enddate'];
            
            $this->data['page_title'] = "Proses Auto Bill";
            $this->data['q'] = @$param['q'];
            $this->data['page'] = @$param['page'];
            $this->data['limit'] = @$param['limit'];
            $this->data['rows'] = $this->model->get_all($param);
            $this->data['sales'] = $this->sales->get_rows(array('order'=>'SLSNO'),'SLSNO','SLSNAME');            
            $this->data['outlet'] = $this->outlet->get_rows(array('order'=>'OUTNO'),'OUTNO','OUTNAME');            
            $this->data['content'] = parent::view($this->view.'grid',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    function form($orderno=null)
    {
        $this->load->model('model_sales','sales');
        $this->load->model('model_warehouse','warehouse');
        $this->load->model('model_outlet','outlet');
        $this->load->model('model_produk','produk');
        $this->load->model('model_penjualan_detail','detail');
        
        if($this->session->userdata('login') == TRUE)
        {
            $this->data['page_title'] = "Proses Detail Order";
            if($orderno)
            {
                $this->data['row'] = $this->model->get_row(array('filter'=>"orderno='$orderno'"));
                $this->data['detail'] = $this->detail->get_array_all(array('filter'=>"ORDERNO = '$orderno'","order"=>"PCODE"));
            }
            if(isset($_POST['data']))
            {
                $data = $_POST['data'];

                if ( !$orderno ) 
                {
                    $warehouse = $this->warehouse->get_row(array('filter'=>"WRCODE = $data[wrcode]"));
					$sales = $this->sales->get_row(array('filter'=>"SLSNO = $data[slsno]"));
					$outlet = $this->outlet->get_row(array('filter'=>"OUTNO = '$data[outno]'"));
					$header['WRCODE'] = $data['wrcode'];
					$header['SLSNO'] = $data['slsno'];
					$header['OUTNO'] = $data['outno'];
                    $header['ORDERDATE'] = dGetDate($data['orderdate']);
                    $header['SUBTOTAL'] = $data['subtotal'];
                    
                    $orderno = $this->model->add($header);
                    
                    redirect($this->controller."form/$orderno");
                }
                
                if ( $orderno ) {
                    $konversi = $this->_konversi($data);
                    
                    $row = $this->detail->get_row(array('filter'=>"ORDERNO = '$orderno' AND PCODE = '$data[pcode]'"), "DB.*");
                    
                    if ( $row ) 
                    {
                        $detail['QTY'] = $row['QTY'] + fNumber($konversi['QTY']);
                        $detail['AMOUNT'] = $row['QTY'] + fNumber($konversi['AMOUNT']);
                        
                        $this->detail->edit(array("ORDERNO" => $orderno, "PCODE"=> $data['pcode']), $detail);    
                    } 
                    else 
                    {
                        $detail['ORDERNO'] = $orderno;
                        $detail['PCODE'] = $data['pcode'];
					    $detail['QTY'] = fNumber($konversi['qty']);
					    $detail['AMOUNT'] = fNumber($konversi['amount']);
                        
                        $this->detail->add($detail);
                    }
                }
                redirect($this->controller."form/$orderno");
            }
            $this->data['orderno'] = $orderno;
			$this->data['sales'] = $this->sales->get_rows(array('order'=>'SLSNO'),'SLSNO','SLSNAME');
			$this->data['warehouse'] = $this->warehouse->get_rows(array('order'=>'WRCODE'),'WRCODE','WRNAME');
			$this->data['outlet'] = $this->outlet->get_rows(array('order'=>'OUTNO'),'OUTNO','OUTNAME');
			$this->data['produk'] = $this->produk->get_rows(array('order'=>'PCODE'),'PCODE','PNAME');
            $this->data['content'] = parent::view($this->view.'form',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login"); 
        }        
    }
        
    function delete_detail($iddbeli, $iddetail) {
        $this->load->model('model_penjualan_detail','detail');
        
        $status = $this->detail->delete($iddetail);
        
        if ( $status )
            redirect($this->controller."form/$dorderno");    
    }
	
	private function _konversi($data) {
        $this->load->model('model_produk', 'produk');
        
        $produk = $this->produk->get_row(array('filter' => "PCODE = '$data[pcode]'"));
        
        $qtytotal = 0;
        $jmltotal = 0;
        
        if ( $data["qty1"] == 0 and $data["qty2"] == 0 and $data["qty3"] == 0 )
            return false;
        
        if ( $data["qty1"] > 0 ) {
            $qtytotal += $data["qty1"] * $produk["CONV1"] * $produk["CONV2"];
        }
        
        if ( $data["qty2"] > 0 ) {
            $qtytotal += $data["qty2"] * $produk["CONV2"];
        }
        
        if ( $data["qty3"] > 0 ) {
            $qtytotal += $data["qty3"];
        }
        
        $jmltotal = $qtytotal * $produk["PPRICE3"];
        
        $return['QTY'] = $qtytotal;
        $return['AMOUNT'] = $jmltotal;
        
        return $return;
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
