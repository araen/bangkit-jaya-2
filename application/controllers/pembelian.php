<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pembelian extends MY_Controller {
    protected $cls_model = "model_pembelian";
    protected $controller;
	protected $view = "admin/pembelian/";
    
    function __construct()
    {
        parent::__construct();
        $this->controller = $this->config->item('base_url')."pembelian/";
        
        $this->data['isopname'] = isOpname(); 
    }
    
    public function index()
    {
        if($this->session->userdata('login') == TRUE){
            if(isset($_POST['page'])){
                $param = array(
                    'page'   => $_POST['page'],
                    'limit'  => 10,
                    'order'  => 'IDBELI DESC',
                    'filter' => "",
                    'q'      => $_POST['q']
                );
            }
            else
            {
                $param = array(
                    'page'   => 1,
                    'limit'  => 10,
                    'order'  => 'IDBELI DESC',
                    'filter' => "", 
                    'q'      => "",
                );        
            }
            
            $this->data['page_title'] = "Transaksi Penerimaan Barang Unit";
            $this->data['q'] = @$param['q'];
            $this->data['page'] = @$param['page'];
            $this->data['limit'] = @$param['limit'];
            $this->data['rows'] = $this->model->get_all($param);
            
            $this->data['content'] = parent::view($this->view.'grid',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    function form($idbeli=null)
    {
        $this->load->model('model_warehouse','warehouse');
        $this->load->model('model_produk','produk');
        $this->load->model('model_stock','stock');
        $this->load->model('model_stockmove','stockmove');
        $this->load->model('model_supplier','supplier');
        $this->load->model('model_van','van');
        $this->load->model('model_pembelian_detail','detail');
        
        //define controller
        $this->session->set_userdata('controller', 'pembelian');
        
        if($this->session->userdata('login') == TRUE)
        {
            $this->data['page_title'] = "Detail Penerimaan Unit";
            
            $warehouse = $this->warehouse->get_all();
            
            foreach ( $warehouse['data'] as $wr ) 
                $a_warehouse[$wr['WRTYPE']][] = $wr['WRCODE'];
            
            $post = $this->input->post();
            
            if($post['data'] and !isOpname()) 
            {
                //get warehouse
                $record['WRCODE'] = $a_warehouse['U'][0];
                $record['TGLBELI'] = date('Y-m-d H:i:s');
                $record['POSTSTATUS'] = 1;
                $record['SUPPNO'] = $post['data']['suppno'];
                $record['REFDOCNO'] = $post['document_format'];
                $record['NOTE'] = $post['note'];
                
                // add header
                $idbeli = $this->model->add($record);

                if ( $idbeli ) 
                {
                    $t_total = 0;
                    
                    foreach ( $post['pcode'] as $key => $value ) {
                        $detail['IDBELI'] = $idbeli;
                        $detail['PCODE'] = $key;
                        $detail['QTYBELI'] = $post['pqty'][$key];
                        $detail['AMOUNTITEM'] = $post['pprice'][$key];
                        $detail['JMLBELI'] = $detail['QTYBELI'] * $detail['AMOUNTITEM'];
                        
                        $row = $this->detail->get_row(array('filter'=>"IDBELI = '$idbeli' AND PCODE = '$key'"), "DB.*");
                        
                        //add or update detail
                        if ( $row )
                            $status = $this->detail->edit(array("IDBELI" => $idbeli, "PCODE"=> $key), $detail);
                        else
                            $status = $this->detail->add($detail);
                        
                        //add stock move    
                        if($status) 
                        {
                            $stockmove['STMVCODE'] = 'R';    
                            $stockmove['WRCODE'] = $record['WRCODE'];    
                            $stockmove['PCODE'] = $key;    
                            $stockmove['STOCKNUM'] = $detail['QTYBELI'];    
                            $stockmove['BALANCE'] = empty($post['document_format']) ? -1 * $detail['AMOUNTITEM'] : 0; 
                            
                            $status = $this->stockmove->add($stockmove);
                            
                            // ketika jenis penerimaan unit dg refno, mengurangi stok log
                            if($post['document_format']) 
                            {
                                $stockmove['STMVCODE'] = 'M';    
                                $stockmove['WRCODE'] = $a_warehouse['L'][0];    
                                $stockmove['PCODE'] = substr($key, 0, 2) . '01';    
                                $stockmove['STOCKNUM'] = -1 * $detail['QTYBELI'];    
                                $stockmove['BALANCE'] = 0;
                                
                                $status = $this->stockmove->add($stockmove);    
                            }
                        }
                        
                        $t_total += $detail['JMLBELI'];
                    }
                    
                    //update total
                    $total['TOTALBELI'] = $t_total;
                    $this->model->edit(array("IDBELI" => $idbeli), $total);
                    
                    redirect('cart/reset_chart/R');
                }
            }
            
            if( $idbeli ) 
            {
                $this->data['idbeli'] = $idbeli;
                $this->data['row'] = $this->model->get_row(array('filter'=>"idbeli='$idbeli'"));
                $this->data['detail'] = $this->detail->get_array_all(array('filter'=>"IDBELI = '$idbeli'","order"=>"PCODE"));
            
                $suppliers = $this->supplier->get_array_all();
                
                $a_supplier = array();
                foreach ( $suppliers as $key => $value ) 
                    $a_supplier[$value['SUPPNO']] = $value;
                    
                $this->data['supplier'] = $a_supplier;
            }
            
            $this->data['content'] = parent::view($this->view.'form',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");    
        }        
    }
    
    function faktur($idbeli) {
        $this->load->model('model_produk','product');
        $this->load->model('model_pembelian_detail','detail');
        
        $this->data['konversi'] = $this->product->get_konversi(); 
        $this->data['data'] = $this->model->get_row(array('filter'=>"idbeli='$idbeli'")); 
        $this->data['detail'] = $this->detail->get_array_all(array('filter'=>"IDBELI = '$idbeli'","order"=>"PCODE"));

        parent::view($this->view.'faktur');    
    }
    
    function rekap($semester,$tahun) {
        if ( $semester == 1 ) { 
            $minthn = 1;
            $maxthn = 6;
        } else {
            $minthn = 7;
            $maxthn = 12;
        }
            
        $rows = $this->model->get_rekap(array('filter'=>"(CONVERT(BLNBAYAR,UNSIGNED INTEGER) BETWEEN $minthn AND $maxthn) and THNBAYAR = $tahun"));
    
        $i=0;
        $arrData = array();
        foreach ( $rows as $row ) {
            $arrData[$row['ID']]['NAMA'] = $row['NAMA'];
            $arrData[$row['ID']]['BLOK'] = $row['BLOK'];
            if ( $row['JNSBAYAR'] != "AG" ) {
                $arrData[$row['ID']]['JNSBAYAR'][$row['JNSBAYAR']]['JNS'] = $row['JNSBAYAR'];
                $arrData[$row['ID']]['JNSBAYAR'][$row['JNSBAYAR']]['JMLBAYAR'] = $row['JMLBAYAR'];
            }
            else {
                $arrData[$row['ID']]['JNSBAYAR'][$row['JNSBAYAR']][$row['BLNBAYAR']]['JNS'] = $row['JNSBAYAR'];
                $arrData[$row['ID']]['JNSBAYAR'][$row['JNSBAYAR']][$row['BLNBAYAR']]['JMLBAYAR'] = $row['JMLBAYAR'];
            }
        }
        
        $arrSemester = get_bulan_all();
        for ( $i = $minthn; $i <= $maxthn; $i++ ) {
            $a_semester[$i] = strtoupper($arrSemester[str_pad($i,2,'0',STR_PAD_LEFT)]);    
        }
        
		$this->data['ntahun'] = $tahun;
        $this->data['nsemester'] = $semester;
        $this->data['semester'] = $a_semester;
        $this->data['rows'] = $arrData;
        parent::view($this->view.'rekap');
    }
    
    function delete_detail($iddbeli, $iddetail) {
        $this->load->model('model_pembelian_detail','detail');
        
        $status = $this->detail->delete($iddetail);
        
        if ( $status )
            redirect($this->controller."form/$iddbeli");    
    }
    
    private function _konversi($data) {
        $this->load->model('model_produk', 'produk');
        
        $produk = $this->produk->get_row(array('filter' => "PCODE = '$data[pcode]'"));
        
        $qtytotal = 0;
        $jmltotal = 0;
        
        if ( $data["qtybeli1"] == 0 and $data["qtybeli2"] == 0 and $data["qtybeli3"] == 0 )
            return false;
        
        if ( $data["qtybeli1"] > 0 ) {
            $qtytotal += $data["qtybeli1"] * $produk["CONV2"] * $produk["CONV3"];
            $jmltotal += $data["qtybeli1"] * $produk["PPRICE1"];
        }
        
        if ( $data["qtybeli2"] > 0 ) {
            $qtytotal += $data["qtybeli2"] * $produk["CONV3"];
            $jmltotal += $data["qtybeli2"] * $produk["PPRICE2"];
        }
        
        if ( $data["qtybeli3"] > 0 ) {
            $qtytotal += $data["qtybeli3"];
            $jmltotal += $data["qtybeli3"] * $produk["PPRICE3"];
        }
        
        $return['QTYBELI'] = $qtytotal;
        $return['JMLBELI'] = $jmltotal;
        
        return $return;
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
