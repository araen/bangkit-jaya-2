<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MY_Controller {
    protected $cls_model   = "model_user";
		
    function __construct()
    {
        parent::__construct();
        $this->controller = "main";
    }
	
	public function index()
	{
		if($this->session->userdata('login') == TRUE){
            $this->load->model('model_order','order');
            //$this->load->model('model_stock','stock');
            
            $tahun = date('Y');
            
            $this->data['rekapsalesposting'] = $this->order->getRekapSales($tahun, 3);
            $this->data['rekapsalesunpost'] = $this->order->getRekapSales($tahun, 2);
            $this->data['rekapproduct'] = $this->order->getRekapProduct($tahun);
            //$this->data['rekapstock'] = $this->stock->getRekapStock();
            
            $this->data['content'] = $this->load->view('admin/grafik',$this->data,TRUE);
			parent::view("admin/index");
		}else{
			parent::view("admin/login");
		}
	}
	
	//================== Proses Login + Logout ================
	function login()
	{
        $this->load->model('model_role','role');		
		$_POST = $this->security->xss_clean($_POST);
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $result = $this->model_user->login($username,md5($password)); 
        
        if ( $result ) {
            $this->role->set_role();
        }
        
        redirect($this->controller);
	}
	
	function logout()
	{	
		$this->load->model('model_user');

        foreach($this->session->all_userdata() as $key=>$value)
        {
            $this->session->unset_userdata($key);
            $this->session->sess_destroy();
        }
		redirect($this->controller);
	}
    
	function grafik()
	{
		$this->load->model('model_laporan','grafik');
		
		$row = $this->model->getJmlBeli();
		$this->data['beli'] = $row['data'][0];
		
		$this->data['content'] = $this->load->view('admin/grafik',$this->data,TRUE);
	}
	
    function forgot()
    {
        $this->load->model('model_user');
        //$this->load->model('model_setting');
        $this->load->model('model_profile');
        $this->load->library('email');
   
        /*if(isset($_POST['email']))
        {
            $user = $this->model_user->get_all(array("filter"=>"EMAIL='$_POST[email]'"),"A.*");
            //$setting = $this->model_setting->get_all();
            $profile = $this->model_profile->get_all();
            foreach($profile->result() as $row)
                $arrProfile[$row->PROFCD] = $row->VALUE;
   
            if($user['total'] > 0)
            {
                $id = '';$name = '';$username = '';
                foreach($user['data'] as $row)
                {
                    $id = $row['ID'];
                    $name = $row['NAME'];
                    $username = $row['USERNAME'];
                }
                $newpass = str_rand(6);
                $message = "
                    Salam, $name.<br>
                    Atas permohonan untuk reset password, berikut ini username dan password yang baru :<br>
                    <dd>Username : $username</dd>
                    <dd>Password : $newpass</dd>
                    Mohon segera ubah password Anda melalui menu profil.<br>
                    Demikian informasi yang dapat kami sampaikan, semoga bermanfaat bagi Anda.  
                    ";
                $param = array(
                    'name'      => $arrProfile['profname'],
                    'from'      => $arrSetting['smtpuser'],
                    'subject'   => 'Reset Password',
                    'message'   => $message,
                    'to'        => array($_POST['email']) 
                );
                $mail = send_mail($arrProfile,$param);
                if($mail == 'success')
                {
                    $this->model_user->edit($id,array("PASSWORD"=>md5($newpass)));    
                }
                echo $mail;
                return;    
            }
            else
            {
                echo "fail";
                return;  
            }        
        }*/
        $this->load->view('admin/forgot');
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
