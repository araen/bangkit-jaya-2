<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pembayaran extends MY_Controller {
    protected $cls_model = "model_pembayaran";
    protected $controller;
	protected $view = "admin/pembayaran/";
    
    function __construct()
    {
        parent::__construct();
        $this->controller = $this->config->item('base_url')."pembayaran/";
    }
    
    public function index()
    {
        $this->load->model('model_sales','sales');
        $this->load->model('model_outlet','outlet');
        $this->load->model('model_warehouse','warehouse');
        
        if($this->session->userdata('login') == TRUE){
            if(isset($_POST['page'])){
                $param = array(
                    'page'   => $_POST['page'],
                    'limit'  => 10,
                    'order'  => '(TAGIHAN-PAYMENT) DESC, INVNO DESC',
                    'filter' => "",
                    'q'      => $_POST['q']
                );
            }
            else
            {
                $param = array(
                    'page'   => 1,
                    'limit'  => 10,
                    'order'  => '(TAGIHAN-PAYMENT) DESC, INVNO DESC',
                    'filter' => "", 
                    'q'      => "",
                );        
            }
            
            $this->data['outlet'] = $this->outlet->get_rows(array('order'=>'OUTNO'),'OUTNO','OUTNAME');
            $this->data['sales'] = $this->sales->get_rows(array('order'=>'SLSNO'),'SLSNO','SLSNAME');
            
            $this->data['page_title'] = "Transaksi Pembayaran";
            $this->data['q'] = @$param['q'];
            $this->data['page'] = @$param['page'];
            $this->data['limit'] = @$param['limit'];
            $this->data['rows'] = $this->model->get_all($param);
            $this->data['content'] = parent::view($this->view.'grid',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    function form($invno=null)
    {
        $this->load->model('model_van','van');
        $this->load->model('model_sales','sales');
        $this->load->model('model_setting','setting');
        $this->load->model('model_warehouse','warehouse');
        $this->load->model('model_invoice','invoice');
        $this->load->model('model_penjualan','penjualan');
        
        if($this->session->userdata('login') == TRUE)
        {
            $this->data['page_title'] = "Detail Pembayaran";
            
            if($invno)
            {
                $this->data['row'] = $this->invoice->get_row(array('filter'=>"INVNO='$invno'"));
                $this->data['order'] = $this->penjualan->get_array_all(array('filter'=>"INVNO = '$invno'",'order'=>'ORDERNO'));
                $this->data['payment'] = $this->model->get_array_all(array('filter'=>"INVNO = '$invno'",'order'=>'PAYNO'));
            }
            
            if(isset($_POST['data']))
            {
                $data = $_POST['data'];
                $data['paydate'] = date('Y-m-d');
                
                if ( isset($data['idpayment']) ) {
                    $tagihan = $this->data['row']['TAGIHAN'];
                    $realpay = ($this->data['row']['PAYMENT'] - $data['pybefore'] ) + $data['amount'];
                    
                    if ( ($realpay > $tagihan) and $data['amount'] > 0 ) 
                    {
                        $this->trans_message(0,'edit','jumlah pembayaran melebihi tagihan');    
                    } 
                    else 
                    {
                        $record['amount'] = $data['amount'];
                        $status = $this->model->edit($data['idpayment'], $record);
                        
                        $this->trans_message($status,'edit'); 
                    }           
                }
                else {
                    $tagihan = $this->data['row']['TAGIHAN'];
                    $realpay = $this->data['row']['PAYMENT'] + $data['amount'];
                    
                    if ( ($realpay > $tagihan) and $data['amount'] > 0 ) 
                    {
                        $this->trans_message(0,'add','jumlah pembayaran melebihi tagihan');    
                    } 
                    else 
                    {
                        $status = $this->model->add($data);
                        $this->trans_message($status,'add');
                    }
                }
                
                redirect($this->controller."form/$invno");
            }
            
            $setting = $this->setting->get_setting();
            $this->data['addpayment'] = (($this->data['row']['TAGIHAN'] == $this->data['row']['PAYMENT']) ) ? false : true;
            
            $this->data['sales'] = $this->sales->get_rows(array('order'=>'SLSNO'),'SLSNO','SLSNAME');
            $this->data['warehouse'] = $this->warehouse->get_rows(array('order'=>'WRCODE'),'WRCODE','WRNAME');
            
            $this->data['invno'] = $invno;
            $this->data['content'] = parent::view($this->view.'form',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login"); 
        }        
    }
    
    function delete_detail($invno, $idpayment) {
        $status = $this->model->delete($idpayment);

        if ( $status )
            redirect($this->controller."form/$invno");    
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
