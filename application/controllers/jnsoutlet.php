<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jnsoutlet extends MY_Controller {
    protected $cls_model = "model_jnsoutlet";
    protected $controller;
	protected $view = "admin/jnsoutlet/";
    
    function __construct()
    {
        parent::__construct();
        $this->controller = $this->config->item('base_url')."jnsoutlet/"; 
    }
    
    public function index()
    {
        if($this->session->userdata('login') == TRUE){
            if(isset($_POST['page'])){
                $param = array(
                    'page'   => $_POST['page'],
                    'limit'  => 10,
                    'order'  => 'ATTCODE ASC',
                    'filter' => "",
                    'q'      => $_POST['q']
                );
            }
            else
            {
                $param = array(
                    'page'   => 1,
                    'limit'  => 10,
                    'order'  => 'ATTCODE ASC',
                    'filter' => "", 
                    'q'      => "",
                );        
            }
            
            $this->data['page_title'] = "Jenis Outlet";
            $this->data['q'] = @$param['q'];
            $this->data['page'] = @$param['page'];
            $this->data['limit'] = @$param['limit'];
            $this->data['rows'] = $this->model->get_all($param);
            $this->data['content'] = parent::view($this->view.'grid',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    function form($attcode=null)
    {
        $this->load->model('model_jnsoutlet','jnsoutlet');
        
        if($this->session->userdata('login') == TRUE)
        {
            $this->data['page_title'] = "Detail Jenis Outlet";
            if($attcode)
            {	
                $this->data['row'] = $this->model->get_row(array('filter'=>"attcode='$attcode'"));
            }
            if(isset($_POST['data']))
            {
                $data = $_POST['data'];
				
                if($attcode)
                {
                    $this->model->edit($attcode,$data);                 
                } else {
                    $this->model->add($data);
                }
                redirect($this->controller);
            }
            $this->data['jnsoutlet'] = $this->jnsoutlet->get_rows(array('order'=>'ATTCODE'),'ATTCODE','ATTNAME');
            $this->data['content'] = parent::view($this->view.'form',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");    
        }        
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
