<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Opname extends MY_Controller {
    protected $cls_model = "model_status";
    protected $controller;
	protected $view = "admin/opname/";
    
    function __construct()
    {
        parent::__construct();
        $this->controller = $this->config->item('base_url')."opname/";
        
        $this->data['isopname'] = isOpname();
    }
    
    public function index()
    {
        $this->load->model('model_warehouse','warehouse');
        $this->load->model('model_produk','produk');
        $this->load->model('model_stock','stock');
        $this->load->model('model_stockmove','stockmove');
        $this->load->model('model_setting','setting');
        
        $setting = $this->setting->get_setting();
        $product = $this->produk->get_combo_product();
        
        if($this->session->userdata('login') == TRUE){
            if(isset($_POST['page'])){
                $param = array(
                    'page'   => $_POST['page'],
                    'limit'  => 100,
                    'order'  => '',
                    'filter' => "WRCODE = '$_POST[wrcode]' and STOCKDATE = '$setting[WRDATE]'",
                    'q'      => ''
                );
            }
            else
            {
                $param = array(
                    'page'   => 1,
                    'limit'  => 100,
                    'order'  => '',
                    'filter' => "WRCODE = '$_POST[wrcode]' and STOCKDATE = '$setting[WRDATE]'", 
                    'q'      => "",
                );        
            }
            
            if( $_POST['opname'] ) {
                $this->model->edit(array("OSTATUSCODE" => 1), array("OSTATUSVALUE" => $_POST['status']));
            }
            
            if( $_POST['save'] ) 
            {
                foreach ( $_POST['stock'] as $pcode => $value ) {
                    if( !empty($_POST['data'][$pcode]['qty1']) ) {
                        if( $value['qty1'] > $_POST['data'][$pcode]['qty1'] ) {
                            $stockmove['STMVCODE'] = 'O';    
                            $stockmove['WRCODE'] = $_POST['wrcode'];    
                            $stockmove['PCODE'] = $pcode;    
                            $stockmove['STOCKNUM'] = -1 * ($value['qty1'] - $_POST['data'][$pcode]['qty1']);    
                            $stockmove['BALANCE'] = -1 * ($stockmove['STOCKNUM'] * $product[$pcode]['PPRICE1']);
                        }
                        else {
                            $stockmove['STMVCODE'] = 'I';    
                            $stockmove['WRCODE'] = $_POST['wrcode'];    
                            $stockmove['PCODE'] = $pcode;    
                            $stockmove['STOCKNUM'] = ($_POST['data'][$pcode]['qty1'] - $value['qty1']);    
                            $stockmove['BALANCE'] = $stockmove['STOCKNUM'] * $product[$pcode]['PPRICE1'];    
                        }
                        
                        $status = $this->stockmove->add($stockmove);
                    }                        
                }
            }
            
            $this->data['wrcode'] = $_POST['wrcode'];
            $this->data['isopname'] = isOpname();
            
            $this->data['page_title'] = "Opname Gudang";
            $this->data['q'] = @$param['q'];
            $this->data['page'] = @$param['page'];
            $this->data['limit'] = @$param['limit'];
            $this->data['rows'] = $this->stock->get_all($param);
            $this->data['warehouse'] = $this->warehouse->get_rows(array('order'=>'WRCODE'),'WRCODE','WRNAME');
            $this->data['content'] = parent::view($this->view.'grid',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    private function _konversi($data) {
        $this->load->model('model_produk', 'produk');
        
        $this->produk->reset_query();
        $produk = $this->produk->get_row(array('filter' => "PCODE = '$data[pcode]'"));
        
        $qtytotal = 0;
        $jmltotal = 0;
        
        if ( $data["qty1"] == 0 and $data["qty2"] == 0 and $data["qty3"] == 0 )
            return false;
        
        if ( $data["qty1"] > 0 ) {
            $qtytotal += $data["qty1"] * $produk["CONV2"] * $produk['CONV3'];
            $jmltotal += $data["qty1"] * $produk["PPRICE1"];
        }
        
        if ( $data["qty2"] > 0 ) {
            $qtytotal += $data["qty2"] * $produk["CONV3"];
            $jmltotal += $data["qty2"] * $produk["PPRICE2"];
        }
        
        if ( $data["qty3"] > 0 ) {
            $qtytotal += $data["qty3"];
            $jmltotal += $data["qty3"] * $produk["PPRICE3"];
        }
        
        $return['QTY'] = $qtytotal;
        $return['AMOUNT'] = $jmltotal;
        
        return $return;
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
