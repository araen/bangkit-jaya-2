<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reppenjualan extends MY_Controller {
	protected $cls_model = "model_laporan";
	protected $controller;
	protected $view = "admin/reppenjualan/";
	
	function __construct()
	{
		parent::__construct();
		$this->controller = $this->config->item('base_url')."reppenjualan/";
	}
	
	public function index()
    {   
        $this->load->model('model_sales','sales');
        $this->load->model('model_produk','produk');
        if($this->session->userdata('login') == TRUE){
            $this->data['page_title'] = "Laporan Penjualan";
            
            $this->data['sales'] = $this->sales->get_rows(array('order'=>'SLSNO'),'SLSNO','SLSNAME');
            $this->data['produk'] = $this->produk->get_rows(array('order'=>'PCODE'),'PCODE','PNAME');
            $this->data['content'] = parent::view($this->view.'repp_penjualan',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
		
	public function rep_penjualan(){
        $this->load->model('model_order','order');
		$this->load->model('model_produk','product');
        $this->load->model('model_warehouse','warehouse');
        
        $post = $_POST['data'];
        
        $param['filter'] = "INVNO IS NOT NULL 
        AND (DATE_FORMAT(ORDERDATE,'%d-%m-%Y') >= '$post[tglmulai]' AND DATE_FORMAT(ORDERDATE,'%d-%m-%Y') <= '$post[tglselesai]')";
        $this->data['rows'] = $this->order->get_all($param);

        $this->data['tglmulai'] = $post['tglmulai'];
        $this->data['tglselesai'] = $post['tglselesai'];
        
		parent::view($this->view.'rep_penjualan');
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
