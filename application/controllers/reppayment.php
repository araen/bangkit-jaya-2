<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reppayment extends MY_Controller {
	protected $cls_model = "model_laporan";
	protected $controller;
	protected $view = "admin/reppayment/";
	
	function __construct()
	{
		parent::__construct();
		$this->controller = $this->config->item('base_url')."reppayment/";
	}
	
	public function index()
    {   
        $this->load->model('model_sales','sales');
        $this->load->model('model_produk','produk');
        if($this->session->userdata('login') == TRUE){
            $this->data['page_title'] = "Laporan Pelunasan";
            
            $this->data['sales'] = $this->sales->get_rows(array('order'=>'SLSNO'),'SLSNO','SLSNAME');
            $this->data['produk'] = $this->produk->get_rows(array('order'=>'PCODE'),'PCODE','PNAME');
            $this->data['content'] = parent::view($this->view.'repp_payment',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
		
	public function rep_penjualan(){
        $this->load->model('model_pembayaran','pembayaran');
        $this->load->model('model_sales','sales');
		$this->load->model('model_produk','product');
        $this->load->model('model_warehouse','warehouse');
        $this->load->model('model_outlet','outlet');
        
        $post = $_POST['data'];
        
        $filter = "1=1 ";
        if($post['slsno']) 
            $filter .= "SLSNO = '$post[slsno]' ";
        if($post['tglmulai'] and $post['tglselesai'])
            $filter .= "AND (DATE_FORMAT(ORDERDATE,'%d/%m/%Y') >= '$post[tglmulai]' AND DATE_FORMAT(ORDERDATE,'%d/%m/%Y') <= '$post[tglselesai]') ";
        if($post['lunas'] == 1)
            $filter .= "AND (TAGIHAN - PAYMENT) = 0 ";
        else
            $filter .= "AND (TAGIHAN - PAYMENT) <> 0 ";
        
        $param['filter'] = $filter;    
        $param['limit'] = 100; 
   
        $this->data['rows'] = $this->pembayaran->get_pembayaran($param);

        $this->data['tglmulai'] = $post['tglmulai'];
        $this->data['tglselesai'] = $post['tglselesai'];
        
        //debug($this->data['rows']);
        $this->data['status'] = $post['lunas'];
        $this->data['outlet'] = $this->outlet->get_rows(array('order'=>'OUTNO'),'OUTNO','OUTNAME');
        $this->data['warehouse'] = $this->warehouse->get_rows(array('order'=>'WRCODE'),'WRCODE','WRNAME');
        $this->data['sales'] = $this->sales->get_rows(array('order'=>'SLSNO'),'SLSNO','SLSNAME');
		parent::view($this->view.'rep_payment');
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
