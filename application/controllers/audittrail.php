<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Audittrail extends MY_Controller {
    protected $cls_model = "model_laporan";
    protected $controller;
	protected $view = "admin/audittrail/";
    
    function __construct()
    {
        parent::__construct();
        $this->controller = $this->config->item('base_url')."audittrail/";
    }
    
    public function index()
    {
        if($this->session->userdata('login') == TRUE){
            $this->warehouse();    
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    function warehouse() 
    {
        $this->load->model('model_laporan','laporan');
        $this->load->model('model_warehouse','warehouse');
        
        if($this->session->userdata('login') == TRUE){
            $this->data['page_title'] = "Laporan Audit Trail Gudang";
            $this->data['q'] = @$param['q'];
            $this->data['page'] = @$param['page'];
            $this->data['limit'] = @$param['limit'];
            $this->data['warehouse'] = $this->warehouse->get_rows(array('order'=>'WRCODE'),'WRCODE','WRNAME');
            $this->data['content'] = parent::view($this->view.'repp_audittrailgudang',true);
            parent::view("admin/index");    
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    public function rep_audittrailgudang(){
        $this->load->model('model_laporan','laporan');
        $this->load->model('model_produk','product');
        $this->load->model('model_outlet','outlet');
        $this->load->model('model_supplier','supplier');
        
        $post = $_POST['data'];
        
        $param['filter'] = "1 = 1 ";
        if($post['wrcode'])
            $param['filter'] .= "AND WRCODE = '$post[wrcode]' ";
        if(!empty($post['tglmulai']) and !empty($post['tglselesai']))
            $param['filter'] .= "AND (TGL >= '".date('Y-m-d',strtotime($post['tglmulai']))."' AND TGL <= '".date('Y-m-d',strtotime($post['tglselesai']))."')";
        
        $param['order'] = "TGL, PCODE";
        $this->data['rows'] = $this->laporan->audit_trail_gudang($param);
        
        $this->data['tglmulai'] = $post['tglmulai'];
        $this->data['tglselesai'] = $post['tglselesai'];
        
        $outlet = $this->outlet->get_rows(array('order'=>'OUTNO'),'OUTNO','OUTNAME');
        $supplier = $this->supplier->get_rows(array('order'=>'SUPPNO'),'SUPPNO','SUPPNAME');
        
        $this->data['thpart'] = $outlet + $supplier;
        
        parent::view($this->view.'rep_audittrailgudang');
    }
    
    function van() 
    {
        $this->load->model('model_laporan','laporan');
        $this->load->model('model_van','van');
        
        if($this->session->userdata('login') == TRUE){
            $this->data['page_title'] = "Laporan Audit Trail Van";
            $this->data['q'] = @$param['q'];
            $this->data['page'] = @$param['page'];
            $this->data['limit'] = @$param['limit'];
            
            $this->data['vancode'] = $post['vancode'];
            $this->data['van'] = $this->van->get_rows(array('order'=>'VANCODE'),'VANCODE','VANNAME');
            $this->data['content'] = parent::view($this->view.'repp_audittrailvan',true);
            parent::view("admin/index");    
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    public function rep_audittrailvan(){
        $this->load->model('model_laporan','laporan');
        $this->load->model('model_produk','product');
        
        $post = $_POST['data'];

        $param['filter'] = "1 = 1 ";
        if($post['vancode'])
            $param['filter'] .= "AND VANCODE = '$post[vancode]' ";
        if(!empty($post['tglmulai']) and !empty($post['tglselesai']))
            $param['filter'] .= "AND (TGL >= '".date('Y-m-d',strtotime($post['tglmulai']))."' AND TGL <= '".date('Y-m-d',strtotime($post['tglselesai']))."')";
        
        $param['order'] = "TGL, PCODE";
        $this->data['rows'] = $this->laporan->audit_trail_van($param);

        $this->data['tglmulai'] = $post['tglmulai'];
        $this->data['tglselesai'] = $post['tglselesai'];
        
        $this->data['konversi'] = $this->product->get_konversi();
        parent::view($this->view.'rep_audittrailvan');
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
