<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order extends MY_Controller {
    protected $cls_model = "model_penjualan";
    protected $controller;
	protected $view = "admin/order/";
    
    function __construct()
    {
        parent::__construct();
        $this->controller = $this->config->item('base_url')."order/";
        
        $this->data['isopname'] = isOpname(); 
    }
    
    public function index()
    {
        if($this->session->userdata('login') == TRUE){
            if(isset($_POST['page'])){
                $param = array(
                    'page'   => $_POST['page'],
                    'limit'  => 10,
                    'order'  => 'ORDERNO DESC',
                    'filter' => "",
                    'q'      => $_POST['q']
                );
            }
            else
            {
                $param = array(
                    'page'   => 1,
                    'limit'  => 10,
                    'order'  => 'ORDERNO DESC',
                    'filter' => "", 
                    'q'      => "",
                );        
            }
            
            $this->data['page_title'] = "Transaksi Input Data Order";
            $this->data['q'] = @$param['q'];
            $this->data['page'] = @$param['page'];
            $this->data['limit'] = @$param['limit'];
            $this->data['rows'] = $this->model->get_all($param);            
            $this->data['content'] = parent::view($this->view.'grid',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    function form($orderno=null)
    {
        $this->load->model('model_van','van');
        $this->load->model('model_sales','sales');
        $this->load->model('model_cart','cart'); 
        $this->load->model('model_warehouse','warehouse');
        $this->load->model('model_invoice','invoice');
        $this->load->model('model_pembayaran','pembayaran');
        $this->load->model('model_outlet','outlet');
        $this->load->model('model_produk','produk');
        $this->load->model('model_stock','stock');
        $this->load->model('model_stockmove','stockmove');
        $this->load->model('model_stock_van','stockvan');
        $this->load->model('model_setting','setting');
        $this->load->model('model_penjualan_detail','detail');
        
        //define controller
        $this->session->set_userdata('controller', 'order');
        
        if($this->session->userdata('login') == TRUE)
        {
            $this->data['page_title'] = "Detail Order";
            
            //get warehouse
            $warehouse = $this->warehouse->get_row(array('filter'=>"WRTYPE = 'U'"));
            
            $post = $this->input->post();
            
            if($post['data'] and !isOpname()) 
            {
                $data = $post['data'];

                // add invoice
                $invoice['INVDATE'] = date('Y-m-d');
                $invno = $this->invoice->add($invoice);
                
                // add header order
                $record['WRCODE'] = $warehouse['WRCODE'];    
                $record['ORDERDATE'] = date('Y-m-d H:i:s');    
                $record['OUTNO'] = $data['outno'];    
                $record['OSTATUSCODE'] = 3;    
                $record['SUBTOTAL'] = $post['total'];    
                $record['PRINTINV'] = 1;
                $record['INVNO'] = $invno;

                $idorder = $this->model->add($record);
                
                // add payment
                $payment['PAYDATE'] = date('Y-m-d');
                $payment['INVNO'] = $invno;
                $payment['AMOUNT'] = $post['payment'];
                
                $status = $this->pembayaran->add($payment);
                
                if ( $idorder ) 
                {
                    $t_total = 0;
                    
                    foreach ( $post['pcode'] as $key => $value ) {
                        $detail['ORDERNO'] = $idorder;
                        $detail['PCODE'] = $key;
                        $detail['QTY'] = $post['pqty'][$key];
                        $detail['AMOUNTITEM'] = $post['pprice'][$key];
                        $detail['DISC'] = $post['pdisc'][$key];
                        $detail['AMOUNTDISC'] = $detail['QTY'] * ($detail['DISC'] / 100 * $detail['AMOUNTITEM']);
                        $detail['AMOUNT'] = ($detail['QTY'] * $detail['AMOUNTITEM']) - $detail['AMOUNTDISC'];
                        
                        $row = $this->detail->get_row(array('filter'=>"ORDERNO = '$idorder' AND PCODE = '$key'"), "DB.*");
                        
                        //add or update detail
                        if ( $row )
                            $status = $this->detail->edit(array("ORDERNO" => "$idorder", "PCODE"=> $key), $detail);
                        else
                            $status = $this->detail->add($detail);
                        
                        //add stock move    
                        if($status) 
                        {
                            $stockmove['STMVCODE'] = 'S';    
                            //$stockmove['TRANSDATE'] = $record['ORDERDATE'];    
                            $stockmove['WRCODE'] = $record['WRCODE'];    
                            $stockmove['PCODE'] = $key;    
                            $stockmove['STOCKNUM'] = -1 * $detail['QTY'];    
                            $stockmove['BALANCE'] = $detail['AMOUNT'] + $detail['AMOUNTDISC']; 

                            $status = $this->stockmove->add($stockmove);
                            
                            if ( $detail['AMOUNTDISC'] > 0 )
                            {
                                $stockmove['STMVCODE'] = 'D';
                                $stockmove['STOCKNUM'] = 0;
                                $stockmove['BALANCE'] = -1 * $detail['AMOUNTDISC'];
                                
                                $status = $this->stockmove->add($stockmove);   
                            } 
                        }
                        
                        $t_total += $detail['AMOUNT'];
                    }
                    
                    $this->session->unset_userdata('cust_disc');
                    
                    $session = $this->session->all_userdata();
                    
                    $this->cart->delete(array("SESSIONID"=> $session['id_user'], "TRXCODE" => "S"));
                    
                    redirect("order/form/$orderno");
                }   
            }
            
            if($orderno)
            {
                $this->data['orderno'] = $orderno;
                $this->data['row'] = $this->model->get_row(array('filter'=>"orderno='$orderno'"));                
                $stcode = $this->data['row']['STCODE'];
                $this->data['detail'] = $this->detail->get_array_all(array('filter'=>"ORDERNO = '$orderno'","order"=>"PCODE"), $stcode);
            
                $outlet = $this->outlet->get_array_all();
                
                $a_outlet = array();
                foreach ( $outlet as $key => $value ) 
                    $a_outlet[$value['OUTNO']] = $value;
                    
                $this->data['a_outlet'] = $a_outlet;
            }
            
            $this->data['setting'] = $setting;
            $this->data['sales'] = $this->sales->get_rows(array('order'=>'SLSNO'),'SLSNO','SLSNAME');
            $this->data['van'] = $this->van->get_rows(array('order'=>'VANCODE'),'VANCODE','VANNAME');
			$this->data['warehouse'] = $this->warehouse->get_rows(array('order'=>'WRCODE'),'WRCODE','WRNAME');
			$this->data['outlet'] = $this->outlet->get_rows(array('order'=>'OUTNO'),'OUTNO','OUTNAME');
			$this->data['produk'] = $this->produk->get_rows(array('order'=>'PCODE'),'PCODE','PNAME');
            $this->data['konversi'] = $this->produk->get_konversi();
            $this->data['content'] = parent::view($this->view.'form',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login"); 
        }        
    }
        
    function delete_detail($orderno, $orderdetail) {
        $this->load->model('model_order_detail','detail');

        $status = $this->detail->delete($orderdetail);

        if ( $status )
            redirect($this->controller."form/$orderno");    
    }
    
    function invoice($orderno, $isinvoice = true){
        $this->load->model('model_sales','sales');
        $this->load->model('model_produk','product');
        
        $row = $this->model->get_row(array('filter'=>"ORDERNO = '$orderno'"));
        
        $record['PRINTINV'] = 1;
        $record['PRINTNUM'] = $row['PRINTNUM'] + 1;
        
        $this->model->edit($orderno, $record);
        
        //$rows['konversi'] = $this->product->get_konversi();
        $rows['isinvoice'] = $isinvoice;
        $rows['data'] = $this->model->get_order(array('filter'=>"ORDERNO = '$orderno'"));
        $rows['detail'] = $this->model->get_order_detail(array('filter'=>"ORDERNO = '$orderno'"));
        $rows['sales'] = $this->sales->get_rows(array('order'=>'SLSNO'),'SLSNO','SLSNAME');
        $rows['setting'] = $this->data['setting'];
        if($isinvoice)
            $this->load->view($this->view.'invoice', $rows);
        else
            $this->load->view($this->view.'suratjalan', $rows);
    }
    
    function usediscount($idpromo) {
        $this->load->model('model_cart', 'cart');
        $this->load->model('model_promo', 'promo');
        
        $promo = $this->promo->get_row(array('filter' => "IDPROMO = '$idpromo'",'order'=>'IDPROMO'));
        $cart = $this->cart->get_all(
            array(
                'filter' => "SESSIONID = '".$this->session->userdata('id_user')."' AND TRXCODE = 'S'",
                'page' => 1,
                'limit' => 100,
            ));
        
        foreach ( $cart['data'] as $row ) {
            $record['DISC'] = $promo['TOTALDISC'];    
            $record['TOTAL'] = ($row['QTY'] * $row['PPRICE']) - ($promo['TOTALDISC']/100 * ($row['QTY'] * $row['PPRICE']));

            $this->cart->editcart(array('SESSIONID' => $row['SESSIONID'], 'TRXCODE' => $row['TRXCODE'], 'PCODE' => $row['PCODE']), $record);    
        }
        
        $this->session->set_userdata('cust_disc', $promo['TOTALDISC']);
        
        redirect($this->controller."form");
    }
	
	private function _konversi($data) {
        $this->load->model('model_produk', 'produk');
        
        $produk = $this->produk->get_row(array('filter' => "PCODE = '$data[pcode]'"));
        
        $qtytotal = 0;
        $jmltotal = 0;
        
        if ( $data["qty1"] == 0 and $data["qty2"] == 0 and $data["qty3"] == 0 )
            return false;
        
        if ( $data["qty1"] > 0 ) {
            $qtytotal += $data["qty1"] * $produk["CONV2"] * $produk['CONV3'];
            $jmltotal += $data["qty1"] * $produk["PPRICE1"];
        }
        
        if ( $data["qty2"] > 0 ) {
            $qtytotal += $data["qty2"] * $produk["CONV3"];
            $jmltotal += $data["qty2"] * $produk["PPRICE2"];
        }
        
        if ( $data["qty3"] > 0 ) {
            $qtytotal += $data["qty3"];
            $jmltotal += $data["qty3"] * $produk["PPRICE3"];
        }
        
        $return['QTY'] = $qtytotal;
        $return['AMOUNT'] = $jmltotal;
        
        return $return;
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
