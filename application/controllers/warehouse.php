<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Warehouse extends MY_Controller {
    protected $cls_model = "model_warehouse";
    protected $controller;
	protected $view = "admin/warehouse/";
    
    function __construct()
    {
        parent::__construct();
        $this->controller = $this->config->item('base_url')."warehouse/"; 
    }
    
    public function index()
    {
        if($this->session->userdata('login') == TRUE){
            if(isset($_POST['page'])){
                $param = array(
                    'page'   => $_POST['page'],
                    'limit'  => 10,
                    'order'  => 'WRCODE ASC',
                    'filter' => "",
                    'q'      => $_POST['q']
                );
            }
            else
            {
                $param = array(
                    'page'   => 1,
                    'limit'  => 10,
                    'order'  => 'WRCODE ASC',
                    'filter' => "", 
                    'q'      => "",
                );        
            }
            
            $this->data['page_title'] = "Warehouse";
            $this->data['q'] = @$param['q'];
            $this->data['page'] = @$param['page'];
            $this->data['limit'] = @$param['limit'];
            $this->data['a_type'] = array('L' => 'Log', 'U' => 'Unit');
            $this->data['rows'] = $this->model->get_all($param);
            $this->data['content'] = parent::view($this->view.'grid',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    function form($wrcode=null)
    {
        $this->load->model('model_warehouse','warehouse');
        
        if($this->session->userdata('login') == TRUE)
        {
            $this->data['page_title'] = "Detail Warehouse";
            if($wrcode)
            {	
				
                $this->data['row'] = $this->model->get_row(array('filter'=>"wrcode='$wrcode'"));
            }
            if(isset($_POST['data']))
            {
                $data = $_POST['data'];
                if($wrcode)
                {
                    $this->model->edit($wrcode,$data);                 
                } else {
                    $this->model->add($data);
                }
                redirect($this->controller);
            }
            $this->data['warehouse'] = $this->warehouse->get_rows(array('order'=>'WRCODE'),'WRCODE','WRNAME');
            $this->data['content'] = parent::view($this->view.'form',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");    
        }        
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
