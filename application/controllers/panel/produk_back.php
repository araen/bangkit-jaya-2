<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produk extends MY_Controller {
    protected $cls_model = "model_produk";
    protected $controller;
	protected $view = "admin/produk/";
    
    function __construct()
    {
        parent::__construct();
        $this->controller = $this->config->item('cpanel_dir')."produk/"; 
    }
    
    public function index()
    {
        if($this->session->userdata('login') == TRUE){
            if(isset($_POST['page'])){
                $param = array(
                    'page'   => $_POST['page'],
                    'limit'  => 10,
                    'order'  => 'PCODE ASC',
                    'filter' => "",
                    'q'      => $_POST['q']
                );
            }
            else
            {
                $param = array(
                    'page'   => 1,
                    'limit'  => 10,
                    'order'  => 'PCODE ASC',
                    'filter' => "", 
                    'q'      => "",
                );        
            }
            
            $this->data['page_title'] = "Produk";
            $this->data['q'] = @$param['q'];
            $this->data['page'] = @$param['page'];
            $this->data['limit'] = @$param['limit'];
            $this->data['rows'] = $this->model->get_all($param);
            $this->data['content'] = parent::view($this->view.'grid',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    function form($pcode=null)
    {
        $this->load->model('model_produk','produk');
		//$this->load->model('model_blok','blok');
        
        if($this->session->userdata('login') == TRUE)
        {
            $this->data['page_title'] = "Detail Produk";
            if($pcode)
            {
                $this->data['row'] = $this->model->get_row(array('filter'=>"pcode=$pcode"));
            }
            if(isset($_POST['data']))
            {
                $data = $_POST['data'];
                
				//$data['tgllahir'] = dGetDate($data['tgllahir']);
                if($pcode)
                {
                    $this->model->edit($pcode,$data);                 
                } else {
                    $this->model->add($data);
                }
                redirect($this->controller);
            }
			//$this->data['blok'] = $this->blok->get_rows(array(),'ID','NAMA');
            $this->data['produk'] = $this->produk->get_rows(array(),'PCODE','PNAME');
            $this->data['content'] = parent::view($this->view.'form',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");    
        }        
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
