<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pembayaran extends MY_Controller {
    protected $cls_model = "model_pembayaran";
    protected $controller;
	protected $view = "admin/pembayaran/";
    
    function __construct()
    {
        parent::__construct();
        $this->controller = $this->config->item('cpanel_dir')."pembayaran/"; 
    }
    
    public function index()
    {
        if($this->session->userdata('login') == TRUE){
            if(isset($_POST['page'])){
                $param = array(
                    'page'   => $_POST['page'],
                    'limit'  => 10,
                    'order'  => 'ID ASC',
                    'filter' => "",
                    'q'      => $_POST['q']
                );
            }
            else
            {
                $param = array(
                    'page'   => 1,
                    'limit'  => 10,
                    'order'  => 'ID ASC',
                    'filter' => "", 
                    'q'      => "",
                );        
            }
            
            $this->data['page_title'] = "Transaksi Pembayaran";
            $this->data['q'] = @$param['q'];
            $this->data['page'] = @$param['page'];
            $this->data['limit'] = @$param['limit'];
            $this->data['rows'] = $this->model->get_all($param);
            $this->data['content'] = parent::view($this->view.'grid',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    function form($id=null)
    {
        $this->load->model('model_blok','blok');
        $this->load->model('model_penyewa','penyewa');
        $this->load->model('model_pembayaran_detail','detail');
        
        if($this->session->userdata('login') == TRUE)
        {
            $this->data['page_title'] = "Detail Pembayaran";
            if($id)
            {
                $this->data['row'] = $this->model->get_row(array('filter'=>"id=$id"));
                $this->data['detail'] = $this->detail->get_array_all(array('filter'=>"IDBATCH = $id","order"=>"TGLBAYAR"));
            }
            if(isset($_POST['data']))
            {
                $data = $_POST['data'];

                if ( isset($_POST['idbatch']) ) 
                {
                    $idbatch = $_POST['idbatch'];
                }
                else {
                    $blok = $this->blok->get_row(array('filter'=>"ID = $data[idblok]"));
                    $batch['IDPENYEWA'] = $data['idpenyewa'];
                    $batch['IDBLOK'] = $data['idblok'];
                    $batch['TOTALBAYAR'] = $blok['NILAISEWA'];
                    
                    $idbatch = $this->model->add($batch);
                }
                
                if ( $idbatch ) {
                    $detail['IDBATCH'] = $idbatch;
                    $detail['TGLBAYAR'] = dGetDate($data['tglbayar']);
                    $detail['JMLBAYAR'] = fNumber($data['jmlbayar']);
                    $detail['JNSBAYAR'] = $data['jnsbayar'];
                    $detail['KETERANGAN'] = $data['keterangan'];
                    
                    $this->detail->add($detail);
                }
                redirect($this->controller."form/$idbatch");
            }
            $this->data['id'] = $id;
			$this->data['blok'] = $this->blok->get_rows(array(),'ID','NAMA');
            $this->data['penyewa'] = $this->penyewa->get_rows(array(),'ID','NAMA');
            $this->data['content'] = parent::view($this->view.'form',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");    
        }        
    }
    
    function kwitansi($idbatch, $iddetail) {
        $this->load->model('model_pembayaran_detail','detail');
        
        $row = $this->model->get_all(array('filter'=>"id=$idbatch"));
        $this->data['batch'] = $row['data'][0];
        $this->data['detail'] = $this->detail->get_array_all(array('filter'=>"IDBATCH = $idbatch","order"=>"TGLBAYAR"));
        $this->data['row'] = $this->detail->get_row(array('filter'=>"ID = $iddetail"));
        
        parent::view($this->view.'kwitansi');
    }
    
    function rekap($semester,$tahun) {
        if ( $semester == 1 ) { 
            $minthn = 1;
            $maxthn = 6;
        } else {
            $minthn = 7;
            $maxthn = 12;
        }
            
        $rows = $this->model->get_rekap(array('filter'=>"(CONVERT(BLNBAYAR,UNSIGNED INTEGER) BETWEEN $minthn AND $maxthn) and THNBAYAR = $tahun"));
    
        $i=0;
        $arrData = array();
        foreach ( $rows as $row ) {
            $arrData[$row['ID']]['NAMA'] = $row['NAMA'];
            $arrData[$row['ID']]['BLOK'] = $row['BLOK'];
            if ( $row['JNSBAYAR'] != "AG" ) {
                $arrData[$row['ID']]['JNSBAYAR'][$row['JNSBAYAR']]['JNS'] = $row['JNSBAYAR'];
                $arrData[$row['ID']]['JNSBAYAR'][$row['JNSBAYAR']]['JMLBAYAR'] = $row['JMLBAYAR'];
            }
            else {
                $arrData[$row['ID']]['JNSBAYAR'][$row['JNSBAYAR']][$row['BLNBAYAR']]['JNS'] = $row['JNSBAYAR'];
                $arrData[$row['ID']]['JNSBAYAR'][$row['JNSBAYAR']][$row['BLNBAYAR']]['JMLBAYAR'] = $row['JMLBAYAR'];
            }
        }
        
        $arrSemester = get_bulan_all();
        for ( $i = $minthn; $i <= $maxthn; $i++ ) {
            $a_semester[$i] = strtoupper($arrSemester[str_pad($i,2,'0',STR_PAD_LEFT)]);    
        }
        
		$this->data['ntahun'] = $tahun;
        $this->data['nsemester'] = $semester;
        $this->data['semester'] = $a_semester;
        $this->data['rows'] = $arrData;
        parent::view($this->view.'rekap');
    }
    
    function delete_detail($idbatch, $iddetail) {
        $this->load->model('model_pembayaran_detail','detail');
        
        $status = $this->detail->delete($iddetail);
        
        if ( $status )
            redirect($this->controller."form/$idbatch");    
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
