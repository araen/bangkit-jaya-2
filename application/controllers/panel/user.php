<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends MY_Controller {
    protected $cls_model = "model_user";
	protected $controller;
    protected $view       = "admin/user/";
	
	function __construct()
    {
		// Call the Model constructor
        parent::__construct();

        $this->controller = $this->config->item('cpanel_dir')."user/";
    }
	
	function index(){
		if($this->session->userdata('login') === TRUE){

			if(isset($_POST['page'])){
				$param = array(
                    'page'   => $_POST['page'],
                    'limit'  => 20,
				    'q'      => $_POST['q'],
                );
			}
            else
            {
                $param = array(
                    'page'   => 1,
                    'limit'  => 20,
                    'q'      => '',
                );        
            }
			
			$this->data['user']= $this->model->get_all($param);
			$this->data['q'] = @$param['q'];
			$this->data['page'] = @$param['page'];
			$this->data['limit'] = @$param['limit'];
			
			$this->data['content'] = parent::view($this->view.'users',true);
            parent::view("admin/index");
		}else{
			parent::view("admin/login");
		}
	}
	
	function form($id=0){
		//if($this->session->userdata('login') === TRUE && in_array(101,$this->access)){
			//$this->load->model('model_group','group');
            
			if(isset($_POST['save'])){
				
				if($id == 0){
					$data = $_POST['data'];
					if($data['password'] == '')
						unset($data['password']);
					else
						$data['password'] = md5($data['password']);
					$this->model->add($data);
				}else{
					$data = $_POST['data'];
					if($data['password'] == ''){
						unset($data['password']);
					}else{
						$data['password'] = md5($data['password']);
					}	
					$this->model->edit($id,$data);
				}
			
				redirect($this->controller.'user');
			}
            $data['id'] = $id;
            $data['user'] = $this->model->get_row(array('filter'=>"ID = $id"));
			//$data['groups'] = $this->group->get_rows(array(),'ID','NAME');
			$view['content']= $this->load->view($this->view.'form',$data,true);
			$this->load->view('admin/index',$view);
		/*}else{
			redirect('admin/main');
		}*/
	}
	
	function delete($id=0){
		if($this->session->userdata('login')){
			
			$this->model->delete($id);
			
			redirect($this->controller.'user');
		}else{
			parent::view("admin/login"); 
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */