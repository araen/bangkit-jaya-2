<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blok extends MY_Controller {
    protected $cls_model = "model_blok";
    protected $controller;
	protected $view = "admin/blok/";
    
    function __construct()
    {
        parent::__construct();
        $this->controller = $this->config->item('cpanel_dir')."blok/"; 
    }
    
    public function index()
    {
        if($this->session->userdata('login') == TRUE){
            if(isset($_POST['page'])){
                $param = array(
                    'page'   => $_POST['page'],
                    'limit'  => 10,
                    'order'  => 'ID ASC',
                    'filter' => "",
                    'q'      => $_POST['q']
                );
            }
            else
            {
                $param = array(
                    'page'   => 1,
                    'limit'  => 10,
                    'order'  => 'ID ASC',
                    'filter' => "", 
                    'q'      => "",
                );        
            }
            
            $this->data['page_title'] = "Blok";
            $this->data['q'] = @$param['q'];
            $this->data['page'] = @$param['page'];
            $this->data['limit'] = @$param['limit'];
            $this->data['rows'] = $this->model->get_all($param);
            $this->data['content'] = parent::view($this->view.'grid',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    function form($id=null)
    {
        $this->load->model('model_stand','stand');
		$this->load->model('model_blok','blok');
        
        if($this->session->userdata('login') == TRUE)
        {
            $this->data['page_title'] = "Detail Blok";
            if($id)
            {
                $this->data['row'] = $this->model->get_row(array('filter'=>"id=$id"));
            }
            if(isset($_POST['data']))
            {
                $data = $_POST['data'];
                $data['NILAISEWA'] = fNumber($data['nilaisewa']);
                if($id)
                {
                    $this->model->edit($id,$data);                 
                } else {
                    $this->model->add($data);
                }
                redirect($this->controller);
            }
			$this->data['stand'] = $this->stand->get_rows(array(),'ID','NAMA');
            $this->data['blok'] = $this->blok->get_rows(array(),'id','nama');
            $this->data['content'] = parent::view($this->view.'form',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");    
        }        
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
