<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class File extends CI_Controller {
	var $controller;
    var $view;
	var $access = array();
	
	function __construct()
    {
		// Call the Model constructor
        parent::__construct();
        $this->controller = $this->config->item('cpanel_dir')."file/";
        $this->view = $this->config->item('cpanel_dir')."file/";
    }
	
	function index($path="data"){
		if($this->session->userdata('login') === TRUE){
			$this->load->model('model_filemanager');
			
			if(isset($_POST['path']))
				$path = $_POST['path'];
			
			if(isset($_POST['dir']) && $_POST['dir'] != ''){
				$path = $_POST['path'];
				$dir_name = $_POST['dir'];
				$this->model_filemanager->create_dir($path,$dir_name);
			}
			
			if(isset($_POST['del']) && $_POST['del'] != ''){
				$path = $_POST['path'];
				$file = $_POST['del'];
				$this->model_filemanager->delete($path,$file);
			}
			
			$data['msg'] = $this->do_upload('file');
			
			$data["path"] = $path;
			$data['file']= $this->model_filemanager->list_dir($path);

			$view['content']= $this->load->view('admin/filemanager/file',$data,TRUE);
			$this->load->view('admin/index',$view);
			
		}else{
			redirect($this->config->item('cpanel_dir').'main');
		}
	}
	
	function image_content($path="data"){
		if($this->session->userdata('login') === TRUE){
			$this->load->model('model_filemanager');
			
			if(isset($_POST['path']))
				$path = $_POST['path'];
			
			if(isset($_POST['dir']) && $_POST['dir'] != ''){
				$path = $_POST['path'];
				$dir_name = $_POST['dir'];
				$this->model_filemanager->create_dir($path,$dir_name);
			}
			
			if(isset($_POST['del']) && $_POST['del'] != ''){
				$path = $_POST['path'];
				$file = $_POST['del'];
				$this->model_filemanager->delete($path,$file);
			}
			
			$data['msg'] = $this->do_upload('image');
			
			$data["path"] = $path;
			$data['file']= $this->model_filemanager->list_image($path);

			$this->load->view('admin/filemanager/image',$data);

		}else{
			redirect($this->config->item('cpanel_dir').'main');
		}
	}
	
	function image_url($path="data"){
		if($this->session->userdata('login') === TRUE){
			$this->load->model('model_filemanager');

            if(isset($_POST['path']))
                $path = $_POST['path'];
                
			if(isset($_POST['dir']) && $_POST['dir'] != ''){
				$path = $_POST['path'];
				$dir_name = $_POST['dir'];
				$this->model_filemanager->create_dir($path,$dir_name);
			}
			
			if(isset($_POST['del']) && $_POST['del'] != ''){
				$path = $_POST['path'];
				$file = $_POST['del'];
				$this->model_filemanager->delete($path,$file);
			}
			
			$data['msg'] = $this->do_upload('file');
			
			$data["path"] = $path;
			$data['file']= $this->model_filemanager->list_image($path);

			$this->load->view('admin/filemanager/image_url',$data);

		}else{

			redirect($this->config->item('cpanel_dir').'main');
		}
	}
    
    function file_url($path="data"){
        if($this->session->userdata('login') === TRUE){
            $this->load->model('model_filemanager');
            
            if(isset($_POST['path']))
                $path = $_POST['path'];
            
            if(isset($_POST['dir']) && $_POST['dir'] != ''){
                $path = $_POST['path'];
                $dir_name = $_POST['dir'];
                $this->model_filemanager->create_dir($path,$dir_name);
            }
            
            if(isset($_POST['del']) && $_POST['del'] != ''){
                $path = $_POST['path'];
                $file = $_POST['del'];
                $this->model_filemanager->delete($path,$file);
            }
            
            $data['msg'] = $this->do_upload('file');
            
            $data["path"] = $path;
            $data['file']= $this->model_filemanager->list_dir($path);

            $this->load->view('admin/filemanager/file_url',$data);
        }else{
            redirect($this->config->item('cpanel_dir').'main');
        }
    }
	
	function do_upload($type='file')
	{
		if (!empty($_FILES)) {
			$config['upload_path'] = "./$_POST[path]/";
			
			if($type=='file')
				$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp|pdf|doc|docx|xls|xlsx|txt';
			if($type=='image')
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				
            $config['file_name'] = fStrRand(8,false);
			$config['max_size']	= '10240';
			$config['max_width']  = '1600';
			$config['max_height']  = '1400';

			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload())
			{
				return $this->upload->display_errors();
			}
			else
			{
                unlink($_FILES['userfile']['tmp_name']);
                $data = $this->upload->data();
                list($type,$format) = explode('/',$data['file_type']);
                if($type == 'image')
                    $this->_resize($data);
				
                return "Success..";
			}
		}
	}
    
    function _resize($file)
    {
        $this->load->library('wideimage');
        $this->load->library('simpleimage');
        $filename = $file['file_name'];
        $thumbdir = $this->config->item('thumbnail_dir');
        $this->simpleimage->load($file['full_path']);
        $this->simpleimage->resize(280,130);
        $this->simpleimage->save($thumbdir."280/".$filename);
        $this->simpleimage->resize(120,120);
        $this->simpleimage->save($thumbdir."120/".$filename);
        $this->simpleimage->resize(210,240);
        $this->simpleimage->save($thumbdir."210/".$filename);
        $this->simpleimage->resize(64,64);
        $this->simpleimage->save($thumbdir."64/".$filename);
        /*$newimage = $this->wideimage->loadFromFile($file['full_path']);
        $newimage->resize(280, 130)->saveToFile($thumbdir."280/".$filename);*/
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */