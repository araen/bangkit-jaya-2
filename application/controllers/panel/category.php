<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category extends MY_Controller {
    protected $cls_model = "model_category";
    protected $controller;
	protected $view = "admin/category/";
    
    function __construct()
    {
        parent::__construct();
        $this->controller = $this->config->item('cpanel_dir')."category/"; 
    }
    
    public function index()
    {
        if($this->session->userdata('login') == TRUE){
            if(isset($_POST['page'])){
                $param = array(
                    'page'   => $_POST['page'],
                    'limit'  => 10,
                    'order'  => 'ID ASC',
                    'filter' => "",
                    'q'      => $_POST['q']
                );
            }
            else
            {
                $param = array(
                    'page'   => 1,
                    'limit'  => 10,
                    'order'  => 'ID ASC',
                    'filter' => "", 
                    'q'      => "",
                );        
            }
            
            $this->data['page_title'] = "Category";
            $this->data['q'] = @$param['q'];
            $this->data['page'] = @$param['page'];
            $this->data['limit'] = @$param['limit'];
            $this->data['rows'] = $this->model->get_all($param);
            $this->data['content'] = parent::view($this->view.'grid',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    function form($id=null)
    {
        $this->load->model('model_category','category');
        
        if($this->session->userdata('login') == TRUE)
        {
            $this->data['page_title'] = "Detil Category";
            if($id)
            {
                $this->data['row'] = $this->model->get_row(array('filter'=>"id=$id"));
            }
            if(isset($_POST['data']))
            {
                $data = $_POST['data'];
                
                if($id)
                {
                    $this->model->edit($id,$data);                 
                } else {
                    $this->model->add($data);
                }
                redirect($this->controller);
            }
            $this->data['category'] = $this->category->get_rows(array(),'id','name');
            $this->data['content'] = parent::view($this->view.'form',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");    
        }        
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
