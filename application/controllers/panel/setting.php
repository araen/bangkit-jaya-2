<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting extends MY_Controller {
	protected $cls_model = "model_setting";
    protected $controller;
    protected $view = "admin/setting/";
	
	function __construct()
    {
		// Call the Model constructor
        parent::__construct();
        $this->controller = $this->config->item('cpanel_dir')."setting/"; 
    }
	
	function index(){
		if($this->session->userdata('login') === TRUE){
			
			if(isset($_POST['save'])){
				$data = $_POST['data'];
				foreach($data as $key=>$value)
					$this->model->edit(array('VARIABLE'=>$key),array('VALUE'=>$value));
			}
			$this->data['site']= $this->model->get_array_all();
			
			$this->data['content'] = parent::view('admin/setting/form',true);
            parent::view("admin/index");
		}else{
		    parent::view("admin/login");
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */