<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jnssales extends MY_Controller {
    protected $cls_model = "model_jnssales";
    protected $controller;
	protected $view = "admin/jnssales/";
    
    function __construct()
    {
        parent::__construct();
        $this->controller = $this->config->item('base_url')."jnssales/"; 
    }
    
    public function index()
    {
        if($this->session->userdata('login') == TRUE){
            if(isset($_POST['page'])){
                $param = array(
                    'page'   => $_POST['page'],
                    'limit'  => 10,
                    'order'  => 'STCODE ASC',
                    'filter' => "",
                    'q'      => $_POST['q']
                );
            }
            else
            {
                $param = array(
                    'page'   => 1,
                    'limit'  => 10,
                    'order'  => 'STCODE ASC',
                    'filter' => "", 
                    'q'      => "",
                );        
            }
            
            $this->data['page_title'] = "Jenis Sales";
            $this->data['q'] = @$param['q'];
            $this->data['page'] = @$param['page'];
            $this->data['limit'] = @$param['limit'];
            $this->data['rows'] = $this->model->get_all($param);
            $this->data['content'] = parent::view($this->view.'grid',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    function form($stcode=null)
    {
        $this->load->model('model_jnssales','jnssales');
        
        if($this->session->userdata('login') == TRUE)
        {
            $this->data['page_title'] = "Detail Jenis Sales";
            if($stcode)
            {	
                $this->data['row'] = $this->model->get_row(array('filter'=>"stcode='$stcode'"));
            }
            if(isset($_POST['data']))
            {
                $data = $_POST['data'];
				
                if($stcode)
                {
                    $this->model->edit($stcode,$data);                 
                } else {
                    $this->model->add($data);
                }
                redirect($this->controller);
            }
            $this->data['jnssales'] = $this->jnssales->get_rows(array('order'=>'STCODE'),'STCODE','STNAME');
            $this->data['content'] = parent::view($this->view.'form',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");    
        }        
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
