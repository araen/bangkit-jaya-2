<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu extends MY_Controller {
    protected $cls_model = "auth";
    protected $controller;
	protected $view = "admin/menu/";
    
    function __construct()
    {
        parent::__construct();
        $this->controller = $this->config->item('base_url')."menu/"; 
    }
    
    public function index()
    {
        if($this->session->userdata('login') == TRUE){
            if(isset($_POST['page'])){
                $param = array(
                    'page'   => $_POST['page'],
                    'limit'  => 10,
                    'order'  => 'PBCODE ASC',
                    'filter' => "",
                    'q'      => $_POST['q']
                );
            }
            else
            {
                $param = array(
                    'page'   => 1,
                    'limit'  => 10,
                    'order'  => 'PBCODE ASC',
                    'filter' => "", 
                    'q'      => "",
                );        
            }
            
            if ( isset($_POST['submit']) ) {
                $data = $_POST['data'];

                if ( !isset($data['parentcode']) ) {
                    $record['MNNAME'] = $data['menuname'];
                    $record['ISACTIVE'] = !empty($data['isactive']) ? 1 : 0;

                    if ( !empty($data['menucode']) ) {
                        $status = $this->model->edit($data['menucode'], $record);
                        $this->trans_message($status,'edit');
                    } else {
                        $status = $this->model->add($record);
                        $this->trans_message($status,'add');
                    }    
                } else {
                    $record['MNCODE'] = $data['parentcode'];
                    $record['CMNNAME'] = $data['menuname'];
                    $record['CMNURL'] = $data['menufile'];
                    $record['ISACTIVE'] = !empty($data['isactive']) ? 1 : 0;
                    
                    if ( !empty($data['menucode']) ) {
                        $status = $this->model->edit_child($data['menucode'], $record);
                        $this->trans_message($status,'edit');
                    } else {
                        $status = $this->model->add_child($record);
                        $this->trans_message($status,'add');
                    }
                }
                
                redirect($this->controller);
            }
            
            $this->data['page_title'] = "Menu";
            $this->data['q'] = @$param['q'];
            $this->data['page'] = @$param['page'];
            $this->data['limit'] = @$param['limit'];
            
            $this->model->reset_query();
            $this->data['rows'] = $this->model->get_menu();
            $this->data['content'] = parent::view($this->view.'grid',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    function form($pbcode=null)
    {
        $this->load->model('model_brand','brand');
        
        if($this->session->userdata('login') == TRUE)
        {
            $this->data['page_title'] = "Detail Brand";
            if($pbcode)
            {
                $this->data['row'] = $this->model->get_row(array('filter'=>"pbcode=$pbcode"));
            }
            if(isset($_POST['data']))
            {
                $data = $_POST['data'];
                
                if($pbcode)
                {
                    $this->model->edit($pbcode,$data);                 
                } else {
                    $this->model->add($data);
                }
                redirect($this->controller);
            }
            $this->data['brand'] = $this->brand->get_rows(array('order'=>'PBCODE'),'PBCODE','PBNAME');
            $this->data['content'] = parent::view($this->view.'form',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");    
        }        
    }
    
    function delete_child($key) {
        $status = $this->model->delete_child($key);
        $this->trans_message($status,'delete');
        
        redirect($this->controller);
    }
    
    function upcmenu($key1, $key2) {
        $this->model->edit_child($key1, array('CMNCODE' => $key1."X"));
        $this->model->edit_child($key2, array('CMNCODE' => $key1));
        $status = $this->model->edit_child(array('CMNCODE' => $key1."X"), array('CMNCODE' => $key2));
        $this->trans_message($status,'edit');
        
        redirect($this->controller);
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
