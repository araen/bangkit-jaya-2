<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Outlet extends MY_Controller {
    protected $cls_model = "model_outlet";
    protected $controller;
	protected $view = "admin/outlet/";
    
    function __construct()
    {
        parent::__construct();
        $this->controller = $this->config->item('base_url')."outlet/"; 
    }
    
    public function index()
    {
        if($this->session->userdata('login') == TRUE){
            if(isset($_POST['page'])){
                $param = array(
                    'page'   => $_POST['page'],
                    'limit'  => 10,
                    'order'  => 'OUTNO ASC',
                    'filter' => "",
                    'q'      => $_POST['q']
                );
            }
            else
            {
                $param = array(
                    'page'   => 1,
                    'limit'  => 10,
                    'order'  => 'OUTNO ASC',
                    'filter' => "", 
                    'q'      => "",
                );        
            }
            
            if( isset($_POST['upload']) ) {
                $path = base_assets()."cache/";
                
                $uploadedfile = uploadfile($path, $_FILES, 'uptemplate');

                $data = excelreader($uploadedfile);
                
                $status = $this->model->add_data_excel($data, 4);
                
                $this->trans_message($status, 'add');
                
                if ( $data ) {
                    unlink($uploadedfile['full_path']);
                    unlink($_FILES['uptemplate']['tmp_name']);
                }                                    
                
                redirect($this->controller);      
            }
            
            $this->data['page_title'] = "Customer";
            $this->data['q'] = @$param['q'];
            $this->data['page'] = @$param['page'];
            $this->data['limit'] = @$param['limit'];
            $this->data['rows'] = $this->model->get_all($param);
            $this->data['content'] = parent::view($this->view.'grid',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    function form($outno=null)
    {
        $this->load->model('model_outlet','outlet');
        $this->load->model('model_jnsoutlet','jnsoutlet');
        
        if($this->session->userdata('login') == TRUE)
        {
            $this->data['page_title'] = "Detail Customer";
            if($outno)
            {	
				
                $this->data['row'] = $this->model->get_row(array('filter'=>"outno='$outno'"));
            }
            if(isset($_POST['data']))
            {
                $data = $_POST['data'];
				
				$data['firsttrans'] = dGetDate($data['firsttrans']);
				$data['firstopen'] = dGetDate($data['firstopen']);
				$data['regdate'] = dGetDate($data['regdate']);
                if($outno)
                {
                    $this->model->edit($outno,$data);                 
                } else {
                    $this->model->add($data);
                }
                redirect($this->controller);
            }
            $this->data['jnsoutlet'] = $this->jnsoutlet->get_rows(array('order'=>'ATTCODE'),'ATTCODE','ATTNAME');
            $this->data['outlet'] = $this->outlet->get_rows(array('order'=>'OUTNO'),'OUTNO','OUTNAME');
            $this->data['content'] = parent::view($this->view.'form',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");    
        }        
    }
    
    function template() {
        $this->load->helper('download');

        $data = file_get_contents(base_assets()."template/outlet.xlsx");
        force_download('outlet.xlsx',$data);
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
