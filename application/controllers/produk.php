<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produk extends MY_Controller {
    protected $cls_model = "model_produk";
    protected $controller;
	protected $view = "admin/produk/";
    
    function __construct()
    {
        parent::__construct();
        $this->controller = $this->config->item('base_url')."produk/"; 
    }
    
    public function index()
    {
        $this->load->library('phpexcel');
        
        if($this->session->userdata('login') == TRUE){
            if(isset($_POST['page'])){
                $param = array(
                    'page'   => $_POST['page'],
                    'limit'  => 10,
                    'order'  => 'PCODE ASC',
                    'filter' => "",
                    'q'      => $_POST['q']
                );
            }
            else
            {
                $param = array(
                    'page'   => 1,
                    'limit'  => 10,
                    'order'  => 'PCODE ASC',
                    'filter' => "", 
                    'q'      => "",
                );        
            }
            
            if( isset($_POST['upload']) ) {
                $path = base_assets()."cache/";
                
                $uploadedfile = uploadfile($path, $_FILES, 'uptemplate');

                $data = excelreader($uploadedfile);
                
                $status = $this->model->add_data_excel($data, 4);
                
                $this->trans_message($status, 'add');
                
                if ( $data ) {
                    unlink($uploadedfile['full_path']);
                    unlink($_FILES['uptemplate']['tmp_name']);
                }                                    
                
                redirect($this->controller);      
            }
            
            $this->data['page_title'] = "Produk";
            $this->data['q'] = @$param['q'];
            $this->data['page'] = @$param['page'];
            $this->data['limit'] = @$param['limit'];
            $this->data['rows'] = $this->model->get_all($param);
            $this->data['content'] = parent::view($this->view.'grid',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    function form($pcode=null)
    {
        $this->load->model('model_produk','produk');
		$this->load->model('model_brand','brand');
		$this->load->model('model_supplier','supplier');
        
        if($this->session->userdata('login') == TRUE)
        {
            $this->data['page_title'] = "Detail Produk";
            if($pcode)
            {
                $this->data['row'] = $this->model->get_row(array('filter'=>"pcode='$pcode'"));
            }
            if(isset($_POST['data']))
            {
                $data = $_POST['data'];
				$data['plaunch'] = dGetDate($data['plaunch']);
                $data['pprice1'] = fNumber($data['pprice1']);
                $data['pprice2'] = fNumber($data['pprice2']);
                $data['pprice3'] = fNumber($data['pprice3']);
                $data['conv1'] = fNumber($data['conv1']);
                $data['conv2'] = fNumber($data['conv2']);
                $data['conv3'] = fNumber($data['conv3']);
                $data['sprice1'] = fNumber($data['sprice1']);

                if($pcode)
                {
                    $this->model->edit($pcode,$data);                 
                } else {
                    $this->model->add($data);
                }
                redirect($this->controller);
            }
            
            $this->data['produk'] = $this->produk->get_rows(array('order'=>'PCODE'),'PCODE','PNAME');
            $this->data['brand'] = $this->brand->get_rows(array('order'=>'PBCODE'),'PBCODE','PBNAME');
            $this->data['supplier'] = $this->supplier->get_rows(array('order'=>'SUPPNO'),'SUPPNO','SUPPNAME');
			$this->data['content'] = parent::view($this->view.'form',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");    
        }        
    }
    
    function template() {
        $this->load->helper('download');

        $data = file_get_contents(base_assets()."template/produk.xlsx");
        force_download('produk.xlsx',$data);
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
