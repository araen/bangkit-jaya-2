<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends MY_Controller {
    protected $cls_model    = "model_user";
	protected $controller;
    protected $view         = "admin/user/";
	
	function __construct()
    {
		// Call the Model constructor
        parent::__construct();

        $this->controller = $this->config->item('cpanel_dir')."user/";
    }
	
	function index(){
		if($this->session->userdata('login') === TRUE){

			if(isset($_POST['page'])){
				$param = array(
                    'page'   => $_POST['page'],
                    'limit'  => 20,
				    'q'      => $_POST['q'],
                );
			}
            else
            {
                $param = array(
                    'page'   => 1,
                    'limit'  => 20,
                    'q'      => '',
                );        
            }
			
			$this->data['user']= $this->model->get_all($param);
			$this->data['q'] = @$param['q'];
			$this->data['page'] = @$param['page'];
			$this->data['limit'] = @$param['limit'];
			
			$this->data['content'] = parent::view($this->view.'users',true);
            parent::view("admin/index");
		}else{
			parent::view("admin/login");
		}
	}
	
	function form($id=0){
		//if($this->session->userdata('login') === TRUE && in_array(101,$this->access)){
			
			if(isset($_POST['save'])){
				
				if($id == 0){
					$data = $_POST['data'];
					if ( !empty($data['uspassword']) and !empty($data['uspassword']) and  $data['passwordconfirm'] == $data['passwordconfirm'] ) 
                    {
                        unset($data['passwordconfirm']);

						$data['uspassword'] = md5($data['uspassword']);
					    $status = $this->model->add($data);
                        
                        $this->trans_message($status, 'add');
                    } else {
                        $this->trans_message(0, 'add', 'konfirmasi password tidak sama');
                        
                        redirect('user/form/'.$id);
                    }
                    redirect($this->controller.'user');
				}else{
					$data = $_POST['data'];

                    if ( $data['uspassword'] == $data['passwordconfirm'] ) 
                    {
                        unset($data['passwordconfirm']);                    
                        
                        if($data['uspassword'] == ''){
						    unset($data['uspassword']);
					    }else{
						    $data['uspassword'] = md5($data['uspassword']);
					    }                                                       
					    $this->model->edit($id,$data);
                        
                        $this->trans_message(1, 'edit');
                    } else {
                        $this->trans_message(0, 'edit', 'konfirmasi password tidak sama');
                        
                        redirect('user/form/'.$id);
                    }
                    redirect($this->controller.'user');
				}
			}
            $this->data['id'] = $id;
			$this->data['user'] = $this->model->get_row(array('filter'=>"ID = $id"));
			$this->data['content'] = parent::view($this->view.'form',true);
            parent::view("admin/index");
		/*}else{
			redirect('admin/main');
		}*/
	}
	
	function delete($id=0){
		if($this->session->userdata('login')){
			
			$this->model->delete($id);
			
			redirect($this->controller.'user');
		}else{
			parent::view("admin/login"); 
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */