<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Van extends MY_Controller {
    protected $cls_model = "model_van";
    protected $controller;
	protected $view = "admin/van/";
    
    function __construct()
    {
        parent::__construct();
        $this->controller = $this->config->item('base_url')."van/"; 
    }
    
    public function index()
    {
        if($this->session->userdata('login') == TRUE){
            if(isset($_POST['page'])){
                $param = array(
                    'page'   => $_POST['page'],
                    'limit'  => 10,
                    'order'  => 'VANCODE ASC',
                    'filter' => "",
                    'q'      => $_POST['q']
                );
            }
            else
            {
                $param = array(
                    'page'   => 1,
                    'limit'  => 10,
                    'order'  => 'VANCODE ASC',
                    'filter' => "", 
                    'q'      => "",
                );        
            }
            
            $this->data['page_title'] = "Van";
            $this->data['q'] = @$param['q'];
            $this->data['page'] = @$param['page'];
            $this->data['limit'] = @$param['limit'];
            $this->data['rows'] = $this->model->get_all($param);
            $this->data['content'] = parent::view($this->view.'grid',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    function form($vancode=null)
    {
        $this->load->model('model_van','van');
        
        if($this->session->userdata('login') == TRUE)
        {
            $this->data['page_title'] = "Detail Van";
            if($vancode)
            {	
				
                $this->data['row'] = $this->model->get_row(array('filter'=>"vancode='$vancode'"));
            }
            if(isset($_POST['data']))
            {
                $data = $_POST['data'];
				
                if($vancode)
                {
                    $status = $this->model->edit($vancode,$data);
                    
                    $this->trans_message($status,'edit');                 
                } else {
                    $status = $this->model->add($data);
                    
                    $this->trans_message($status,'add');
                }                                                
                redirect($this->controller);
            }
            $this->data['van'] = $this->van->get_rows(array('order'=>'VANCODE'),'VANCODE','VANNAME');
            $this->data['content'] = parent::view($this->view.'form',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");    
        }        
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
