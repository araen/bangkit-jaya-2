<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invoice extends MY_Controller {
    protected $cls_model = "model_invoice";
    protected $controller;
	protected $view = "admin/invoice/";
    
    function __construct()
    {
        parent::__construct();
        $this->controller = $this->config->item('base_url')."invoice/"; 
    }
    
    public function index()
    {
        $this->load->model('model_sales','sales');
        $this->load->model('model_penjualan','penjualan');
        $this->load->model('model_proses_order','proses_order');
        
        if($this->session->userdata('login') == TRUE){
            if(isset($_POST['cari'])){
                $param = array(
                    'page'   => 1,
                    'limit'  => 10,
                    'order'  => 'ORDERNO DESC',
                    'filter' => "ORDERDATE BETWEEN '".dGetDate($_POST['startdate'])."' 
                                 AND '".dGetDate($_POST['enddate'])."'
                                 AND OSTATUSCODE = 3 AND PRINTINV = 1", 
                    'q'      => "",
                ); 
                
                $this->data['rows'] = $this->proses_order->get_all($param);
            }
            else
            {
                $param = array(
                    'page'   => 1,
                    'limit'  => 10,
                    'order'  => 'ORDERNO DESC',
                    'filter' => "1=0",
                    'q'      => "",
                );        
            }
            
            if(isset($_POST['cetak'])) {
                $a_invno = array();
                if(count($_POST['invoice']) > 0) {
                    foreach ( $_POST['orderno'] as $key => $val ) {
                        $this->penjualan->reset_query();
                        $row = $this->penjualan->get_row(array('filter'=>"ORDERNO = '$val'"));
                        
                        $record['PRINTINV'] = 1;
                        $record['PRINTNUM'] = $row['PRINTNUM'] + 1;
                        
                        $this->penjualan->edit($val, $record);
                    }
                    
                    foreach ( $_POST['invoice'] as $key => $val ) {
                        if ( !in_array($val, $a_invno) ) 
                            $a_invno[] = $val;
                    }
                                            
                    $this->_printinv($a_invno, $_POST['orderno']);
                    return;
                }
            }
            
            $this->data['slsno'] = $_POST['sales'];
            $this->data['startdate'] = $_POST['startdate'];
            $this->data['enddate'] = $_POST['enddate'];
            
            $this->data['page_title'] = "Cetak Invoice";
            $this->data['q'] = @$param['q'];
            $this->data['page'] = @$param['page'];
            $this->data['limit'] = @$param['limit'];
            $this->data['rows'] = $this->penjualan->get_all($param); 
            $this->data['sales'] = $this->sales->get_rows(array('order'=>'SLSNO'),'SLSNO','SLSNAME');
            $this->data['content'] = parent::view($this->view.'grid',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    private function _printinv($a_invno = array(), $a_orderno = array()) {
        $this->load->model('model_produk','product');
        
        $param['filter'] = "INVNO IN ('".implode("','", $a_invno)."')";
        
        $this->model->reset_query();
        $this->data['rows'] = $this->model->get_order($param);
        
        $this->model->reset_query();
        $a_detail = $this->model->get_order_detail(array('filter'=>"ORDERNO IN ('".implode("','",$a_orderno)."')"));
        
        foreach ( $a_detail as $row ) {
            $this->data['detail'][$row['ORDERNO']][] = $row;
        }
        $this->data['konversi'] = $this->product->get_konversi();
        
        $this->load->view($this->view."invoice", $this->data);            
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
