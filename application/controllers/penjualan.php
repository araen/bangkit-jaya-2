<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Penjualan extends MY_Controller {
    protected $cls_model = "model_penjualan";
    protected $controller;
	protected $view = "admin/penjualan/";
    
    function __construct()
    {
        parent::__construct();
        $this->controller = $this->config->item('base_url')."penjualan/"; 
    }
    
    public function index()
    {
        if($this->session->userdata('login') == TRUE){
            if(isset($_POST['page'])){
                $param = array(
                    'page'   => $_POST['page'],
                    'limit'  => 10,
                    'order'  => 'ORDERNO DESC',
                    'filter' => "",
                    'q'      => $_POST['q']
                );
            }
            else
            {
                $param = array(
                    'page'   => 1,
                    'limit'  => 10,
                    'order'  => 'ORDERNO DESC',
                    'filter' => "", 
                    'q'      => "",
                );        
            }
            
            $this->data['page_title'] = "Transaksi Input Data Penjualan";
            $this->data['q'] = @$param['q'];
            $this->data['page'] = @$param['page'];
            $this->data['limit'] = @$param['limit'];
            $this->data['rows'] = $this->model->get_all($param);
            $this->data['content'] = parent::view($this->view.'grid',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    function form($orderno=null)
    {
        $this->load->model('model_sales','sales');
        $this->load->model('model_warehouse','warehouse');
        $this->load->model('model_outlet','outlet');
        $this->load->model('model_produk','produk');
        $this->load->model('model_penjualan_detail','detail');
        
        if($this->session->userdata('login') == TRUE)
        {
            $this->data['page_title'] = "Detail Penjualan";
            if($orderno)
            {
                $this->data['row'] = $this->model->get_row(array('filter'=>"orderno='$orderno'"));
                $this->data['detail'] = $this->detail->get_array_all(array('filter'=>"ORDERNO = '$orderno'","order"=>"PCODE"));
            }
            if(isset($_POST['data']))
            {
                $data = $_POST['data'];

                if ( !$orderno ) 
                {
                    $warehouse = $this->warehouse->get_row(array('filter'=>"WRCODE = $data[wrcode]"));
					$sales = $this->sales->get_row(array('filter'=>"SLSNO = $data[slsno]"));
					$outlet = $this->outlet->get_row(array('filter'=>"OUTNO = '$data[outno]'"));
					$header['WRCODE'] = $data['wrcode'];
					$header['SLSNO'] = $data['slsno'];
					$header['OUTNO'] = $data['outno'];
                    $header['ORDERDATE'] = dGetDate($data['orderdate']);
                    $header['SUBTOTAL'] = $data['subtotal'];
                    
                    $orderno = $this->model->add($header);
                    
                    redirect($this->controller."form/$orderno");
                }
                
                if ( $orderno ) {
                    $konversi = $this->_konversi($data);
                    
                    $row = $this->detail->get_row(array('filter'=>"ORDERNO = '$orderno' AND PCODE = '$data[pcode]'"), "DB.*");
                    
                    if ( $row ) 
                    {
                        $detail['QTY'] = $row['QTY'] + fNumber($konversi['QTY']);
                        $detail['AMOUNT'] = $row['QTY'] + fNumber($konversi['AMOUNT']);
                        
                        $this->detail->edit(array("ORDERNO" => $orderno, "PCODE"=> $data['pcode']), $detail);    
                    } 
                    else 
                    {
                        $detail['ORDERNO'] = $orderno;
                        $detail['PCODE'] = $data['pcode'];
					    $detail['QTY'] = fNumber($konversi['qty']);
					    $detail['AMOUNT'] = fNumber($konversi['amount']);
                        
                        $this->detail->add($detail);
                    }
                }
                redirect($this->controller."form/$orderno");
            }
            $this->data['orderno'] = $orderno;
			$this->data['sales'] = $this->sales->get_rows(array('order'=>'SLSNO'),'SLSNO','SLSNAME');
			$this->data['warehouse'] = $this->warehouse->get_rows(array('order'=>'WRCODE'),'WRCODE','WRNAME');
			$this->data['outlet'] = $this->outlet->get_rows(array('order'=>'OUTNO'),'OUTNO','OUTNAME');
			$this->data['produk'] = $this->produk->get_rows(array('order'=>'PCODE'),'PCODE','PNAME');
            $this->data['content'] = parent::view($this->view.'form',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login"); 
        }        
    }
        
    function delete_detail($iddbeli, $iddetail) {
        $this->load->model('model_penjualan_detail','detail');
        
        $status = $this->detail->delete($iddetail);
        
        if ( $status )
            redirect($this->controller."form/$dorderno");    
    }
	
	private function _konversi($data) {
        $this->load->model('model_produk', 'produk');
        
        $produk = $this->produk->get_row(array('filter' => "PCODE = '$data[pcode]'"));
        
        $qtytotal = 0;
        $jmltotal = 0;
        
        if ( $data["qty1"] == 0 and $data["qty2"] == 0 and $data["qty3"] == 0 )
            return false;
        
        if ( $data["qty1"] > 0 ) {
            $qtytotal += $data["qty1"] * $produk["CONV1"] * $produk["CONV2"];
        }
        
        if ( $data["qty2"] > 0 ) {
            $qtytotal += $data["qty2"] * $produk["CONV2"];
        }
        
        if ( $data["qty3"] > 0 ) {
            $qtytotal += $data["qty3"];
        }
        
        $jmltotal = $qtytotal * $produk["PPRICE3"];
        
        $return['QTY'] = $qtytotal;
        $return['AMOUNT'] = $jmltotal;
        
        return $return;
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
