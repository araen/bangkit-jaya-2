<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Returnsupp extends MY_Controller {
    protected $cls_model = "model_pembelian";
    protected $controller;
	protected $view = "admin/returnsupp/";
    
    function __construct()
    {
        parent::__construct();
        $this->controller = base_url()."returnsupp/";
        
        $this->data['isopname'] = isOpname(); 
    }
    
    public function index()
    {
        if($this->session->userdata('login') == TRUE){
            if(isset($_POST['page'])){
                $param = array(
                    'page'   => $_POST['page'],
                    'limit'  => 10,
                    'order'  => 'IDBELI DESC',
                    'filter' => "POSTSTATUS = 1",
                    'q'      => $_POST['q']
                );
            }
            else
            {
                $param = array(
                    'page'   => 1,
                    'limit'  => 10,
                    'order'  => 'IDBELI DESC',
                    'filter' => "POSTSTATUS = 1", 
                    'q'      => "",
                );        
            }
            
            $this->data['page_title'] = "Transaksi Retur Pembelian";
            $this->data['q'] = @$param['q'];
            $this->data['page'] = @$param['page'];
            $this->data['limit'] = @$param['limit'];
            $this->data['rows'] = $this->model->get_all($param);
            $this->data['content'] = parent::view($this->view.'grid',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    function form($idbeli=null)
    {
        $this->load->model('model_warehouse','warehouse');
        $this->load->model('model_produk','produk');
        $this->load->model('model_stock','stock');
        $this->load->model('model_stockmove','stockmove');
        $this->load->model('model_supplier','supplier');
        $this->load->model('model_van','van');
        $this->load->model('Model_pembelian_detail','detail');
        $this->load->model('Model_return_supplier','retsupp');
        
        if($this->session->userdata('login') == TRUE)
        {
            $this->data['page_title'] = "Detail Pembelian";
            if($idbeli)
            {
                $this->data['row'] = $this->model->get_row(array('filter'=>"IDBELI='$idbeli'"));
                $this->data['detail'] = $this->retsupp->get_array_all(array('filter'=>"IDBELI = '$idbeli'","order"=>"PCODE"));
            
                $produk = $this->produk->get_array_all();
                
                $a_produk = array();
                foreach($produk as $pr) {
                    $a_produk[$pr['PCODE']]['PPRICE1'] = $pr['PPRICE1'];    
                    $a_produk[$pr['PCODE']]['SPRICE1'] = $pr['SPRICE1'];    
                }
            }

            if( isset($_POST['save']) )
            {
                if ( $idbeli and !isOpname() ) 
                {
                    foreach ( $_POST['qtyret'] as $key => $value ) 
                    {
                        $row = $this->detail->get_row(array('filter' => "IDDBELI = '$key'"),"DB.*");
                        
                        $data['pcode'] = $row['PCODE'];
                        
                        foreach ( $value as $k => $val )
                            $data["qty$k"] = $val;
                        
                        $detail['QTYRET'] = $_POST['qtyret'][$key][1];
                        $detail['AMOUNTRET'] = $_POST['qtyret'][$key][1] * $a_produk[$row['PCODE']]['PPRICE1'];
                        $detail['RETDATE'] = date('Y-m-d H:i:s');
                        
                        if ( $detail['QTYRET'] > 0 ) {
                            $status = $this->detail->edit(array("IDDBELI" => $key), $detail);
                        
                            //add stock move    
                            if($status) 
                            {
                                $stockmove['STMVCODE'] = 'T';    
                                $stockmove['TRANSDATE'] = $detail['RETDATE'];    
                                $stockmove['WRCODE'] = $this->data['row']['WRCODE'];    
                                $stockmove['PCODE'] = $row['PCODE'];    
                                $stockmove['STOCKNUM'] = -1 * $detail['QTYRET'];    
                                $stockmove['BALANCE'] = $detail['AMOUNTRET']; 

                                $status = $this->stockmove->add($stockmove);
                            }
                        }
                    }
                    
                    $this->model->edit($idbeli, array("RETLOCK" => 1));
                    
                    redirect($this->controller."form/$idbeli", true);
                }
            }
            $this->data['idbeli'] = $idbeli;
			$this->data['van'] = $this->van->get_rows(array('order'=>'VANCODE'),'VANCODE','VANNAME');
            $this->data['supplier'] = $this->supplier->get_rows(array('order'=>'SUPPNO'),'SUPPNO','SUPPNAME');
            $this->data['warehouse'] = $this->warehouse->get_rows(array('order'=>'WRCODE'),'WRCODE','WRNAME');
			$this->data['produk'] = $this->produk->get_rows(array('order'=>'PCODE'),'PCODE','PNAME');
            $this->data['konversi'] = $this->produk->get_konversi();
            $this->data['content'] = parent::view($this->view.'form',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");    
        }        
    }

    function delete_detail($iddbeli, $iddetail) {
        $this->load->model('model_pembelian_detail','detail');
        
        $status = $this->detail->delete($iddetail);
        
        if ( $status )
            redirect($this->controller."form/$iddbeli");    
    }
    
    private function _konversi($data) {
        $this->load->model('model_produk', 'produk');
        
        $this->produk->reset_query();
        $produk = $this->produk->get_row(array('filter' => "PCODE = '$data[pcode]'"));
        
        $qtytotal = 0;
        $jmltotal = 0;
        
        if ( $data["qty1"] == 0 and $data["qty2"] == 0 and $data["qty3"] == 0 )
            return false;
        
        if ( $data["qty1"] > 0 ) {
            $qtytotal += $data["qty1"] * $produk["CONV2"] * $produk['CONV3'];
            $jmltotal += $data["qty1"] * $produk["PPRICE1"];
        }
        
        if ( $data["qty2"] > 0 ) {
            $qtytotal += $data["qty2"] * $produk["CONV3"];
            $jmltotal += $data["qty2"] * $produk["PPRICE2"];
        }
        
        if ( $data["qty3"] > 0 ) {
            $qtytotal += $data["qty3"];
            $jmltotal += $data["qty3"] * $produk["PPRICE3"];
        }
        
        $return['QTY'] = $qtytotal;
        $return['AMOUNT'] = $jmltotal;
        
        return $return;
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
