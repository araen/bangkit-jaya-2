<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Returnorder extends MY_Controller {
    protected $cls_model = "model_penjualan";
    protected $controller;
	protected $view = "admin/returnorder/";
    
    function __construct()
    {
        parent::__construct();
    }
    
    public function index()
    {
        if($this->session->userdata('login') == TRUE){
            if(isset($_POST['page'])){
                $param = array(
                    'page'   => $_POST['page'],
                    'limit'  => 10,
                    'order'  => 'ORDERNO DESC',
                    'filter' => "OSTATUSCODE = 3",
                    'q'      => $_POST['q']
                );
            }
            else
            {
                $param = array(
                    'page'   => 1,
                    'limit'  => 10,
                    'order'  => 'ORDERNO DESC',
                    'filter' => "OSTATUSCODE = 3", 
                    'q'      => "",
                );        
            }
            
            $this->data['page_title'] = "Transaksi Retur Penjualan";
            $this->data['q'] = @$param['q'];
            $this->data['page'] = @$param['page'];
            $this->data['limit'] = @$param['limit'];
            $this->data['rows'] = $this->model->get_all($param);
            $this->data['content'] = parent::view($this->view.'grid',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    function form($orderno=null)
    {
        $this->load->model('model_warehouse','warehouse');
        $this->load->model('model_produk','produk');
        $this->load->model('model_stock','stock');
        $this->load->model('model_stockmove','stockmove');
        $this->load->model('model_sales','sales');
        $this->load->model('model_outlet','outlet');
        $this->load->model('Model_pembelian_detail','detail');
        $this->load->model('model_return_order','retorder');
        
        if($this->session->userdata('login') == TRUE)
        {
            $this->data['page_title'] = "Detail Penjualan";
            if($orderno)
            {
                $this->data['row'] = $this->model->get_row(array('filter'=>"ORDERNO='$orderno'"));
                $this->data['detail'] = $this->retorder->get_array_all(array('filter'=>"ORDERNO = '$orderno'","order"=>"PCODE"));
            
                $produk = $this->produk->get_array_all();
                
                $a_produk = array();
                foreach($produk as $pr) {
                    $a_produk[$pr['PCODE']]['PPRICE1'] = $pr['PPRICE1'];    
                    $a_produk[$pr['PCODE']]['SPRICE1'] = $pr['SPRICE1'];    
                }
            }

            if( isset($_POST['save']) )
            {
                if ( $orderno and !isOpname() ) 
                {
                    foreach ( $_POST['qtyret'] as $key => $value ) 
                    {
                        // reinitiate object
                        $this->load->model('model_order_detail','dorder');
                        $row = $this->dorder->get_row(array('filter' => "DORDERNO = '$key'"),"DB.*");
                        
                        $data['pcode'] = $row['PCODE'];
                        
                        foreach ( $value as $k => $val )
                            $data["qty$k"] = $val;
                        
                        $detail['QTYRET'] = $_POST['qtyret'][$key][1];
                        $detail['AMOUNTRET'] = ($_POST['qtyret'][$key][1] * $a_produk[$row['PCODE']]['SPRICE1']) - ($row['DISC']/100 * ($_POST['qtyret'][$key][1] * $a_produk[$row['PCODE']]['SPRICE1']));
                        $detail['RETTYPE'] = $_POST['rettype'][$key];
                        $detail['RETDATE'] = date('Y-m-d H:i:s');
                        
                        if ( $detail['QTYRET'] > 0 ) {
                            $status = $this->dorder->edit(array("DORDERNO" => $key), $detail);
                            
                            if($status) 
                            {
                                $stockmove['STMVCODE'] = 'P';    
                                $stockmove['TRANSDATE'] = $detail['RETDATE'];    
                                $stockmove['WRCODE'] = $this->data['row']['WRCODE'];    
                                $stockmove['PCODE'] = $row['PCODE'];
                                
                                if ( $detail['RETTYPE'] == 'R' ) {
                                    $stockmove['STOCKNUM'] = $detail['QTYRET'];    
                                    $stockmove['BALANCE'] = -1 * $detail['AMOUNTRET']; 
                                }
                                else {
                                    $stockmove['STOCKNUM'] = -1 * $detail['QTYRET'];    
                                    $stockmove['BALANCE'] = -1 * $detail['AMOUNTRET'];
                                }
                                
                                $status = $this->stockmove->add($stockmove);        
                            }
                        }
                    }
                    
                    $this->model->edit($orderno, array("RETLOCK" => 1));
                    
                    redirect($this->controller."/form/$orderno", true);
                }
            }
            $this->data['orderno'] = $orderno;
			$this->data['warehouse'] = $this->warehouse->get_rows(array('order'=>'WRCODE'),'WRCODE','WRNAME');
            $this->data['sales'] = $this->sales->get_rows(array('order'=>'SLSNO'),'SLSNO','SLSNAME');
            $this->data['outlet'] = $this->outlet->get_rows(array('order'=>'OUTNO'),'OUTNO','OUTNAME');
			$this->data['produk'] = $this->produk->get_rows(array('order'=>'PCODE'),'PCODE','PNAME');
            $this->data['konversi'] = $this->produk->get_konversi();
            $this->data['content'] = parent::view($this->view.'form',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");    
        }        
    }

    function delete_detail($iddbeli, $iddetail) {
        $this->load->model('model_pembelian_detail','detail');
        
        $status = $this->detail->delete($iddetail);
        
        if ( $status )
            redirect($this->controller."form/$iddbeli");    
    }
    
    private function _konversi($data) {
        $this->load->model('model_produk', 'produk');
        
        $this->produk->reset_query();
        $produk = $this->produk->get_row(array('filter' => "PCODE = '$data[pcode]'"));
        
        $qtytotal = 0;
        $jmltotal = 0;
        
        if ( $data["qty1"] == 0 and $data["qty2"] == 0 and $data["qty3"] == 0 )
            return false;
        
        if ( $data["qty1"] > 0 ) {
            $qtytotal += $data["qty1"] * $produk["CONV2"] * $produk['CONV3'];
            $jmltotal += $data["qty1"] * $produk["PPRICE1"];
        }
        
        if ( $data["qty2"] > 0 ) {
            $qtytotal += $data["qty2"] * $produk["CONV3"];
            $jmltotal += $data["qty2"] * $produk["PPRICE2"];
        }
        
        if ( $data["qty3"] > 0 ) {
            $qtytotal += $data["qty3"];
            $jmltotal += $data["qty3"] * $produk["PPRICE3"];
        }
        
        $return['QTY'] = $qtytotal;
        $return['AMOUNT'] = $jmltotal;
        
        return $return;
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
