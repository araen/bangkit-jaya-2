<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Brand extends MY_Controller {
    protected $cls_model = "model_brand";
    protected $controller;
	protected $view = "admin/brand/";
    
    function __construct()
    {
        parent::__construct();
        $this->controller = $this->config->item('base_url')."brand/"; 
    }
    
    public function index()
    {
        if($this->session->userdata('login') == TRUE){
            if(isset($_POST['page'])){
                $param = array(
                    'page'   => $_POST['page'],
                    'limit'  => 10,
                    'order'  => 'PBCODE ASC',
                    'filter' => "",
                    'q'      => $_POST['q']
                );
            }
            else
            {
                $param = array(
                    'page'   => 1,
                    'limit'  => 10,
                    'order'  => 'PBCODE ASC',
                    'filter' => "", 
                    'q'      => "",
                );        
            }
            
            $this->data['page_title'] = "Brand";
            $this->data['q'] = @$param['q'];
            $this->data['page'] = @$param['page'];
            $this->data['limit'] = @$param['limit'];
            $this->data['rows'] = $this->model->get_all($param);
            $this->data['content'] = parent::view($this->view.'grid',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    function form($pbcode=null)
    {
        $this->load->model('model_brand','brand');
        
        if($this->session->userdata('login') == TRUE)
        {
            $this->data['page_title'] = "Detail Brand";
            if($pbcode)
            {
                $this->data['row'] = $this->model->get_row(array('filter'=>"pbcode='$pbcode'"));
            }
            if(isset($_POST['data']))
            {
                $data = $_POST['data'];
                
                if($pbcode)
                {
                    $this->model->edit($pbcode,$data);                 
                } else {
                    $this->model->add($data);
                }
                redirect($this->controller);
            }
            $this->data['brand'] = $this->brand->get_rows(array('order'=>'PBCODE'),'PBCODE','PBNAME');
            $this->data['content'] = parent::view($this->view.'form',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");    
        }        
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
