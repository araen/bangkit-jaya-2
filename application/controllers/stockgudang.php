<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stockgudang extends MY_Controller {
	protected $cls_model = "model_laporan";
	protected $controller;
	protected $view = "admin/stockgudang/";
	
	function __construct()
	{
		parent::__construct();
		$this->controller = $this->config->item('base_url')."stockgudang/";
	}
	
	public function index()
    {
        if($this->session->userdata('login') == TRUE){
            
            $this->data['page_title'] = "Laporan Stock Gudang";
            $this->data['content'] = parent::view($this->view.'repp_stockgudang',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
		
	public function rep_stockgudang(){
        $this->load->model('model_stock','stock');
        $this->load->model('model_setting','setting');
		$this->load->model('model_produk','product');
        $this->load->model('model_warehouse','warehouse');
        
        $post = $_POST['data'];

        $setting = $this->setting->get_setting();
        $this->data['wrcode'] = $post['wrcode'];
        $this->data['warehouse'] = $this->warehouse->get_rows(array('order'=>'WRCODE'),'WRCODE','WRNAME');
               
        $param['filter'] = "WRCODE = '$post[wrcode]' AND STOCKDATE = '$setting[WRDATE]'";
        $this->data['rows'] = $this->stock->get_all($param);

		parent::view($this->view.'rep_stockgudang');
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
