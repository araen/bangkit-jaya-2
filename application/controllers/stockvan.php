<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stockvan extends MY_Controller {
	protected $cls_model = "model_laporan";
	protected $controller;
	protected $view = "admin/stockvan/";
	
	function __construct()
	{
		parent::__construct();
		$this->controller = $this->config->item('base_url')."stockvan/";
	}
	
	public function index()
    {
        if($this->session->userdata('login') == TRUE){
            
            $this->data['page_title'] = "Laporan Stock Van";
            $this->data['content'] = parent::view($this->view.'repp_stockvan',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
		
	public function rep_stockvan(){
        $this->load->model('model_stock_van','stockvan');
        $this->load->model('model_produk','product');
		$this->load->model('model_van','van');

        $post = $_POST['data'];
        
        $param['filter'] = "VANCODE = '$post[vancode]'";
        $this->data['rows'] = $this->stockvan->get_all($param);

        $this->data['konversi'] = $this->product->get_konversi();
        $this->data['vancode'] = $post['vancode'];
        $this->data['van'] = $this->van->get_rows(array('order'=>'VANCODE'),'VANCODE','VANNAME');
		
		parent::view($this->view.'rep_stockvan');
	}
	
	
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
