<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Role extends MY_Controller {
    protected $cls_model    = "model_role";
	protected $controller;
    protected $view         = "admin/role/";
	
	function __construct()
    {
        parent::__construct();

        $this->controller = $this->config->item('cpanel_dir')."role/";
    }
	
	function index(){
		if($this->session->userdata('login') === TRUE){

			if(isset($_POST['page'])){
				$param = array(
                    'page'   => $_POST['page'],
                    'limit'  => 10,
				    'q'      => $_POST['q'],
                );
			}
            else
            {
                $param = array(
                    'page'   => 1,
                    'limit'  => 10,
                    'q'      => '',
                );        
            }
			
			$this->data['role']= $this->model->get_all($param);
			$this->data['q'] = @$param['q'];
			$this->data['page'] = @$param['page'];
			$this->data['limit'] = @$param['limit'];
			
			$this->data['content'] = parent::view($this->view.'grid',true);
            parent::view("admin/index");
		}else{
			parent::view("admin/login");
		}
	}
    
    function form($idrole=null)
    {
        $this->load->model('model_user','user');
        if($this->session->userdata('login') == TRUE)
        {
            $this->data['page_title'] = "Detail Role";
            if($idrole)
            {
                $this->data['idrole'] = $idrole;
                $this->data['row'] = $this->model->get_row(array('filter'=>"IDROLE=$idrole"));
                $this->data['detail'] = $this->model->get_user_role(array('filter'=>"IDROLE=$idrole"));
            }
            if(isset($_POST['data']))
            {
                $data = $_POST['data'];
                
                if($idrole)
                {
                    $status = $this->model->edit($idrole,$data);                 
                    
                    $this->trans_message($status, 'edit');
                } else {
                    $idrole = $this->model->add($data);
                    
                    $this->trans_message($idrole, 'add');
                }
                
                if(isset($_POST['detail']))
                {
                    $detail = $_POST['detail'];
                    $detail['IDROLE'] = $idrole;
                    
                    $row = $this->model->get_user_role(array('filter'=>"IDROLE = $idrole AND ID = $detail[iduser]"));
                    if ($row)
                        $this->trans_message(0, 'add', 'user sudah terdaftar sebelumnya');
                    else { 
                        $status = $this->model->add_user($detail);
                        $this->trans_message($status, 'add');
                    }
                }
                redirect($this->controller."form/$idrole");
            }
            $this->data['user'] = $this->user->get_rows(array('order'=>'ID'),'ID','USNAME');
            $this->data['content'] = parent::view($this->view.'form',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");    
        }        
    }
    
    function access($idrole) {
        $this->load->model('auth','auth');
        $this->load->model('model_access','access');
        
        $role = $this->model->get_rows(array('order'=>'IDROLE'),'IDROLE','NAMA');
        $this->data['page_title'] = "Access Menu $role[$idrole]";
        
        $this->auth->reset_query();
        $this->data['rows'] = $this->auth->get_menu();
        $this->data['access'] = $this->access->get_access(array('filter' => "IDROLE = $idrole"));
        
        if ( isset($_POST['submit']) ) 
        {
            $this->access->delete(array('IDROLE' => $idrole));
            foreach ( $_POST['access'] as $key => $record ) {
                $record['IDROLE'] = $idrole;
                $record['CMNCODE'] = $key;
                
                $status = $this->access->add($record);
                
                $this->trans_message($status, 'add');        
            }
            redirect($this->controller."access/$idrole");
        }
        
        $this->data['idrole'] = $idrole;
        $this->data['content'] = parent::view($this->view.'menu',true);
        parent::view("admin/index");
    }
    
    function delete_detail($idrole, $iddetail) {
        $status = $this->model->delete_detail(array('IDROLE' => $idrole,'IDUSER' => $iddetail));
        $this->trans_message($status,'delete');    
        
        redirect($this->controller."form/$idrole");    
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */