<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');  

class Vanload extends MY_Controller {
    protected $cls_model = "model_trx_van";
    protected $controller;
    protected $view = "admin/vanload/";
    
    function __construct() 
    {
        parent::__construct();
        $this->controller = base_url()."vanload/";
        
        $this->data['isopname'] = isOpname();    
    }
    
    public function index() 
    {
        if($this->session->userdata('login') == TRUE)
        {
            if(isset($_POST['page'])){
                $param = array(
                    'page'   => $_POST['page'],
                    'limit'  => 10,
                    'order'  => "DAYIN DESC",
                    'filter' => "",
                    'q'      => $_POST['q']
                );
            }
            else
            {
                $param = array(
                    'page'   => 1,
                    'limit'  => 10,
                    'order'  => "DAYIN DESC",
                    'filter' => "DAYIN IS NULL", 
                    'q'      => "",
                );        
            }
            
            $this->data['page_title'] = "Van Load";
            $this->data['limit'] = @$param['limit'];
            $this->data['page'] = @$param['page'];
            $this->data['q'] = @$param['q'];
            $this->data['rows'] = $this->model->get_all($param);
            $this->data['content'] = parent::view($this->view.'grid',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    function form($idtvan=null)
    {
        $this->load->model('model_van','van');
        $this->load->model('model_sales','sales');
        $this->load->model('model_stock','stock');
        $this->load->model('model_produk','produk');
        $this->load->model('model_trx_van','trxvan');
        $this->load->model('model_stock_van','stockvan');
        $this->load->model('model_warehouse','warehouse');
        $this->load->model('model_trx_van_detail','trxdvan');
        
        if($this->session->userdata('login') == TRUE)
        {
            $this->data['page_title'] = "Detail Van Load";

            if($idtvan) 
            {
                $this->data['row'] = $this->trxvan->get_row(array('filter'=>"idtvan = '$idtvan'",'order'=>'IDTVAN'));    
                $this->data['detail'] = $this->trxdvan->get_array_all(array('filter'=>"idtvan = '$idtvan'",'order'=>'IDTVAN'));    
            }
            
            if(isset($_POST['proses']) and !isOpname() ) 
            {
                $this->stock->van_order($idtvan, false);
                
                $this->stockvan->stock_order($idtvan, true);
                
                $record['SUBTOTAL'] = $_POST['total'];
                $record['OSTATUSCODE'] = 3;
                $status = $this->model->edit($idtvan, $record);
                die($this->model->last_query);
                $this->trans_message($status, 'edit');
                      
                redirect($this->controller."form/$idtvan", true);        
            }
            
            if(isset($_POST['data']))
            {
                $data = $_POST['data'];
                
                if( !$idtvan )
                {
                    $header['WRCODE'] = $data['wrcode'];
                    $header['SLSNO'] = $data['slsno'];
                    $header['VANCODE'] = $data['vancode'];
                    $header['OSTATUSCODE'] = 2;
                    $header['DAYOUT'] = dGetDate($data['dayout']);
                    $header['SUBTOTAL'] = $data['subtotal'];
                    
                    $idtvan = $this->model->add($header);
                    
                    $this->trans_message($idtvan, 'add');
                    
                    redirect($this->controller."form/$idtvan");
                } 
                elseif($idtvan) 
                {
                    $konversi = $this->_konversi($data);
                    
                    $row = $this->trxdvan->get_row(array('filter'=>"IDTVAN = '$idtvan' AND PCODE = '$data[pcode]'"));

                    if ( $row ) 
                    {
                        $detail['QTY'] = $row['QTY'] + fNumber($konversi['QTY']);
                        $detail['AMOUNT'] = $row['QTY'] + fNumber($konversi['AMOUNT']);
                        
                        $status = $this->trxdvan->edit(array("IDTVAN" => $idtvan, "PCODE"=> $data['pcode']), $detail);    
                    
                        $this->trans_message($status, 'edit');
                    } 
                    else 
                    {
                        $detail['IDTVAN'] = $idtvan;
                        $detail['PCODE'] = $data['pcode'];
                        $detail['QTY'] = fNumber($konversi['QTY']);
                        $detail['AMOUNT'] = fNumber($konversi['AMOUNT']);
                        
                        $status = $this->trxdvan->add($detail);

                        $this->trans_message($status, 'add');
                    }
                }
                redirect($this->controller."form/$idtvan");
            }
            
            $this->data['idtvan'] = $idtvan;
            $this->data['sales'] = $this->sales->get_rows(array(),"SLSNO", "SLSNAME");
            $this->data['van'] = $this->van->get_rows(array('order'=>'VANCODE'),'VANCODE','VANNAME');
            $this->data['warehouse'] = $this->warehouse->get_rows(array('order'=>'WRCODE'),'WRCODE','WRNAME');
            $this->data['konversi'] = $this->produk->get_konversi();
            $this->data['content'] = parent::view($this->view.'form',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");    
        }        
    }
    
    function delete_detail($idtvan, $iddtvan) 
    {
        $this->load->model('model_trx_van_detail','trxdvan');
        
        $status = $this->trxdvan->delete($iddtvan);
        
        if($status)
            redirect($this->controller."form/$idtvan");    
    }
    
    private function _konversi($data) {
        $this->load->model('model_produk', 'produk');
        
        $produk = $this->produk->get_row(array('filter' => "PCODE = '$data[pcode]'"));
        
        $qtytotal = 0;
        $jmltotal = 0;
        
        if ( $data["vstock1"] == 0 and $data["vstock2"] == 0 and $data["vstock3"] == 0 )
            return false;
        
        if ( $data["vstock1"] > 0 ) {
            $qtytotal += $data["vstock1"] * $produk["CONV2"] * $produk["CONV3"];
            $jmltotal += $data["vstock1"] * $produk["PPRICE1"];
        }
        
        if ( $data["vstock2"] > 0 ) {
            $qtytotal += $data["vstock2"] * $produk["CONV3"];
            $jmltotal += $data["vstock2"] * $produk["PPRICE2"];
        }
        
        if ( $data["vstock3"] > 0 ) {
            $qtytotal += $data["vstock3"];
            $jmltotal += $data["vstock3"] * $produk["PPRICE3"];
        }
        
        $return['QTY'] = $qtytotal;
        $return['AMOUNT'] = $jmltotal;

        return $return;
    }    
}