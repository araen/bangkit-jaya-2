<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cart extends MY_Controller {
    protected $cls_model = "model_cart";
    protected $controller;
	protected $view = "admin/pembelian/";
    
    function __construct()
    {
        parent::__construct();
        $this->controller = $this->config->item('base_url')."brand/"; 
    }
    
    function reset_chart($type = 'R') {
        $this->load->model('model_cart');
        
        $session = $this->session->all_userdata();
        
        $this->model_cart->delete(array("SESSIONID"=> $session['id_user'], "TRXCODE" => "$type"));
        
        redirect($session['controller'].'/form');
    }
    
    function delete_cart($chartid) {
        $this->load->model('model_cart');
        
        $session = $this->session->all_userdata();
        
        $key = explode('_', $chartid);
        
        $r_key['SESSIONID'] = $key[0];    
        $r_key['PCODE'] = $key[1];    
        $r_key['TRXCODE'] = $key[2];
        
        $this->model_cart->delete($r_key);
        
        redirect($session['controller'].'/form');    
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
