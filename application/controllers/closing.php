<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Closing extends MY_Controller {
    protected $cls_model = "model_setting";
    protected $controller;
	protected $view = "admin/closing/";
    
    function __construct()
    {
        parent::__construct();
        $this->controller = $this->config->item('base_url')."closing/"; 
    }
    
    public function index()
    {
        $this->load->model('model_stock','stock');
        $this->load->model('model_setting','setting');
        
        if($this->session->userdata('login') == TRUE){
            $this->data['page_title'] = "Closing Harian";
            $this->data['setting'] = $this->setting->get_setting();
            
            //$current_date = strtotime($this->data['setting']['WRDATE']);
            //$next_date = mktime(0,0,0, date('m', $current_date), date('d', $current_date) + 1, date('Y', $current_date));
            
            list($hr, $bln, $thn) = explode('-', $_POST['tglgudang']);
            
            if(isset($_POST['closing'])) {
                $record['VALUE'] = $thn .'-'. $bln .'-'. $hr;

                $this->stock->closing($this->data['setting'], $record['VALUE']);

                $this->model->edit(array('SKEY' => 'WRDATE'), $record); 

                redirect($this->controller);
            }
            
            $this->data['content'] = parent::view($this->view.'grid',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
