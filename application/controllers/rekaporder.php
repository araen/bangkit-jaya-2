<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rekaporder extends MY_Controller {
    protected $cls_model = "model_penjualan";
    protected $controller;
	protected $view = "admin/rekaporder/";
    
    function __construct()
    {
        parent::__construct();
    }
    
    public function index()
    {
        if($this->session->userdata('login') == TRUE){
            $this->outlet();    
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    function outlet() {
        if($this->session->userdata('login') == TRUE){
            $this->load->model('model_outlet','outlet');
            $this->data['page_title'] = "Rekap Data Order Per Outlet";
            
            $this->data['outlet'] = $this->outlet->get_rows(array('order'=>'OUTNO'),'OUTNO','OUTNAME');
            $this->data['content'] = parent::view($this->view.'repp_rekapoutlet',true);
            parent::view("admin/index");    
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    function rep_outlet()
    {
        $this->load->model('model_order', 'order');
        $this->load->model('model_setting', 'setting');
        
        $post = $_POST['data'];
        
        $param['filter'] = "1=1 ";
        if ( $post['slsno'] )
            $param['filter'] .= "AND o.SLSNO = '$post[slsno]' ";
        if ( $post['tglmulai'] )
            $param['filter'] .= "AND DATE_FORMAT(v.INVDATE,'%d-%m-%Y') >= '$post[tglmulai]' AND DATE_FORMAT(v.INVDATE,'%d-%m-%Y') <= '$post[tglselesai]' ";
        //$param['filter'] .= "AND v.INVNO NOT IN (SELECT INVNO FROM t_payment)";
        $data = $this->order->get_rekap_outlet($param);

        $a_sales = array();
        foreach ( $data['data'] as $row ) 
            $a_sales[$row['SLSNO']] = $row['SLSNO']."-".$row['SALES'];
            
        foreach ( $data['data'] as $row ) 
            $a_van[$row['CARNO']] = $row['CARNO'];    

        $data['tglmulai'] = $post['tglmulai'];    
        $data['tglselesai'] = $post['tglselesai'];    
        $data['sales'] = $a_sales;
        $data['van'] = $a_van;

        $data['setting'] = $this->setting->get_setting();
        $this->load->view($this->view."rep_outlet", $data);    
    }
    
    function product() {
        if($this->session->userdata('login') == TRUE){
            $this->load->model('model_outlet','outlet'); 
            $this->data['page_title'] = "Rekap Data Order Per Product";
                
            $this->data['outlet'] = $this->outlet->get_rows(array('order'=>'OUTNO'),'OUTNO','OUTNAME');
            $this->data['content'] = parent::view($this->view.'repp_rekapproduct',true);
            parent::view("admin/index");    
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    function rep_product() {
        $this->load->model('model_order_detail', 'detail');
        $this->load->model('model_produk', 'product');
        $this->load->model('model_setting', 'setting');
        $this->load->model('model_outlet','outlet');
        
        $post = $_POST['data'];
        
        $param['filter'] = "1=1 ";
        if ( $post['slsno'] )
            $param['filter'] .= "AND SL.SLSNO = '$post[slsno]' ";
        if ( $post['tglmulai'] )
            $param['filter'] .= "AND DATE_FORMAT(IV.INVDATE,'%d-%m-%Y') >= '$post[tglmulai]' AND DATE_FORMAT(IV.INVDATE,'%d-%m-%Y') <= '$post[tglselesai]' ";
        //$param['filter'] .= "AND IV.INVNO NOT IN (SELECT INVNO FROM t_payment)"; 
        $data['data'] = $this->detail->get_rekap($param);

        $a_sales = array();
        foreach ( $data['data'] as $row ) 
            $a_sales[$row['SLSNO']] = $row['SLSNO']."-".$row['SLSNAME']; 
            
        $a_order = array();
        foreach ( $data['data'] as $row ) 
            $a_order[$row['ORDERNO']] = $row['ORDERNO'];
            
        $a_produk = array();
        foreach ( $data['data'] as $row ) { 
            $a_produk[$row['PCODE']]['PNAME'] = $row['PNAME']; 
            $a_produk[$row['PCODE']]['QTY'] += $row['QTY']; 
            $a_produk[$row['PCODE']]['AMOUNT'] += $row['AMOUNT']; 
        }
        
        $data['tglmulai'] = $post['tglmulai'];    
        $data['tglselesai'] = $post['tglselesai'];
        $data['sales'] = $a_sales;
        $data['van'] = $a_van;
        $data['produk'] = $a_produk;
        $data['order'] = $a_order;
        $data['outno'] = $data['outno'];
        $data['konversi'] = $this->product->get_konversi();
        
        $data['outlet'] = $this->outlet->get_rows(array('order'=>'OUTNO'),'OUTNO','OUTNAME');
        $data['setting'] = $this->setting->get_setting();
        $this->load->view($this->view."rep_product", $data);    
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
