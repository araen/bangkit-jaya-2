<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sales extends MY_Controller {
    protected $cls_model = "model_sales";
    protected $controller;
	protected $view = "admin/sales/";
    
    function __construct()
    {
        parent::__construct();
        $this->controller = $this->config->item('base_url')."sales/"; 
    }
    
    public function index()
    {
        if($this->session->userdata('login') == TRUE){
            if(isset($_POST['page'])){
                $param = array(
                    'page'   => $_POST['page'],
                    'limit'  => 10,
                    'order'  => 'SLSNO ASC',
                    'filter' => "",
                    'q'      => $_POST['q']
                );
            }
            else
            {
                $param = array(
                    'page'   => 1,
                    'limit'  => 10,
                    'order'  => 'SLSNO ASC',
                    'filter' => "", 
                    'q'      => "",
                );        
            }
            
            $this->data['page_title'] = "Sales";
            $this->data['q'] = @$param['q'];
            $this->data['page'] = @$param['page'];
            $this->data['limit'] = @$param['limit'];
            $this->data['rows'] = $this->model->get_all($param);
            $this->data['content'] = parent::view($this->view.'grid',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    function form($slsno=null)
    {
        $this->load->model('model_van','van');
        $this->load->model('model_sales','sales');
        $this->load->model('model_jnssales','jnssales');
        
        if($this->session->userdata('login') == TRUE)
        {
            $this->data['page_title'] = "Detail Sales";
            if($slsno)
            {	
				
                $this->data['row'] = $this->model->get_row(array('filter'=>"slsno=$slsno"));
            }
            if(isset($_POST['data']))
            {
                $data = $_POST['data'];
				
				$data['workdate'] = dGetDate($data['workdate']);
				$data['transdate'] = dGetDate($data['transdate']);
				$data['birthdate'] = dGetDate($data['birthdate']);
                if($slsno)
                {
                    $this->model->edit($slsno,$data);                 
                } else {
                    $this->model->add($data);
                }
                redirect($this->controller);
            }
            $this->data['jnssales'] = $this->jnssales->get_rows(array('order'=>'STCODE'),'STCODE','STNAME');
            $this->data['van'] = $this->van->get_rows(array('order'=>'VANCODE'),'VANCODE','VANNAME');
            $this->data['sales'] = $this->sales->get_rows(array('order'=>'SLSNO'),'SLSNO','SLSNAME');
            $this->data['content'] = parent::view($this->view.'form',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");    
        }        
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
