<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');  

class Ajax extends MY_Controller 
{
    protected $cls_model = "model_van";
        
    function __construct() 
    {
        parent::__construct();
        
        $this->output->enable_profiler(FALSE);
    }
    
    function van() 
    {
        $data = $this->model->get_array_all(array('filter'=>"lower(VANCODE) like '%".strtolower($_GET['q'])."%' or lower(VANNAME) like '%".strtolower($_GET['q'])."%'"));
        
        $return = array();
        foreach ( $data as $row ) 
        {
            $return[] = array('key' => $row['VANCODE'], 'label' => $row['VANNAME']);    
        }
        
        echo json_encode($return);   
    }
    
    function warehouse()
    {
        $this->load->model('model_warehouse','warehouse');
        $data = $this->warehouse->get_array_all(array('filter'=>"lower(WRCODE) like '%".strtolower($_GET['q'])."%' or lower(WRNAME) like '%".strtolower($_GET['q'])."%'"));
        
        $return = array();
        foreach ( $data as $row ) 
        {
            $return[] = array('key' => $row['WRCODE'], 'label' => $row['WRNAME']);    
        }
        
        echo json_encode($return);    
    }
    
    function product($type)
    {
        $this->load->model('model_produk','produk');
        $data = $this->produk->get_array_all(
            array('filter'=>"PGROUP = '$type' and (lower(PCODE) like '%".strtolower($_GET['q'])."%' 
            or lower(PNAME) like '%".strtolower($_GET['q'])."%')"));
        
        $return = array();
        foreach ( $data as $row ) 
        {
            $return[] = array('key' => $row['PCODE'], 'label' => $row['PNAME'], 'pprice1' => $row['PPRICE1'], 'sprice1' => $row['SPRICE1']);    
        }
        
        echo json_encode($return);    
    }
    
    function supplier() 
    {
        $this->load->model('model_supplier','supplier');
        $data = $this->supplier->get_array_all(array('filter'=>"lower(SUPPNO) like '%".strtolower($_GET['q'])."%' or lower(SUPPNAME) like '%".strtolower($_GET['q'])."%'"));
        
        $return = array();
        foreach ( $data as $row ) 
        {
            $return[] = array('key' => $row['SUPPNO'], 'label' => $row['SUPPNAME'], 'address' => $row['SUPPADD'], 'phone' => $row['SUPPPHONE']);    
        }
        
        echo json_encode($return);    
    }
    
    function sales($type = '') 
    {
        $this->load->model('model_sales','sales');
        $data = $this->sales->get_array_all(array('filter'=>"lower(SLSNO) like '%".strtolower($_GET['q'])."%' or lower(SLSNAME) like '%".strtolower($_GET['q'])."%'"));
        
        $return = array();
        foreach ( $data as $row ) 
        {
            if ( empty($type) or $type == $row['STCODE'] )
            $return[] = array('key' => $row['SLSNO'], 'label' => $row['SLSNAME']);    
        }
        
        echo json_encode($return);    
    }
    
    function outlet() 
    {
        $this->load->model('model_outlet','outlet');
        $data = $this->outlet->get_array_all(array('filter'=>"lower(OUTNO) like '%".strtolower($_GET['q'])."%' or lower(OUTNAME) like '%".strtolower($_GET['q'])."%'"));
        
        $return = array();
        foreach ( $data as $row ) 
        {
            $return[] = array('key' => $row['OUTNO'], 'label' => $row['OUTNAME'], 'address' => $row['OUTADD'], 'phone' => $row['OPHONE']);    
        }
        
        echo json_encode($return);    
    }
    
    function rsvdoc()
    {
        $this->load->model('model_pembelian','pembelian');
        $data = $this->pembelian->get_all(
            array('filter'=>"DOCNO like '%".strtoupper($_GET['q'])."%'"));
        
        $return = array();
        foreach ( $data['data'] as $row ) 
        {
            $return[$row['DOCNO']] = array('key' => $row['DOCNO'], 'label' => $row['DOCNO']);    
        }
        
        echo json_encode($return);    
    }
    
    function outlet_promo() 
    {
        $this->load->model('model_outlet_promo','outlet_promo');
        
        $data = $this->outlet_promo->get_outlet_promo($_POST['q']);
        
        $html = "<table width='100%'>
            <tr>
                <td width='40%'><b>Nama</b></td>
                <td width='60%'>$data[TITLE]</td>
            </tr>
            <tr>
                <td><b>Deskripsi</b></td>
                <td>$data[DESCRIPTION]</td>
            </tr>
            <tr>
                <td><b>Nominal</b></td>
                <td>$data[TOTALDISC] %</td>
            </tr>
            <tr>
                <td colspan='2' align='center'><a class='button green' id='usediscount' href='".base_url().'order/usediscount/'.$data['IDPROMO']."' onclick='usediscount(this);return false'>Gunakan Diskon</a></td>
            </tr>
        </table>";
        
        echo json_encode(array('count' => count($data), 'html' => $html));
    }
    
    function order() 
    {
        $this->load->model('model_pembelian','pembelian');
        $data = $this->pembelian->get_array_all(array('filter'=>"DOCNO like '%".strtoupper($_GET['q'])."%'"));

        $return = array();
        foreach ( $data as $row ) 
        {
            $return[] = array('key' => $row['DOCNO'], 'label' => $row['DOCNO']);    
        }
        
        echo json_encode($return);    
    }
    
    function load_warehouse() {
        $this->load->model('model_warehouse', 'warehouse');
        if(isset($_POST['page'])){
            $param = array(
                'page'   => @$_POST['page'],
                'limit'  => 10,
                'q'      => @$_POST['q'],
                'order'  => "WRCODE ASC" 
            );
        }
        else
        {
            $param = array(
                'page'   => 1,
                'limit'  => 10,
                'q'      => '',
                'order'  => "WRCODE ASC"                
            );        
        }
        
        $this->data['page_title'] = "Warehouse";
        $this->data['q'] = @$param['q'];
        $this->data['page'] = @$param['page'];
        $this->data['limit'] = @$param['limit'];
        $this->data['rows'] = $this->warehouse->get_all($param);
        parent::view('admin/warehouse/load_warehouse');
    }
    
    function load_supplier() {
        $this->load->model('model_supplier', 'supplier');
        if(isset($_POST['page'])){
            $param = array(
                'page'   => @$_POST['page'],
                'limit'  => 10,
                'q'      => @$_POST['q'],
                'order'  => "SUPPNO ASC" 
            );
        }
        else
        {
            $param = array(
                'page'   => 1,
                'limit'  => 10,
                'q'      => '',
                'order'  => "SUPPNO ASC"                
            );        
        }
        
        $this->data['page_title'] = "Supplier";
        $this->data['q'] = @$param['q'];
        $this->data['page'] = @$param['page'];
        $this->data['limit'] = @$param['limit'];
        $this->data['rows'] = $this->supplier->get_all($param);
        parent::view('admin/supplier/load_supplier');
    }
    
    function load_sales($type = '') {
        $this->load->model('model_sales', 'sales');
        if(isset($_POST['page'])){
            $param = array(
                'page'   => @$_POST['page'],
                'limit'  => 10,
                'q'      => @$_POST['q'],
                'filter' => !empty($type) ? "STCODE = '$type'" : "",
                'order'  => "SLSNO ASC" 
            );
        }
        else
        {
            $param = array(
                'page'   => 1,
                'limit'  => 10,
                'q'      => '',
                'filter' => !empty($type) ? "STCODE = '$type'" : "",
                'order'  => "SLSNO ASC"                
            );        
        }
        
        $this->data['page_title'] = "Sales";
        $this->data['q'] = @$param['q'];
        $this->data['page'] = @$param['page'];
        $this->data['limit'] = @$param['limit'];
        $this->data['rows'] = $this->sales->get_all($param);
        parent::view('admin/sales/load_sales');
    }
    
    function load_product() {
        $this->load->model('model_produk', 'product');
        if(isset($_POST['page'])){
            $param = array(
                'page'   => @$_POST['page'],
                'limit'  => 10,
                'q'      => @$_POST['q'],
                'filter' => "",
                'order'  => "PCODE ASC" 
            );
        }
        else
        {
            $param = array(
                'page'   => 1,
                'limit'  => 10,
                'q'      => '',
                'filter' => "",
                'order'  => "PCODE ASC"                
            );        
        }
        
        $this->data['page_title'] = "Product";
        $this->data['q'] = @$param['q'];
        $this->data['page'] = @$param['page'];
        $this->data['limit'] = @$param['limit'];
        $this->data['rows'] = $this->product->get_all($param);

        parent::view('admin/produk/load_product');
    }
    
    function historyprice($pcode) {
        $this->load->model('model_price', 'price');
        $param = array(
            'page'   => 1,
            'limit'  => 10,
            'q'      => '',
            'filter' => "PCODE = '$pcode'",
            'order'  => "PCODE ASC"                
        );        
        
        $this->data['page_title'] = "Histori Harga";
        $this->data['q'] = @$param['q'];
        $this->data['page'] = @$param['page'];
        $this->data['limit'] = @$param['limit'];
        $this->data['rows'] = $this->price->get_all($param);

        parent::view('admin/produk/load_price');
    }
    
    function load_product_gudang($wrcode = '') {
        $this->load->model('model_stock', 'stock');
        $this->load->model('model_produk', 'product');
        if(isset($_POST['page'])){
            $param = array(
                'page'   => @$_POST['page'],
                'limit'  => 10,
                'q'      => @$_POST['q'],
                'filter' => !empty($wrcode) ? "WRCODE = '$wrcode'" : "",
                'order'  => "PCODE ASC" 
            );
        }
        else
        {
            $param = array(
                'page'   => 1,
                'limit'  => 10,
                'q'      => '',
                'filter' => !empty($wrcode) ? "WRCODE = '$wrcode'" : "",
                'order'  => "PCODE ASC"                
            );        
        }
        
        $this->data['page_title'] = "Product";
        $this->data['q'] = @$param['q'];
        $this->data['page'] = @$param['page'];
        $this->data['limit'] = @$param['limit'];
        $this->data['konversi'] = $this->product->get_konversi();
        $this->data['rows'] = $this->stock->get_all($param);
        
        parent::view('admin/stockgudang/load_product');
    }
    
    function load_product_van($vancode = '') {
        $this->load->model('model_stock_van', 'stockvan');
        $this->load->model('model_produk', 'product');
        if(isset($_POST['page'])){
            $param = array(
                'page'   => @$_POST['page'],
                'limit'  => @$_POST['limit'],
                'q'      => @$_POST['q'],
                'filter' => !empty($vancode) ? "VANCODE = '$vancode'" : "",
                'order'  => "PCODE ASC" 
            );
        }
        else
        {
            $param = array(
                'page'   => 1,
                'limit'  => 10,
                'q'      => '',
                'filter' => !empty($vancode) ? "VANCODE = '$vancode'" : "",
                'order'  => "PCODE ASC"                
            );        
        }
        
        $this->data['page_title'] = "Product";
        $this->data['q'] = @$param['q'];
        $this->data['page'] = @$param['page'];
        $this->data['limit'] = @$param['limit'];
        $this->data['konversi'] = $this->product->get_konversi();
        $this->data['rows'] = $this->stockvan->get_all($param);
        parent::view('admin/stockvan/load_product');
    }

    function load_van() {
        $this->load->model('model_van', 'van');
        if(isset($_POST['page'])){
            $param = array(
                'page'   => @$_POST['page'],
                'limit'  => 10,
                'q'      => @$_POST['q'],
                'order'  => "VANCODE ASC" 
            );
        }
        else
        {
            $param = array(
                'page'   => 1,
                'limit'  => 10,
                'q'      => '',
                'order'  => "VANCODE ASC"                
            );        
        }
        
        $this->data['page_title'] = "Van";
        $this->data['q'] = @$param['q'];
        $this->data['page'] = @$param['page'];
        $this->data['limit'] = @$param['limit'];
        $this->data['rows'] = $this->van->get_all($param);
        parent::view('admin/van/load_van');
    }
    
    function load_outlet() {
        $this->load->model('model_outlet', 'outlet');
        if(isset($_POST['page'])){
            $param = array(
                'page'   => @$_POST['page'],
                'limit'  => 10,
                'q'      => @$_POST['q'],
                'order'  => "OUTNO ASC" 
            );
        }
        else
        {
            $param = array(
                'page'   => 1,
                'limit'  => 10,
                'q'      => '',
                'order'  => "OUTNO ASC"                
            );        
        }
        
        $this->data['page_title'] = "Outlet";
        $this->data['q'] = @$param['q'];
        $this->data['page'] = @$param['page'];
        $this->data['limit'] = @$param['limit'];
        $this->data['rows'] = $this->outlet->get_all($param);
        parent::view('admin/outlet/load_outlet');
    }
    
    function load_outlet_type() {
        $this->load->model('model_jnsoutlet', 'jnsoutlet');
        if(isset($_POST['page'])){
            $param = array(
                'page'   => @$_POST['page'],
                'limit'  => 10,
                'q'      => @$_POST['q'],
                'order'  => "ATTCODE ASC" 
            );
        }
        else
        {
            $param = array(
                'page'   => 1,
                'limit'  => 10,
                'q'      => '',
                'order'  => "ATTCODE ASC"                
            );        
        }
        
        $this->data['page_title'] = "Outlet Type";
        $this->data['q'] = @$param['q'];
        $this->data['page'] = @$param['page'];
        $this->data['limit'] = @$param['limit'];
        $this->data['rows'] = $this->jnsoutlet->get_all($param);
        parent::view('admin/jnsoutlet/load_jnsoutlet');
    }
    
    function load_user() {
        $this->load->model('model_user', 'user');
        if(isset($_POST['page'])){
            $param = array(
                'page'   => @$_POST['page'],
                'limit'  => 10,
                'q'      => @$_POST['q'],
                'order'  => "ID ASC" 
            );
        }
        else
        {
            $param = array(
                'page'   => 1,
                'limit'  => 10,
                'q'      => '',
                'order'  => "ID ASC"                
            );        
        }
        
        $this->data['page_title'] = "User";
        $this->data['q'] = @$param['q'];
        $this->data['page'] = @$param['page'];
        $this->data['limit'] = @$param['limit'];
        $this->data['rows'] = $this->user->get_all($param);
        parent::view('admin/user/load_user');
    }  
    
    function login($orderno='', $isinvoice = true) {
        $this->data['orderno'] = $orderno;
        $this->data['isinvoice'] = $isinvoice;
        
        $post = $this->input->post();
        if ( $post['orderno'] ) {
            $password = $this->session->userdata('password');
            
            if( $password == md5($post['password']) ) {
                redirect('order/invoice/'.$post['orderno'].'/'.$post['isinvoice']);
            } else {
                redirect('order/form/'.$post['orderno']);
            }
        }
        
        parent::view('admin/login_auth');
    }
    
    function loadcart() {
        $this->load->model('model_cart');
        
        $rows = $this->model_cart->get_all(
            array(
                'filter' => "SESSIONID = '".$this->session->userdata('id_user')."' 
                            AND TRXCODE = '$_POST[q]'
                            AND PCODE LIKE '%$_POST[p]'",
                'page' => 1,
                'limit' => 100,
            ));

        $i = 0;
        $html = null;
        foreach( $rows['data'] as $row ) {
            $total += $row['TOTAL'];
            
            $html .= "<tr>";
            $html .= "<td>".++$i."</td>";
            $html .= "<td>$row[PCODE]";
            $html .= "<input type='hidden' id='pcode[$row[PCODE]]' name='pcode[$row[PCODE]]' value='$row[PCODE]'>";
            $html .= "</td>";
            $html .= "<td>$row[PNAME]</td>";
            $html .= "<td><input type='text' id='pprice[$row[PCODE]]' name='pprice[$row[PCODE]]' value='$row[PPRICE]'/></td>";
            $html .= "<td><input style='width:30px' type='text' id='pqty[$row[PCODE]]' name='pqty[$row[PCODE]]' value='$row[QTY]'/></td>";
            
            if($_POST['q'] == 'S')
            $html .= "<td><input style='width:30px' type='text' id='pdisc[$row[PCODE]]' name='pdisc[$row[PCODE]]' value='$row[DISC]'/></td>";
            
            $html .= "<td>".fCurrency($row['TOTAL'])."</td>";
            $html .= "<td><a href='".site_url("cart/delete_cart/".$row['SESSIONID'].'_'.$row['PCODE'].'_'.$row['TRXCODE'])."' title='Delete'>
                    <div class='btn-delete'></div>
                    </a></td>";
            $html .= "</tr>";
        }
        
        echo json_encode(array('html' => $html, 'total' => 'Rp. '.fCurrency($total), 'numtotal' => $total));    
    }
    
    function cart() {
        $this->load->model('model_cart');

        if($_POST['mode'] == 'insert') {
            $this->model_cart->add($_POST['q'], $_POST['p'], $_POST['t']);
        }
        elseif($_POST['mode'] == 'update') {
            $this->model_cart->edit($_POST['q'], $_POST['p']);    
        }
    }  
}