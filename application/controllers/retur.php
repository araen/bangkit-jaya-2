<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Retur extends MY_Controller {
    protected $cls_model = "model_laporan";
    protected $controller;
	protected $view = "admin/return/";
    
    function __construct()
    {
        parent::__construct();
        $this->controller = $this->config->item('base_url')."return/";
    }
    
    function index() 
    {
        $this->load->model('model_laporan','laporan');
        $this->load->model('model_warehouse','warehouse');
        
        if($this->session->userdata('login') == TRUE){
            $this->data['page_title'] = "Laporan Retur Barang";
            $this->data['q'] = @$param['q'];
            $this->data['page'] = @$param['page'];
            $this->data['limit'] = @$param['limit'];
            $this->data['warehouse'] = $this->warehouse->get_rows(array('order'=>'WRCODE'),'WRCODE','WRNAME');
            $this->data['content'] = parent::view($this->view.'repp_return',true);
            parent::view("admin/index");    
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    public function rep_return(){
        $this->load->model('model_laporan','laporan');
        $this->load->model('model_produk','product');
        $this->load->model('model_outlet','outlet');
        $this->load->model('model_supplier','supplier');
        
        $post = $_POST['data'];
        
        $param['filter'] = "1 = 1 ";
        if($post['wrcode'])
            $param['filter'] .= "AND WRCODE = '$post[wrcode]' ";
        if(!empty($post['tglmulai']) and !empty($post['tglselesai']))
            $param['filter'] .= "AND (RETDATE >= '".date('Y-m-d',strtotime($post['tglmulai']))."' AND RETDATE <= '".date('Y-m-d',strtotime($post['tglselesai']))."')";
        
        $param['order'] = "RETDATE, PCODE";
        $this->data['rows'] = $this->laporan->return_barang($param);

        $this->data['konversi'] = $this->product->get_konversi();
        
        $outlet = $this->outlet->get_rows(array('order'=>'OUTNO'),'OUTNO','OUTNAME');
        $supplier = $this->supplier->get_rows(array('order'=>'SUPPNO'),'SUPPNO','SUPPNAME');
        
        $this->data['tglmulai'] = $post['tglmulai'];
        $this->data['tglselesai'] = $post['tglselesai'];
        $this->data['thpart'] = $outlet + $supplier;
        
        parent::view($this->view.'rep_return');
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
