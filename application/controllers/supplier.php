<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Supplier extends MY_Controller {
    protected $cls_model = "model_supplier";
    protected $controller;
    protected $view = "admin/supplier/";
    
    function __construct()
    {
        parent::__construct();
        $this->controller = $this->config->item('base_url')."supplier/"; 
    }
    
    public function index()
    {
        if($this->session->userdata('login') == TRUE){
            if(isset($_POST['page'])){
                $param = array(
                    'page'   => $_POST['page'],
                    'limit'  => 10,
                    'order'  => 'SUPPNO ASC',
                    'filter' => "",
                    'q'      => $_POST['q']
                );
            }
            else
            {
                $param = array(
                    'page'   => 1,
                    'limit'  => 10,
                    'order'  => 'SUPPNO ASC',
                    'filter' => "", 
                    'q'      => "",
                );        
            }
            
            $this->data['page_title'] = "Supplier";
            $this->data['q'] = @$param['q'];
            $this->data['page'] = @$param['page'];
            $this->data['limit'] = @$param['limit'];
            $this->data['rows'] = $this->model->get_all($param);
            $this->data['content'] = parent::view($this->view.'grid',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");
        }    
    }
    
    function form($suppno=null)
    {
        $this->load->model('model_supplier','supplier');   
        
        if($this->session->userdata('login') == TRUE)
        {
            $this->data['page_title'] = "Detail Supplier";
            if($suppno)
            {    
                
                $this->data['row'] = $this->model->get_row(array('filter'=>"suppno=$suppno"));
            }
            if(isset($_POST['data']))
            {   
                $data = $_POST['data'];
                if($suppno)
                {
                    $this->model->edit($suppno,$data);                 
                } else {
                    $this->model->add($data);
                }
                redirect($this->controller);
            }                                                                                                 
            $this->data['supplier'] = $this->supplier->get_rows(array('order'=>'SUPPNO'),'SUPPNO','SUPPNAME');
            $this->data['content'] = parent::view($this->view.'form',true);
            parent::view("admin/index");
        }
        else
        {
            parent::view("admin/login");    
        }        
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
