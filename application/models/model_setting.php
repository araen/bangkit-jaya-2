<?php
class Model_setting extends MY_Model {
	var $str_table = "m_setting";
    
    function __construct()
    {
        parent::__construct();
    }
    
    function get_setting() {
        $this->strSQL = "SELECT * FROM ".$this->str_table;
        
        $result = $this->list_array();
        
        $a_data = array();
        foreach ( $result as $row )
            $a_data[$row['SKEY']] = $row['VALUE'];
            
        return $a_data;
    }
}
?>