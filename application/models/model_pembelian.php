<?php
class Model_pembelian extends MY_Model {
	var $str_table = "t_hbeli";
    var $strID = "IDBELI";
	
    function __construct()
    {
        parent::__construct();
    }
    
    #@override
    function get_all($param=array(), $select="*"){       
        $this->strSQL= "SELECT pb.IDBELI,pb.DOCNO,pb.TOTALBELI,pb.TGLBELI,pb.WRCODE,pb.RETLOCK,
                        w.WRNAME GUDANG, pb.POSTSTATUS,sp.SUPPNAME,pb.NOTE
                        FROM {$this->str_table} pb
                        JOIN m_warehouse w
                        ON pb.WRCODE = w.WRCODE
                        LEFT JOIN t_dbeli pd
                        ON pb.idbeli = pd.idbeli
                        LEFT JOIN m_supplier sp
                        ON pb.SUPPNO = sp.SUPPNO
                        GROUP BY pb.IDBELI,pb.TOTALBELI,pb.WRCODE";
        
        foreach ($param as $key=>$value)
        {
            $this->arr_param[$key] = $value;
        }
        
        $arrToReturn['data'] = $this->list_array();
        $arrToReturn['total'] = $this->list_total();
        $this->db->close();
        
        return $arrToReturn;
    }
    
    /*function get_rekap($param=array(), $select="*"){       
        $this->strSQL= "SELECT py.ID,py.NAMA,bk.NAMA BLOK,pd.JNSBAYAR,
                        DATE_FORMAT(tglbayar,'%m') BLNBAYAR,DATE_FORMAT(tglbayar,'%Y') THNBAYAR,SUM(pd.JMLBAYAR) JMLBAYAR
                        FROM {$this->str_table} pb
                        JOIN trx_pembayaran_detail pd
                        ON pb.id = pd.idbatch
                        JOIN penyewa py
                        ON pb.idpenyewa = py.id
                        JOIN blok bk
                        ON pb.idblok = bk.id
                        GROUP BY py.ID,py.NAMA,bk.NAMA,pd.JNSBAYAR,DATE_FORMAT(tglbayar,'%m'),DATE_FORMAT(tglbayar,'%Y')";
        
        foreach ($param as $key=>$value)
        {
            $this->arr_param[$key] = $value;
        }
        
        $arrToReturn = $this->list_array_all();
        $this->db->close();
        
        return $arrToReturn;
    }*/
}
?>