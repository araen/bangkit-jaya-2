<?php
class Model_invoice extends MY_Model {
	var $str_table = "t_invoice";
	var $strID = "INVNO";
    
    function __construct()
    {
        parent::__construct();
    }
    
    function get_row($param, $select="*") {
        $this->strSQL= "SELECT T1.INVNO,T1.INVDATE,T1.SLSNO,T1.OUTNO,T1.ORDERDATE, T1.TAGIHAN, COALESCE(SUM(PY.AMOUNT),0) PAYMENT FROM
        (SELECT IV.INVNO,IV.INVDATE,HO.SLSNO,HO.OUTNO,HO.ORDERDATE, SUM(SUBTOTAL) TAGIHAN
        FROM {$this->str_table} IV
        JOIN t_horder HO
        ON IV.INVNO = HO.INVNO
        GROUP BY IV.INVNO,IV.INVDATE,HO.SLSNO,HO.OUTNO,HO.ORDERDATE) T1
        LEFT JOIN t_payment PY
        ON T1.INVNO = PY.INVNO
        GROUP BY T1.INVNO"; 
        
        return parent::get_row($param);   
    }
    
    function get_order($param) {
        $this->strSQL = "SELECT ho.*, ot.OUTNAME, ot.OUTADD, iv.INVDATE
                        FROM t_horder ho
                        JOIN m_outlet ot
                        ON ho.outno = ot.outno
                        LEFT JOIN t_invoice iv
                        ON ho.INVNO = iv.INVNO ";
        
        foreach ($param as $key=>$value)
        {
            $this->arr_param[$key] = $value;
        }
                
        return $this->get_array_all($param);
    }
    
    function get_order_detail($param) {
        $this->strSQL = "SELECT do.*, pr.PNAME
                        FROM t_dorder do
                        JOIN m_product pr
                        ON do.PCODE = pr.PCODE ";
        
        foreach ($param as $key=>$value)
        {
            $this->arr_param[$key] = $value;
        }
                
        return $this->get_array_all();    
    }
}
?>