<?php
class Model_outlet_promo extends MY_Model {
	var $str_table = "t_outlet_promo";
	var $strID = "ATTCODE";
    
    function __construct()
    {
        parent::__construct();
    }
    
    function get_outlet_promo($outno) {
        $this->strSQL = "SELECT pr.* 
                        FROM t_outlet_promo op
                        JOIN m_outlet ot
                        ON op.attcode = ot.attcode
                        JOIN m_promo pr
                        ON op.idpromo = pr.idpromo
                        WHERE ot.outno = '$outno'
                        and (pr.startdate <= '".date('Y-m-d')."' AND pr.duedate >= '".date('Y-m-d')."')";
                
        return parent::get_row();  
    }
}
?>