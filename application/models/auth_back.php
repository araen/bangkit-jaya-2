<?php
class Auth extends CI_Model {
	
    function Auth()
    {
        parent::__construct();
    }
	
	function get_menu_admin(){
        $this->load->database();
		$id = $this->session->userdata('id_user');
        if($id)
        {
            $this->db->select('*');
            $this->db->from('sys_user A');
            $this->db->join('sys_group B', 'A.ID_GROUP = B.ID','LEFT');
            $this->db->where('A.ID',$id);
            $query = $this->db->get();    
        }					

		$id_menu = array();
		foreach($query->result() as $row)
		{
			$id_menu = json_decode($row->ID_ACCESS,TRUE);
		}
        $in = "(".implode(',',$id_menu).")";
		$sql = "SELECT * FROM 
                sys_menu_admin 
                WHERE ID IN $in AND PARENT='0' 
                ORDER BY ORDERED ASC";
		$query = $this->db->query($sql);
		$menu = array();
		foreach($query->result() as $row)
		{
			$menu[$row->ID]['NAME'] = $row->NAME;
			$menu[$row->ID]['URL'] = $row->LINK;
			$menu[$row->ID]['SUB'] = $this->_get_child($row->ID,$in);
		}
		$this->db->close();
		return $menu;
	}
	
	function _get_child($id,$in){
		$this->load->database();					
		$sql = "SELECT * FROM 
                sys_menu_admin 
                WHERE PARENT=$id 
                AND ID IN $in 
                ORDER BY ORDERED ASC";
		$query = $this->db->query($sql);		
		$menu = array();
		foreach($query->result() as $row)
		{
			$menu[$row->ID]['NAME'] = $row->NAME;
			$menu[$row->ID]['URL'] = $row->LINK;
			$menu[$row->ID]['SUB'] = $this->_get_child($row->ID,$in);
		}
		$this->db->close();
		return $menu;
	}
	
	function get_access(){
		$id = $this->session->userdata('id_user');
		$this->load->database();					
		if($id)
        {
            $this->db->select('*');
            $this->db->from('user A');
            $this->db->join('`group` B', 'A.ID_GROUP = B.ID','LEFT');
            $this->db->where('A.ID',$id);
            $query = $this->db->get();    
        }
		
		$id_menu = array();
		foreach($query->result() as $row)
		{
			$id_menu = json_decode($row->ID_ACCESS,TRUE);
		}
		$id_menu = array();
        foreach($query->result() as $row)
        {
            $id_menu = json_decode($row->ID_ACCESS,TRUE);
        }
        $in = "(".implode(',',$id_menu).")";
		$sql = "SELECT * FROM 
                menu_admin 
                WHERE ID IN $in";
		$query = $this->db->query($sql);
		$access = array();
		foreach($query->result() as $row)
		{
			$access[$row->ID] = $row->ID_ACCESS;
			
		}
		//var_dump($access);
		$this->db->close();
		return $access;
	}
	
	function get_menu($menutype=''){
		$this->load->database();
        $lang = '';
        //if($this->session->userdata($this->config->item('sess_prefix').$_SERVER['REMOTE_ADDR']))
        if($this->session->userdata('lang'))
        {
            //$lang = $this->session->userdata($this->config->item('sess_prefix').$_SERVER['REMOTE_ADDR']);
            $lang = $this->session->userdata('lang');
        }
        else
        {
            $lang = $this->model_language->get_all(array('filter'=>"IS_DEFAULT='1'"))->row()->NAME;    
        }					
		$sql = "SELECT A.* 
                FROM menu A
                LEFT JOIN lang B
                ON A.LANG_ID = B.ID
                WHERE A.PARENT=0 
                AND A.PUBLISHED=1 
                AND A.MENUTYPE='$menutype'
                AND B.NAME = '$lang' 
                ORDER BY A.ORDERED ASC";
		$query = $this->db->query($sql);
		
        $menu = array();
		foreach($query->result() as $row)
		{
			$menu[$row->ID]['NAME'] = $row->NAME;
            $menu[$row->ID]['LINK'] = $row->LINK;
			$menu[$row->ID]['DESCRIPTION'] = $row->DESCRIPTION;
			$menu[$row->ID]['SUB'] = $this->_menu_child($row->ID);
		}
		$this->db->close();
		return $menu;
	}
	
	function _menu_child($id){
		$this->load->database();					
		$sql = "SELECT * 
                FROM menu 
                WHERE PARENT=$id 
                AND PUBLISHED=1 
                ORDER BY ORDERED ASC";
		$query = $this->db->query($sql);		
		$menu = array();
		foreach($query->result() as $row)
		{
			$menu[$row->ID]['NAME'] = $row->NAME;
            $menu[$row->ID]['LINK'] = $row->LINK;
            $menu[$row->ID]['DESCRIPTION'] = $row->DESCRIPTION;
            $menu[$row->ID]['SUB'] = $this->_menu_child($row->ID);
		}
		$this->db->close();
		return $menu;
	}
    
    function add_captcha($data){
        $this->db->trans_start();
        foreach($data as $key=>$value){
            if(is_string($value)){
                $value = "'".$value."'";
            }
            $field[] = $key;
            $rec[] = $value;
        }
        $sql = "INSERT INTO CAPTCHA(".strtoupper(implode(',',$field)).")VALUES(".implode(",",$rec).")";
        $this->db->query($sql);
        $this->db->trans_complete();
        $return = $this->db->trans_status();
        $this->db->close();
        return $return;
    }
    
    function delete_captcha(){
        $expiration_time = time()-90;
        $this->db->trans_start();
        $sql = "DELETE FROM CAPTCHA WHERE CAPTCHA_TIME < $expiration_time";
        $this->db->query($sql);
        $this->db->trans_complete();
        $return = $this->db->trans_status();
        $this->db->close();
    }
    
    function validate_captcha($word='',$ip=''){
        $expiration_time = time()-90;
        $sql = "SELECT COUNT(*) AS COUNT FROM CAPTCHA
                WHERE WORD='$word' AND IP_ADDRESS='$ip' AND CAPTCHA_TIME > $expiration_time";
        
        $result = $this->db->query($sql);        
        foreach($result->result_array as $row){
            if($row['COUNT'] > 0 ){
                return true;
            }else{
                return false;
            }
        }
        
        $this->db->close();
    }
}
?>
