<?php
class Model_produk extends MY_Model {
	var $str_table = "m_product";
	var $strID = "PCODE";
    
    function __construct()
    {
        parent::__construct();
    }
	
	function get_konversi() {
        $result = $this->get_array_all();
        
        $a_data = array();
        foreach ( $result as $row ) {
            $a_data[$row['PCODE']]['CONV1'] = $row['CONV1']; 
            $a_data[$row['PCODE']]['CONV2'] = $row['CONV2']; 
            $a_data[$row['PCODE']]['CONV3'] = $row['CONV3']; 
        }
        
        return $a_data;
    }
    
    function add_data_excel($data, $ndx) {
        for ( $i = $ndx; $i <= count($data); $i++ )
        {
            $record['PCODE'] = $data[$i]['A'];
            $record['PBCODE'] = $data[$i]['B'];
            $record['PNAME'] = strtoupper($data[$i]['C']);
            $record['PWEIGHT'] = $data[$i]['D'];
            $record['PPRICE1'] = $data[$i]['E'];
            $record['PPRICE2'] = $data[$i]['F'];
            $record['PPRICE3'] = $data[$i]['G'];
            $record['CONV1'] = $data[$i]['H'];
            $record['CONV2'] = $data[$i]['I'];
            $record['CONV3'] = $data[$i]['J'];
            $record['PLAUNCH'] = date('Y-m-d');
            
            $this->add($record);
        }
        
        return true;    
    }
    
    function get_combo_product() {
        $result = $this->get_array_all();
        
        $a_product = array();
        foreach( $result as $row ) {
            $a_product[$row['PCODE']]['PPRICE1'] = $row['PPRICE1'];    
            $a_product[$row['PCODE']]['SPRICE1'] = $row['SPRICE1'];    
        }
        
        return $a_product;    
    }
}
?>