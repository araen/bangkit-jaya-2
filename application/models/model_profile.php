<?php
class Model_profile extends MY_Model {
	var $str_table = "m_profile";
    function Model_profile()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function get_setting(){
		$query = $this->get_array_all();
        foreach($query as $row){
            $data[$row['PROFCD']] = $row['VALUE'];
        }
        return $data;
	}
    
    function get_value($profcd){
        $this->load->database();

        $sql = "SELECT * FROM {$this->str_table} WHERE PROFCD LIKE '%$profcd%'";
        $query = $this->db->query($sql);
        foreach($query->result_array() as $row){
            $data['value'] = $row['VALUE'];
        }
        $this->db->close();
        
        return $data;
    }
    
	 function edit($param = array(),$data){
        $this->db->trans_start();
        foreach($param as $key=>$value)
            $this->db->where($key,$value);
        
        $this->db->update($this->str_table,$data);
        $this->db->trans_complete();
        $return = $this->db->trans_status();
        $this->db->close();
    }
}
?>