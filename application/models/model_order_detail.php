<?php
class Model_order_detail extends MY_Model {
	var $str_table = "t_dorder";
	var $strID = "DORDERNO";
    
    function __construct()
    {
        parent::__construct();
    }
    
    function get_array_all($param=array(), $select="DB.*,PR.PNAME"){       
        $this->strSQL= "SELECT $select 
                        FROM {$this->str_table} DB
                        JOIN m_product PR
                        ON DB.PCODE = PR.PCODE";
        
        foreach ($param as $key=>$value)
        {
            $this->arr_param[$key] = $value;
        }
        
        $arrToReturn = $this->list_array_all();;
        $this->db->close();
        
        return $arrToReturn;
    }
    
    function get_rekap($param = array()) {
        $this->strSQL = "SELECT DO.PCODE,PR.PNAME, SL.SLSNO, SL.SLSNAME,SUM(DO.QTY) QTY
                        FROM t_horder HO
                        JOIN t_dorder DO
                        ON HO.ORDERNO = DO.ORDERNO
                        JOIN m_product PR
                        ON DO.PCODE = PR.PCODE
                        LEFT JOIN m_sales SL
                        ON HO.SLSNO = SL.SLSNO
                        JOIN t_invoice IV
                        ON HO.INVNO = IV.INVNO
                        WHERE $param[filter]
                        GROUP BY DO.PCODE,PR.PNAME, SL.SLSNO, SL.SLSNAME";
        
        $arrToReturn = $this->list_array_all();;
        $this->db->close();
        
        return $arrToReturn;
    }
}
?>