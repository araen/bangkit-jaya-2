<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_access extends MY_Model {
    protected $str_table    = "m_access";
    protected $strID        = 'IDROLE';
    
    function __construct()
    {
        parent::__construct();
    }
    
    function get_access($param) {
        $arr_prm = array(
            'page'  => 1,
            'limit' => 1,
            'filter'=> @$param['filter'],
        );
        $this->strSQL = "SELECT * FROM m_access";
        
        $result = parent::get_array_all($param);
        
        $a_access = array();
        foreach ( $result as $row ) {
            $a_access[$row['CMNCODE']]['ISREAD'] = $row['ISREAD'];
            $a_access[$row['CMNCODE']]['ISCREATE'] = $row['ISCREATE'];
            $a_access[$row['CMNCODE']]['ISUPDATE'] = $row['ISUPDATE'];
            $a_access[$row['CMNCODE']]['ISDELETE'] = $row['ISDELETE'];
        }
        
        return $a_access;
    }
}