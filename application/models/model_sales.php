<?php
class Model_sales extends MY_Model {
	var $str_table = "m_sales";
	var $strID = "SLSNO";
    
    function __construct()
    {
        parent::__construct();
    }
    
    function get_sales_combo($first = true) 
    {
        $data = $this->get_array_all();
        
        $arr_result = array();
        
        if($first)
            $arr_result[''] = '-- Pilih Sales --';
        
        foreach ( $data as $row )
        {
            $arr_result[$row['SLSNO']] = $row['SLSNAME'].' - '.$row['STCODE'];    
        }
        return $arr_result;
    }
	
	/*function blok($conn) {
		$sql = "select id, nama from blok order by id";
		return Query::arrQuery($conn,$sql);
	}*/
}
?>