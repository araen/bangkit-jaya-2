<?php
class Model_cart extends MY_Model {
	var $str_table = "t_cart";
	var $strID = "IDCHART";
    
    function __construct()
    {
        parent::__construct();
    }
    
    function add($data, $type = 'R', $qty = 1, $disc = 0) {
        $session = $this->session->all_userdata();

        $disc = $session['cust_disc'];
        
        $row = $this->get_row(array('filter' => "SESSIONID = '".$session['id_user']."' and PCODE = '".$data['key']."'"));
        
        if( empty($row) ) {
            $record['SESSIONID'] = $session['id_user'];
            $record['TRXCODE'] = $type;
            $record['PCODE'] = $data['key'];
            $record['PNAME'] = $data['label'];
            $record['QTY'] = $qty;
            $record['DISC'] = $disc;
            
            if($type == 'R') {
                $record['PPRICE'] = $data['pprice1'];
                $record['TOTAL'] = ($qty * $data['pprice1']) - ($qty * ($disc/100 * $data['pprice1']));
            } else {
                $record['PPRICE'] = $data['sprice1'];
                $record['TOTAL'] = ($qty * $data['sprice1']) - ($qty * ($disc/100 * $data['sprice1']));
            }
            
            $status = parent::add($record);
        }
        else {
            $record['QTY'] = $row['QTY'] + $qty;
            $record['TOTAL'] = ($record['QTY'] * $row['PPRICE']) - ($record['QTY'] * ($disc/100 * $row['PPRICE']));
            
            parent::edit(array("SESSIONID"=> $session['id_user'], "PCODE" => $data['key'], "TRXCODE" => "$type"), $record);    
        }
    }
    
    function edit($data, $type = 'R') {
        $session = $this->session->all_userdata();
        
        $record['PPRICE'] = $data[1];
        $record['QTY'] = $data[2];
        $record['DISC'] = $data[3];
        $record['TOTAL'] = ($record['QTY'] * $record['PPRICE']) - ($record['DISC'] / 100 * $record['PPRICE'] * $record['QTY']);
        
        parent::edit(array("SESSIONID"=> $session['id_user'], "PCODE" => $data[0], "TRXCODE" => "$type"), $record);    
    }
    
    function editcart($param, $data) {
        parent::edit($param, $data);
    }
}
?>