<?php
class Model_outlet extends MY_Model {
	var $str_table = "m_outlet";
	var $strID = "OUTNO";
    
    function __construct()
    {
        parent::__construct();
    }
    
    function add_data_excel($data, $ndx) {
        for ( $i = $ndx; $i <= count($data); $i++ )
        {
            $record['OUTNO'] = $data[$i]['A'];
            $record['ATTCODE'] = $data[$i]['B'];
            $record['OUTNAME'] = strtoupper($data[$i]['C']);
            $record['OUTADD'] = $data[$i]['D'];
            $record['OCITY'] = $data[$i]['E'];
            $record['OPHONE'] = $data[$i]['F'];
            $record['OCONTACT'] = $data[$i]['G'];
            $record['FLIMIT'] = $data[$i]['H'];
            $record['REGDATE'] = date('Y-m-d');
            $record['FIRSTOPEN'] = date('Y-m-d');
            
            $status = $this->add($record);
        }
        
        return true;    
    }
	
	/*function blok($conn) {
		$sql = "select id, nama from blok order by id";
		return Query::arrQuery($conn,$sql);
	}*/
}
?>