<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_user extends MY_Model {
    protected $str_table    = "m_user";
    protected $strID        = "ID";
    
    function __construct()
    {
        parent::__construct();
    }
	
    public function login($username,$password)
    {
        $user = $this->get_all(array(
                        "filter" => "USNAME = '$username' AND USPASSWORD = '$password'",
                        "limit"=>1));        
        if($user['total'] > 0)
        {
            foreach($user['data'] as $row)
            {
                $data = array(
                            'login'     => TRUE,
                            'id_user'   => $row['ID'],
                            /*'name'      => $row['NAME'],*/
                            'username'  => $row['USNAME'],
                            'password'  => $password,
                        );
            }
            $this->session->set_userdata($data);
            return true;
        }
        else
        {
            return false;
        }
    }	
}