<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_user extends MY_Model {
    
    function __construct()
    {
        parent::__construct();
        
        $this->str_table = "sys_user";
    }
	
    public function login($username,$password)
    {
        $user = $this->get_all(array(
                        "filter" => "USERNAME = '$username' AND PASSWORD = '$password'",
                        "limit"=>1));        
        if($user['total'] > 0)
        {
            foreach($user['data'] as $row)
            {
                $data = array(
                            'login'     => TRUE,
                            'id_user'   => $row['ID'],
                            'name'      => $row['NAME'],
                            'username'  => $row['USERNAME'],
                            'group'     => $row['ID_GROUP'],
                        );
            }
            $this->session->set_userdata($data);
            return true;
        }
        else
        {
            return false;
        }
    }	
}