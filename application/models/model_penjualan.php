<?php
class Model_penjualan extends MY_Model {
	var $str_table = "t_horder";
    var $strID = "ORDERNO";
	
    function __construct()
    {
        parent::__construct();
    }
    
    #@override
    function get_all($param=array(), $select="*"){       
        $this->strSQL= "SELECT o.ORDERNO,o.SUBTOTAL,o.ORDERDATE,o.WRCODE,o.RETLOCK,w.WRNAME GUDANG,o.OSTATUSCODE,o.INVNO,
						o.SLSNO, s.SLSNAME SALES, o.OUTNO, ot.OUTNAME OUTLET, ot.OUTADD, o.PRINTINV,v.VANNAME
                        FROM {$this->str_table} o
                        LEFT JOIN m_warehouse w
                        ON o.WRCODE = w.WRCODE
                        LEFT JOIN m_van v
                        ON o.VANCODE = v.VANCODE
						LEFT JOIN m_sales s
                        ON o.SLSNO = s.SLSNO
						LEFT JOIN m_outlet ot
                        ON o.OUTNO = ot.OUTNO
                        GROUP BY o.ORDERNO,o.SUBTOTAL,o.WRCODE";
        
        foreach ($param as $key=>$value)
        {
            $this->arr_param[$key] = $value;
        }
        
        $arrToReturn['data'] = $this->list_array();
        $arrToReturn['total'] = $this->list_total();
        $this->db->close();
        
        return $arrToReturn;
    }
    
    function get_row($param=array(), $select="*") {
        $this->strSQL = "SELECT HO.*, SL.STCODE
                        FROM {$this->str_table} HO
                        LEFT JOIN m_sales SL
                        ON HO.SLSNO = SL.SLSNO";
        
        foreach ($param as $key=>$value)
        {
            $this->arr_param[$key] = $value;
        }
                        
        return parent::get_row($param);   
    }
    
    function get_order($param) {
        $this->strSQL = "SELECT ho.*, ot.OUTNAME, ot.OUTADD, iv.INVDATE
                        FROM t_horder ho
                        JOIN m_outlet ot
                        ON ho.outno = ot.outno
                        LEFT JOIN t_invoice iv
                        ON ho.INVNO = iv.INVNO ";
        
        foreach ($param as $key=>$value)
        {
            $this->arr_param[$key] = $value;
        }
                
        return parent::get_row($param);
    }
    
    function get_order_detail($param) {
        $this->strSQL = "SELECT do.*, pr.PNAME
                        FROM t_dorder do
                        JOIN m_product pr
                        ON do.PCODE = pr.PCODE ";
        
        foreach ($param as $key=>$value)
        {
            $this->arr_param[$key] = $value;
        }
                
        return parent::get_array_all();    
    }
}
?>