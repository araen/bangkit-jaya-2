<?php
class Model_pembelian_detail extends MY_Model {
	var $str_table = "t_dbeli";
	var $strID = "IDDBELI";
    
    function __construct()
    {
        parent::__construct();
    }
    
    function get_array_all($param=array(), $select="DB.*,PR.PNAME"){       
        $this->strSQL= "SELECT $select 
                        FROM {$this->str_table} DB
                        JOIN m_product PR
                        ON DB.PCODE = PR.PCODE";
        
        foreach ($param as $key=>$value)
        {
            $this->arr_param[$key] = $value;
        }
        
        $arrToReturn = $this->list_array_all();
        $this->db->close();
        
        return $arrToReturn;
    }
}
?>