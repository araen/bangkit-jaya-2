<?php
class Model_trx_van extends MY_Model {
	var $str_table = "t_hvan";
	var $strID = "IDTVAN";
    
    function __construct()
    {
        parent::__construct();
    }
    
    function get_all($param=array(), $select="*"){       
        $this->strSQL= "SELECT TV.*, VN.VANNAME, WR.WRNAME 
                        FROM {$this->str_table} TV
                        JOIN m_van VN ON TV.VANCODE = VN.VANCODE
                        LEFT JOIN m_warehouse WR ON TV.WRCODE = WR.WRCODE";
        
        foreach ($param as $key=>$value)
        {
            $this->arr_param[$key] = $value;
        }
        
        $arrToReturn['data'] = $this->list_array();
        $arrToReturn['total'] = $this->list_total();
        $this->db->close();
        
        return $arrToReturn;
    }
    
    function get_row($param=array(),$select = "*") {
        $this->strSQL= "SELECT TV.*, VN.VANNAME, WR.WRNAME 
                        FROM {$this->str_table} TV
                        JOIN m_van VN ON TV.VANCODE = VN.VANCODE
                        LEFT JOIN m_warehouse WR ON TV.WRCODE = WR.WRCODE";
        
        return parent::get_row($param, $select);    
    }
}
?>