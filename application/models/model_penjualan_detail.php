<?php
class Model_penjualan_detail extends MY_Model {
	var $str_table = "t_dorder";
	var $strID = "ORDERNO";
    
    function __construct()
    {
        parent::__construct();
    }
    
    function get_array_all($param=array(), $salestype = 'TO'){       
        if ( $salestype == 'TO' ) {
            $this->strSQL= "SELECT DB.*,PR.PNAME, ST.STOCK 
                            FROM {$this->str_table} DB
                            JOIN t_horder HO
                            ON DB.ORDERNO = HO.ORDERNO
                            JOIN m_product PR
                            ON DB.PCODE = PR.PCODE
                            LEFT JOIN (SELECT PCODE, WRCODE, SUM(qtystock) STOCK
                            FROM t_stock st
                            GROUP BY PCODE, WRCODE) ST
                            ON PR.PCODE = ST.PCODE
                            AND HO.WRCODE = ST.WRCODE";
        } else {
            $this->strSQL= "SELECT DB.*,PR.PNAME, ST.STOCK 
                            FROM {$this->str_table} DB
                            JOIN t_horder HO
                            ON DB.ORDERNO = HO.ORDERNO
                            LEFT JOIN m_sales SL
                            ON HO.SLSNO = SL.SLSNO
                            JOIN m_product PR
                            ON DB.PCODE = PR.PCODE
                            LEFT JOIN (SELECT PCODE, VANCODE, SUM(qtystock) STOCK
                            FROM t_van st
                            GROUP BY PCODE, VANCODE) ST
                            ON PR.PCODE = ST.PCODE
                            AND SL.VANCODE = ST.VANCODE";   
        }

        foreach ($param as $key=>$value)
        {
            $this->arr_param[$key] = $value;
        }
        
        $arrToReturn = $this->list_array_all();;
        $this->db->close();
        
        return $arrToReturn;
    }
    
    function getSumDetail($orderno) {
        $this->strSQL = "SELECT SUM(AMOUNT) AMOUNT
        FROM {$this->str_table}
        WHERE ORDERNO = '$orderno'
        GROUP BY ORDERNO";
        
        $result = $this->db->query($this->strSQL)->row_array();

        return $result['AMOUNT'];
    }
}
?>