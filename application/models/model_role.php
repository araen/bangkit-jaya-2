<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_role extends MY_Model {
    protected $str_table    = "m_role";
    protected $strID        = 'IDROLE';
    
    function __construct()
    {
        parent::__construct();
    }
    
    function get_user_role($param) {
        $arr_prm = array(
            'page'  => 1,
            'limit' => 1,
            'filter'=> @$param['filter'],
        );
        
        $this->strSQL = "SELECT US.*,UR.IDROLE 
                        FROM m_userrole UR
                        JOIN m_user US
                        ON UR.IDUSER = US.ID ";
        return parent::get_array_all($param);
    }
    
    function get_access($param) {
        $arr_prm = array(
            'page'  => 1,
            'limit' => 1,
            'filter'=> @$param['filter'],
        );
        $this->strSQL = "SELECT * FROM m_access";
        
        $result = parent::get_array_all($param);
        
        $a_access = array();
        foreach ( $result as $row ) {
            $a_access[$row['CMNCODE']]['ISREAD'] = $row['ISREAD'];
            $a_access[$row['CMNCODE']]['ISCREATE'] = $row['ISCREATE'];
            $a_access[$row['CMNCODE']]['ISUPDATE'] = $row['ISUPDATE'];
            $a_access[$row['CMNCODE']]['ISDELETE'] = $row['ISDELETE'];
        }
        
        return $a_access;
    }
    
    function set_role(){
        $iduser = $this->session->userdata('id_user');

        $arr_prm = array(
            'page'  => 1,
            'limit' => 1,
            'filter'=> "IDUSER = $iduser",
        );
        
        $this->str_table = "m_userrole";
        $row = parent::get_row($arr_prm);
        if ( $row )
        $this->session->set_userdata('id_role', $row['IDROLE']);        
    }
    
    function add_user($data) {
        $this->str_table = "m_userrole";
        
        return parent::add($data);
    }
    
    function delete_detail($param) {
        $this->str_table = "m_userrole";
        
        return parent::delete($param);
    }
}