<?php
class Model_return_supplier extends MY_Model {
	var $str_table = "t_retsupp";
	var $strID = "IDDBELI";
    
    function __construct()
    {
        parent::__construct();
    }
    
    function get_array_all($param=array(), $select="DB.*,PR.PNAME"){       
        $this->strSQL= "SELECT $select 
                        FROM t_dbeli DB 
                        JOIN m_product PR
                        USING (PCODE)";
        
        foreach ($param as $key=>$value)
        {
            $this->arr_param[$key] = $value;
        }
        
        $arrToReturn = $this->list_array_all();
        $this->db->close();
        
        return $arrToReturn;
    }
}
?>