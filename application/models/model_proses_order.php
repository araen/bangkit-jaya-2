<?php
class Model_proses_order extends MY_Model {
	var $str_table = "t_horder";
    var $strID = "ORDERNO";
	
    function __construct()
    {
        parent::__construct();
    }
    
    #@override
    function get_all($param=array(), $select="*"){       
        $this->strSQL= "SELECT o.ORDERNO,o.SUBTOTAL,o.ORDERDATE,o.WRCODE,w.WRNAME GUDANG,o.INVNO,
						o.SLSNO, s.SLSNAME SALES, o.OUTNO, ot.OUTNAME OUTLET, o.OSTATUSCODE, o.PRINTINV
                        FROM {$this->str_table} o
                        LEFT JOIN m_warehouse w
                        ON o.WRCODE = w.WRCODE
						LEFT JOIN m_sales s
                        ON o.SLSNO = s.SLSNO
						LEFT JOIN m_outlet ot
                        ON o.OUTNO = ot.OUTNO
                        GROUP BY o.ORDERNO,o.SUBTOTAL,o.WRCODE";
        
        foreach ($param as $key=>$value)
        {
            $this->arr_param[$key] = $value;
        }
        
        $arrToReturn['data'] = $this->list_array();
        $arrToReturn['total'] = $this->list_total();
        $this->db->close();
        
        return $arrToReturn;
    }
    
}
?>