<?php
class Model_stock_van extends MY_Model {
	var $str_table = "t_van";
	var $strID = "IDSVAN";
    
    function __construct()
    {
        parent::__construct();
    }
    
    function get_all($param=array(), $select="*"){       
        $this->strSQL= "SELECT ST.IDSVAN,ST.VANCODE,ST.PCODE,ST.QTYSTOCK,ST.TOTALBAYAR,
                        PR.PNAME,PR.PPRICE1,PR.PPRICE2,PR.PPRICE3,VN.VANNAME, COALESCE(OV.QTY,0) QTYVAN, COALESCE(DV.QTY,0) QTYVANORDER
                        FROM t_van ST
                        JOIN m_product PR
                        ON ST.PCODE = PR.PCODE
                        JOIN m_van VN
                        ON ST.VANCODE = VN.VANCODE
                        LEFT JOIN
                        (SELECT HV.VANCODE, DV.PCODE, SUM(DV.QTY) QTY, SUM(DV.AMOUNT) AMOUNT
                        FROM t_hvan HV
                        JOIN t_dvan DV
                        ON HV.IDTVAN = DV.IDTVAN
                        WHERE HV.OSTATUSCODE = 2
                        AND HV.DAYIN IS NOT NULL
                        GROUP BY HV.WRCODE, DV.PCODE) OV
                        ON ST.VANCODE = OV.VANCODE
                        AND ST.PCODE = OV.PCODE
                        LEFT JOIN
                        (SELECT HO.VANCODE, DO.PCODE, SUM(DO.QTY) QTY, SUM(DO.AMOUNT) AMOUNT
                        FROM t_horder HO
                        JOIN t_dorder DO
                        ON HO.ORDERNO = DO.ORDERNO
                        WHERE HO.OSTATUSCODE = 2
                        AND HO.VANCODE IS NOT NULL
                        AND HO.WRCODE IS NULL
                        GROUP BY HO.VANCODE, DO.PCODE) DV
                        ON ST.VANCODE = DV.VANCODE
                        AND ST.PCODE = DV.PCODE";
        
        foreach ($param as $key=>$value)
        {
            $this->arr_param[$key] = $value;
        }
        
        $arrToReturn['data'] = $this->list_array();        
        $arrToReturn['total'] = $this->list_total();
        $this->db->close();
        
        return $arrToReturn;
    }
	
    function stock_order($idtvan, $approval = true) {
        $this->strSQL= "SELECT hv.VANCODE, dv.PCODE, dv.QTY, dv.AMOUNT
                        FROM t_dvan dv
                        JOIN t_hvan hv USING (IDTVAN)
                        WHERE hv.IDTVAN = '$idtvan'";
        
        $arrReturn = $this->list_array_all(); 

        foreach ( $arrReturn as $row ) 
        {
            $record['VANCODE'] = $row['VANCODE'];
            $record['PCODE'] = $row['PCODE'];
            $record['QTYSTOCK'] = $row['QTY'];
            $record['TOTALBAYAR'] = $row['AMOUNT'];
            
            $this->strSQL = "SELECT * FROM {$this->str_table}";
            $stock = $this->get_row(array("filter" => "VANCODE = '$row[VANCODE]' AND PCODE = '$row[PCODE]'"));
            
            if ( $stock ) 
            {
                $record['QTYSTOCK'] = $record['QTYSTOCK'] - (( $approval ) ? -1 * $stock['QTYSTOCK'] : $stock['QTYSTOCK']);    
                $record['TOTALBAYAR'] = $record['TOTALBAYAR'] - (( $approval ) ? -1 * $stock['TOTALBAYAR'] : $stock['TOTALBAYAR']);
                
                $this->edit($stock['IDSVAN'], $record);
            }
            else 
                $this->add($record);
        }
    }
    
    function stock_update($vancode, $pcode, $qty, $amount, $approval = true) {
        # cek apakah merupakan penerimaan
        if ($approval)
            $sql = "UPDATE {$this->str_table} SET QTYSTOCK = QTYSTOCK + $qty, TOTALBAYAR = TOTALBAYAR + $amount
                    WHERE VANCODE = '$vancode' AND PCODE = '$pcode'";
        else
            $sql = "UPDATE {$this->str_table} SET QTYSTOCK = QTYSTOCK - $qty, TOTALBAYAR = TOTALBAYAR - $amount
                    WHERE VANCODE = '$vancode' AND PCODE = '$pcode'";
                  
        $status = $this->db->query($sql);
        
        $this->last_query = $sql;
        
        return $status;
    }
}
?>