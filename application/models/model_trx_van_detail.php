<?php
class Model_trx_van_detail extends MY_Model {
	var $str_table = "t_dvan";
	var $strID = "IDDTVAN";
    
    function __construct()
    {
        parent::__construct();
    }
    
    function get_array_all($param=array(), $select="*"){       
        $this->strSQL= "SELECT DV.*, PR.PNAME, ST.STOCK 
                        FROM {$this->str_table} DV
                        JOIN t_hvan HV ON DV.IDTVAN = HV.IDTVAN
                        JOIN m_product PR ON DV.PCODE = PR.PCODE
                        LEFT JOIN (SELECT PCODE, VANCODE, SUM(qtystock) STOCK
                        FROM t_van st
                        GROUP BY PCODE, VANCODE) ST
                        ON PR.PCODE = ST.PCODE
                        AND HV.VANCODE = ST.VANCODE";
        
        return parent::get_array_all($param, $select);
    }
}
?>