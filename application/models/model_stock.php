<?php
class Model_stock extends MY_Model {
    var $str_table = "t_stock";
	var $strID = "STOCKDATE";
    
    function __construct()
    {
        parent::__construct();
    }
    
    function get_all($param=array(), $select="*"){       
        $this->strSQL= "SELECT PR.PCODE, WR.WRCODE, WR.WRNAME, PR.PNAME, ST.STOCKDATE,
                        COALESCE(SUM(QTYSTART),0) QTYSTART,
                        COALESCE(SUM(MV.QTYIN),0) QTYIN,
                        COALESCE(SUM(MV.QTYOUT),0) QTYOUT,
                        COALESCE(COALESCE(SUM(QTYSTART),0) + COALESCE(SUM(MV.QTYIN),0) - COALESCE(SUM(MV.QTYOUT),0), 0) STOCK,
                        COALESCE(SUM(SALDOSTART),0) SALDOSTART,
                        COALESCE(SUM(MV.DEBIT),0) DEBIT,
                        COALESCE(SUM(MV.KREDIT),0) KREDIT,
                        COALESCE(COALESCE(SUM(SALDOSTART),0) + COALESCE(SUM(MV.DEBIT),0) - COALESCE(SUM(MV.KREDIT),0),0) SALDOEND
                        FROM M_PRODUCT PR 
                        JOIN M_WAREHOUSE WR
                        ON PR.PGROUP = WR.WRTYPE
                        LEFT JOIN T_STOCK ST
                        ON PR.PCODE = ST.PCODE
                        LEFT JOIN
                        (SELECT DATE_FORMAT(TRANSDATE, '%Y-%m-%d') TRANSDATE, WRCODE, PCODE, 
                        COALESCE(SUM(CASE WHEN STOCKNUM > 0 THEN STOCKNUM END),0) QTYIN,
                        COALESCE(SUM(CASE WHEN STOCKNUM < 0 THEN ABS(STOCKNUM) END),0) QTYOUT,
                        COALESCE(SUM(CASE WHEN BALANCE > 0 THEN BALANCE END),0) DEBIT,
                        COALESCE(SUM(CASE WHEN BALANCE < 0 THEN ABS(BALANCE) END),0) KREDIT
                        FROM T_STOCKMOVE 
                        GROUP BY DATE_FORMAT(TRANSDATE, '%Y-%m-%d'), WRCODE, PCODE
                        ORDER BY WRCODE, PCODE) MV
                        ON ST.STOCKDATE = MV.TRANSDATE
                        AND ST.WRCODE = MV.WRCODE
                        AND ST.PCODE = MV.PCODE
                        GROUP BY PR.PCODE, WR.WRCODE, WR.WRNAME, PR.PNAME, ST.STOCKDATE
                        ORDER BY PR.PCODE, WR.WRCODE, ST.STOCKDATE";
        
        foreach ($param as $key=>$value)
        {
            $this->arr_param[$key] = $value;
        }
        
        $arrToReturn['data'] = $this->list_array();
        $arrToReturn['total'] = $this->list_total();
        $this->db->close();
        
        return $arrToReturn;
    }
    
    function stock_approval($idbeli) {
        $this->strSQL= "SELECT WRCODE, PCODE, QTYBELI, JMLBELI
                        FROM t_dbeli db
                        JOIN t_hbeli hb USING (IDBELI)
                        WHERE hb.IDBELI = $idbeli";
        
        $arrReturn = $this->list_array_all(); 
        
        foreach ( $arrReturn as $row ) 
        {
            $record['WRCODE'] = $row['WRCODE'];
            $record['PCODE'] = $row['PCODE'];
            $record['QTYSTOCK'] = $row['QTYBELI'];
            $record['TOTALBAYAR'] = $row['JMLBELI'];
            
            $this->strSQL = "SELECT * FROM {$this->str_table}";
            $stock = $this->get_row(array("filter" => "WRCODE = '$row[WRCODE]' AND PCODE = '$row[PCODE]'"));
            
            if ( $stock ) 
            {
                $record['QTYSTOCK'] = $record['QTYSTOCK'] + $stock['QTYSTOCK'];    
                $record['TOTALBAYAR'] = $record['TOTALBAYAR'] + $stock['TOTALBAYAR'];
                
                $this->edit($stock['IDSTOCK'], $record);    
            }
            else 
                $this->add($record);
        }
    }
    
    function stock_order($orderno) {
        $this->strSQL= "SELECT WRCODE, PCODE, QTY, AMOUNT
                        FROM t_dorder do
                        JOIN t_horder ho USING (ORDERNO)
                        WHERE ho.ORDERNO = '$orderno'";
        
        $arrReturn = $this->list_array_all(); 

        foreach ( $arrReturn as $row ) 
        {
            $record['WRCODE'] = $row['WRCODE'];
            $record['PCODE'] = $row['PCODE'];
            $record['QTYSTOCK'] = $row['QTY'];
            $record['TOTALBAYAR'] = $row['AMOUNT'];
            
            $this->strSQL = "SELECT * FROM {$this->str_table}";
            $stock = $this->get_row(array("filter" => "WRCODE = '$row[WRCODE]' AND PCODE = '$row[PCODE]'"));
            
            if ( $stock ) 
            {
                $record['QTYSTOCK'] = $stock['QTYSTOCK'] - $record['QTYSTOCK'];    
                $record['TOTALBAYAR'] = $stock['TOTALBAYAR'] - $record['TOTALBAYAR'];
                
                $this->edit($stock['IDSTOCK'], $record);    
            }
            else 
                $this->add($record);
        }    
    }
    
    function stock_update($wrcode, $pcode, $qty, $amount, $approval = true) {
        if ($approval)
            $sql = "UPDATE {$this->str_table} SET QTYSTOCK = QTYSTOCK + $qty, TOTALBAYAR = TOTALBAYAR + $amount
                    WHERE WRCODE = '$wrcode' AND PCODE = '$pcode'";
        else
            $sql = "UPDATE {$this->str_table} SET QTYSTOCK = QTYSTOCK - $qty, TOTALBAYAR = TOTALBAYAR - $amount
                    WHERE WRCODE = '$wrcode' AND PCODE = '$pcode'";
                    
        $status = $this->db->query($sql);
        
        $this->last_query = $sql;
        
        return $status;
    }
    
    function van_order($idtvan, $approval = true) {
        $this->strSQL= "SELECT hv.WRCODE, dv.PCODE, dv.QTY, dv.AMOUNT
                        FROM t_dvan dv
                        JOIN t_hvan hv USING (IDTVAN)
                        WHERE hv.IDTVAN = '$idtvan'";
        
        $arrReturn = $this->list_array_all(); 

        foreach ( $arrReturn as $row ) 
        {
            $record['WRCODE'] = $row['WRCODE'];
            $record['PCODE'] = $row['PCODE'];
            $record['QTYSTOCK'] = $row['QTY'];
            $record['TOTALBAYAR'] = $row['AMOUNT'];
            
            $this->strSQL = "SELECT * FROM {$this->str_table}";
            $stock = $this->get_row(array("filter" => "WRCODE = '$row[WRCODE]' AND PCODE = '$row[PCODE]'"));
            
            if ( $stock ) 
            {
                $record['QTYSTOCK'] = $stock['QTYSTOCK'] - (( $approval ) ? (-1 * $record['QTYSTOCK']) : $record['QTYSTOCK']);    
                $record['TOTALBAYAR'] = $stock['TOTALBAYAR'] - (( $approval ) ? (-1 * $record['TOTALBAYAR']) : $record['TOTALBAYAR']);
                
                $this->edit($stock['IDSTOCK'], $record);    
            }
            else 
                $this->add($record);
        }    
    }
    
    function getRekapStock($wrcode = '') {
        $sql = "SELECT st.pcode, pr.pname, SUM(qtystock) qty
                FROM t_stock st
                LEFT JOIN m_product pr
                ON st.pcode = pr.pcode ";
        $sql .= !empty($wrcode) ? "where st.wrcode = '$wrcode'" : "";
        $sql .= "GROUP BY st.pcode, pr.pname";
        
        $this->strSQL = $sql;
        
        $result = $this->get_array_all(); 
        
        $a_result = array();
        
        foreach ( $result as $row ) {
            $a_result['product'][$row['pcode']] = $row['pname'];
            $a_result['qty'][$row['pcode']] = $row['qty'];
        }    
        
        return $a_result;
    }
    
    function closing($setting, $closingdate) {
        $wrdate = ($closingdate > $setting['WRDATE']) ? $setting['WRDATE'] : $closingdate;
        
        //delete stock > stockdate
        $query = $this->db->query("DELETE FROM t_stock WHERE STOCKDATE > '$wrdate'");
        
        $param = array('filter' => "STOCKDATE = '$wrdate'");
        
        $result = $this->get_all($param);

        foreach( $result['data'] as $row ) {
            $record = array();
            $record['QTYIN'] = $row['QTYIN'];   
            $record['QTYOUT'] = $row['QTYOUT'];   
            $record['DEBIT'] = $row['DEBIT'];
            $record['KREDIT'] = $row['KREDIT'];
            
            //update stock
            $this->stock->edit(array(
                'STOCKDATE' => "$setting[WRDATE]",    
                'WRCODE' => "$row[WRCODE]",    
                'PCODE' => "$row[PCODE]"    
            ), $record);
        }
        
        $this->db->query("UPDATE t_stock
        SET QTYSTOCK = (COALESCE(QTYSTART,0) + COALESCE(QTYIN,0) - COALESCE(QTYOUT,0)), SALDOEND = (COALESCE(SALDOSTART,0) + COALESCE(DEBIT,0) - COALESCE(KREDIT,0))
        WHERE STOCKDATE = '$wrdate'");
        
        if($closingdate > $setting['WRDATE']) {
            $this->db->query("INSERT INTO t_stock(STOCKDATE, PCODE, WRCODE, QTYSTART, SALDOSTART)
            SELECT '$closingdate' STOCKDATE, PCODE, WRCODE, QTYSTOCK, SALDOEND
            FROM t_stock 
            WHERE stockdate = '$wrdate'");
        }
    }
}
?>
