<?php
class Model_price extends MY_Model {
	var $str_table = "t_price";
	var $strID = "IDPRICE";
    
    function __construct()
    {
        parent::__construct();
    }
    
    #@override
    function get_all($param=array(), $select="*"){       
        $this->strSQL= "SELECT pc.*, pr.PNAME
                        FROM t_price pc
                        JOIN m_product pr
                        ON pc.pcode = pr.pcode";
        
        foreach ($param as $key=>$value)
        {
            $this->arr_param[$key] = $value;
        }
        
        $arrToReturn['data'] = $this->list_array();
        $arrToReturn['total'] = $this->list_total();
        $this->db->close();
        
        return $arrToReturn;
    }
}
?>