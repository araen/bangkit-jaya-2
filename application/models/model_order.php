<?php
class Model_order extends MY_Model {
	var $str_table = "t_horder";
    var $strID = "ORDERNO";
	
    function __construct()
    {
        parent::__construct();
    }
    
    #@override
    function get_all($param=array(), $select="*"){       
        $this->strSQL= "SELECT o.ORDERNO,o.SUBTOTAL,o.ORDERDATE,o.WRCODE,w.WRNAME GUDANG,
						o.SLSNO, s.SLSNAME SALES, o.OUTNO, ot.OUTNAME OUTLET, o.INVNO, v.INVDATE
                        FROM {$this->str_table} o
                        LEFT JOIN m_warehouse w
                        ON o.WRCODE = w.WRCODE
						LEFT JOIN m_sales s
                        ON o.SLSNO = s.SLSNO
						LEFT JOIN m_outlet ot
                        ON o.OUTNO = ot.OUTNO
                        LEFT JOIN t_invoice v
                        ON o.INVNO = v.INVNO
                        GROUP BY o.ORDERNO,o.SUBTOTAL,o.WRCODE";

        foreach ($param as $key=>$value)
        {
            $this->arr_param[$key] = $value;
        }
        
        $arrToReturn['data'] = $this->list_array();
        $arrToReturn['total'] = $this->list_total();
        $this->db->close();
        
        return $arrToReturn;
    }
    
    function getRekapSales($tahun = '', $status = 3) {
        $this->strSQL = "SELECT sl.slsno, sl.slsname,DATE_FORMAT(orderdate,'%m') bulan,SUM(do.amount) total 
        FROM t_horder ho 
        JOIN t_dorder do
        ON ho.orderno = do.orderno
        LEFT JOIN m_sales sl ON ho.slsno = sl.slsno 
        WHERE ostatuscode = $status 
        AND STR_TO_DATE(orderdate,'%Y') = STR_TO_DATE('$tahun','%Y') 
        GROUP BY sl.slsno, sl.slsname, DATE_FORMAT(orderdate,'%m')
        ORDER BY sl.slsno"; 
              
        $result = $this->get_array_all();
        
        $a_result['salesman'] = array();
        $a_result['omset'] = array();
        
        foreach ( $result as $row ) {
            $a_result['salesman'][$row['slsno']] = $row['slsname'];
            
            for ( $i = 1; $i <= 12; $i++ )
                $a_result['omset'][$row['slsno']][$i] = (isset($a_result['omset'][$row['slsno']][$i]) ? $a_result['omset'][$row['slsno']][$i] : 0);
            
            $a_result['omset'][$row['slsno']][(int)$row['bulan']] = $row['total'];
        }

        return $a_result;
    }
    
    function getRekapProduct($tahun = '') {
        $this->strSQL = "SELECT DO.pcode,pr.pname,DATE_FORMAT(HO.orderdate,'%m') bulan,SUM(DO.amount) total 
        FROM t_dorder DO
        JOIN t_horder HO
        ON DO.orderno = HO.orderno 
        JOIN m_product pr
        ON DO.pcode = pr.pcode
        WHERE HO.ostatuscode = 3 
        AND STR_TO_DATE(orderdate,'%Y') = STR_TO_DATE('$tahun','%Y') 
        GROUP BY DO.pcode,pr.pname,DATE_FORMAT(orderdate,'%m')";
        
        $result = $this->get_array_all();
        
        $a_result['product'] = array();
        $a_result['omset'] = array();
        
        foreach ( $result as $row ) {
            $a_result['product'][$row['pcode']] = $row['pname'];
            
            for ( $i = 1; $i <= 12; $i++ )
                $a_result['omset'][$row['pcode']][$i] = (isset($a_result['omset'][$row['pcode']][$i]) ? $a_result['omset'][$row['pcode']][$i] : 0);
            
            $a_result['omset'][$row['pcode']][(int)$row['bulan']] = $row['total'];
        }

        return $a_result;
    }
    
    function get_rekap_outlet($param) {
        $this->strSQL= "SELECT o.OUTNO, ot.OUTNAME OUTLET, SUM(d.AMOUNT) SUBTOTAL, SUM(AMOUNTDISC) DISC
                        FROM {$this->str_table} o
                        JOIN t_dorder d
                        ON o.ORDERNO = d.ORDERNO
                        LEFT JOIN m_warehouse w
                        ON o.WRCODE = w.WRCODE
                        LEFT JOIN m_sales s
                        ON o.SLSNO = s.SLSNO
                        LEFT JOIN m_outlet ot
                        ON o.OUTNO = ot.OUTNO
                        LEFT JOIN t_invoice v
                        ON o.INVNO = v.INVNO
                        WHERE $param[filter]
                        GROUP BY o.OUTNO, ot.OUTNAME";
        
        $arrToReturn['data'] = $this->list_array();
        $arrToReturn['total'] = $this->list_total();
        $this->db->close();
        
        return $arrToReturn;    
    }
}
?>