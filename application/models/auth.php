<?php
class Auth extends MY_Model {
	var $str_table = "m_menu";
    var $strID = "MNCODE";
    
    function Auth()
    {
        parent::__construct();
    }
	
	function get_menu_admin(){
        $this->load->database();
		$id = $this->session->userdata('id_user');
        if($id)
        {
            $this->db->select('*');
            $this->db->from('sys_user A');
            $this->db->join('sys_group B', 'A.ID_GROUP = B.ID','LEFT');
            $this->db->where('A.ID',$id);
            $query = $this->db->get();    
        }					

		$id_menu = array();
		foreach($query->result() as $row)
		{
			$id_menu = json_decode($row->ID_ACCESS,TRUE);
		}
        $in = "(".implode(',',$id_menu).")";
		$sql = "SELECT * FROM 
                sys_menu_admin 
                WHERE ID IN $in AND PARENT='0' 
                ORDER BY ORDERED ASC";
		$query = $this->db->query($sql);
		$menu = array();
		foreach($query->result() as $row)
		{
			$menu[$row->ID]['NAME'] = $row->NAME;
            $menu[$row->ID]['URL'] = $row->LINK;
			$menu[$row->ID]['SUB'] = $this->_get_child($row->ID,$in);
		}
		$this->db->close();
		return $menu;
	}
	
	function _get_child($id,$in){
		$this->load->database();					
		$sql = "SELECT * FROM 
                sys_menu_admin 
                WHERE PARENT=$id 
                AND ID IN $in 
                ORDER BY ORDERED ASC";
		$query = $this->db->query($sql);		
		$menu = array();
		foreach($query->result() as $row)
		{
			$menu[$row->ID]['NAME'] = $row->NAME;
            $menu[$row->ID]['URL'] = $row->LINK;
			$menu[$row->ID]['ISACTIVE'] = $row->ISACTIVE;
			$menu[$row->ID]['SUB'] = $this->_get_child($row->ID,$in);
		}
		$this->db->close();
		return $menu;
	}
	
	function get_access($accesspage=''){
		$idrole = $this->session->userdata('id_role');

        $sql = "SELECT AC.* 
                FROM m_access AC
                JOIN m_cmenu CM
                ON AC.CMNCODE = CM.CMNCODE
                WHERE CM.CMNURL LIKE '$accesspage%'
                AND AC.IDROLE = '$idrole'";
                        
        $row = $this->db->query($sql)->row_array();

        $a_access['c_read'] = $row['ISREAD'];
        $a_access['c_create'] = $row['ISCREATE'];
        $a_access['c_update'] = $row['ISUPDATE'];
        $a_access['c_delete'] = $row['ISDELETE'];
        
        return $a_access;
	}
    
    function get_menu($idrole = '') {
        $param = array();
        if (!empty($idrole))
            $param['filter'] = "IDROLE = $idrole";

        $param['order'] = "CMNCODE";        
        
        $this->strSQL = "SELECT AC.IDROLE,MN.MNCODE, MN.MNNAME, MN.ISACTIVE MENUACTIVE, 
                        CM.CMNCODE, CM.CMNNAME, CM.CMNURL, CM.ISACTIVE SUBMENUACTIVE,AC.ISREAD
                        FROM m_cmenu CM 
                        JOIN m_menu MN
                        ON CM.MNCODE = MN.MNCODE
                        LEFT JOIN m_access AC
                        ON AC.CMNCODE = CM.CMNCODE";
        $result = $this->get_array_all($param);
        
        $mncode = null;
        $a_menu = array();
        foreach ( $result as $row ) {
            if ( $mncode == null or $mncode != $row['MNCODE'] )
                $a_submenu = array();    
            
            $a_submenu[$row['CMNCODE']]['NAME'] = $row['CMNNAME'];    
            $a_submenu[$row['CMNCODE']]['LEVEL'] = 2;    
            $a_submenu[$row['CMNCODE']]['ISACTIVE'] = $row['SUBMENUACTIVE'];    
            $a_submenu[$row['CMNCODE']]['ISREAD'] = $row['ISREAD'];    
            $a_submenu[$row['CMNCODE']]['URL'] = $row['CMNURL'];
            
            $a_menu[$row['MNCODE']]['NAME'] = $row['MNNAME'];    
            $a_menu[$row['MNCODE']]['LEVEL'] = 1;    
            $a_menu[$row['MNCODE']]['URL'] = '#';    
            $a_menu[$row['MNCODE']]['ISACTIVE'] = $row['MENUACTIVE'];    
            $a_menu[$row['MNCODE']]['SUB'] = $a_submenu;
            
            $mncode = $row['MNCODE'];
        }
        
        return $a_menu;
    }

	function _get_menu($menutype=''){
		$this->load->database();
        					
		$sql = "SELECT * 
                FROM m_menu
                ORDER BY MNCODE";
		$query = $this->db->query($sql);
		
        $menu = array();
		foreach($query->result() as $row)
		{
            $menu[$row->MNCODE]['NAME'] = $row->MNNAME;
            $menu[$row->MNCODE]['LEVEL'] = 1;
            $menu[$row->MNCODE]['URL'] = "#";
            $menu[$row->MNCODE]['ISACTIVE'] = $row->ISACTIVE;
			$menu[$row->MNCODE]['SUB'] = $this->_menu_child($row->MNCODE);
		}
		$this->db->close();
		return $menu;
	}
	
	function _menu_child($id){
		$this->load->database();					
		$sql = "SELECT * 
                FROM m_cmenu 
                WHERE MNCODE=$id 
                ORDER BY CMNCODE";
		$query = $this->db->query($sql);		
		$menu = array();
		foreach($query->result() as $row)
		{
            $menu[$row->CMNCODE]['NAME'] = $row->CMNNAME;
            $menu[$row->CMNCODE]['LEVEL'] = 2;
            $menu[$row->CMNCODE]['ISACTIVE'] = $row->ISACTIVE;
			$menu[$row->CMNCODE]['URL'] = $row->CMNURL;
		}
		$this->db->close();
		return $menu;
	}
    
    function add_captcha($data){
        $this->db->trans_start();
        foreach($data as $key=>$value){
            if(is_string($value)){
                $value = "'".$value."'";
            }
            $field[] = $key;
            $rec[] = $value;
        }
        $sql = "INSERT INTO CAPTCHA(".strtoupper(implode(',',$field)).")VALUES(".implode(",",$rec).")";
        $this->db->query($sql);
        $this->db->trans_complete();
        $return = $this->db->trans_status();
        $this->db->close();
        return $return;
    }
    
    function delete_captcha(){
        $expiration_time = time()-90;
        $this->db->trans_start();
        $sql = "DELETE FROM CAPTCHA WHERE CAPTCHA_TIME < $expiration_time";
        $this->db->query($sql);
        $this->db->trans_complete();
        $return = $this->db->trans_status();
        $this->db->close();
    }
    
    function validate_captcha($word='',$ip=''){
        $expiration_time = time()-90;
        $sql = "SELECT COUNT(*) AS COUNT FROM CAPTCHA
                WHERE WORD='$word' AND IP_ADDRESS='$ip' AND CAPTCHA_TIME > $expiration_time";
        
        $result = $this->db->query($sql);        
        foreach($result->result_array as $row){
            if($row['COUNT'] > 0 ){
                return true;
            }else{
                return false;
            }
        }
        
        $this->db->close();
    }
    
    function add_child($data){
        $this->str_table = 'm_cmenu';
        $this->strID = 'CMNCODE';
        
        return parent::add($data);   
    }
    
    function edit_child($param, $data){
        $this->str_table = 'm_cmenu';
        $this->strID = 'CMNCODE';
        
        return parent::edit($param, $data);   
    }
    
    function delete_child($param){
        $this->str_table = 'm_cmenu';
        $this->strID = 'CMNCODE';
        
        return parent::delete($param);   
    }
}
?>
