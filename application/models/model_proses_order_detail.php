<?php
class Model_proses_order_detail extends MY_Model {
	var $str_table = "t_dorder";
	var $strID = "ORDERNO";
    
    function __construct()
    {
        parent::__construct();
    }
    
    function get_array_all($param=array(), $select="DB.*,PR.PNAME"){       
        $this->strSQL= "SELECT $select 
                        FROM {$this->str_table} DB
                        JOIN m_product PR
                        ON DB.PCODE = PR.PCODE";
        
        foreach ($param as $key=>$value)
        {
            $this->arr_param[$key] = $value;
        }
        
        $arrToReturn = $this->list_array_all();;
        $this->db->close();
        
        return $arrToReturn;
    }
}
?>