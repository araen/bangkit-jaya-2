<?php
class Model_pembayaran extends MY_Model {
	var $str_table = "t_payment";
    var $strID = "IDPAYMENT";
    
    function __construct()
    {
        parent::__construct();
    }
    
    #@override
    function get_all($param=array(), $select="*"){       
        $this->strSQL= "SELECT T1.INVNO,T1.INVDATE,T1.SLSNO,T1.OUTNO,T1.ORDERDATE, T1.TAGIHAN, COALESCE(SUM(PY.AMOUNT),0) PAYMENT,PY.PAYNO FROM
        (SELECT IV.INVNO,IV.INVDATE,HO.SLSNO,HO.OUTNO,HO.ORDERDATE, SUM(SUBTOTAL) TAGIHAN
        FROM t_invoice IV
        JOIN t_horder HO
        ON IV.INVNO = HO.INVNO 
        GROUP BY IV.INVNO,IV.INVDATE,HO.SLSNO,HO.OUTNO,HO.ORDERDATE) T1
        LEFT JOIN t_payment PY
        ON T1.INVNO = PY.INVNO
        GROUP BY T1.INVNO, T1.TAGIHAN";
        
        foreach ($param as $key=>$value)
        {
            $this->arr_param[$key] = $value;
        }
        
        $arrToReturn['data'] = $this->list_array();
        $arrToReturn['total'] = $this->list_total();
        $this->db->close();
        
        return $arrToReturn;
    }
    
    #@override
    function get_pembayaran($param=array(), $select="*"){       
        $this->strSQL= "SELECT T1.INVNO,T1.INVDATE,T1.SLSNO,T1.OUTNO,T1.ORDERNO,T1.ORDERDATE,T1.WRCODE, T1.TAGIHAN, COALESCE(SUM(PY.AMOUNT),0) PAYMENT FROM
        (SELECT IV.INVNO,IV.INVDATE,HO.SLSNO,HO.OUTNO,HO.ORDERNO,HO.ORDERDATE,HO.WRCODE, SUM(SUBTOTAL) TAGIHAN
        FROM t_invoice IV
        JOIN t_horder HO
        ON IV.INVNO = HO.INVNO
        GROUP BY IV.INVNO,IV.INVDATE,HO.SLSNO,HO.OUTNO,HO.ORDERDATE) T1
        LEFT JOIN t_payment PY
        ON T1.INVNO = PY.INVNO
        GROUP BY T1.INVNO, T1.TAGIHAN";
        
        foreach ($param as $key=>$value)
        {
            $this->arr_param[$key] = $value;
        }
        
        $arrToReturn = $this->list_array_all();
        $this->db->close();

        return $arrToReturn;
    }
}
?>