<?php
class Model_laporan extends MY_Model {
	
	function __construct()
	{
		parent::__construct();
	}
	
	function getStockGudang(){
		$this->strSQL = "SELECT s.IDSTOCK, s.PCODE, p.PNAME NAMABARANG, s.WRCODE, w.WRNAME GUDANG, s.QTYSTOCK, s.TOTALBAYAR
						FROM t_stock s
						JOIN m_product p 
						ON p.PCODE = s.PCODE
						JOIN m_warehouse w 
						ON w.WRCODE = s.WRCODE";
		
		$arrToReturn = $this->list_array_all();
		$this->db->close();
		
		return $arrToReturn;
	}
	
	function getStockVan(){
		$this->strSQL = "SELECT tv.IDTVAN, tv.VANID, v.VANNAME VAN, 
						tv.WRCODE, w.WRNAME GUDANG, 
						tv.PCODE, p.PNAME NAMABARANG,  
						SUM(tv.VSTOCK) VSTOCK
						FROM t_van tv
						JOIN m_van v
						ON v.VANCODE = tv.VANID
						JOIN m_warehouse w 
						ON w.WRCODE = tv.WRCODE
						JOIN m_product p 
						ON p.PCODE = tv.PCODE
						GROUP BY tv.IDTVAN, tv.VANID, 
						tv.WRCODE, w.WRNAME, 
						tv.PCODE, p.PNAME";
						
		
		$arrToReturn = $this->list_array_all();
		$this->db->close();
		
		return $arrToReturn;
	}
	
	function getJmlBeli(){
		$this->strSQL = "SELECT DATE_FORMAT(tglbeli, '%m') BLNORDER, DATE_FORMAT(tglbeli, '%y') THNORDER, SUM(totalbeli) AS total
							FROM t_hbeli
							WHERE DATE_FORMAT(tglbeli, '%m') = '04'
							AND DATE_FORMAT(tglbeli, '%y') = '15'
							GROUP BY DATE_FORMAT(tglbeli, '%m'), DATE_FORMAT(tglbeli, '%y')";
							
		foreach ($param as $key=>$value)
		{
			$this->arr_param[$key] = $value;
		}
		
		$arrToReturn['data'] = $this->list_array();
		$arrToRetutn['total'] = $this->list_total();
		$this->db->close();
		
		return $arrToReturn;
	}
	
	function audit_trail_gudang($param = array()) {
        $this->strSQL = "SELECT T1.WRCODE, WR.WRNAME, T1.TYPE, T1.TGL, T1.PCODE, T1.THPART, PR.PNAME, QTY, AMOUNT FROM
                        (SELECT WRCODE, 'M' TYPE, TGLBELI TGL, PCODE, SUPPNO THPART, QTYBELI QTY, JMLBELI AMOUNT
                        FROM T_HBELI HB
                        JOIN T_DBELI DB
                        ON HB.IDBELI = DB.IDBELI
                        UNION ALL
                        SELECT WRCODE, 'K' TYPE, ORDERDATE TGL, PCODE, OUTNO THPART, QTY, AMOUNT
                        FROM T_HORDER HB
                        JOIN T_DORDER DB
                        ON HB.ORDERNO = DB.ORDERNO
                        UNION ALL
                        SELECT WRCODE, CASE WHEN RETTYPE = 'R' THEN 'M' ELSE 'K' END TYPE, RETDATE TGL, PCODE, OUTNO THPART, QTY, AMOUNT
                        FROM T_HORDER HB
                        JOIN T_DORDER DB
                        ON HB.ORDERNO = DB.ORDERNO
                        AND RETDATE IS NOT NULL
                        UNION ALL
                        SELECT WRCODE, 'K' TYPE, RETDATE TGL, PCODE, SUPPNO THPART, QTYBELI QTY, JMLBELI AMOUNT
                        FROM T_HBELI HB
                        JOIN T_DBELI DB
                        ON HB.IDBELI = DB.IDBELI
                        AND RETDATE IS NOT NULL
                        UNION ALL
                        SELECT WRCODE, CASE WHEN DAYIN IS NOT NULL THEN 'M' WHEN DAYOUT IS NOT NULL THEN 'K' END TYPE,
                        CASE WHEN DAYIN IS NOT NULL THEN DAYIN ELSE DAYOUT END TGL, PCODE, NULL THPART, QTY, AMOUNT
                        FROM T_HVAN HV
                        JOIN T_DVAN DV
                        ON HV.IDTVAN = DV.IDTVAN) T1
                        JOIN M_WAREHOUSE WR
                        ON T1.WRCODE = WR.WRCODE
                        JOIN M_PRODUCT PR
                        ON T1.PCODE = PR.PCODE";
        
        foreach ($param as $key=>$value)
        {
            $this->arr_param[$key] = $value;
        }
        
        $result = $this->list_array_all();
        $this->db->close();
        
        $i = 0;
        $a_data = array();
        foreach ( $result as $row ) {
            $a_data[$row['WRCODE']]['WRCODE'] = $row['WRCODE'];
            $a_data[$row['WRCODE']]['WRNAME'] = $row['WRNAME'];
            $a_data[$row['WRCODE']]['DETAIL'][$i]['PCODE'] = $row['PCODE'];
            $a_data[$row['WRCODE']]['DETAIL'][$i]['PNAME'] = $row['PNAME'];
            $a_data[$row['WRCODE']]['DETAIL'][$i]['THPART'] = $row['THPART'];
            $a_data[$row['WRCODE']]['DETAIL'][$i]['TGL'] = $row['TGL'];
            $a_data[$row['WRCODE']]['DETAIL'][$i]['TYPE'] = $row['TYPE'];
            $a_data[$row['WRCODE']]['DETAIL'][$i]['QTY'] = $row['QTY'];
            $a_data[$row['WRCODE']]['DETAIL'][$i]['AMOUNT'] = $row['AMOUNT'];
            $i++;            
        }
        return $a_data;
    }
    
    function audit_trail_van($param = array()) {
        $this->strSQL= "SELECT HV.VANCODE,VANNAME, CASE WHEN DAYIN IS NOT NULL THEN 'M' ELSE 'K' END TYPE, 
                        CASE WHEN DAYIN IS NOT NULL THEN DAYIN ELSE DAYOUT END TGL, DV.PCODE, PR.PNAME, QTY, AMOUNT
                        FROM T_HVAN HV 
                        JOIN T_DVAN DV
                        ON HV.IDTVAN = DV.IDTVAN
                        JOIN M_VAN VN
                        ON HV.VANCODE = VN.VANCODE
                        JOIN M_PRODUCT PR
                        ON DV.PCODE = PR.PCODE";     
        
        foreach ($param as $key=>$value)
        {
            $this->arr_param[$key] = $value;
        }
        
        $result = $this->list_array_all();
        $this->db->close();
        
        $i = 0;
        $a_data = array();
        foreach ( $result as $row ) {
            $a_data[$row['VANCODE']]['VANCODE'] = $row['VANCODE'];
            $a_data[$row['VANCODE']]['VANNAME'] = $row['VANNAME'];
            $a_data[$row['VANCODE']]['DETAIL'][$i]['PCODE'] = $row['PCODE'];
            $a_data[$row['VANCODE']]['DETAIL'][$i]['PNAME'] = $row['PNAME'];
            $a_data[$row['VANCODE']]['DETAIL'][$i]['TGL'] = $row['TGL'];
            $a_data[$row['VANCODE']]['DETAIL'][$i]['TYPE'] = $row['TYPE'];
            $a_data[$row['VANCODE']]['DETAIL'][$i]['QTY'] = $row['QTY'];
            $a_data[$row['VANCODE']]['DETAIL'][$i]['AMOUNT'] = $row['AMOUNT'];
            $i++;            
        }
        
        return $a_data;
    }
    
    function return_barang($param = array()) {
        $this->strSQL= "SELECT T1.*, WR.WRNAME, PR.PNAME FROM
                        (SELECT HO.WRCODE, DO.PCODE, DO.RETTYPE, DO.RETDATE, DO.QTYRET, DO.AMOUNTRET
                        FROM t_horder HO
                        JOIN t_dorder DO
                        ON HO.ORDERNO = DO.ORDERNO
                        UNION ALL
                        SELECT HB.WRCODE, DB.PCODE, 'T' RETTYPE, DB.RETDATE, DB.QTYRET, DB.AMOUNTRET
                        FROM t_hbeli HB
                        JOIN t_dbeli DB
                        ON HB.IDBELI = DB.IDBELI) T1
                        JOIN m_warehouse WR
                        ON T1.WRCODE = WR.WRCODE
                        JOIN m_product PR
                        ON T1.PCODE = PR.PCODE
                        WHERE RETDATE IS NOT NULL";     
        
        foreach ($param as $key=>$value)
        {
            $this->arr_param[$key] = $value;
        }
        
        $result = $this->list_array_all();
        $this->db->close();
        
        $i = 0;
        $a_data = array();
        foreach ( $result as $row ) {
            $a_data[$row['WRCODE']]['WRCODE'] = $row['WRCODE'];
            $a_data[$row['WRCODE']]['WRNAME'] = $row['WRNAME'];
            $a_data[$row['WRCODE']]['DETAIL'][$i]['PCODE'] = $row['PCODE'];
            $a_data[$row['WRCODE']]['DETAIL'][$i]['PNAME'] = $row['PNAME'];
            $a_data[$row['WRCODE']]['DETAIL'][$i]['RETTYPE'] = $row['RETTYPE'];
            $a_data[$row['WRCODE']]['DETAIL'][$i]['RETDATE'] = $row['RETDATE'];
            $a_data[$row['WRCODE']]['DETAIL'][$i]['QTYRET'] = $row['QTYRET'];
            $a_data[$row['WRCODE']]['DETAIL'][$i]['AMOUNTRET'] = $row['AMOUNTRET'];
            $i++;            
        }

        return $a_data;   
    }
}
?>