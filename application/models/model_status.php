<?php
class Model_status extends MY_Model {
	var $str_table = "m_ostatus";
    
    function __construct()
    {
        parent::__construct();
    }
	
	function get_setting(){
		$query = $this->get_array_all();
        foreach($query as $row){
            $data[$row['VARIABLE']] = $row['VALUE'];
        }
        return $data;
	}
    
    function get_value($code){
        $this->load->database();

        $sql = "SELECT * FROM {$this->str_table} WHERE OSTATUSCODE = '$code'";
        $data = $this->db->query($sql)->row_array();

        $this->db->close();
        
        return $data;
    }
    
	 function edit($param = array(),$data){
        $this->db->trans_start();
        foreach($param as $key=>$value)
            $this->db->where($key,$value);
        
        $this->db->update($this->str_table,$data);
        $this->db->trans_complete();
        $return = $this->db->trans_status();
        $this->db->close();
    }
}
?>