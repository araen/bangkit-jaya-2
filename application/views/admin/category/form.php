<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>  
<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >    
    <h1><?php echo @$page_title?></h1>
    
    <form id="form" action="" class="uniform" method="post">
        <fieldset>
            <legend><?php echo @$page_title?></legend>
            <dl class="inline">
                <dt><label for="name">Nama</label></dt>
                <dd>
                    <input type="hidden" name="ID" value="<?php echo @$row['id']?>" />
                    <input type="text" id="name" name="data[name]" autocomplete="off" value="<?php echo @$row['name']?>" size="50" />
                </dd>
                
                <dt><label for="alamat">Bobot</label></dt>
                <dd>
                    <input type="text" id="bobot" name="data[bobot]" autocomplete="off" value="<?php echo @$row['bobot']?>" size="50" />
                </dd>
                
                <div class="buttons">
                    <button name="save" class="button green" onclick="Submit()">Save</button>
                    <a href="<?php echo $this->config->item('base_url').$cpanel_dir?>category" class="button white" >Cancel</a>
                </div>
        </fieldset>
    </form>
</article>