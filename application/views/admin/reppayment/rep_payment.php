<html>
<head>
<title>Laporan Pelunasan</title>
<style type="text/css">
#main_wrapper {
	margin:auto;
	font-family:Arial, Helvetica, sans-serif;
	width:18cm;
}

table{font-size:12px;border-collapse:collapse;}

.style1 {
	font-size: 24px;
	font-weight: bold;
}

th {border-top:1px solid;border-bottom:1px solid}
th,td {padding-top:5px;padding-bottom:5px;}
#rounded-box{border:1px dashed;border-radius: 25px;padding:5px;}
.fill {background-color:#CCCCCC}
@media print
{
    #noprint { display: none; }
}
div#noprint{
    background-color : #E0E0E0;
    margin: auto;
    padding: 10px;
}
.button.green {
    background: #75ae5c none repeat scroll 0 0;
    border: 1px solid #3b6e22;
}
.button {
    background: #28a0b2 none repeat scroll 0 0;
    border: 1px solid #0d717e;
}
.button {
    box-shadow: 0 1px 2px rgba(255, 255, 255, 0.6) inset, 0 -5px 15px rgba(0, 0, 0, 0.3) inset, 1px 1px 1px #ccc;
}
.button {
    color: #fff;
    cursor: pointer;
    display: inline-block;
    font: bold 11px/110% Tahoma,sans-serif;
    margin: 0 3px 0 1px;
    outline: medium none;
    overflow: visible;
    padding: 6px 8px;
    text-align: center;
    text-shadow: 1px 1px 1px #555;
    vertical-align: baseline;
    width: auto;
}
</style>
</head>
<body>
<div id="main_wrapper">
    <div id="noprint" width="100%" style="text-align:center">
        <button class="button green" onClick="window.print()">Print</button>
    </div>
  <table border="0" width="100%">
    <tr>
      <td colspan="2"><b>
        <?= $setting['CORPORATE_NAME']?>
      </b></td>
      <td colspan="5" rowspan="2"><div align="center" class="style1">LAPORAN PELUNASAN</div></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2"><?= $setting['ADDRESS']?></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2">Telp. 
      <?= $setting['PHONE']?></td>
      <td colspan="5" align="center"><b><?= ($status == 1) ? "Pembayaran Lunas" : "Pembayaran Belum Lunas"?></b></td>
      <td>Tgl. <?= dateIndo(dGetDate($tglmulai)).' - '.dateIndo(dGetDate($tglselesai))?></td>
    </tr>
    <tr>
      <td colspan="5"></td>
    </tr>
    <tr bgcolor="#CCCCCC">
      <th >No.</th>
      <th>Orderno</th>
      <th>Sales</th>
      <th>Tgl. Inv</th>
      <th>Outlet</th>
      <th>Tagihan (Rp)</th>
      <th>Total Bayar (Rp)</th>
      <th>Sisa Bayar (Rp)</th>
    </tr>
    <tr bgcolor="#CCCCCC">
      <td align="center">1</td>
      <td align="center">2</td>
      <td align="center">3</td>
      <td align="center">4</td>
      <td align="center">5</td>
      <td align="center">6</td>
      <td align="center">7</td>
      <td align="center">8</td>
    </tr>
    
    <?php 
	  $i=0;
	  $tot1 = 0;
	  $tot2 = 0;
	  $tot3 = 0;
	  foreach ( $rows as $rec ):
		  $tottagihan += $rec['TAGIHAN'];
		  $totpayment += $rec['PAYMENT'];
		  $totsaldo += ($rec['TAGIHAN'] - $rec['PAYMENT']);
	  ?>
    <tr>
      <td colspan="8"></td>
    </tr>
    <tr valign="top">
      <td align="center"><?= ++$i;?>
        .</td>
      <td align="center"><?= $rec['ORDERNO']?></td>
      <td><?= $sales[$rec['SLSNO']]?></td>
      <td><?= dateIndo($rec['INVDATE'])?></td>
      <td><?= $outlet[$rec['OUTNO']]?></td>
      <td align="right"><?= fCurrency($rec['TAGIHAN'])?></td>
      <td align="right"><?= fCurrency($rec['PAYMENT'])?></td>
      <td align="right"><?= fCurrency(($rec['TAGIHAN'] - $rec['PAYMENT']))?></td>
    </tr>
    <?php endforeach;?>
    <tr>
      <td colspan="8"><hr /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><b>TOTAL</b></td>
      <td align="right"><?= fCurrency($tottagihan)?></td>
      <td align="right"><?= fCurrency($totpayment)?></td>
      <td align="right"><?= fCurrency($totsaldo) ?></td>
    </tr>
  </table>
</div>
</div>
</body>
</html>
<script type="text/javascript">

function printDiv(divName){
	alert('s');
	var printContents = document.getElementById(divName).innerHTML;
	w = window.open();
	w.document.write(printContents);
	w.print();
	w.close();
}
</script>