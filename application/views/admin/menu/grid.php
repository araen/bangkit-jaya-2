<style type="text/css">
    tr.menusub{display: none;}
    div#menugrid{float: left;}
    div#menuform{float: right;} 
    div#cleared {clear: both;}
    table#tbform td{padding:5px}
</style>
<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >	
	<h1><?php echo @$page_title?></h1>
	
	<form id="form" action="<?php echo base_url().$cpanel_dir.'menu'?>" method="post">
        <?= $transaction_message?>
		<br />
        <div id="content">
            <div id="menugrid" style="width: 50%;">
		        <table id="table1" class="gtable">
			        <thead>
				        <tr>
					        <th>Kode</th>
                            <th>Nama</th>
					        <th>Aksi</th>
				        </tr>
			        </thead>
			        <tbody>
				        <?php
                        if(isset($rows)): 
                        $i=0;
                        foreach($rows as $key => $row): ?>
				        <tr>
					        <td width="1%"><?php echo $key?></td>
                            <td><a id="menulink" data-parent="<?= $key?>|<?= $row['NAME']?>" data-aktif="<?= $row['ISACTIVE']?>" data-value="<?= $key?>" href="javascript:void(0)" data-sub="collapse" onclick="menuToggle(this)"><?php echo "<b>".$row['NAME']."</b>"?></a></td>
					        <td>
                                <a href="javascript:void(0)" onclick="addchild($(this).parents('tr').find('a#menulink'))"><div class="btn-child"></div></a>
                                <a href="javascript:void(0)" onclick="viewmenu($(this).parents('tr').find('a#menulink'))"><div class="btn-edit"></div></a>
                                <?php if( count($row['SUB']) == 0 ) {?>
                                    <a href="<?= $cls."/delete/$key"?>"><div class="btn-delete"></div></a>
                                <?php }?>
                            </td>
				        </tr>                  
                        <?php
                        $i = 0; 
                        $pk = null;
                        $a_key = array_keys($row['SUB']);
                        foreach ( $row['SUB'] as $k => $rec ){?>
                            <tr id="child-<?= $key?>" class="menusub">
                                <td width="1%"><?php echo str_repeat("&nbsp;", 5).$k?></td>
                                <td><a id="menusublink" href="javascript:void(0)" data-file="<?= $rec['URL']?>" data-aktif="<?= $rec['ISACTIVE']?>" data-value="<?= $k?>" data-parent="<?= $key?>|<?= $row['NAME']?>" onclick="viewsubmenu(this)" ><?php echo str_repeat("&nbsp;", 5).$rec['NAME']?></a></td>
                                <td>
                                    <a href="javascript:void(0)" onclick="viewsubmenu($(this).parents('tr').find('a#menusublink'))"><div class="btn-edit"></div></a>
                                    <a href="<?= is_null($pk) ? "javascript:void(0)" : "$cls/upcmenu/$pk/$k"?>"><div class="btn-up"></div></a>
                                    <a href="<?= is_null($a_key[($i+1)]) ? "javascript:void(0)" : "$cls/upcmenu/$k/".$a_key[($i+1)]?>"><div class="btn-down"></div></a>
                                    <a href="<?= $cls."/delete_child/$k"?>" onclick="return confirm('Anda yakin akan menghapus menu ini?')"><div class="btn-delete"></div></a>
                                </td>
                            </tr>    
                        <?php 
                            $pk = $k;
                            $i++;
                        }?>
				        <?php endforeach; endif; ?>
			        </tbody>
		        </table>
            </div>
            <div id="menuform" style="width: 45%;">
                <input id="submit" class="button green" type="button" name="submit" value="Tambah" onclick="addmenu()"> 
                <fieldset>
                    <legend>Form</legend>
                    <table width="100%" id="tbform">
                        <tr>
                            <td><b>Parent</b></td>
                            <td>
                                <input id="parentcode" type="hidden" name="data[parentcode]">
                                <input id="parentname" type="text" readonly="readonly" autocomplete="off">
                            </td>
                        </tr>
                        <tr>
                            <td><b>Menu</b></td>
                            <td>
                                <input id="menucode" type="hidden" name="data[menucode]">
                                <input id="menuname" type="text" name="data[menuname]" autocomplete="off">
                            </td>
                        </tr>
                        <tr>
                            <td><b>File Menu</b></td>
                            <td>
                                <input id="menufile" type="text" name="data[menufile]" autocomplete="off">
                            </td>
                        </tr>
                        <tr>
                            <td><b>Aktif</b></td>
                            <td>
                                <input id="isactive" type="checkbox" name="data[isactive]" value="1">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center"><input id="submit" class="button green" type="submit" name="submit" value="Simpan"></td>
                        </tr>
                    </table>
                </fieldset>
            </div>
            <div id="cleared"></div>
        </div>
	</form>
</article>
<script type="text/javascript">
    $ = jQuery;
    function menuToggle(obj) {
        var sub = $(obj).attr('data-sub');
        var menu = $(obj).attr('data-parent').split('|');
        var key = menu[0];
        
        $('tr.menusub').hide();
        if (sub == 'collapse') {
            $(obj).attr('data-sub','expand');
            $('tr#child-'+key).show();
        } else {
            $(obj).attr('data-sub','collapse');
        }
        $('input').not("#submit, #isactive").val('');
    }
    
    function addmenu() {
        $('#parentcode').attr('disabled','disabled');
        $('#parentname').attr('disabled','disabled');
        $('#menucode').attr('disabled','disabled');
        $('#menufile').attr('disabled','disabled');
        $('#isactive').removeAttr('checked');
        
        $('#parentname').val('(null)');
        $('#menuname').val('');
        $('#menufile').val('(null)');
        
        $('#menuname').focus();    
    }
    
    function addchild(obj) {
        var parent = $(obj).attr('data-parent').split('|');
        var pcode = parent[0];
        var pname = parent[1].trim();
        
        $('#parentcode').removeAttr('disabled');
        $('#parentname').removeAttr('disabled');
        $('#menufile').removeAttr('disabled');
        
        $('#parentcode').val(pcode);
        $('#parentname').val(pname);        
    }
    
    function viewmenu(obj) {
        var mcode = $(obj).attr('data-value');
        var mname = $(obj).text().trim();
        var filemenu = $(obj).attr('data-file');
        var isactive = $(obj).attr('data-aktif');
       
        $('#parentcode').attr('disabled','disabled');
        $('#menufile').attr('disabled','disabled');
        $('#parentname').attr('disabled','disabled');
        $('#menucode').removeAttr('disabled'); 
        $('#isactive').removeAttr('checked');
        
        $('#parentname').val('(null)');
        $('#menufile').val('(null)');
        
        $('#menucode').val(mcode);
        $('#menuname').val(mname);
        
        if(isactive == 1) {
            $('#isactive').attr('checked','checked');
        } else {
            $('#isactive').removeAttr('checked');
        }
    }
    
    function viewsubmenu(obj) {
        var parent = $(obj).attr('data-parent').split('|');
        var pcode = parent[0];
        var pname = parent[1].trim();
        var mcode = $(obj).attr('data-value');
        var mname = $(obj).text().trim();
        var filemenu = $(obj).attr('data-file');
        var isactive = $(obj).attr('data-aktif');
        
        $('#parentcode').removeAttr('disabled');
        $('#parentname').removeAttr('disabled');
        $('#menufile').removeAttr('disabled');
        
        $('#parentcode').val(pcode);
        $('#parentname').val(pname);
        $('#menucode').val(mcode);
        $('#menuname').val(mname);
        $('#menufile').val(filemenu);
        $('#isactive').removeAttr('checked');
        if(isactive == 1) {
            $('#isactive').attr('checked','checked');
        } else {
            $('#isactive').removeAttr('checked');
        }
    }
</script>