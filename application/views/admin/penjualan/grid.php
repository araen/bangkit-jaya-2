<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >	
	<h1><?php echo @$page_title?></h1>
	
	<form id="form" action="<?php echo base_url().$cpanel_dir.'penjualan'?>" method="post">
		<div class="tablefooter clearfix">
			<div class="actions">
				<b>Cari :</b>
				<input type="text" name="q" id="q" value="<?php echo $q ?>" autocomplete="off" />
                <?php echo str_repeat("&nbsp;",3)?>
                <b>Page :</b>
				<?php echo fPaging($page,$limit,$rows['total'],array('name'=>'page','onchange'=>'jQuery("#form").submit()'))?>
				<?php echo str_repeat("&nbsp;",3)?>
				<input type="submit" class="button blue" value="Filter">
				<?php echo str_repeat("&nbsp;",3)?>
				<a href="<?php echo base_url()?>penjualan/form" class="button green" >Tambah</a>
			    <?php echo str_repeat("&nbsp;",3)?>
            </div>
		</div>
		<br />
		<table id="table1" class="gtable sortable">
			<thead>
				<tr>
					<th>No Order</th>
					<th>Gudang</th>
					<th>Sales</th>
					<th>Outlet</th>
					<th>Tanggal Transaksi</th>
					<th>Total Order</th>
                    <th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php
                if(isset($rows['data'])): 
                $i=0;
                $day = get_hari_all();
                foreach($rows['data'] as $row): ?>
				<tr>
					<td width="1%"><?php echo $row['ORDERNO'] ?></td>
					<td><?php echo $row['GUDANG']?></td>
					<td><?php echo $row['SALES']?></td>
					<td><?php echo $row['OUTLET']?></td>
					<td><?php echo dateIndo($row['ORDERDATE'])?></td>
					<td><?php echo fCurrency($row['SUBTOTAL'])?></td>
                    <td>
                        <a href="<?php echo base_url()."penjualan/form/$row[ORDERNO]"?>" title="Edit" ><div class="btn-edit"></div></a>
                        <a href="<?php echo base_url()."penjualan/delete/$row[ORDERNO]"?>" title="Delete" onclick="return confirm('Apakah yakin hapus data ini?');"><div class="btn-delete"></div></a>                    
                    </td>
				</tr>
				<?php endforeach; endif; ?>
			</tbody>
		</table>
  </form>
</article>
<script type="text/javascript">
function goCetak() {
    var smt = jQuery('#semester').val();
    var thn = jQuery('#tahun').val();
    window.open('<?php echo base_url()."$cpanel_dir/penjualan/rekap/"?>'+smt+"/"+thn);
}
</script>