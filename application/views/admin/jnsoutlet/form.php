<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>  
<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >    
    <h1><?php echo @$page_title?></h1>
    
    <form id="form" action="" class="uniform" method="post">
        <fieldset>
            <legend><?php echo @$page_title?></legend>
            <dl class="inline">
				<dt><label for="attcode">Kode Jenis Outlet</label></dt>
                <dd>
                    <input type="text" id="attcode" name="data[attcode]" autocomplete="off" value="<?php echo @$row['ATTCODE']?>" size="40" />
                </dd>
				
				<dt><label for="attname">Nama Jenis Outlet</label></dt>
                <dd>
                    <input type="text" id="attname" name="data[attname]" autocomplete="off" value="<?php echo @$row['ATTNAME']?>" size="50" />
                </dd>
                
                <div class="buttons">
                    <button name="save" class="button green" onclick="Submit()">Simpan</button>
                    <a href="<?php echo $this->config->item('base_url')?>jnsoutlet" class="button white" >Batal</a>
                </div>
        </fieldset>
    </form>
</article>