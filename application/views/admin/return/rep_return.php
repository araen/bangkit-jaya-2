<html>
<head>
<title>Laporan Retur Barang</title>
<style type="text/css">
#main_wrapper {
	margin:auto;
	font-family:Arial, Helvetica, sans-serif;
	width:18cm;
}

table{font-size:12px;border-collapse:collapse;}

.style1 {
	font-size: 24px;
	font-weight: bold;
}

th {border-top:1px solid;border-bottom:1px solid}
th,td {padding-top:5px;padding-bottom:5px;}
#rounded-box{border:1px dashed;border-radius: 25px;padding:5px;}
.fill {background-color:#CCCCCC}

tr.content-header td{background-color: #F0F0F0;}
@media print
{
    #noprint { display: none; }
}
div#noprint{
    background-color : #E0E0E0;
    margin: auto;
    padding: 10px;
}
.button.green {
    background: #75ae5c none repeat scroll 0 0;
    border: 1px solid #3b6e22;
}
.button {
    background: #28a0b2 none repeat scroll 0 0;
    border: 1px solid #0d717e;
}
.button {
    box-shadow: 0 1px 2px rgba(255, 255, 255, 0.6) inset, 0 -5px 15px rgba(0, 0, 0, 0.3) inset, 1px 1px 1px #ccc;
}
.button {
    color: #fff;
    cursor: pointer;
    display: inline-block;
    font: bold 11px/110% Tahoma,sans-serif;
    margin: 0 3px 0 1px;
    outline: medium none;
    overflow: visible;
    padding: 6px 8px;
    text-align: center;
    text-shadow: 1px 1px 1px #555;
    vertical-align: baseline;
    width: auto;
}
</style>
</head>
<body>
<div id="main_wrapper">
<div id="noprint" width="100%" style="text-align:center">
    <button class="button green" onClick="window.print()">Print</button>
</div>
  <table border="0" width="100%">
    <tr>
      <td colspan="3"><b>
        <?= $setting['CORPORATE_NAME']?>
      </b></td>
      <td colspan="7" rowspan="3"><div align="center" class="style1">RETUR BARANG</div></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="3"><?= $setting['ADDRESS']?></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3">Telp. 
      <?= $setting['PHONE']?></td>
      <td colspan="2">Tgl.
      <?= dateIndo(dGetDate($tglmulai)).' - '.dateIndo(dGetDate($tglselesai))?></td>
    </tr>
    <tr>
      <td colspan="7"></td>
    </tr>
    <tr bgcolor="#CCCCCC">
      <th >No.</th>
      <th>Tgl</th>
      <th>PCODE</th>
      <th>Barang</th>
      <th colspan="3">Jumlah Masuk (Resell)</th>
      <th>Nilai (Rp)</th>
      <th colspan="3">Jumlah Keluar (Trash)</th>
      <th>Nilai (Rp)</th>
    </tr>
    <tr bgcolor="#CCCCCC">
      <td rowspan="2" align="center">1</td>
      <td rowspan="2" align="center">2</td>
      <td rowspan="2" align="center">3</td>
      <td rowspan="2" align="center">4</td>
      <td colspan="3" align="center">6</td>
      <td rowspan="2" align="center">7</td>
      <td colspan="3" align="center">8</td>
      <td rowspan="2" align="center">9</td>
    </tr>
    <tr bgcolor="#CCCCCC">
      <td align="center">JML1</td>
      <td align="center">JML2</td>
      <td align="center">JML3</td>
      <td align="center">JML1</td>
      <td align="center">JML2</td>
      <td align="center">JML3</td>
    </tr>
    <?php 
	  $i=0;
	  foreach ( $rows as $rec ):
      ?>
    <tr valign="top" class="content-header">
      <td align="center"><b><?= $rec['WRCODE']?></b></td>
      <td colspan="11"><b><?= $rec['WRNAME']?></b></td>
    </tr>
    <?php foreach ( $rec['DETAIL'] as $row ) :?>
    <tr>
      <td align="right"><?= ++$i?></td>
      <td align="center"><?= date('d/m/Y', strtotime($row['RETDATE']))?></td>
      <td align="center"><?= $row['PCODE']?></td>
      <td><?= $row['PNAME']?></td>
      <?php 
	    if($row['RETTYPE'] == 'R'){
            $totin1 += $row['QTYRET'];
            
            $totamountin += $row['AMOUNTRET'];
	  ?>
      <td align="right"><?= $row['QTYRET']?></td>
      <td align="right">-</td>
      <td align="right">-</td>
      <td align="right"><?= fCurrency($row['AMOUNTRET'])?></td>
      <?php } else {?>
      <td align="right">-</td>
      <td align="right">-</td>
      <td align="right">-</td>
      <td align="right">-</td>
      <?php }?>
      
      <?php 
        if($row['RETTYPE'] == 'T'){
            $totout1 += $row['QTYRET'];
            
            $totamountout += $row['AMOUNTRET'];
      ?>
      <td align="right"><?= $row['QTYRET']?></td>
      <td align="right">-</td>
      <td align="right">-</td>
      <td align="right"><?= fCurrency($row['AMOUNTRET'])?></td>
      <?php } else {?>
      <td align="right">-</td>
      <td align="right">-</td>
      <td align="right">-</td>
      <td align="right">-</td>
      <?php }?>
    </tr>
    <?php endforeach;?>
    <?php endforeach;?> 
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td align="right"><b>
        <?= fCurrency($totin1) ?>
      </b></td>
      <td align="right"><b>
        <?= fCurrency($totin2) ?>
      </b></td>
      <td align="right"><b>
        <?= fCurrency($totin3) ?>
      </b></td>
      <td align="right"><b>
        <?= fCurrency($totamountin) ?>
      </b></td>
      <td align="right"><b>
        <?= fCurrency($totout1) ?>
      </b></td>
      <td align="right"><b>
        <?= fCurrency($totout2) ?>
      </b></td>
      <td align="right"><b>
        <?= fCurrency($totout3) ?>
      </b></td>
      <td align="right"><b>
        <?= fCurrency($totamountout) ?>
      </b></td>
    </tr>
  </table>
</div>
</div>
</body>
</html>
