<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >    
    <h1><?php echo @$page_title?></h1>
    
    <form id="form" action="" class="uniform" method="post">
        <fieldset>
            <legend><?php echo @$page_title?></legend>
            <dl class="inline">
                <dt><label for="wrcode">Gudang / Warehouse</label></dt>
                <dd>
					<?php if($idbeli){$disabled = "disabled";echo "<input type='hidden' name='iddbeli' value='$idbeli'/>";}?>
                    <?= fSelect('data[wrcode]',array('style'=>'width:200px',$disabled=>$disabled),$warehouse,$row['WRCODE'])?>
                </dd>
                
                <dt>
		          <label for="tglbeli">Tanggal Beli</label>
		        </dt>
                <dd>
                    <input type="text" id="tglbeli" name="data[tglbeli]" class="date" autocomplete="off" value="<?php echo !empty($row['TGLBELI']) ? date('d-m-Y',strtotime($row['TGLBELI'])) : date('d-m-Y')?>" size="25" />
                </dd>
                
                <dt>
                  <label for="totalbeli">Total Pembelian</label>
                </dt>
                <dd>
                    <input type="text" id="totalbeli" name="data[totalbeli]" autocomplete="off" value="<?php echo fCurrency(@$row['TOTALBELI'])?>" size="15" disabled="disabled"/>
                </dd>

                <div class="buttons">
                    <?php if ( !$idbeli and !$isopname ) {?>
                        <button name="save" class="button green" onclick="Submit()">Simpan</button>
                    <?php }?>
                    <a href="<?php echo $this->config->item('base_url')?>pembelian" class="button white" >Kembali</a>
                </div>
        </fieldset>
				
		<?php if ( $idbeli ):?>
		<fieldset>
			<legend>Detail Barang</legend>
			<dl class="inline">
				<dt><label for="pcode">Nama Barang</label></dt>
                <dd>
                    <?= fSelect('data[pcode]',array(),$produk,$row['PCODE'])?>
                </dd>
				
				<dt><label for="qtybeli">Quantity1</label></dt>
                <dd>
                    <input type="text" id="qtybeli1" name="data[qtybeli1]" autocomplete="off" value="<?php echo fCurrency(@$row['QTYBELI1'])?>" size="15" />
                </dd>
                
                <dt><label for="qtybeli">Quantity2</label></dt>
                <dd>
                    <input type="text" id="qtybeli2" name="data[qtybeli2]" autocomplete="off" value="<?php echo fCurrency(@$row['QTYBELI2'])?>" size="15" />
                </dd>
                
                <dt><label for="qtybeli">Quantity3</label></dt>
                <dd>
                    <input type="text" id="qtybeli3" name="data[qtybeli3]" autocomplete="off" value="<?php echo fCurrency(@$row['QTYBELI3'])?>" size="15" />
                </dd>
				
				<!--<dt><label for="jmlbeli">Sub Total</label></dt>
                <dd>
                    <input type="text" id="jmlbeli" name="data[jmlbeli]" autocomplete="off" value="<?php echo fCurrency(@$row['JMLBELI'])?>" size="15" />
                </dd>-->
                
                <div class="buttons">
                    <?php if( $row['POSTSTATUS'] == 0 and !$isopname ) {?>
                        <button name="save" class="button green" onclick="Submit()">Simpan</button>
                    <?php }?>
                </div>
		</fieldset>
		<?php endif;?>
        <?php if ( $idbeli ):?>
        <table id="table1" class="gtable sortable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>PCODE</th>
                    <th>Nama Barang</th>
                    <th>Quantity</th>
                    <th>Jumlah</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $i=0;
                $total=0;
                foreach ( $detail as $rec ):
                $total += $rec['JMLBELI'];
                ?>
                <tr>
                    <td><?= ++$i?></td>
                    <td><?= $rec['PCODE']?></td>
                    <td><?= $rec['PNAME']?></td>
                    <td><?= fCurrency($rec['QTYBELI'])?></td>
                    <td><?= fCurrency($rec['JMLBELI'])?></td>
                    <td>
                        <a href="<?php echo base_url().$cpanel_dir."pembelian/delete_detail/$idbeli/$rec[IDDBELI]"?>" title="Delete" onclick="return confirm('Apakah yakin hapus data ini?');"><div class="btn-delete"></div></a>
					</td>
                </tr>
                <?php endforeach;?>
                <tr>
                    <td colspan="4" align="right"><b>TOTAL</b></td>
                    <td><?= fCurrency($total)?></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="6" align="center">
                        <?php if( $row['POSTSTATUS'] == 0 and !$isopname ) {?>
                            <button name="sahkan" type="submit" class="button green">Sahkan</button>
                        <?php }?>
                    </td>
                </tr>
            </tbody>
        </table>
      <?php endif;?>
    </form>
</article>