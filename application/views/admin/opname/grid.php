<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >	
	<h1><?php echo @$page_title?></h1>
	
	<form id="form" action="<?php echo base_url().$cpanel_dir.'opname'?>" method="post">
		<fieldset>
            <legend><?php echo @$page_title?></legend> 
                <dl class="inline">
                <dt><label for="wrcode">Opname</label></dt>
                <dd>
                    <input type="radio" value="1" name="status" <?= ($isopname) ? "checked" : ""?>> Ya
                    <input type="radio" value="0" name="status" <?= (!$isopname) ? "checked" : ""?>> Tidak
                </dd>
                <dt></dt>
                <dd>
                    <input type="submit" name="opname" value="Simpan" class="button green">
                </dd>
        </fieldset>
        
        <div class="tablefooter clearfix">
            <div class="actions">
                <b>Gudang / Warehouse :</b>
                <?= fSelect('wrcode',array('style'=>'width:200px'), $warehouse, $wrcode, '-- Pilih Gudang --')?>
                <?= fPaging($page,$limit,$rows['total'],array('name'=>'page'))?>
                <input type="submit" class="button blue" value="Filter">
            </div>
        </div>
        
		<br />
		<table id="table1" class="gtable">
			<thead>
				<tr>
				  <th rowspan="2" style="vertical-align:middle">No</th>
				  <th rowspan="2" style="vertical-align:middle">Warehouse</th>
				  <th rowspan="2" style="vertical-align:middle">Product</th>
				  <th style="text-align:center" colspan="4">Qty Stock</th>
				  <th style="text-align: center;" colspan="4">Total Harga</th>
				  <th rowspan="2" style="text-align:center">Qty Penyesuaian</th>
			  </tr>
			  <tr>
					<th>Qty Awal</th>
                    <th>Qty In</th>
                    <th>Qty Out</th>
                    <th>Qty Akhir</th>
                    <th>Saldo Awal</th>
                    <th>Debit</th>
                    <th>Kredit</th>
					<th>Saldo Akhir</th>
			  </tr>
			</thead>
			<tbody>
				<?php
                if(isset($rows['data'])): 
                $i=0;
                $day = get_hari_all();
                foreach($rows['data'] as $row): 
                ?>
				<tr>
					<td width="1%"><?php echo ++$i; ?></td>
					<td><?php echo $row['WRNAME']?></td>
                    <td><?php echo $row['PNAME']?></td>
                    <td><?php echo $row['QTYSTART']?></td>
                    <td><?php echo $row['QTYIN']?></td>
                    <td><?php echo $row['QTYOUT']?></td>
                    <td>
                        <label class="available">
                        <?php echo $row['STOCK']?>
                        <input type="hidden" name="stock[<?= $row['PCODE']?>][qty1]" value="<?= $row['STOCK']?>">
                        </label>
                    </td>
                    <td><?php echo fCurrency($row['SALDOSTART'])?></td>
                    <td><?php echo fCurrency($row['DEBIT'])?></td>
                    <td><?php echo fCurrency($row['KREDIT'])?></td>
                    <td>
                        <label class="available">
                        <?php echo fCurrency($row['SALDOEND'])?>
                        </label>
                    </td>
					<td align="center"><input type="text" style="text-align: right;width: 50px;" class="gridinput" id="data[<?= $row['PCODE']?>][qty1]" name="data[<?= $row['PCODE']?>][qty1]"></td>
				</tr>
				<?php endforeach; endif; ?>
			</tbody>
		</table>
<?php if ( $isopname ) {?>
        <div style="text-align: center;margin-top: 5px;">
            <input type="submit" name="save" value="Simpan" class="button green">
        </div>
        <?php }?>
  </form>
</article>
<script>
    jQuery(document).ready(function(){
        jQuery(".gridinput").removeAttr('disabled');
    });
</script>