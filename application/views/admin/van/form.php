<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>  
<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >    
    <h1><?php echo @$page_title?></h1>
    
    <form id="form" action="" class="uniform" method="post">
        <fieldset>
            <legend><?php echo @$page_title?></legend>
            <dl class="inline">
				<dt><label for="vancode">Kode Van</label></dt>
                <dd>
                    <input type="text" id="vancode" name="data[vancode]" autocomplete="off" value="<?php echo @$row['VANCODE']?>" size="40" />
                </dd>
				
				<dt><label for="vanname">Nama van</label></dt>
                <dd>
                    <input type="text" id="vanname" name="data[vanname]" autocomplete="off" value="<?php echo @$row['VANNAME']?>" size="50" />
                </dd>
				<?php if($c_create){?>
                <div class="buttons">
                    <button name="save" class="button green" onclick="Submit()">Simpan</button>
                    <a href="<?php echo $this->config->item('base_url')?>van" class="button white" >Batal</a>
                </div>
                <?php }?>
        </fieldset>
    </form>
</article>