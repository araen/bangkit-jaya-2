<style>
.label{}
</style>
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/default/js/jquery.xautox.js"></script>

<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >    
    <h1><?php echo @$page_title?></h1>
    
    <?php if( $isopname ) {?>
        <div class="alert">Tidak dapat melakukan transaksi vanload karena sedang dilakukan proses opname</div>
    <?php }?>
    
    <?= $transaction_message?>
    
    <form id="form" action="" class="uniform" method="post">
        <fieldset>
            <legend><?php echo @$page_title?></legend>
            <table width="100%" border="0" class="form">
                <tr>
                    <td class="label"><label for="vancode">Kode Van</label></td>
                    <td>
                        <?php if($idtvan){$disabled = "disabled";}?>
                        <?= fInputText('van_format',array('style'=>'width:200px','class'=>'validate[required]','readonly'=>'readonly',$disabled=>$disabled),$van[$row['VANCODE']])?>
                        <input type="hidden" id="data[vancode]" name="data[vancode]" value="<?= $row['VANCODE']?>" class="van" />
                        <a class="nyromodal" href="<?= base_url()."ajax/load_van"?>"><img src="<?= base_url()."templates/admin/default/images/icons/information-octagon.png"?>" /></a>
                    </td>
                    <td class="label">Sales</td>
                    <td>
                        <?php if($idtvan){$disabled = "disabled";}?>
                        <?= fInputText('sales_format',array('style'=>'width:200px','class'=>'validate[required]','readonly'=>'readonly',$disabled=>$disabled),$sales[$row['SLSNO']])?>
                        <input type="hidden" id="data[slsno]" name="data[slsno]" value="<?= $row['SLSNO']?>" class="sales" />
                        <a class="nyromodal" href="<?= base_url()."ajax/load_sales/CVS"?>"><img src="<?= base_url()."templates/admin/default/images/icons/information-octagon.png"?>" /></a>
                    </td>
                </tr>
                <tr>
                    <td class="label">Gudang</td>
                    <td>
                        <?php if($idtvan){$disabled = "disabled";}?>
                        <?= fInputText('warehouse_format',array('style'=>'width:200px','class'=>'validate[required]','readonly'=>'readonly',$disabled=>$disabled),$warehouse[$row['WRCODE']])?>
                        <input type="hidden" id="data[wrcode]" name="data[wrcode]" value="<?= $row['WRCODE']?>" class="warehouse" />
                        <a class="nyromodal" href="<?= base_url()."ajax/load_warehouse"?>"><img src="<?= base_url()."templates/admin/default/images/icons/information-octagon.png"?>" /></a>
                    </td>
                    <td class="label"><label for="vanname">Tanggal Masuk</label></td>
                    <td><input type="text" class="date" id="dayout" name="data[dayin]" autocomplete="off" value="<?php echo !empty($row['DAYIN']) ? date('d-m-Y',strtotime($row['DAYIN'])) : date('d-m-Y', strtotime($setting['WRDATE']))?>" size="50" /></td>
                </tr>
                <tr>
                <td colspan="4">
                    <div class="buttons">
                        <?php if(empty($idtvan) and !$isopname and $c_create) {?>
                        <button name="save" class="button green" onclick="Submit()">Simpan</button>
                        <?php }?>
                        <a href="<?php echo base_url()?>vanload" class="button white" >Kembali</a>
                    </div>
                </td>
                </tr>
            </table>
        </fieldset>
        
        <?php if ( $idtvan ):?>
        <fieldset>
            <legend>Detail Barang</legend>
            <dl class="inline">
                <dt><label for="pcode">Nama Barang</label></dt>
                <dd>
                    <?= fInputText('product_format',array('style'=>'width:200px','readonly'=>'readonly'))?>
                    <input type="hidden" id="data[pcode]" name="data[pcode]" class="product" >
                    <a class="nyromodal" href="<?= base_url()."ajax/load_product_van/$row[VANCODE]"?>"><img src="<?= base_url()."templates/admin/default/images/icons/information-octagon.png"?>"></a>
                </dd>
                
                <dt><label for="qtybeli">Jumlah Stok</label></dt>
                <dd>
                    <input type="text" id="qty1" name="data[vstock1]" autocomplete="off" size="15" />
                    <input type="text" id="qty2" name="data[vstock2]" autocomplete="off" size="15" />
                    <input type="text" id="qty3" name="data[vstock3]" autocomplete="off" size="15" />
                </dd>
                <?php if($row['POSTSTATUS'] == 0){?>
                <div class="buttons">
                    <button name="save" class="button green" onclick="Submit()">Simpan</button>
                </div>
                <?php }?>
        </fieldset>
        <?php endif;?>
        
        <?php if ( $idtvan ):?>
        <table id="table1" class="gtable sortable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>PCODE</th>
                    <th>Nama Barang</th>
                    <th>Qty1</th>
                    <th>Qty2</th>
                    <th>Qty3</th>
                    <th>Jumlah</th>
                    <?php if( $row['OSTATUSCODE'] == 2 ) {?>
                    <th>Stock</th>
                    <th>Aksi</th>
                    <?php }?>
                </tr>
            </thead>
            <tbody>
                <?php 
                $i=0;
                $total=0;
                $outstock = 0;
                foreach ( $detail as $rec ):
                    $outstock = ( $rec['QTY'] > $rec['STOCK'] ) ? ++$outstock : $outstock;
                    $total += $rec['AMOUNT'];
                    
                    $conv = rekonversi($konversi, $rec['PCODE'], $rec['QTY']);
                ?>
                <tr>
                    <td><?= ++$i?></td>
                    <td><?= $rec['PCODE']?></td>
                    <td><?= $rec['PNAME']?></td>
                    <td><?= $conv['QTY1']?></td>
                    <td><?= $conv['QTY2']?></td>
                    <td><?= $conv['QTY3']?></td>
                    <td><?= fCurrency($rec['AMOUNT'])?></td>
                    <?php if( $row['OSTATUSCODE'] == 2 ){?>
                    <td>
                        <?php if ( $outstock == 0 ) {?>
                            <label class="available">Available</label>
                        <?php } else {?>
                            <label class="danger">Out of Stock</label>
                        <?php }?>
                    </td>
                    <td>
                        <?php if($c_delete){?>
                        <a href="<?php echo "$cls/delete_detail/$idtvan/$rec[IDDTVAN]"?>" title="Delete" onclick="return confirm('Apakah yakin hapus data ini?');"><div class="btn-delete"></div></a>
                        <?php }?>
                    </td>
                    <?php }?>
                </tr>
                <?php endforeach;?>
                <tr>
                    <td colspan="<?= ( $row['OSTATUSCODE'] == 2 ) ? 7 : 6;?>" align="right"><b>TOTAL</b></td>
                    <td colspan="2"><b><?= fCurrency($total)?><input type="hidden" name="total" value="<?= $total?>"></b></td>
                </tr>
                <tr>
                    <td colspan="<?= ( $row['OSTATUSCODE'] == 2 ) ? 9 : 7;?>" align="center">
                        <?php if ( $outstock == 0 and !$isOpname ) {?>
                        <?php if( $row['OSTATUSCODE'] == 2 ) {?>
                            <button name="proses" type="submit" class="button green">Proses</button>
                        <?php }
                        }?>
                    </td>
                </tr>
            </tbody>
        </table>
      <?php endif;?>
    </form>
</article>

<script>
    jQuery(document).ready(function(){
        jQuery("#vanid_format").xautox('<?= base_url()."ajax/van"?>', {targetid: "vanid"});    
        jQuery("#wrcode_format").xautox('<?= base_url()."ajax/warehouse"?>', {targetid: "wrcode"});    
        jQuery("#pcode_format").xautox('<?= base_url()."ajax/product"?>', {targetid: "pcode"});    
    });
</script>