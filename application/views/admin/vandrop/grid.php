<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >	
	<h1><?php echo @$page_title?></h1>
	
	<form id="form" action="<?php echo base_url().$cpanel_dir.'vanload'?>" method="post">
		<div class="tablefooter clearfix">
			<div class="actions">
				<b>Cari :</b>
				<input type="text" name="q" id="q" value="<?php echo $q ?>" autocomplete="off" />
                <?php echo str_repeat("&nbsp;",3)?>
                <b>Page :</b>
				<?php echo fPaging($page,$limit,$rows['total'],array('name'=>'page','onchange'=>'jQuery("#form").submit()'))?>
				<?php echo str_repeat("&nbsp;",3)?>
				<input type="submit" class="button blue" value="Filter">
				<?php echo str_repeat("&nbsp;",3)?>
                <?php if ($c_create){?>
				<a href="<?php echo base_url() ?>vandrop/form" class="button green" >Tambah</a>
                <?php }?>
			</div>
		</div>
		<br />
		<table id="table1" class="gtable sortable">
			<thead>
				<tr>
                    <th>No</th>
                    <th>Nama Van</th>
                    <th>Gudang</th>
                    <th>Tgl Masuk</th>
                    <th>Total Drop</th>
                    <th>Status</th>
                    <th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php
                if(isset($rows['data'])): 
                $i=0;
                foreach($rows['data'] as $row): ?>
				<tr>
                    <td width="1%"><?php echo ++$i?></td>
                    <td><?php echo $row['VANNAME']?></td>
                    <td><?php echo $row['WRNAME']?></td>
                    <td><?php echo !empty($row['DAYIN']) ? dateIndo($row['DAYIN']) : dateIndo(date('Y-m-d'))?></td>
                    <td><?php echo fCurrency($row['SUBTOTAL'])?></td>
                    <td>
                        <?php if ( $row['OSTATUSCODE'] == 2 ) {?>
                            <label class="warning">Diajukan</label>
                        <?php }elseif ( $row['OSTATUSCODE'] == 3 ) {?>
                            <label class="available">Diproses</label>
                        <?php }?>
                    </td>
                    <td width="10%">
                        <?php if($c_update){?>
                        <a href="<?php echo base_url()."vandrop/form/$row[IDTVAN]"?>" title="Edit" ><div class="btn-edit"></div></a>
                        <?php }?>
                        <?php if ( $row['OSTATUSCODE'] == 2 and $c_delete) {?>
                        <a href="<?php echo base_url()."vandrop/delete/$row[IDTVAN]"?>" title="Delete" onclick="return confirm('Apakah yakin hapus data ini?');"><div class="btn-delete"></div></a>                    </td>
                        <?php }?>
                </tr>
				<?php endforeach; endif; ?>
			</tbody>
		</table>
  </form>
</article>
