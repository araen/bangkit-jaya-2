<?php $cpanel_dir = $this->config->item('cpanel_dir');?>  
<article >	
	<h1>Users</h1>
	
	<form id="form" action="<?php echo base_url().$cpanel_dir.'user'?>" method="post">
		<div class="tablefooter clearfix">
			<div class="actions">
				<b>Cari :</b>
				<input type="text" name="q" id='q' value="<?php echo $q ?>" autocomplete="off" />
				<?php echo str_repeat("&nbsp;",3)?>
				<b>Page :</b>
				<select name="page" onChange="$('#form').submit()">
					<?php
						$total = $user['total'];
						$totpage=0;
						if(($total%$limit) == 0)
							$totpage=$total/$limit;
						else
							$totpage=(int)($total/$limit) + 1;
						
						echo "<option value='1' ".(1 == $page?'selected="selected"':'')." >&nbsp;1</option>";
						for($i=2;$i<=$totpage;$i++){
							echo "<option value='$i' ".($i == $page?'selected="selected"':'')." >&nbsp;$i</option>";
						}
					?>
				</select>
				<?php echo str_repeat("&nbsp;",3)?>
				<input type="submit" class="button blue" value="Filter">
				<?php echo str_repeat("&nbsp;",3)?>
				<?php if($c_create){?>
                <a href="<?php echo base_url().$cpanel_dir?>user/form" class="button green" >Tambah</a>
                <?php }?>
			</div>
			<div class="pagination">
				<?php echo "<b>RECORD ".($page*$limit-$limit+1)." - ".($page==$totpage?$total:$page*$limit)." FROM $total</b>";?>
			</div>
		</div>
		<br />
		<table id="table1" class="gtable sortable">
			<thead>
				<tr>
					<th>ID</th>
					<th>Name</th>
                    <th>Full Name</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php 
                $i=0;
                foreach($user['data'] as $row){ ?>
				<tr>
					<td><?php echo ++$i?></td>
                    <td><a href="<?php echo base_url().$cpanel_dir."user/form/$row[ID]"?>" title="Edit" ><?php echo $row['USNAME']?></a></td>
					<td><?php echo $row['FULLNAME']?></td>
					<td>    
                        <?php if($c_update){?>
						<a href="<?php echo base_url().$cpanel_dir."user/form/$row[ID]"?>" title="Edit" ><div class="btn-edit"></div></a>
						<?php }?>
                        <?php if($c_delete){?>
                        <a href="<?php echo base_url().$cpanel_dir."user/delete/$row[ID]"?>" onClick="return confirm('Data akan dihapus. Lanjutkan ?')"title="Delete"><div class="btn-delete"></div></a>
					    <?php }?>
                    </td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</form>
</article>
