<?php $cpanel_dir = $this->config->item('cpanel_dir');?>  
<article >	
	<h1>Form User</h1>
    <?= $transaction_message?>
	<form id="form" class="uniform" method="post">
		<fieldset>
			<legend>User</legend>
			<dl class="inline">
				<dt><label for="usname">Username</label></dt>
                <dd><input type="text" id="usname" name="data[usname]" class="medium validate[required]" value="<?php echo @$user['USNAME']?>" size="50" /></dd>
                
                <dt><label for="usname">Full Name</label></dt>
				<dd><input type="text" id="fullname" name="data[fullname]" class="medium validate[required]" value="<?php echo @$user['FULLNAME']?>" size="50" /></dd>
				
				<dt><label for="uspassword">Password</label></dt>
				<dd><input type="password" id="uspassword" name="data[uspassword]" class="medium <?php echo ($id==0)?"validate[required]":""?>" /></dd>
				
				<dt><label for="uspassword">Confirm Password<label></dt>
				<dd><input type="password" id="passwordconfirm" name="data[passwordconfirm]" class="medium <?php echo ($id==0)?"validate[required]":""?>" /></dd>
				
				<!--<dt><label for="cdlevel">CD Level</label></dt>
                <dd><input type="text" id="cdlevel" name="data[cdlevel]" class="medium validate[required]" value="<?php echo @$user['CDLEVEL']?>" /></dd>
				
				<dt><label for="code">Code</label></dt>
                <dd><input type="text" id="code" name="data[code]" class="medium validate[required]" value="<?php echo @$user['CODE']?>" /></dd>-->
			</dl>
            <?php if($c_create){?>
			<div class="buttons">
				<button type="submit" name="save" class="button green">Save</button>
				<a href="<?php echo base_url().$cpanel_dir;?>user" class="button white" >Cancel</a>
			</div>
            <?php }?>
		</fieldset>
	</form>
</article>

<script>
$ = jQuery;
$(document).ready(function(){
	$("#form").validationEngine();
});
</script>