<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >	
	<h1><?php echo @$page_title?></h1>
	
	<form id="form" action="<?php echo base_url().$cpanel_dir.'investor'?>" method="post">
		<div class="tablefooter clearfix">
			<div class="actions">
				<b>Search :</b>
				<input type="text" name="q" id="q" value="<?php echo $q ?>" autocomplete="off" />
                <?php echo str_repeat("&nbsp;",3)?>
                <b>Page :</b>
				<?php echo fPaging($page,$limit,$rows['total'],array('name'=>'page','onchange'=>'jQuery("#form").submit()'))?>
				<?php echo str_repeat("&nbsp;",3)?>
				<input type="submit" class="button blue" value="Apply">
				<?php echo str_repeat("&nbsp;",3)?>
				<a href="<?php echo base_url().$cpanel_dir?>investor/form" class="button green" >ADD</a>
			</div>
		</div>
		<br />
		<table id="table1" class="gtable sortable">
			<thead>
				<tr>
					<th>NO</th>
                    <th>Nama</th>
                    <th>Instansi</th>
                    <th>No HP</th>
                    <th>Email</th>
                    <th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php
                if(isset($rows['data'])): 
                $i=0;
                $day = get_hari_all();
                foreach($rows['data'] as $row): ?>
				<tr>
					<td width="1%"><?php echo ++$i?></td>
                    <td><?= $row['name']?></td>
                    <td><?= $row['instansi']?></td>
                    <td><?= $row['no_hp']?></td>
                    <td><?= $row['email']?></td>
                    <td width="10%">
                        <a href="<?php echo base_url().$cpanel_dir."investor/form/$row[id]"?>" title="Edit" ><div class="btn-edit"></div></a>
                        <a href="<?php echo base_url().$cpanel_dir."investor/delete/$row[id]"?>" title="Delete" onclick="return confirm('Apakah yakin hapus data ini?');"><div class="btn-delete"></div></a>
                    </td>
				</tr>
				<?php endforeach; endif; ?>
			</tbody>
		</table>
	</form>
</article>
