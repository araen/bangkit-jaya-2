<?php $cpanel_dir = $this->config->item('cpanel_dir');?>  
<article >	
	<h1>Form Role</h1>
    <?= $transaction_message?>
	<form id="form" class="uniform" method="post">
		<fieldset>
			<legend>Role</legend>
			<dl class="inline">
				<dt><label for="usname">Nama Role</label></dt>
                <dd><input type="text" id="nama" name="data[nama]" class="medium validate[required]" value="<?php echo @$row['NAMA']?>" size="40" /></dd>
			</dl>
			<div class="buttons">
				<?php if ( empty($idrole) and $c_create ) {?>
                <button type="submit" name="save" class="button green">Save</button>
				<?php }?>
                <a href="<?php echo $this->config->item('base_url')?>role" class="button white" >Kembali</a>
			</div>
		</fieldset>
        <br>
        <?php if ( !empty($idrole) ) {?>
        <fieldset>
            <legend>User</legend>
            <dl class="inline">
                <dt><label for="usname">Nama User</label></dt>
                <dd>
                    <?= fInputText('user_format',array('style'=>'width:200px','readonly'=>'readonly','class'=>'validate[required]'))?>
                    <input type="hidden" id="detail[iduser]" name="detail[iduser]" class="user" >
                    <a class="nyromodal" href="<?= base_url()."ajax/load_user"?>"><img src="<?= base_url()."templates/admin/default/images/icons/information-octagon.png"?>"></a>
                </dd>
            </dl>
            <div class="buttons">
                <button type="submit" name="save" class="button green">Save</button>
            </div>
        </fieldset>
        <?php }?>
        <?php if (!empty($idrole)){?>
            <table id="table1" class="gtable sortable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>User Name</th>
                        <th>Full Name</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $i = 0;
                    foreach ( $detail as $rec ) {?>
                    <tr>
                        <td><?= ++$i?></td>
                        <td><?= $rec['USNAME']?></td>
                        <td><?= $rec['FULLNAME']?></td>
                        <td>
                            <?php if ($c_delete){?>
                            <a href="<?php echo "$cls/delete_detail/$idrole/$rec[ID]"?>" title="Delete" onclick="return confirm('Apakah yakin hapus data ini?');"><div class="btn-delete"></div></a>
                            <?php }?>
                        </td>
                    </tr>
                    <?php }?>
                </tbody>
            </table>
        <?php }?>
	</form>
</article>

<script>
$ = jQuery;
$(document).ready(function(){
	$("#form").validationEngine();
});
</script>