<style type="text/css">
    div#menugrid{float: left;}
    div#menuform{float: right;} 
    div#cleared {clear: both;}
    table#tbform td{padding:5px}
</style>
<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >	
	<h1><?php echo @$page_title?></h1>
	
	<form id="form" action="<?php echo base_url().$cpanel_dir."role/access/$idrole"?>" method="post">
        <?= $transaction_message?>
		<br />
		<table id="table1" class="gtable">
			<thead>
				<tr>
					<th>Kode</th>
                    <th>Menu</th>
                    <th>Read</th>
                    <th>Add</th>
                    <th>Update</th>
					<th>Delete</th>
				</tr>
			</thead>
			<tbody>
				<?php
                if(isset($rows)): 
                $i=0;
                foreach($rows as $key => $row): ?>
				<tr>
					<td width="1%"><?php echo $key?></td>
                    <td><a id="menulink"><?php echo "<b>".$row['NAME']."</b>"?></a></td>
					<td colspan="4"></td>
				</tr>                  
                <?php
                $i = 0; 
                $pk = null;
                $a_key = array_keys($row['SUB']);
                foreach ( $row['SUB'] as $k => $rec ){?>
                    <tr id="child-<?= $key?>" class="menusub">
                        <td width="1%"><?php echo str_repeat("&nbsp;", 5).$k?></td>
                        <td><?php echo str_repeat("&nbsp;", 5).$rec['NAME']?></td>
                        <td><?= fInputCheck("access[$k][ISREAD]",array('value'=>'1', 'class'=>"read-$key"),$access[$k]['ISREAD'])?></td>
                        <td><?= fInputCheck("access[$k][ISCREATE]",array('value'=>'1', 'class'=>"create-$key"),$access[$k]['ISCREATE'])?></td>
                        <td><?= fInputCheck("access[$k][ISUPDATE]",array('value'=>'1', 'class'=>"update-$key"),$access[$k]['ISUPDATE'])?></td>
                        <td><?= fInputCheck("access[$k][ISDELETE]",array('value'=>'1', 'class'=>"delete-$key"),$access[$k]['ISDELETE'])?></td>
                    </tr>    
                <?php 
                    $pk = $k;
                    $i++;
                }?>
				<?php endforeach; endif; ?>
			</tbody>
		</table>
        <div class="buttons">
            <?php if($c_create){?>
            <input type="submit" class="button green" name="submit" value="Simpan" />
            <?php }?>
            <a href="<?php echo $this->config->item('base_url')?>role" class="button white" >Kembali</a>
        </div>
	</form>
</article>
<script type="text/javascript">
    $ = jQuery;
</script>