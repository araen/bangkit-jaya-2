<script type="text/javascript" src="<?php echo base_url()?>templates/admin/default/js/jquery.xautox.js"></script>    
<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >    
    <h1><?php echo @$page_title?></h1>
    
    <?php if( $isopname ) {?>
        <div class="alert">Tidak dapat melakukan transaksi pembelian karena sedang dilakukan proses opname</div>
    <?php }?>
    
    <?= $transaction_message?>
    
    <form id="form" action="" class="uniform" method="post">
        <fieldset>
            <legend><?php echo @$page_title?></legend>
            <dl class="inline">
                <dt><label for="invno">Nomor Invoice</label></dt>
                <dd>
                    <input type="hidden" name="data[invno]" value="<?php echo @$row['INVNO']?>"/>
                    <input type="text" autocomplete="off" value="<?php echo @$row['INVNO']?>" size="15" disabled="disabled" style="width: 200px;"/>
                </dd>
                
                <dt><label for="invno">Tgl Invoice</label></dt>
                <dd>
                    <input type="text" autocomplete="off" value="<?php echo date('d-m-Y',strtotime($row['INVDATE']))?>" size="15" disabled="disabled" style="width: 200px;"/>
                </dd>
                
                <dt><label for="invno">Jml Tagihan</label></dt>
                <dd>
                    <input type="text" autocomplete="off" value="<?php echo fCurrency($row['TAGIHAN'])?>" size="15" disabled="disabled" style="width: 200px;"/>
                </dd>
                
                <div class="buttons">
                    <a href="<?php echo $cls?>" class="button white" >Kembali</a>
                </div>
        </fieldset>
        
        <fieldset>
            <legend>Detail Transaksi Order</legend>
            <table id="table1" class="gtable sortable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>No Order</th>
                        <th>Gudang</th>
                        <th>Sales</th>
                        <th>Jumlah</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $i=0;
                    $total=0;
                    foreach ( $order as $rec ):
                        $total += $rec['SUBTOTAL'];
                    ?>
                    <tr>
                        <td><?= ++$i?></td>
                        <td><?= $rec['ORDERNO']?></td>
                        <td><?= $warehouse[$rec['WRCODE']]?></td>
                        <td><?= $sales[$rec['SLSNO']]?></td>
                        <td><?= fCurrency($rec['SUBTOTAL'])?></td>
                    </tr>
                    <?php endforeach;?>
                    <tr>
                        <td colspan="4" align="right"><b>TOTAL</b></td>
                        <td><b><?= fCurrency($total)?></b></td>
                    </tr>
                </tbody>
            </table>
        </fieldset>
        
        <?php if ( $addpayment ){?>
        <fieldset>
            <legend>Pembayaran</legend>
            <dl class="inline paymentform">
                <dt><label id="amount" for="amount">Jumlah Bayar</label></dt>
                <dd>
                    <input type="text" id="amount" name="data[amount]" style="width: 200px;text-align: right;" autocomplete="off" >
                </dd>
            <?php if($c_create){?>    
            <div class="buttons">
                <button name="save" class="button green" onclick="Submit()">Simpan</button>
            </div>
            <?php }?>
        </fieldset>
        <?php }?>
        
        <fieldset>
            <legend>Detail Pembayaran</legend>
            <table id="table1" class="gtable sortable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>No Pembayaran</th>
                        <th>Tgl Pembayaran</th>
                        <th>Jumlah</th>
                        <?php if ( $addpayment ){?>
                        <th></th>
                        <?php }?>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $i=0;
                    $total=0;
                    foreach ( $payment as $rec ):
                        $total += $rec['AMOUNT'];
                    ?>
                    <tr>
                        <td><?= ++$i?></td>
                        <td><?= $rec['PAYNO']?></td>
                        <td><?= date('d-m-Y', strtotime($rec['PAYDATE']))?></td>
                        <td><?= fCurrency($rec['AMOUNT'])?></td>
                        <?php if ( $addpayment ){?> 
                        <td>
                            <?php if ( $setting['WRDATE'] == date('Y-m-d') ) { ?>    
                            <a href="javascript:void(0)" title="Edit" onclick="goEdit('<?= $rec['IDPAYMENT']?>','<?= $rec['PAYNO']?>',<?= $rec['AMOUNT']?>);return false;"><div class="btn-edit"></div></a>
                            <a href="<?php echo "$cls/delete_detail/$row[INVNO]/$rec[IDPAYMENT]"?>" title="Delete" onclick="return confirm('Apakah yakin hapus data ini?');"><div class="btn-delete"></div></a>
                            <?php }?>
                        </td>
                        <?php }?>
                    </tr>
                    <?php endforeach;?>
                    <tr>
                        <td colspan="3" align="right"><b>TOTAL</b></td>
                        <td colspan="2"><b><?= fCurrency($total)?></b></td>
                    </tr>
                    <?php if (($row['TAGIHAN'] - $total) > 0){?>
                    <tr>
                        <td colspan="3" align="right"><b>Kurang Bayar</b></td>
                        <td colspan="2" style="color: #FF0000;"><b><?= fCurrency($row['TAGIHAN'] - $total)?></b></td>
                    </tr>
                    <?php }?>
                </tbody>
            </table>
        </fieldset>
    </form>
</article>
<script type="text/javascript">
    $ = jQuery;
    
    function goEdit(idpayment, payno, amount) {
        $('div#editpayment').remove();
        $('label#amount').parent().before(
            '<div id="editpayment"><dt>'+
            '<label>No Pembayaran</label>'+
            '</dt>'+
            '<dd>'+
            '<input id="idpayment" type="hidden" name="data[idpayment]" value="'+ idpayment +'"><b>'+payno+'</b>'+
            '<input id="pybefore" type="hidden" name="data[pybefore]" value="'+ amount +'">'+
            '</dd></div>'
        );
        $('input#amount').val(amount);
    }
</script>