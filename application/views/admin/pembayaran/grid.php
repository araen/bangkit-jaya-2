<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >	
	<h1><?php echo @$page_title?></h1>
	
	<form id="form" action="<?php echo $cls?>" method="post">
		<div class="tablefooter clearfix">
			<div class="actions">
				<b>Cari :</b>
				<input type="text" name="q" id="q" value="<?php echo $q ?>" autocomplete="off" />
                <?php echo str_repeat("&nbsp;",3)?>
                <b>Page :</b>
				<?php echo fPaging($page,$limit,$rows['total'],array('name'=>'page','onchange'=>'jQuery("#form").submit()'))?>
				<?php echo str_repeat("&nbsp;",3)?>
				<input type="submit" class="button blue" value="Filter">
            </div>
		</div>
		<br />
		<table id="table1" class="gtable sortable">
			<thead>
				<tr>
					<th>No Invoice</th>
					<th>Sales</th>
					<th>Outlet</th>
					<th>Tgl Transaksi</th>
                    <th>Total Tagihan</th>
                    <th>Status Pembayaran</th>
                    <th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php
                if(isset($rows['data'])): 
                $i=0;
                $day = get_hari_all();
                foreach($rows['data'] as $row): 
                    $payment = ceil(($row['PAYMENT'] / $row['TAGIHAN']) * 100);
                ?>
				<tr>
					<td width="1%"><?php echo $row['INVNO'] ?></td>
					<td><?php echo $sales[$row['SLSNO']]?></td>
					<td><?php echo $outlet[$row['OUTNO']]?></td>
					<td><?php echo dateIndo($row['ORDERDATE'])?></td>
                    <td><?php echo fCurrency($row['TAGIHAN'])?></td>
					<td align="center">
                        <?php if ( $payment == 100 ) {?>
                            <label class="available">Lunas</label>
                        <?php }else {?>
                            <label class="<?= ($payment >= 50) ? "warning" : "danger"?>"><?= $payment?> %</label>
                        <?php }?>
                    </td>
                    <td>
                        <?php if($c_update){?>
                        <a href="<?php echo "$cls/form/$row[INVNO]"?>" title="Edit" ><div class="btn-edit"></div></a>
                        <!--<a href="<?php echo base_url()."order/delete/$row[INVNO]"?>" title="Delete" onclick="return confirm('Apakah yakin hapus data ini?');"><div class="btn-delete"></div></a>-->                    
                        <?php }?>
                    </td>
				</tr>
				<?php endforeach; endif; ?>
			</tbody>
		</table>
  </form>
</article>
<script type="text/javascript">
function goCetak() {
    var smt = jQuery('#semester').val();
    var thn = jQuery('#tahun').val();
    window.open('<?php echo base_url()."$cpanel_dir/order/rekap/"?>'+smt+"/"+thn);
}
</script>