<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>  
<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >    
    <h1><?php echo @$page_title?></h1>
    
    <div id="map_canvas" style="width: 100%;height: 400px;"></div>
    <form id="form" action="" class="uniform" method="post">
        <fieldset>
            <legend><?php echo @$page_title?></legend>
            <dl class="inline">
                <dt><label for="name">Nama</label></dt>
                <dd>
                    <input type="hidden" name="ID" value="<?php echo @$row['id']?>" />
                    <input type="text" id="name" name="data[name]" autocomplete="off" value="<?php echo @$row['name']?>" size="50" />
                </dd>
                
                <dt><label for="alamat">Alamat</label></dt>
                <dd>
                    <textarea cols="50" name="data[alamat]" rows="5"><?= $row['alamat']?></textarea>
                </dd>
                
                <dt><label for="name">Ketegori</label></dt>
                <dd>
                    <?= fSelect('data[category_id]',array(),$category)?>
                </dd>
                
                <dt><label for="name">Latitude</label></dt>
                <dd>
                    <input type="text" id="latFld" readonly="readonly" name="data[latitude]" autocomplete="off" value="<?php echo @$row['latitude']?>" size="50" />
                </dd>
                
                <dt><label for="name">Longitude</label></dt>
                <dd>
                    <input type="text" id="lngFld" readonly="readonly" name="data[longitude]" autocomplete="off" value="<?php echo @$row['longitude']?>" size="50" />
                </dd>
                
                <div class="buttons">
                    <button name="save" class="button green" onclick="Submit()">Save</button>
                    <a href="<?php echo $this->config->item('base_url').$cpanel_dir?>location" class="button white" >Cancel</a>
                </div>
        </fieldset>
    </form>
</article>
<script type="text/javascript">
    var map;
    var markersArray = [];
    
    <?php
    echo "var lat = ".((!$row['latitude'])?"-7.265722535101327":$row['latitude']).";";
    echo "var lng = ".((!$row['longitude'])?"112.75206327438354":$row['longitude']).";";
    ?>

    jQuery(document).ready(function(){
        initMap();
    })
    
    function initMap()
    {
        var latlng = new google.maps.LatLng(lat, lng);
        var myOptions = {
            zoom: 14,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
        
        marker = new google.maps.Marker({
            map:map,
            draggable:true,
            animation: google.maps.Animation.DROP,
            position: latlng
        });

  
        // add a click event handler to the map object
        google.maps.event.addListener(map, "click", function(event)
        {
            // place a marker
            placeMarker(event.latLng);

            // display the lat/lng in your form's lat/lng fields
            jQuery("#latFld").val(event.latLng.lat());
            jQuery("#lngFld").val(event.latLng.lng());
        });
    }
    function placeMarker(location) {
        // first remove all markers if there are any
        deleteOverlays();

        var marker = new google.maps.Marker({
            position: location, 
            map: map
        });

        // add marker in markers array
        markersArray.push(marker);

        //map.setCenter(location);
    }

    // Deletes all markers in the array by removing references to them
    function deleteOverlays() {
        if (markersArray) {
            for (i in markersArray) {
                markersArray[i].setMap(null);
            }
        markersArray.length = 0;
        }
    }
</script>