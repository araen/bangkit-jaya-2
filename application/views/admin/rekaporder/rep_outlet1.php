<html>
    <head>
    	<title>Rekapitulasi Per Outlet</title>
        <style type="text/css">
            table{
				font-family: "Courier New", Courier, monospace;
                font-size: 11px;
				border-collapse:collapse;
			}
			table#main {
                width : 700px;
                margin: auto;
                font-family: "Courier New", Courier, monospace;
                font-size: 11px;
				border-collapse:collapse;
            }
			.header{
				border-top:1px solid;
				border-bottom:1px solid;
				text-align:center
			}
			.title{font-size:16px}
			.top{border-top : 1px solid}
        .style1 {border-top: 1px solid; font-weight: bold; }
        </style>
    </head>
    <body>
        <table id="main" width="100%" border="0">
          <tr>
            <td colspan="11" class="title"><b>REKAPITULASI PEROUTLET CV. TOPJAYA</b></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="2"><div align="right">Print Date</div></td>
            <td>:<?= date('d/m/Y H:i')?></td>
            <td>&nbsp;</td>
            <td colspan="2">Tgl Gdng: </td>
            <td colspan="2"><?= date('d/m/Y', strtotime($setting['WRDATE']))?></td>
          </tr>
          <tr>
            <td>NO. REKAP</td>
            <td>:</td>
            <td colspan="3">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>BERANGKAT PKL</td>
            <td>:</td>
            <td colspan="3"><?= date('H:i')?>              WIB</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>MOBIL</td>
            <td>:</td>
            <td colspan="3"><?= isset($van) ? implode(',',$van) : "";?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>TOTAL SALESMAN</td>
            <td>:</td>
            <td colspan="3"><?= count($sales)?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td style="vertical-align:top">SALESMAN</td>
            <td style="vertical-align:top">:</td>
            <td colspan="4"><?= isset($sales) ? implode(',',$sales) : "";?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td style="width:100px">&nbsp;</td>
            <td style="width:1px">&nbsp;</td>
            <td style="width:100px">&nbsp;</td>
            <td style="width:100px">&nbsp;</td>
            <td style="width:20px">&nbsp;</td>
            <td>&nbsp;</td>
            <td style="width:50px">&nbsp;</td>
            <td style="width:20px">&nbsp;</td>
            <td style="width:20px">&nbsp;</td>
            <td style="width:50px">&nbsp;</td>
            <td style="width:20px">&nbsp;</td>
          </tr>
          <tr>
            <td class="header">TGL FAKTUR</td>
            <td class="header">&nbsp;</td>
            <td class="header">NO ORDER</td>
            <td class="header">NO FAKTUR</td>
            <td class="header">OUTLET</td>
            <td class="header">NAMA OUTLET</td>
            <td class="header">BRUTTO</td>
            <td class="header">TPR. UANG</td>
            <td class="header">VOL DISC</td>
            <td class="header">NILAI FAKTUR</td>
            <td class="header">BIAYA KULI</td>
          </tr>
          <?php foreach ($data as $row){
		  	$total += $row['SUBTOTAL'];
		  ?>
          <tr>
            <td align="center"><?= date('d/m/Y', strtotime($row['INVDATE']))?></td>
            <td>&nbsp;</td>
            <td><?= $row['ORDERNO']?></td>
            <td><?= $row['INVNO']?></td>
            <td align="center"><?= $row['OUTNO']?></td>
            <td><?= $row['OUTLET']?></td>
            <td align="right"><?= fCurrency($row['SUBTOTAL'])?></td>
            <td align="right">0</td>
            <td align="right">0</td>
            <td align="right"><?= fCurrency($row['SUBTOTAL'])?></td>
            <td align="right">0</td>
          </tr>
          <?php }?>
          <tr>
            <td class="top">&nbsp;</td>
            <td class="top">&nbsp;</td>
            <td class="top">&nbsp;</td>
            <td class="top">&nbsp;</td>
            <td class="top">&nbsp;</td>
            <td class="top"><div align="right"><strong>Total :</strong></div></td>
            <td align="right" class="top"><strong>
            <?= fCurrency($total)?>
            </strong></td>
            <td align="right" class="style1">0</td>
            <td align="right" class="style1">0</td>
            <td align="right" class="top"><strong>
            <?= fCurrency($total)?>
            </strong></td>
            <td align="right" class="style1">0</td>
          </tr>
        </table>
</body>
</html>
 