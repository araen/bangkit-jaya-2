<html>
    <head>
    	<title>Rekapitulasi Per Outlet</title>
        <style type="text/css">
            table{
				font-family: "Courier New", Courier, monospace;
                font-size: 11px;
				border-collapse:collapse;
			}
			table#main {
                width : 700px;
                margin: auto;
                font-family: "Courier New", Courier, monospace;
                font-size: 11px;
				border-collapse:collapse;
            }
			.header{
				border-top:1px solid;
				border-bottom:1px solid;
				text-align:center
			}
			.title{font-size:16px}
			.top{border-top : 1px solid}
            .style1 {border-top: 1px solid; font-weight: bold; }
            @media print
            {
                #noprint { display: none; }
            }
            div#noprint{
                width : 900px;
                background-color : #E0E0E0;
                margin: auto;
                padding: 10px;
            }
            .button.green {
                background: #75ae5c none repeat scroll 0 0;
                border: 1px solid #3b6e22;
            }
            .button {
                background: #28a0b2 none repeat scroll 0 0;
                border: 1px solid #0d717e;
            }
            .button {
                box-shadow: 0 1px 2px rgba(255, 255, 255, 0.6) inset, 0 -5px 15px rgba(0, 0, 0, 0.3) inset, 1px 1px 1px #ccc;
            }
            .button {
                color: #fff;
                cursor: pointer;
                display: inline-block;
                font: bold 11px/110% Tahoma,sans-serif;
                margin: 0 3px 0 1px;
                outline: medium none;
                overflow: visible;
                padding: 6px 8px;
                text-align: center;
                text-shadow: 1px 1px 1px #555;
                vertical-align: baseline;
                width: auto;
            }
        </style>
    </head>
    <body>
        <div id="noprint" width="100%" style="text-align:center">
            <button class="button green" onClick="window.print()">Print</button>
        </div>
        <table id="main" width="100%" border="0">
          <tr>
            <td colspan="6" class="title"><b>REKAPITULASI PERPRODUK <?= strtoupper($setting['CORPORATE_NAME'])?></b></td>
          </tr>
          <tr>
            <td width="70">Print Date</td>
            <td colspan="2"><?= date('d/m/Y H:i')?></td>
            <td width="125">Tgl Gdng</td>
            <td colspan="2"><?= date('d/m/Y', strtotime($setting['WRDATE']))?></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="2">&nbsp;</td>
            <td>&nbsp;</td>
            <td width="56">&nbsp;</td>
            <td width="56">&nbsp;</td>
          </tr>
          <tr>
            <td>NO. REKAP</td>
            <td colspan="2">&nbsp;</td>
            <td>SALESMAN</td>
            <td><?= count($sales)?></td>
            <td>&nbsp;</td>
          </tr>
          
          <tr>
            <td colspan="3" style="vertical-align:top"><?= isset($sales) ? implode(',',$sales) : "";?></td>
            <td colspan="3" style="vertical-align:top">Tgl.
            <?= dateIndo(dGetDate($tglmulai)).' - '.dateIndo(dGetDate($tglselesai))?></td>
          </tr>
          <tr>
            <td style="width:50px">&nbsp;</td>
            <td colspan="2" style="width:100px">&nbsp;</td>
            <td style="width:100px">&nbsp;</td>
            <td style="width:100px">&nbsp;</td>
            <td style="width:100px">&nbsp;</td>
          </tr>
          <tr>
            <td rowspan="2" class="header">PCODE</td>
            <td colspan="2" rowspan="2" class="header">NAMA BARANG</td>
            <td colspan="3" class="header">TOTAL</td>
          </tr>
          <tr>
            <td class="header">JML UN.1</td>
            <td class="header">JML UN.2</td>
            <td class="header">JML UN.3</td>
          </tr>
          <?php 
		  $tot1 = 0;
		  $tot2 = 0;
		  $tot3 = 0;
		  $total = 0;
		  foreach ($produk as $key => $row){
		  	$total += $row['AMOUNT'];
		  ?>
          <tr>
            <td align="center"><?= $key?></td>
            <td colspan="2"><?= $row['PNAME']?></td>
            <td align="center"><?= $row['QTY']?></td>
            <td align="center">0</td>
            <td align="center">0</td>
          </tr>
          <?php }?>
          <tr>
            <td class="top">&nbsp;</td>
            <td colspan="2" align="right" class="top">&nbsp;</td>
            <td align="center" class="style1">&nbsp;</td>
            <td align="center" class="top">&nbsp;</td>
            <td align="center" class="style1">&nbsp;</td>
          </tr>
          
          <tr>
            <td>TOTAL</td>
            <td align="center" style="width:100px"><?= $tot1?></td>
            <td align="left">UNIT1</td>
            <td align="right">Total Faktur</td>
            <td align="center"><?= count($order)?></td>
            <td align="center">&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td align="center"><?= $tot2?></td>
            <td align="left">UNIT2</td>
            <td align="right">Total Netto</td>
            <td align="center"><?= fCurrency($total)?></td>
            <td align="center">&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td align="center"><?= $tot3?></td>
            <td align="left">UNIT3</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan="2" align="right">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center">&nbsp;</td>
          </tr>
        </table>
</body>
</html>
 