<html>
    <head>
    	<title>Rekapitulasi Per Customer</title>
        <style type="text/css">
            table{
				font-family: "Courier New", Courier, monospace;
                font-size: 11px;
				border-collapse:collapse;
			}
			table#main {
                width : 700px;
                margin: auto;
                font-family: "Courier New", Courier, monospace;
                font-size: 11px;
				border-collapse:collapse;
            }
			.header{
				border-top:1px solid;
				border-bottom:1px solid;
				text-align:center
			}
			.title{font-size:16px}
			.top{border-top : 1px solid}
            .style1 {border-top: 1px solid; font-weight: bold; }
            @media print
            {
                #noprint { display: none; }
            }
            div#noprint{
                width : 900px;
                background-color : #E0E0E0;
                margin: auto;
                padding: 10px;
            }
            .button.green {
                background: #75ae5c none repeat scroll 0 0;
                border: 1px solid #3b6e22;
            }
            .button {
                background: #28a0b2 none repeat scroll 0 0;
                border: 1px solid #0d717e;
            }
            .button {
                box-shadow: 0 1px 2px rgba(255, 255, 255, 0.6) inset, 0 -5px 15px rgba(0, 0, 0, 0.3) inset, 1px 1px 1px #ccc;
            }
            .button {
                color: #fff;
                cursor: pointer;
                display: inline-block;
                font: bold 11px/110% Tahoma,sans-serif;
                margin: 0 3px 0 1px;
                outline: medium none;
                overflow: visible;
                padding: 6px 8px;
                text-align: center;
                text-shadow: 1px 1px 1px #555;
                vertical-align: baseline;
                width: auto;
            }
        </style>
    </head>
    <body>
        <div id="noprint" width="100%" style="text-align:center">
            <button class="button green" onClick="window.print()">Print</button>
        </div>
        <table id="main" border="0">
          <tr>
            <td colspan="7" class="title"><b>REKAPITULASI PER CUSTOMER <?= strtoupper($setting['CORPORATE_NAME'])?></b></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="2">Print Date :<br>
            <?= date('d/m/Y H:i')?></td>
            <td>Tgl Gdng: <br>
            <?= date('d/m/Y', strtotime($setting['WRDATE']))?></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>NO. REKAP</td>
            <td>:</td>
            <td colspan="3">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>BERANGKAT PKL</td>
            <td colspan="4">:
            <?= date('H:i')?>
            WIB</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>MOBIL</td>
            <td colspan="4">:
            <?= isset($van) ? implode(',',$van) : "";?></td>
            <td colspan="2">Tgl. <?= dateIndo(dGetDate($tglmulai)).' - '.dateIndo(dGetDate($tglselesai))?></td>
          </tr>
          <!--<tr>
            <td>TOTAL SALESMAN</td>
            <td>:</td>
            <td colspan="3"><?= count($sales)?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td style="vertical-align:top">SALESMAN</td>
            <td style="vertical-align:top">:</td>
            <td colspan="4"><?= isset($sales) ? implode(',',$sales) : "";?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>-->
          <tr>
            <td style="width:100px">&nbsp;</td>
            <td>&nbsp;</td>
            <td style="width:100px">&nbsp;</td>
            <td style="width:100px">&nbsp;</td>
            <td style="width:20px">&nbsp;</td>
            <td>&nbsp;</td>
            <td style="width:50px">&nbsp;</td>
          </tr>
          <tr>
            <!--<td class="header">TGL FAKTUR</td>
            <td class="header">&nbsp;</td>
            <td class="header">NO ORDER</td>
            <td class="header">NO FAKTUR</td>-->
            <td class="header">CUSTNO</td>
            <td class="header">NAMA CUSTOMER</td>
            <td class="header">BRUTTO</td>
            <td class="header">TPR. UANG</td>
            <td class="header">VOL DISC</td>
            <td class="header">NILAI FAKTUR</td>
            <td class="header">BIAYA KULI</td>
          </tr>
          <?php foreach ($data as $row){
              $disc += $row['DISC'];
		  	  $total += $row['SUBTOTAL'];
		  ?>
          <tr>
            <!--<td align="center"><?= date('d/m/Y', strtotime($row['INVDATE']))?></td>
            <td>&nbsp;</td>
            <td><?= $row['ORDERNO']?></td>
            <td><?= $row['INVNO']?></td>-->
            <td align="center"><?= !empty($row['OUTNO']) ? $row['OUTNO'] : 'NO NUMBER'?></td>
            <td><?= !empty($row['OUTLET']) ? $row['OUTLET'] : 'NONAME'?></td>
            <td align="right"><?= fCurrency($row['SUBTOTAL'])?></td>
            <td align="center">0</td>
            <td align="right"><?= fCurrency($row['DISC'])?></td>
            <td align="right"><?= fCurrency($row['SUBTOTAL'] - $row['DISC'])?></td>
            <td align="right">0</td>
          </tr>
          <?php }?>
          <tr>
            <!--<td class="top">&nbsp;</td>
            <td class="top">&nbsp;</td>
            <td class="top">&nbsp;</td>
            <td class="top">&nbsp;</td>-->
            <td class="top">&nbsp;</td>
            <td class="top"><div align="right"><strong>Total :</strong></div></td>
            <td align="right" class="top"><strong>
            <?= fCurrency($total)?>
            </strong></td>
            <td align="center" class="style1">0</td>
            <td align="right" class="style1"><?= fCurrency($disc)?></td>
            <td align="right" class="top"><strong>
            <?= fCurrency($total - $disc)?>
            </strong></td>
            <td align="right" class="style1">0</td>
          </tr>
        </table>
</body>
</html>