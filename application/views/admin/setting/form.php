			<?php
				$data = array();
				foreach($site as $row){
					$data[strtoupper($row['KEY'])] = $row['VALUE'];
				}
			?>
			<article id="settings">
				<h1>Settings</h1>
				<form id="form" class="uniform" action="" method="post">
					<ul class="tabs">
                        <li><a href="#contact">Site Contact</a></li>
					</ul>
					<div class="tabcontent">			
						<div id="contact">
							<dl class="inline">
								<dt><label for="admin_email">Corporate Name </label></dt>
								<dd>
									<input type="text" id="corporate_name" class="medium" name="data[corporate_name]" value="<?php echo $data['CORPORATE_NAME']?>" />
									<small>This is your corporate name</small>
								</dd>
								
								<dt><label for="admin_email">Address </label></dt>
								<dd>
									<input type="text" id="corporate_address" class="medium" name="data[address]" value="<?php echo $data['ADDRESS']?>" />
									<small>This is your corporate address</small>
								</dd>

								<dt><label for="admin_email">Phone </label></dt>
								<dd>
									<input type="text" id="corporate_phone" class="medium" name="data[phone]" value="<?php echo $data['PHONE']?>" />
									<small>This is your corporate phone.  Example : 0000-00000</small>
								</dd>
								
								<dt><label for="admin_email">Mobile </label></dt>
								<dd>
									<input type="text" id="corporate_fax" class="medium" name="data[mobile]" value="<?php echo $data['mobile']?>" />
									<small>This is your corporate fax. Example : 0000-00000</small>
								</dd>
								
								<dt><label for="admin_email">Email Contact </label></dt>
                                <dd>
                                    <input type="text" id="corporate_email" class="medium" name="data[email_contact]" value="<?php echo $data['email_contact']?>" />
                                    <small>This your corporate email contact. Example : contact@corporate.com</small>
                                </dd>
                                
                                <!--<dt><label for="admin_email">BB PIN </label></dt>
                                <dd>
                                    <input type="text" id="bb_pin" class="medium" name="data[bb_pin]" value="<?php echo $data['bb_pin']?>" />
                                    <small>This your Blackberry PIN. Example : 3AC7HN</small>
                                </dd>
                                
                                <dt><label for="admin_email">Yahoo Messenger ID </label></dt>
								<dd>
									<input type="text" id="bb_pin" class="medium" name="data[messenger]" value="<?php echo $data['messenger']?>" />
									<small>This your Yahoo Messenger ID. Example : email@yahoo.com</small>
								</dd>-->
							</dl>
						</div>
					</div>
					<div class="buttons">
						<button type="submit" name="save" class="button">Save Settings</button>
					</div>
				</form>
			</article>
			
			<script>
			$(document).ready(function($){
				//$("#form").validationEngine();
				$('ul.tabs').tabs();
			});
			</script>
