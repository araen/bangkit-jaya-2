<!DOCTYPE HTML>
<html lang="en">
<head>
<title>Administrator Login</title>
<meta charset="utf-8">
<link rel="shortcut icon" href="<?php echo base_url()?>templates/default/images/favicon.ico" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>templates/admin/default/css/style.css">
<link rel="icon" href="<?php echo base_url()?>data/images/logo.png" />
<!--[if lte IE 8]>
<script type="text/javascript" src="js/html5.js"></script>
<![endif]-->
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/plugins/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/default/js/cufon-yui.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/default/js/Delicious_500.font.js"></script>
<script type="text/javascript">
$(function() {
	Cufon.replace('#site-title');
	$('.msg').click(function() {
		$(this).fadeTo('slow', 0);
		$(this).slideUp(341);
	});
	$('.msg').hide();
	
	 /*$("#loginform").submit(function() {
		$.ajax({
            url : '<?php echo base_url().'admin/main/login'?>',
			type: 'POST',
            cache : false,
			data: $("#loginform").serialize(),
			beforeSend: function(){
				$("#loading").fadeIn();
				$(".error").hide();
			},
			success: function(data) {
				if(data == 'success'){
					window.location="<?php echo base_url().'admin/main.html'?>";
				}else{
					$("#loading").hide();
					$(".error").fadeIn(1000);
					$("#adminpassword").val(''); 
				}
			}
		});
		return false;
	});*/  
});
</script>
</head>
<body>
<!--<header id="top">
	<div class="container_12 clearfix">
		<div id="logo" class="grid_12">
			<!-- replace with your website title or logo
			<a id="site-title" href=""><span>Administrator</span></a>
			<a id="view-site" href="<?php #echo base_url()?>">View Site</a>
		</div>
	</div>
</header>-->
<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<div id="login" class="box">
	<?/*-------BACKGROUND LOGIN PAGE--------*/?>
	<!--img src="<?/*php echo base_url()*/?>templates/admin/default/images/bg.jpg"-->
	<h2>Sistem Informasi Distribusi Inventory Bangkit Jaya</h2>
	<section>
		<div id="loading" class="information msg">Please wait...</div>
		<div class="error msg">Failed username or password</div>
		<form id="loginform" method="post" action="<?php echo base_url().$cpanel_dir."main/login"?>">
			<dl>
				<dt><label for="username">Username</label></dt>
				<dd><input id="username" autocomplete="off" name="username" type="text" /></dd>
			
				<dt><label for="password">Password</label></dt>
				<dd><input id="adminpassword" name="password" type="password" /></dd>
			</dl>
			<p>
				<button type="submit" class="button gray" id="loginbtn">LOGIN</button>
				<a id="forgot" href="<?php echo base_url().$cpanel_dir.'main/forgot'?>">Lupa Password?</a>
			</p>
		</form>
	</section>
</div>

</body>
</html>