<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >	
	<h1><?php echo @$page_title?></h1>
	
	<form id="form" action="<?php echo base_url().$cpanel_dir.'brand'?>" method="post">
		<div class="tablefooter clearfix">
			<div class="actions">
				<b>Cari :</b>
				<input type="text" name="q" id="q" value="<?php echo $q ?>" autocomplete="off" />
                <?php echo str_repeat("&nbsp;",3)?>
                <b>Page :</b>
				<?php echo fPaging($page,$limit,$rows['total'],array('name'=>'page','onchange'=>'jQuery("#form").submit()'))?>
				<?php echo str_repeat("&nbsp;",3)?>
				<input type="submit" class="button blue" value="Filter">
				<?php echo str_repeat("&nbsp;",3)?>
				<a href="<?php echo base_url() ?>brand/form" class="button green" >Tambah</a>
			</div>
		</div>
        <?= $transaction_message?>
		<br />
		<table id="table1" class="gtable sortable">
			<thead>
				<tr>
					<th>Kode Merk</th>
                    <th>Nama Merk</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php
                if(isset($rows['data'])): 
                $i=0;
                foreach($rows['data'] as $row): ?>
				<tr>
					<td width="1%"><?php echo ++$i?></td>
					<td><?php echo $row['PBNAME']?></td>
					<td width="10%">
                        <a href="<?php echo base_url()."brand/form/$row[PBCODE]"?>" title="Edit" ><div class="btn-edit"></div></a>
                        <a href="<?php echo base_url()."brand/delete/$row[PBCODE]"?>" title="Delete" onclick="return confirm('Apakah yakin hapus data ini?');"><div class="btn-delete"></div></a>
                    </td>
				</tr>
				<?php endforeach; endif; ?>
			</tbody>
		</table>
	</form>
</article>
