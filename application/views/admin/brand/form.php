<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>  
<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >    
    <h1><?php echo @$page_title?></h1>
    
    <form id="form" action="" class="uniform" method="post">
        <fieldset>
            <legend><?php echo @$page_title?></legend>
            <dl class="inline">
				<dt><label for="pbcode">Kode Merk</label></dt>
                <dd>
                    <input type="text" id="pbcode" name="data[pbcode]" autocomplete="off" value="<?php echo @$row['PBCODE']?>" size="40" />
                </dd>
				
				<dt><label for="pbname">Nama Merk</label></dt>
                <dd>
                    <input type="text" id="pbname" name="data[pbname]" autocomplete="off" value="<?php echo @$row['PBNAME']?>" size="50" />
                </dd>
                
                <div class="buttons">
                    <button name="save" class="button green" onclick="Submit()">Simpan</button>
                    <a href="<?php echo $this->config->item('base_url')?>brand" class="button white" >Batal</a>
                </div>
        </fieldset>
    </form>
</article>