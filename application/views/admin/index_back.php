<?php echo $this->load->view('admin/header')?>
<?php $cpanel_dir = $this->config->item('cpanel_dir');?> 
<header id="top">
	<div class="container_12 clearfix">
		<div id="logo" class="grid_5">
			<!-- replace with your website title or logo -->
			<!--a id="site-title" href="dashboard.html"><span>Administrator</span></a-->
		</div>

		<div id="userinfo" class="grid_7">
			Selamat Datang, <a href="<?php echo base_url().$cpanel_dir."user/form/".$this->session->userdata('id_user')?>"><?php echo $this->session->userdata('username')?></a>
		</div>
	</div>
</header>

<nav id="topmenu">
	<div class="container_12 clearfix">
		<div class="grid_12">
			<!--?php $menu = $this->auth->get_menu_admin();?-->
			<ul id="mainmenu" class="sf-menu">
				<li><a href="<?php echo base_url().$cpanel_dir."main.html"?>">Dashboard</a></li>
				<?php 
					//echo "<pre>";var_dump($menu);echo "</pre>";
					$base = base_url();
					function menu($menu,$base,$child=0){
						if($child == 1)
							echo "<ul>";
						foreach($menu as $row){
							$url='#';
							if($row['URL'] != "#")
								$url = "$base$row[URL].html";
							echo "<li >";		
							echo "<a href='$url'>$row[NAME]</a>";
							if(count($row['SUB']) > 0){
								echo "<ul>";
								foreach($row['SUB'] as $child){
									$url='#';
									if($child['URL'] != "#")
										$url = "$base$child[URL].html";
									echo "<li>";
									echo "<a href='$url'>$child[NAME]</a>";
									if(count($child['SUB']) > 0){
										menu($child['SUB'],$base,1);
									}
									echo "</li>";
								}
								echo "</ul>";
									
							}
							echo "</li>";
						}
						
						if($child == 1)
							echo "</ul>";
					}
					//menu($menu,$base);
					?>
			</ul>
			<ul id="usermenu">
                <!--<li><a href="" class="comment" title="comment">1</a></li>
                <li><a href="" class="message" title="message">1</a></li>-->
				<li><a href="<?php echo $this->config->item('base_url').$cpanel_dir?>main/logout.html">Logout</a></li>
			</ul>
		</div>
	</div>
</nav>

<section id="content">
	<section class="container_12 clearfix">
		<section id="main" class="grid_12">
			<?php echo !empty($content)?$content:''; ?>
		</section>
	</section>
</section>

<?php echo $this->load->view('admin/footer')?>
