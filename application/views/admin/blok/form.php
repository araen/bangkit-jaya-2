<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>  
<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >    
    <h1><?php echo @$page_title?></h1>
    
    <form id="form" action="" class="uniform" method="post">
        <fieldset>
            <legend><?php echo @$page_title?></legend>
            <dl class="inline">
				<dt><label for="nama">Nama</label></dt>
                <dd>
					<input type="hidden" name="ID" value="<?php echo @$row['id']?>" />
                    <input type="text" id="nama" name="data[nama]" autocomplete="off" value="<?php echo @$row['NAMA']?>" size="10" />
                </dd>

				<dt><label for="idjenisstand">Jenis Stand</label></dt>
                <dd>
					<?= fSelect('data[idjenisstand]',array(),$stand,$row['IDJENISSTAND'])?>
                </dd>
                
                <dt><label for="panjang">Panjang</label></dt>
                <dd>
                    <input type="text" id="panjang" name="data[panjang]" autocomplete="off" value="<?php echo @$row['PANJANG']?>" size="5" />
                </dd>
				
				<dt><label for="lebar">Lebar</label></dt>
                <dd>
                    <input type="text" id="lebar" name="data[lebar]" autocomplete="off" value="<?php echo @$row['LEBAR']?>" size="5" />
                </dd>
				
				<dt><label for="luas">Luas</label></dt>
                <dd>
                    <input type="text" id="luas" name="data[luas]" autocomplete="off" value="<?php echo @$row['LUAS']?>" size="5" />
                </dd>
				
				<dt><label for="nilaisewa">Nilai Sewa</label></dt>
                <dd>
                    <input type="text" id="nilaisewa" name="data[nilaisewa]" autocomplete="off" value="<?php echo fCurrency(@$row['NILAISEWA'])?>" size="15" />
                </dd>
                
                <div class="buttons">
                    <button name="save" class="button green" onclick="Submit()">Simpan</button>
                    <a href="<?php echo $this->config->item('base_url').$cpanel_dir?>blok" class="button white" >Batal</a>
                </div>
        </fieldset>
    </form>
</article>