<!DOCTYPE HTML>
<html lang="en">
<head>
<title>Administrator</title>
<meta charset="utf-8">

<!-- css for template -->
<link rel="shortcut icon" href="<?php echo base_url()?>templates/default/images/favicon.ico" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>templates/admin/default/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>templates/admin/default/css/skins/blue.css" title="gray">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>templates/admin/default/css/superfish.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>templates/admin/default/css/uniform.default.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>templates/admin/default/css/jquery-ui-timepicker-addon.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>templates/admin/default/css/smoothness/jquery-ui-1.8.8.custom.css">

<!-- css for plugins -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>templates/admin/plugins/validationEngine/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>templates/admin/plugins/nyromodal/css/nyroModal.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>templates/admin/plugins/wysiwyg/jquery.wysiwyg.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>templates/admin/plugins/chosen/chosen.css">

<!--<link href="<?php echo base_url()?>plugins/map/css/demo1.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()?>plugins/map/css/pepper-grinder/jquery-ui-1.9.1.custom.css" rel="stylesheet">-->
<link rel="stylesheet" href="<?php echo base_url()?>templates/admin/default/css/jquery-ui.css" />
<link rel="shortcut icon" href="<?php echo base_url()."templates/admin/default/images/icons/icon2.png"?>">
<!--[if lte IE 8]>
<script type="text/javascript" src="<?php echo base_url()?>assets/admin/js/html5.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/admin/js/selectivizr.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/admin/js/excanvas.min.js"></script>
<![endif]-->

<!-- js plugin -->
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/plugins/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/plugins/validationEngine/jquery.validationEngine-en.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/plugins/validationEngine/jquery.validationEngine.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/plugins/nyromodal/jquery.nyroModal.custom.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/plugins/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/plugins/wysiwyg/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/plugins/wysiwyg/controls/wysiwyg.image.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/plugins/wysiwyg/controls/wysiwyg.link.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/plugins/wysiwyg/controls/wysiwyg.table.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/plugins/textext/bundle.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/plugins/chosen/chosen.jquery.js"></script>    
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/plugins/autocomplete/smartautocomplete.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/default/js/shortcut.js"></script>
<!-- gmaps -->

<!--<script src="<?php echo base_url()?>plugins/map/js/craftmap.js" type="text/javascript"></script> -->

<script src="<?php echo base_url()?>templates/admin/default/js/jquery-ui.js"></script>
<!-- js template -->
<!--<script type="text/javascript" src="<?php echo base_url()?>templates/admin/default/js/jquery-ui-1.8.8.custom.min.js"></script>-->
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/default/js/jquery-ui-sliderAccess.js"></script> 
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/default/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/default/js/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/default/js/superfish.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/default/js/cufon-yui.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/default/js/Delicious_500.font.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/default/js/custom.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/default/js/main.js"></script>

<script type="text/javascript" src="<?php echo base_url()?>templates/admin/plugins/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/plugins/tinymce/jscripts/tiny_mce/jquery.tinymce.js"></script>

<!--<script src="<?php echo base_url()?>plugins/map/js/init.js" type="text/javascript"></script>-->


<script type="text/javascript">
base_url = "<?php echo base_url()?>";

$ = jQuery;

jQuery(function() {
    window.onload = init;
    
    jQuery( ".date" ).datepicker({
        changeMonth:true,
        changeYear:true,
		dateFormat: 'dd-mm-yy'
    });
    jQuery( ".datetime" ).datetimepicker({
        changeMonth:true,
        changeYear:true
    });
    
    jQuery('form').validationEngine();
    
    jQuery(".chzn-select").chosen();
    
    jQuery('.nyromodal').nyroModal();
    
    tinyMCE.init({
    // General options
            mode : "specific_textareas",
            editor_selector : "mceEditor",
            theme : "advanced",
            plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
            elements : 'abshosturls',
            relative_urls : false,
            remove_script_host : false,
            // Theme options
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect",
            theme_advanced_buttons2 : "bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,forecolor,backcolor",
            theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,fullscreen",
            //theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,

            // Skin options
            skin : "o2k7",
            skin_variant : "silver",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : "js/template_list.js",
            external_link_list_url : "js/link_list.js",
            //external_image_list_url : "admin/file",
            media_external_list_url : "js/media_list.js",
            
            file_browser_callback : "FileBrowser",
    });

    function FileBrowser (field_name, url, type, win) {
        var cmsURL = '<?php echo base_url().'templates/admin/plugins/tinymce/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php'?>';

        tinyMCE.activeEditor.windowManager.open({
            file : cmsURL,
            title : 'File Browser',
            width : 800, 
            height : 400,
            resizable : "yes",
            inline : "yes", 
            close_previous : "no"
        }, {
            window : win,
            input : field_name
        });
        return false;
    }
});

function init() {
    shortcut.add("F1",function() {
        var form = '<?php echo base_url().'pembelian/form'?>';
        
        window.location.href = form; 
    });
    
    shortcut.add("F2",function() {
        var form = '<?php echo base_url().'order/form'?>';
        
        window.location.href = form; 
    });    
}
</script>
</head>

<body>

<div align="left" style="background-color:#FFFFFF;position:absolute;display:none;border:1px solid #999999;overflow:auto;overflow-x:hidden;" id="div_autocomplete">
    <table cellspacing="0" cellpadding="3" bgcolor="#FFFFFF" id="tab_autocomplete"></table>
</div>