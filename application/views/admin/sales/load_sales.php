<article >	
	<h1><?php echo @$page_title?></h1>
	
	<form action="<?= "$cls/load_sales"?>" class="nyromodal" method="post">
		<div class="tablefooter clearfix">
			<div class="actions">
				<b>Search :</b>
				<input type="text" name="q" id='q' value="<?php echo $q ?>" />
				<?php echo str_repeat("&nbsp;",3)?>
				<b>Page :</b>
				<?php echo fPaging($page,$limit,$rows['total'],array('name'=>'page','onchange="jQuery("#form").submit()"'))?>
				<?php echo str_repeat("&nbsp;",3)?>
				<input type="submit" class="button blue" value="Apply">
				</div>
		</div>
		<br />
		<table id="table1" class="gtable sortable">
			<thead>
				<tr>
                    <th>Nama Sales</th>
                    <th>Type</th>
					<th>Alamat</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($rows['data'] as $row){ ?>
				<tr>							
                    <td><a id="<?php echo $row['SLSNO']?>" data-type="<?php echo $row['STCODE']?>" href="javascript:" class="select"><?php echo $row['SLSNAME']?></a></td>
                    <td><?php echo $row['STCODE']?></td>
					<td><?php echo $row['SLSADD']?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</form>
</article>

<script type="text/javascript" >
	$ = jQuery;
    
    $(function() {
  		$('.nyromodal').nyroModal();
	});

	$(".select").click(function(){
		var title = $(this).text();
        var textval = $(this).attr('id');
		var slstype = $(this).attr('data-type');
    
        $('#sales_format').val(title);
        $('input.sales').val(textval);
        $('input.slstype').val(slstype);
        
        saleschange();
        
		$.nmTop().close();
	});

    function saleschange() {
        var slstype = $('#slstype').val();
        
        if( slstype == 'CVS' )
            $('td.warehouse-field').parent('tr').remove();        
    }
</script>