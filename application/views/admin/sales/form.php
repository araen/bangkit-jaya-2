<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>  
<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >    
    <h1><?php echo @$page_title?></h1>
    
    <form id="form" action="" class="uniform" method="post">
        <fieldset>
            <legend><?php echo @$page_title?></legend>
            <dl class="inline">
				<dt><label for="slsno">Kode Sales</label></dt>
                <dd>
                    <input type="text" id="slsno" name="data[slsno]" autocomplete="off" value="<?php echo @$row['SLSNO']?>" size="40" />
                </dd>
				
				<dt><label for="slsname">Nama Sales</label></dt>
                <dd>
                    <input type="text" id="slsname" name="data[slsname]" autocomplete="off" value="<?php echo @$row['SLSNAME']?>" size="50" />
                </dd>
				
				<dt><label for="slsadd">Alamat</label></dt>
                <dd>
                    <textarea cols="50" name="data[slsadd]" rows="4"><?php echo @$row['SLSADD']?></textarea>
                </dd>
				
				<dt><label for="slscity">Kota</label></dt>
                <dd>
                    <input type="text" id="slscity" name="data[slscity]" autocomplete="off" value="<?php echo @$row['SLSCITY']?>" size="50" />
                </dd>
				
				<dt><label for="stcode">Jenis Sales</label></dt>
                <dd>
                    <?= fSelect('data[stcode]',array('onchange' => "jnssaleschange(this)"),$jnssales,$row['STCODE'])?>
                </dd>
                
                <dt class="van"><label for="stcode">Van</label></dt>
                <dd class="van">
					<?= fSelect('data[vancode]',array(),$van,$row['VANCODE'])?>
                </dd>
				
				<dt><label for="birthdate">Tanggal Lahir</label></dt>
                <dd>
                    <input type="text" id="birthdate" name="data[birthdate]" class="date" autocomplete="off" value="<?php echo !empty($row['BIRTHDATE']) ? date('d-m-Y',strtotime($row['BIRTHDATE'])) : ""?>" size="25" />
                </dd>
				
				<dt><label for="workdate">Tanggal Kerja</label></dt>
                <dd>
                    <input type="text" id="workdate" name="data[workdate]" class="date" autocomplete="off" value="<?php echo !empty($row['WORKDATE']) ? date('d-m-Y',strtotime($row['WORKDATE'])) : ""?>" size="25" />
                </dd>
				
				<dt><label for="transdate">Tanggal Transaksi</label></dt>
                <dd>
                    <input type="text" id="transdate" name="data[transdate]" class="date" autocomplete="off" value="<?php echo !empty($row['TRANSDATE']) ? date('d-m-Y',strtotime($row['TRANSDATE'])) : ""?>" size="25" />
                </dd>
                <?php if($c_create){?>
                <div class="buttons">
                    <button name="save" class="button green" onclick="Submit()">Simpan</button>
                    <a href="<?php echo $this->config->item('base_url')?>sales" class="button white" >Batal</a>
                </div>
                <?php }?>
        </fieldset>
    </form>
</article>

<script type="text/javascript">
    $ = jQuery;
    
    $(function(){
        var status = $('select[id*="stcode"]').trigger('change');               
    });
    
    function jnssaleschange(obj) {
        var val = $(obj).val();

        var van = $('select[id*="vancode"]');            
        if(val == 'CVS')
            van.removeAttr('disabled');
        else
            van.attr('disabled','disabled');
    }        
</script>