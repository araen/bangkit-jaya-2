<article id="stockgudang">
<h1 align="center"><?php echo @$page_title?></h1>
	<?php $data = array('html' => 'HTML', 'xls' => 'EXCEL'); ?>
	<div class="tablefooter clearfix">
		<form id="form" action="<?= $cls."/rep_stockvan"?>" class="uniform" method="post" target="_blank">
            <div style="text-align: center;padding: 5px;">
                <table align="center" style="margin: auto;width: 400px;" cellpadding="5">
                    <tr>
                      <td width="69"><label for="wrcode"><b>Van</b></label></td>
                        <td width="305">
                            <?= fInputText('van_format',array('style'=>'width:200px','class'=>'validate[required]','readonly'=>'readonly','disabled'=>'disabled'),$outlet[$row['OUTNO']])?>
                            <input type="hidden" id="data[vancode]" name="data[vancode]" value="<?= $row['OUTNO']?>" class="van" >
                            <a class="nyromodal" href="<?= base_url()."ajax/load_van"?>"><img src="<?= base_url()."templates/admin/default/images/icons/information-octagon.png"?>"></a>                      
                        </td>
                  </tr>
                </table>
            </div>
            <div style="text-align: center;padding: 5px;">
                <input type="submit" class="button green" name="stockvan" value="Cetak Stok Van">
            </div>
        </form>
	</div>
</article>
<script type="text/javascript">
function goCetak() {
    window.open('<?php echo base_url()."stockgudang/rep_stockgudang/"?>');
}
</script>

