<html>
<head>
<title>Laporan Stock Van</title>
<style type="text/css">
#main_wrapper {
	margin:auto;
	font-family:Arial, Helvetica, sans-serif;
	width:18cm;
}

table{font-size:12px;border-collapse:collapse;}

.style1 {
	font-size: 24px;
	font-weight: bold;
}

th {border-top:1px solid;border-bottom:1px solid}
th,td {padding-top:5px;padding-bottom:5px;}
#rounded-box{border:1px dashed;border-radius: 25px;padding:5px;}
.fill {background-color:#CCCCCC}
</style>
</head>
<body>
<?php print_r($_POST) ?>
<div id="print-content">
	<form>
		<input type="button" onclick="printDiv('print-content')" value="Cetak"/>
	</form>
<div id="main_wrapper">
<table border="0" width="100%">
  <tr>
    <td colspan="2"><strong>CV. TOP JAYA</strong></td>
    <td rowspan="3"><div align="center" class="style1">STOCK VAN</div></td>
  </tr>
  <tr>
    <td colspan="2">Jl. Raya Krian</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4">Telp. (031) 9999999</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5"></td>
  </tr>
  <tr bgcolor="#CCCCCC">
	<th>No.</th>
	<th>Van</th>
	<th>Gudang</th>
	<th>PCODE</th>
	<th>Barang</th>
	<th>Jumlah</th>
  </tr>
	<tr bgcolor="#CCCCCC">
		<td align="center">1</td>
		<td align="center">2</td>
		<td align="center">3</td>
		<td align="center">4</td>
		<td align="center">5</td>
		<td align="center">6</td>
	</tr>
  <?php 
  $i=0;
  foreach ( $rows as $rec ):
  $jml += $rec['VSTOCK'];
  ?>
  
  <tr>
    <td colspan="6"></td>
    </tr>
  
  <tr valign="top">
	    <td align="center"><?= ++$i;?>.</td>
		<td><?= $rec['VAN']?></td>
	    <td><?= $rec['GUDANG']?></td>
		<td><?= $rec['PCODE']?></td>
	    <td><?= $rec['NAMABARANG']?></td>
	    <td align="right"><?= fCurrency($rec['VSTOCK'])?></td>
  </tr>
  <?php endforeach;?>
  <tr>
    <td colspan="6"><hr /></td>
  </tr>
  <tr>
	<td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
	<td><b>TOTAL</b></td>
	<td align="right"><b><?= fCurrency($jml) ?></b></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</div>
</div>
</body>
</html>