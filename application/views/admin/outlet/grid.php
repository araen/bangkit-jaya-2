<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >	
	<h1><?php echo @$page_title?></h1>
	
	<form id="form" action="<?php echo base_url().$cpanel_dir.'outlet'?>" method="post" enctype="multipart/form-data">
		<div class="tablefooter clearfix">
			<div class="actions">
				<b>Cari :</b>
				<input type="text" name="q" id="q" value="<?php echo $q ?>" autocomplete="off" />
                <?php echo str_repeat("&nbsp;",3)?>
                <b>Page :</b>
				<?php echo fPaging($page,$limit,$rows['total'],array('name'=>'page','onchange'=>'jQuery("#form").submit()'))?>
				<?php echo str_repeat("&nbsp;",3)?>
				<input type="submit" class="button blue" value="Filter">
				<?php echo str_repeat("&nbsp;",3)?>
                <?php if($c_create){?>
				<a href="<?php echo base_url() ?>outlet/form" class="button green" >Tambah</a>
			    <a id="link_template" href="<?= $cls."/template"?>" />Download Template</a><?php echo str_repeat("&nbsp;",3)?>
                <input type="file" id="uptemplate" name="uptemplate" style="display: none;">
                <input type="submit" class="button green" value="Upload" id="upload" name="upload" style="display: none;" >            
                <?php }?>
            </div>
		</div>
		<br />
        <?= $transaction_message?> 
        <br />
		<table id="table1" class="gtable sortable">
			<thead>
				<tr>
					<th>No</th>
                    <th>Nama Customer</th>
                    <th>Alamat</th>
                    <th>Kota</th>
                    <th>No. Telp</th>
                    <th>Jenis Customer</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php
                if(isset($rows['data'])): 
                $i=0;
                foreach($rows['data'] as $row): ?>
				<tr>
					<td width="1%"><?php echo ++$i?></td>
					<td><?php echo $row['OUTNAME']?></td>
					<td><?php echo $row['OUTADD']?></td>
					<td><?php echo $row['OCITY']?></td>
					<td><?php echo $row['OPHONE']?></td>
					<td><?php echo $row['ATTCODE']?></td>
					<td width="10%">
                        <?php if($c_update){?>
                        <a href="<?php echo base_url()."outlet/form/$row[OUTNO]"?>" title="Edit" ><div class="btn-edit"></div></a>
                        <?php }?>
                        <?php if($c_delete){?>
                        <a href="<?php echo base_url()."outlet/delete/$row[OUTNO]"?>" title="Delete" onclick="return confirm('Apakah yakin hapus data ini?');"><div class="btn-delete"></div></a>
                        <?php }?>
                    </td>
				</tr>
				<?php endforeach; endif; ?>
			</tbody>
		</table>
	</form>
</article>
<script type="text/javascript">
    $ = jQuery;
    $('#link_template').click(function(){
        var href = $(this).attr('href');
        
        $('#uptemplate').show();
        $('#upload').show();
        $(this).hide();
    });
</script>
