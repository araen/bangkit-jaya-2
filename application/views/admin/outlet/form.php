<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>  
<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >    
    <h1><?php echo @$page_title?></h1>
    
    <form id="form" action="" class="uniform" method="post">
        <fieldset>
            <legend><?php echo @$page_title?></legend>
				<div id="tabs">
                <ul>
                    <li><a href="#tabs-1">Nama</a></li>
                    <li><a href="#tabs-2">Alamat</a></li>
                    <li><a href="#tabs-3">Transaksi</a></li>
                </ul>
                <div id="tabs-1">
                    <dt><label for="outno">Kode Customer</label></dt>
                    <dd>
                        <input type="text" id="outno" name="data[outno]" autocomplete="off" value="<?php echo @$row['OUTNO']?>" size="40" />
                    </dd>
				    
				    <dt><label for="attcode">Jenis Customer</label></dt>
                    <dd>
					    <?= fSelect('data[attcode]',array(),$jnsoutlet,$row['ATTCODE'])?>
                    </dd>
				    
				    <dt><label for="outname">Nama Customer</label></dt>
                    <dd>
                        <input type="text" id="outname" name="data[outname]" autocomplete="off" value="<?php echo @$row['OUTNAME']?>" size="50" />
                    </dd>
                    
                    <dt><label for="npwp">NPWP</label></dt>
                    <dd>
                        <input type="text" id="npwp" name="data[npwp]" autocomplete="off" value="<?php echo @$row['NPWP']?>" size="50" />
                    </dd>
				</div>
                <div id="tabs-2">
				    <dt><label for="outadd">Alamat</label></dt>
                    <dd>
                        <textarea cols="50" name="data[outadd]" rows="4"><?php echo @$row['OUTADD']?></textarea>
                    </dd>
				    
				    <dt><label for="ocity">Kota</label></dt>
                    <dd>
                        <input type="text" id="ocity" name="data[ocity]" autocomplete="off" value="<?php echo @$row['OCITY']?>" size="50" />
                    </dd>
				    
				    <dt><label for="ophone">No. Telp</label></dt>
                    <dd>
                        <input type="text" id="ophone" name="data[ophone]" autocomplete="off" value="<?php echo @$row['OPHONE']?>" size="50" />
                    </dd>
				    
				    <dt><label for="opost">Kode Pos</label></dt>
                    <dd>
                        <input type="text" id="opost" name="data[opost]" autocomplete="off" value="<?php echo @$row['OPOST']?>" size="50" />
                    </dd>
				    
				    <dt><label for="ocontact">Contact Person</label></dt>
                    <dd>
                        <input type="text" id="ocontact" name="data[ocontact]" autocomplete="off" value="<?php echo @$row['OCONTACT']?>" size="50" />
                    </dd>
				 </div> 
                 <div id="tabs-3">      
				    <dt><label for="firsttrans">Transaksi Pertama</label></dt>
                    <dd>
                        <input type="text" id="firsttrans" name="data[firsttrans]" class="date" autocomplete="off" value="<?php echo date('d-m-Y',strtotime($row['FIRSTTRANS']))?>" size="25" />
                    </dd>
				    
				    <dt><label for="firstopen">Transaksi Terbuka</label></dt>
                    <dd>
                        <input type="text" id="firstopen" name="data[firstopen]" class="date" autocomplete="off" value="<?php echo date('d-m-Y',strtotime($row['FIRSTOPEN']))?>" size="25" />
                    </dd>
				    
				    <dt><label for="regdate">Tanggal Registrasi</label></dt>
                    <dd>
                        <input type="text" id="regdate" name="data[regdate]" class="date" autocomplete="off" value="<?php echo date('d-m-Y',strtotime($row['REGDATE']))?>" size="25" />
                    </dd>
				    
				    <dt><label for="flimit">Batas Transaksi</label></dt>
                    <dd>
                        <input type="text" id="flimit" name="data[flimit]" autocomplete="off" value="<?php echo fCurrency(@$row['FLIMIT'])?>" size="15" />
                    </dd>
                 </div>
		        </div>
                <div class="buttons">
                    <?php if($c_create){?>
                    <button name="save" class="button green" onclick="Submit()">Simpan</button>
                    <?php }?>
                    <a href="<?php echo $this->config->item('base_url')?>outlet" class="button white" >Batal</a>
                </div>
        </fieldset>
    </form>
</article>

<script type="text/javascript">
    jQuery('#tabs').tabs();
</script>