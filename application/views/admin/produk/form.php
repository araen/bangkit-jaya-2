<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>  
<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >    
    <h1><?php echo @$page_title?></h1>
    
    <form id="form" action="" class="uniform" method="post">
        <fieldset>
            <legend><?php echo @$page_title?></legend>
				<div id="tabs">
                <ul>
                    <li><a href="#tabs-1">Nama</a></li>
                    <!--<li><a href="#tabs-2">Harga</a></li>-->
                </ul>
                <div id="tabs-1">
                    <dt><label for="pcode">PCODE</label></dt>
                    <dd>
                        <input type="text" id="pcode" name="data[pcode]" autocomplete="off" value="<?php echo @$row['PCODE']?>" size="40" class="validate[required]" />
                    </dd>
				    
				    <dt><label for="pname">Nama Barang</label></dt>
                    <dd>
                        <input type="text" id="pname" name="data[pname]" autocomplete="off" value="<?php echo @$row['PNAME']?>" size="50" class="validate[required]" />
                    </dd>
                    
				    <dt><label for="pbcode">Merk</label></dt>
                    <dd>
					    <?= fSelect('data[pbcode]',array(),$brand,$row['PBCODE'])?>
                    </dd>
				    
				    <dt><label for="suppno">Supplier</label></dt>
                    <dd>
					    <?= fSelect('data[suppno]',array(),$supplier,$row['SUPPNO'])?>
                    </dd>
				    
                    <dt><label for="pweight">Berat</label></dt>
                    <dd>
                        <input type="text" id="pweight" name="data[pweight]" autocomplete="off" value="<?php echo fCurrency(@$row['PWEIGHT'])?>" size="15" />
                    </dd>
				    
				    <dt><label for="plaunch">Tanggal Launch</label></dt>
                    <dd>
                        <input type="text" id="plaunch" name="data[plaunch]" class="date" autocomplete="off" value="<?php echo date('d-m-Y',strtotime($row['PLAUNCH']))?>" size="25" />
                    </dd>
				    
				    <dt><label for="pstatus">Status</label></dt>
                    <dd>
                        <input type="text" id="pstatus" name="data[pstatus]" autocomplete="off" value="<?php echo @$row['PSTATUS']?>" size="40" />
                    </dd>
                    
                    <dd>
                        <table width="50%">
                            <tr>
                                <td width="50%"><b>Harga Perolehan / m3</b></td>
                                <td width="50%" colspan="2"><b>Harga Jual / m3</b></td>
                            </tr>
                            <tr>
                                <td width="45%"><input type="text" id="pprice1" name="data[pprice1]" autocomplete="off" value="<?php echo fCurrency(@$row['PPRICE1'])?>" size="15" /></td>
                                <td width="45%"><input type="text" id="sprice1" name="data[sprice1]" autocomplete="off" value="<?php echo fCurrency(@$row['SPRICE1'])?>" size="15" /></td>
                                <td width="5%">
                                    <a class="nyromodal" href="<?= base_url()?>ajax/historyprice/<?= $row['PCODE']?>"><img src="<?= base_url()?>templates/admin/default/images/icons/information-octagon.png"></a>
                                </td>
                            </tr>
                        </table>
                        <!--
                        <input type="text" id="pprice2" name="data[pprice2]" autocomplete="off" value="<?php echo fCurrency(@$row['PPRICE2'])?>" size="15" />
                        <input type="text" id="pprice3" name="data[pprice3]" autocomplete="off" value="<?php echo fCurrency(@$row['PPRICE3'])?>" size="15" />
                        -->
                    </dd>
				</div>
                <div class="buttons">
                    <?php if($c_create){?>
                    <button name="save" class="button green" onclick="Submit()">Simpan</button>
                    <?php }?>
                    <a href="<?php echo $this->config->item('base_url')?>produk" class="button white" >Batal</a>
                </div>
        </fieldset>
    </form>
</article>

<script type="text/javascript">
    $ = jQuery;
    $(function(){
        $('#tabs').tabs();    
        
        $('#form').validationEngine();
    });
</script>