<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >	
	<h1><?php echo @$page_title?></h1>
	
	<form id="form" action="<?php echo base_url().$cpanel_dir.'produk'?>" method="post" enctype="multipart/form-data">
		<div class="tablefooter clearfix">
			<div class="actions">
				<b>Cari :</b>
				<input type="text" name="q" id="q" value="<?php echo $q ?>" autocomplete="off" />
                <?php echo str_repeat("&nbsp;",3)?>
                <b>Page :</b>
				<?php echo fPaging($page,$limit,$rows['total'],array('name'=>'page','onchange'=>'jQuery("#form").submit()'))?>
				<?php echo str_repeat("&nbsp;",3)?>
				<input type="submit" class="button blue" name="filter" value="Filter">
				<?php echo str_repeat("&nbsp;",3)?>
                <?php if($c_create){?>
				<a href="<?php echo base_url() ?>produk/form" class="button green" >Tambah</a><?php echo str_repeat("&nbsp;",3)?>
                <a id="link_template" href="<?= $cls."/template"?>" />Download Template</a><?php echo str_repeat("&nbsp;",3)?>
                <input type="file" id="uptemplate" name="uptemplate" style="display: none;">
                <input type="submit" class="button green" value="Upload" id="upload" name="upload" style="display: none;" >            
			    <?php }?>
            </div>
		</div>
        <?= $transaction_message?>
		<br />
		<table id="table1" class="gtable sortable">
			<thead>
				<tr>
                    <th>No</th>
					<th>PCODE</th>
                    <th>Nama Barang</th>
					<th>Berat</th>
					<th>Launch</th>
                    <th>Harga Perolehan/m3</th>
                    <th>Harga Jual/m3</th>
                    <!--<th>Harga 2</th>
                    <th>Harga 3</th>
                    <th>Konv 1</th>
                    <th>Konv 2</th>
                    <th>Konv 3</th>-->
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php
                if(isset($rows['data'])): 
                $i=0;
                $day = get_hari_all();
                foreach($rows['data'] as $row): ?>
				<tr>
					<td width="1%"><?php echo ++$i?></td>
                    <td><?php echo $row['PCODE']?></td>
					<td><?php echo substr($row['PNAME'],0,20);?></td>
					<td><?php echo $this->model->formatNumber($row['PWEIGHT'])?></td>
					<td><?php echo dateIndo($row['PLAUNCH'])?></td>
                    <td><?php echo fCurrency($row['PPRICE1'])?></td>
                    <td><?php echo fCurrency($row['SPRICE1'])?></td>
                    <!--<td><?php echo fCurrency($row['PPRICE2'])?></td>
                    <td><?php echo fCurrency($row['PPRICE3'])?></td>
                    <td><?php echo fCurrency($row['CONV1'])?></td>
                    <td><?php echo fCurrency($row['CONV2'])?></td>
                    <td><?php echo fCurrency($row['CONV3'])?></td>-->
                    <td width="10%">
                        <?php if($c_update){?>
                        <a href="<?php echo base_url()."produk/form/".$row['PCODE']?>" title="Edit" ><div class="btn-edit"></div></a>
                        <?php }?>
                        <?php if($c_delete){?>
                        <a href="<?php echo base_url()."produk/delete/".$row['PCODE']?>" title="Delete" onclick="return confirm('Apakah yakin hapus data ini?');"><div class="btn-delete"></div></a>
                        <?php }?>
                    </td>
				</tr>
				<?php endforeach; endif; ?>
			</tbody>
		</table>
	</form>
</article>
<script type="text/javascript">
    $ = jQuery;
    $('#link_template').click(function(){
        var href = $(this).attr('href');
        
        $('#uptemplate').show();
        $('#upload').show();
        $(this).hide();
    });
</script>