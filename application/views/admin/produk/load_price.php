<article >	
	<h1><?php echo @$page_title?></h1>
	
	<form action="<?= "$cls/load_product"?>" class="nyromodal" method="post">
		<table id="table1" class="gtable sortable">
			<thead>
				<tr>
                    <th>Kode</th>
                    <th>Nama Produk</th>
                    <th>Tanggal/Jam</th>
                    <th>Harga Perolehan</th>
                    <th>Harga Jual</th>
				</tr>
			</thead>
			<tbody>
				<?php 
                $num = 0;
                $trecprice = 0;
                $tselprice = 0;
                foreach($rows['data'] as $row){ 
                    ++$num;
                    $trecprice += $row['RECPRICE'];        
                    $tselprice += $row['SELLPRICE'];        
                ?>
				<tr>							
                    <td><?php echo strtoupper($row['PCODE'])?></td>
                    <td><?php echo $row['PNAME']?></td>
                    <td><?php echo dateIndo($row['DATEPRICE'], true, true)?></td>
                    <td><?php echo fCurrency($row['RECPRICE'])?></td>
                    <td><?php echo fCurrency($row['SELLPRICE'])?></td>
				</tr>
				<?php } ?>
                <tr>
                    <td colspan="3" align="center"><b>Harga Rata-rata</b></td>
                    <td><b><?= fCurrency($trecprice/$num)?></b></td>
                    <td><b><?= fCurrency($tselprice/$num)?></b></td>
                </tr>
			</tbody>
		</table>
	</form>
</article>

<script type="text/javascript" >
	$ = jQuery;
    
    $(function() {
  		$('.nyromodal').nyroModal();
	});

	$(".select").click(function(){
		var title = $(this).text();
		var textval = $(this).attr('id');
    
        $('#product_format').val(title);
        $('input.product').val(textval);
    
		$.nmTop().close();
	})
</script>