<!DOCTYPE HTML>
<html lang="en">
<head>
<title>Forgot Password</title>
<meta charset="utf-8">
<link rel="icon" href="<?php echo $this->config->item('base_url')?>templates/admin/default/images/favicon.png" />
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url')?>templates/admin/default/css/style.css">
<link rel="icon" href="<?php echo $this->config->item('base_url')?>data/images/logo.png" />
<!--[if lte IE 8]>
<script type="text/javascript" src="js/html5.js"></script>
<![endif]-->
<script type="text/javascript" src="<?php echo $this->config->item('base_url')?>templates/admin/plugins/jquery/jquery-1.8.3.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url')?>templates/admin/default/js/cufon-yui.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url')?>templates/admin/default/js/Delicious_500.font.js"></script>
<script type="text/javascript">
$(function() {
	Cufon.replace('#site-title');
	$('.msg').click(function() {
		$(this).fadeTo('slow', 0);
		$(this).slideUp(341);
	});
	$('.msg').hide();
	
	 $("#loginform").submit(function(){
         if(confirm("Password lama Anda akan direset dan dikirim melalui email yang anda inputkan, Anda yakin?")){
             $.ajax({
                url : '<?php echo base_url().'admin/main/forgot'?>',
			    type: 'POST',
			    data: $("#loginform").serialize(),
			    beforeSend: function(){
				    $("#loading").fadeIn();
				    $(".error").hide();
			    },
			    success: function(data) {
				    if(data == "fail"){
                        $("#loading").hide();
                        $(".error").fadeIn(1000);
                        $("#adminpassword").val('');
                    }else if(data == 'success'){
                        window.location="<?php echo base_url().'admin/main.html'?>";
                    }else{
					    alert(data);
				    }
			    }
		    });
         }
		return false;
	});  
});
</script>
</head>
<body>
<header id="top">
	<div class="container_12 clearfix">
		<div id="logo" class="grid_12">
			<!-- replace with your website title or logo -->
			<a id="site-title" href=""><span>Administrator</span></a>
			<a id="view-site" href="<?php echo base_url()?>">View Site</a>
		</div>
	</div>
</header>

<div id="login" class="box">
	<h2>Forgot Password</h2>
	<section>
		<div id="loading" class="information msg">Please wait...</div>
		<div class="error msg">Email didn't registered yet</div>
		<form id="loginform" method="post">
			<dl>
                <dt><label for="username">Email</label></dt>
                <dd><input id="username" autocomplete="off" name="email" type="text" /></dd>
            </dl>
			<p>
				<button type="submit" class="button gray" id="loginbtn">SEND</button>
			    <a id="forgot" href="<?php echo base_url().'admin/main'?>">Login</a>
            </p>
		</form>
	</section>
    <div id="msg"></div>
</div>

</body>
</html>
