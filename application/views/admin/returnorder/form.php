<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >    
    <h1><?php echo @$page_title?></h1>
    
    <?php if( $isopname ) {?>
        <div class="alert">Tidak dapat melakukan transaksi pembelian karena sedang dilakukan proses opname</div>
    <?php }?>
    
    <form id="form" action="" class="uniform" method="post">
        <fieldset>
            <legend><?php echo @$page_title?></legend>
             <table width="100%" border="0" class="form">
                <tr>
                    <td class="label">Nomor Order</td>
                    <td><?php if($orderno){$disabled = "disabled";echo "<input type='hidden' name='orderno' value='$orderno'/>";}?>
                    <input type="text" id="orderno2" name="orderno" autocomplete="off" value="<?php echo @$row['ORDERNO']?>" size="15" disabled="disabled" style="width: 200px;"/></td>
                    <td class="label">Outlet</td>
                    <td><?php if($idbeli){$disabled = "disabled";}?>
                    <?= fInputText('outlet_format',array('style'=>'width:200px','class'=>'validate[required]','readonly'=>'readonly',$disabled=>$disabled),$outlet[$row['OUTNO']])?>
                    <input type="hidden" id="data[outno]" name="data[outno]" value="<?= $row['OUTNO']?>" class="outlet" />
                    <?php if ( !$orderno and !$isopname ) {?>
                    <a class="nyromodal" href="<?= base_url()."ajax/load_outlet"?>"><img src="<?= base_url()."templates/admin/default/images/icons/information-octagon.png"?>" /></a></td>
                    <?php }?>
                </tr>
                <tr>
                    <td class="label">Sales</td>
                    <td>
                        <?php if($idbeli){$disabled = "disabled";}?>
                        <?= fInputText('sales_format',array('style'=>'width:200px','class'=>'validate[required]','readonly'=>'readonly',$disabled=>$disabled),$sales[$row['SLSNO']])?>
                        <input type="hidden" id="data[slsno]" name="data[slsno]" value="<?= $row['SLSNO']?>" class="sales" />
                        <?php if ( !$orderno and !$isopname ) {?>
                        <a class="nyromodal" href="<?= base_url()."ajax/load_sales"?>"><img src="<?= base_url()."templates/admin/default/images/icons/information-octagon.png"?>" /></a>
                        <?php }?>
                    </td>
                    <td class="label"><label for="label">Tanggal Transaksi</label></td>
                    <td><input type="text" id="orderdate" name="data[orderdate]" readonly="readonly" autocomplete="off" value="<?php echo !empty($row['ORDERDATE']) ? date('d-m-Y',strtotime($row['ORDERDATE'])) : date('d-m-Y', strtotime($setting['WRDATE']))?>" size="25" /></td>
                </tr>
                <tr>
                    <?php if($row['STCODE'] == 'TO') {?>
                    <td class="label warehouse-field">Gudang</td>
                    <td class="warehouse-field">
                        <?php if($idbeli){$disabled = "disabled";}?>
                        <?= fInputText('warehouse_format',array('style'=>'width:200px','class'=>'validate[required]','readonly'=>'readonly',$disabled=>$disabled),$warehouse[$row['WRCODE']])?>
                        <input type="hidden" id="data[wrcode]" name="data[wrcode]" value="<?= $row['WRCODE']?>" class="warehouse" />
                        <?php if ( !$orderno and !$isopname ) {?>
                        <a class="nyromodal" href="<?= base_url()."ajax/load_warehouse"?>"><img src="<?= base_url()."templates/admin/default/images/icons/information-octagon.png"?>" /></a>
                        <?php }?>
                    </td>
                    <?php }else{?> 
                    <td class="label van-field">Van</td>
                    <td class="van-field">
                        <?php if($idbeli){$disabled = "disabled";}?>
                        <?= fInputText('vancode_format',array('style'=>'width:200px','class'=>'validate[required]','readonly'=>'readonly',$disabled=>$disabled),$van[$row['VANCODE']])?>
                        <input type="hidden" id="data[vancode]" name="data[vancode]" value="<?= $van[$row['VANCODE']]?>" class="van" />
                    </td>
                    <?php }?>
                    <td class="label"><label for="label">Total Order</label></td>
                    <td><input type="text" id="subtotal" name="subtotal" autocomplete="off" value="<?php echo fCurrency(@$row['SUBTOTAL'])?>" size="25" disabled="disabled"/></td>
                </tr>
                <tr>
                    <td colspan="4">
                        <div class="buttons">
                        <?php if ( !$orderno and !$isopname ) {?>
                        <button name="save" class="button green" onclick="Submit()">Simpan</button>
                        <?php }?>
                        <a href="<?php echo $this->config->item('base_url')?>returnorder" class="button white" >Kembali</a>
                        </div>
                    </td>
                </tr>
            </table>
        </fieldset>
				
        <?php if ( $orderno ):?>
        <table id="table1" class="gtable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>PCODE</th>
                    <th>Nama Barang</th>
                    <th>Qty/m3</th>
                    <th>Jumlah</th>
                    <th>Qty/m3 Ret</th>
                    <th>Jns Retur</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $i=0;
                $total=0;
                foreach ( $detail as $rec ):
                $total += $rec['JMLBELI'];
                ?>
                <tr>
                    <td><?= ++$i?></td>
                    <td><?= $rec['PCODE']?></td>
                    <td><?= $rec['PNAME']?></td>
                    <td><?= $rec['QTY']?></td>
                    <td><?= fCurrency($rec['AMOUNT'])?></td>
                    <td><?= fInputText("qtyret[$rec[DORDERNO]][1]", array('style'=>'width:50px'), $rec['QTYRET'])?></td>
                    <td><?= fSelect("rettype[$rec[DORDERNO]]", array(), array("T" => "Trash", "R" => "Resell"), $rec['RETTYPE'])?></td>
                </tr>
                <?php endforeach;?>
                <?php if(empty($row['RETLOCK'])) {?>
                <tr>
                    <td colspan="12" align="center">
                        <button name="save" type="submit" class="button green">Simpan</button>
                    </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
      <?php endif;?>
    </form>
</article>
<script>
    jQuery(document).ready(function(){
        jQuery(".gridinput").removeAttr('readonly');
    });
</script>