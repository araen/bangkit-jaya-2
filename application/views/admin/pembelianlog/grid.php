<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >	
	<h1><?php echo @$page_title?></h1>
	
	<form id="form" action="<?php echo base_url().$cpanel_dir.'pembelian'?>" method="post">
		<div class="tablefooter clearfix">
			<div class="actions">
				<b>Cari :</b>
				<input type="text" name="q" id="q" value="<?php echo $q ?>" autocomplete="off" />
                <?php echo str_repeat("&nbsp;",3)?>
                <b>Page :</b>
				<?php echo fPaging($page,$limit,$rows['total'],array('name'=>'page','onchange'=>'jQuery("#form").submit()'))?>
				<?php echo str_repeat("&nbsp;",3)?>
				<input type="submit" class="button blue" value="Filter">
				<?php if($c_create){?>
                <?php echo str_repeat("&nbsp;",3)?>
				<a href="<?php echo base_url()?>pembelian/form" class="button green" >Tambah</a>
			    <?php echo str_repeat("&nbsp;",3)?>
                <?php }?>
                <!--?= fSelect('semester',array(),array('1'=>'Semester 1','2'=>'Semester 2'),$row['IDBLOK'])?-->
                <!--?= fSelect('tahun',array(),array(date('Y')=>date('Y'),date('Y')+1=>date('Y')+1),$row['IDBLOK'])?-->
                <!--input type="button" class="button green" name="rekap" value="Cetak Rekap" onclick="goCetak()"-->
            </div>
		</div>
		<br />
		<table id="table1" class="gtable sortable">
			<thead>
				<tr>
					<th>No</th>
                    <th>No. Doc</th>
					<th>Gudang</th>
                    <th>Supplier</th>
					<th>Tanggal Pembelian</th>
                    <th>Total Pembelian</th>
					<th>Status</th>
                    <th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php
                if(isset($rows['data'])): 
                $i=0;
                $day = get_hari_all();
                foreach($rows['data'] as $row): ?>
				<tr>
					<td width="1%"><?php echo ++$i ?></td>
                    <td><?php echo $row['DOCNO']?></td>
                    <td><?php echo $row['GUDANG']?></td>
					<td><?php echo $row['SUPPNAME']?></td>
					<td><?php echo dateIndo($row['TGLBELI'])?></td>
                    <td><?php echo fCurrency($row['TOTALBELI'])?></td>
					<td>
                        <?php if ( $row['POSTSTATUS'] == 0 ) {?>
                            <label class="warning">Diajukan</label>
                        <?php }else {?>
                            <label class="available">Diproses</label>
                        <?php }?>
                    </td>
                    <td>
                        <?php if($c_update){?>
                        <a href="<?php echo base_url()."pembelian/form/$row[IDBELI]"?>" title="Edit" ><div class="btn-edit"></div></a>
                        <?php }?>
                        <?php if ( $row['OSTATUSCODE'] == 2 and $c_delete) {?>
                        <a href="<?php echo base_url()."pembelian/delete/$row[IDBELI]"?>" title="Delete" onclick="return confirm('Apakah yakin hapus data ini?');"><div class="btn-delete"></div></a>                    
                        <?php }?>
                    </td>
				</tr>
				<?php endforeach; endif; ?>
			</tbody>
		</table>
  </form>
</article>
<script type="text/javascript">
$ = jQuery;

$(document).ready(function(){
    var form = '<?php echo $cls.'/form'?>';
    
    $(window).keypress(function(event){
        console.log(event.which);
        
        if(event.which == 13) {
            event.preventDefault();
            
            window.location.href = form;    
        }    
    });
});

function goCetak() {
    var smt = jQuery('#semester').val();
    var thn = jQuery('#tahun').val();
    window.open('<?php echo base_url()."$cpanel_dir/pembelian/rekap/"?>'+smt+"/"+thn);
}
</script>