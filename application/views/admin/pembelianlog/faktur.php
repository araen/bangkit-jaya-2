<html>
    <head>
    	<title>Cetak Faktur Pembelian</title>
        <script type="text/javascript" src="<?php echo base_url()?>templates/admin/plugins/jquery/jquery-1.7.1.min.js"></script>
        <style type="text/css">
            table{
				font-family: "Courier New", Courier, monospace;
                font-size: 11px;
				border-collapse:collapse;
			}
			table#main {
                width : 900px;
                margin: auto;
                font-family: "Courier New", Courier, monospace;
                font-size: 11px;
				border-collapse:collapse;
            }
			.header{
				border-top:1px dashed;
				border-bottom:1px dashed;
				text-align:center
			}
			
            @media print
            {
                #noprint { display: none; }
            }
            
            div#noprint{
                width : 900px;
                background-color : #E0E0E0;
                margin: auto;
                padding: 10px;
            }
            
            .button.green {
                background: #75ae5c none repeat scroll 0 0;
                border: 1px solid #3b6e22;
            }
            .button {
                background: #28a0b2 none repeat scroll 0 0;
                border: 1px solid #0d717e;
            }
            .button {
                box-shadow: 0 1px 2px rgba(255, 255, 255, 0.6) inset, 0 -5px 15px rgba(0, 0, 0, 0.3) inset, 1px 1px 1px #ccc;
            }
            .button {
                color: #fff;
                cursor: pointer;
                display: inline-block;
                font: bold 11px/110% Tahoma,sans-serif;
                margin: 0 3px 0 1px;
                outline: medium none;
                overflow: visible;
                padding: 6px 8px;
                text-align: center;
                text-shadow: 1px 1px 1px #555;
                vertical-align: baseline;
                width: auto;
            }
        </style>
    </head>
    <body>
        <div id="noprint" width="100%" style="text-align:center">
            <button id="back" class="button green">Kembali</button>
            <button class="button green" onClick="window.print()">Print</button>
        </div>
        <table id="main" width="100%" border="0">
          <tr>
            <td colspan="13" align="center"><b>FAKTUR PEMBELIAN</b></td>
          </tr>
          <tr>
            <td colspan="5"> CV. Top Jaya</td>
            <td>&nbsp;</td>
            <td colspan="7">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="5">Situbondo</td>
            <td>&nbsp;</td>
            <td colspan="7">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="5">KP. Krajan RT. 02/002 DESA KERTOSARI</td>
            <td>&nbsp;</td>
            <td colspan="7">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2">Telp. 0338-673587</td>
            <td>No. Dokumen</td>
            <td>:</td>
            <td><?= $data['DOCNO']?></td>
            <td>&nbsp;</td>
            <td>TGL. Faktur</td>
            <td>:</td>
            <td><?= date('d-m-Y',strtotime($data['TGLBELI']))?></td>
            <td>&nbsp;</td>
            <td>Cetak</td>
            <td>:</td>
            <td><?= dateIndo(date('d-m-Y H:i'), false, true)?></td>
          </tr>
          <tr>
            <td colspan="2">66.692.134.1-656.00</td>
            <td>No. Faktur</td>
            <td>:</td>
            <td><?= $data['FAKTURNO']?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>User</td>
            <td>:</td>
            <td><?= $this->session->userdata('username')?></td>
          </tr>
          <tr>
            <td>Gd :
            <?= $data['WRCODE']?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td style="width:70px">&nbsp;</td>
            <td style="width:100px">&nbsp;</td>
            <td style="width:70px">&nbsp;</td>
            <td style="width:1%">&nbsp;</td>
            <td style="width:70px">&nbsp;</td>
            <td style="width:30px">&nbsp;</td>
            <td style="width:70px">&nbsp;</td>
            <td style="width:1%">&nbsp;</td>
            <td style="width:70px">&nbsp;</td>
            <td style="width:25px">&nbsp;</td>
            <td style="width:70px">&nbsp;</td>
            <td style="width:1%">&nbsp;</td>
            <td style="width:120px">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="13" style="height:200px;vertical-align:top"><table width="100%" border="0">
              <tr>
                <td class="header" style="width:10%">PCODE</td>
                <td class="header" style="text-align:left">PCODE NAME</td>
                <td class="header" style="width:5%">BSR</td>
                <td class="header" style="width:5%">TGH</td>
                <td class="header" style="width:5%">KCL</td>
                <td class="header" style="width:10%">HRG.1+PPN</td>
                <td class="header" style="width:10%">TOTAL</td>
                <td class="header" style="width:10%">NETTO</td>
              </tr>
              <?php foreach ( $detail as $row ) {
			    $total += $row['JMLBELI'];
                
                $conv = rekonversi($konversi, $row['PCODE'], $row['QTYBELI']);
			  ?>
              <tr>
                <td align="center"><?= $row['PCODE']?></td>
                <td><?= strtoupper($row['PNAME'])?></td>
                <td><div align="center">
                  <?= $conv['QTY1']?>
                </div></td>
                <td><div align="center">
                  <?= $conv['QTY2']?>
                </div></td>
                <td><div align="center">
                  <?= $conv['QTY3']?>
                </div></td>
                <td align="right"><?= fCurrency($row['JMLBELI'])?></td>
                <td align="right"><?= fCurrency($row['JMLBELI'])?></td>
                <td align="right"><?= fCurrency($row['JMLBELI'])?></td>
              </tr>
              <?php }?>
            </table></td>
          </tr>
          <tr>
            <td colspan="9">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td style="border-top:1px dashed">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="9">&nbsp;</td>
            <td colspan="2" align="right">Jumlah</td>
            <td>:</td>
            <td align="right"><?= fCurrency($total)?></td>          
          </tr>
          
          <tr>
            <td colspan="9">Terbilang :</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td></td>          
          </tr>
          <tr>
            <td colspan="9"><?php echo strtoupper(terbilang($total))?></td>
            <td colspan="2" align="right">NILAI FAKTUR</td>
            <td>:</td>
            <td align="right"><b><?= fCurrency($total)?></b></td>
          </tr>
          <tr>
            <td colspan="9">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td></td>
          </tr>
          
          <tr>
            <td colspan="12">
            <table width="100%" border="0">
              <tr>
                <td align="center" style="width:20%">GUDANG</td>
                <td align="center" style="width:20%">SOPIR</td>
                <td align="center" style="width:20%">TOKO</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><div align="center">(___________________)</div></td>
                <td><div align="center">(___________________)</div></td>
                <td><div align="center">(___________________)</div></td>
              </tr>
            </table></td>
            <td></td>
          </tr>
        </table>
</body>
</html>
<script type="text/javascript">
    $ = jQuery;
    
    $('#back').click(function(){
        location.href = '<?= base_url()."pembelian/form/$data[IDBELI]"?>';    
    });
</script> 