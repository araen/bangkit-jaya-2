<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>  
<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >    
    <h1><?php echo @$page_title?></h1>
    
    <form id="form" action="" class="uniform" method="post">
        <fieldset>
            <legend><?php echo @$page_title?></legend>
            <dl class="inline">
				<dt><label for="wrcode">Kode Warehouse</label></dt>
                <dd>
                    <input type="text" id="wrcode" name="data[wrcode]" autocomplete="off" value="<?php echo @$row['WRCODE']?>" size="40" />
                </dd>
				
				<dt><label for="wrname">Nama Warehouse</label></dt>
                <dd>
                    <input type="text" id="wrname" name="data[wrname]" autocomplete="off" value="<?php echo @$row['WRNAME']?>" size="50" />
                </dd>
                
                <dt><label for="wrname">Type</label></dt>
                <dd>
                    <select name="data[wrtype]">
                        <option value="L" <?= ($row['WRTYPE'] == 'L') ? "selected = 'selected'" : ""?>>Log</option>
                        <option value="U" <?= ($row['WRTYPE'] == 'U') ? "selected = 'selected'" : ""?>>Unit</option>
                    </select>                    
                </dd>
				
                <div class="buttons">
                    <?php if($c_create){?>
                    <button name="save" class="button green" onclick="Submit()">Simpan</button>
                    <?php }?>
                    <a href="<?php echo $this->config->item('base_url')?>warehouse" class="button white" >Batal</a>
                </div>
        </fieldset>
    </form>
</article>