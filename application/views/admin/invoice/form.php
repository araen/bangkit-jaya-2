<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >    
    <h1><?php echo @$page_title?></h1>
    
    <form id="form" action="" class="uniform" method="post">
        <fieldset>
            <legend><?php echo @$page_title?></legend>
            <dl class="inline">
                <dt><label for="slsno">Sales</label></dt>
                <dd>
					<?php if($orderno){$disabled = "disabled";echo "<input type='hidden' name='orderno' value='$orderno'/>";}?>
                    <?= fSelect('data[slsno]',array('style'=>'width:200px',$disabled=>$disabled),$sales,$row['SLSNAME'])?>
                </dd>
				
				<dt><label for="wrcode">Gudang</label></dt>
                <dd>
					<?php if($orderno){$disabled = "disabled";echo "<input type='hidden' name='iddbeli' value='$idbeli'/>";}?>
                    <?= fSelect('data[wrcode]',array('style'=>'width:200px',$disabled=>$disabled),$warehouse,$row['WRCODE'])?>
                </dd>
                
				<dt><label for="outno">Outlet</label></dt>
                <dd>
					<?php if($orderno){$disabled = "disabled";echo "<input type='hidden' name='orderno' value='$orderno'/>";}?>
                    <?= fSelect('data[outno]',array('style'=>'width:200px',$disabled=>$disabled),$outlet,$row['OUTNAME'])?>
                </dd>
				
                <dt>
		          <label for="orderdate">Tanggal Transaksi</label>
		        </dt>
                <dd>
                    <input type="text" id="orderdate" name="data[orderdate]" class="date" autocomplete="off" value="<?php echo !empty($row['ORDERDATE']) ? date('d-m-Y',strtotime($row['ORDERDATE'])) : date('d-m-Y')?>" size="25" />
                </dd>
                
                <dt>
                  <label for="subtotal">Total Order</label>
                </dt>
                <dd>
                    <input type="text" id="subtotal" name="data[subtotal]" autocomplete="off" value="<?php echo fCurrency(@$row['SUBTOTAL'])?>" size="15" disabled="disabled"/>
                </dd>

                <?php if ( !$orderno and $c_create) {?>
                <div class="buttons">
                    <button name="save" class="button green" onclick="Submit()">Simpan</button>
                    <a href="<?php echo $this->config->item('base_url')?>proses_order" class="button white" >Batal</a>
                </div>
                <?php }?>
        </fieldset>
				
        <?php if ( $orderno ):?>
        <table id="table1" class="gtable sortable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>PCODE</th>
                    <th>Nama Barang</th>
                    <th>Quantity</th>
                    <th>Jumlah</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $i=0;
                $total=0;
                foreach ( $detail as $rec ):
                $total += $rec['AMOUNT'];
                ?>
                <tr>
                    <td><?= ++$i?></td>
                    <td><?= $rec['PCODE']?></td>
                    <td><?= $rec['PNAME']?></td>
                    <td><?= fCurrency($rec['QTY'])?></td>
                    <td><?= fCurrency($rec['AMOUNT'])?></td>
                    <td>
                        <?php if($c_update){?>
                        <a href="<?php echo base_url().$cpanel_dir."proses_order/delete_detail/$orderno/$rec[DORDERNO]"?>" title="Delete" onclick="return confirm('Apakah yakin hapus data ini?');"><div class="btn-delete"></div></a>
                        <?php }?>
                        <!--a href="<?//php echo base_url().$cpanel_dir."pembelian/kwitansi/$id/$rec[ID]"?>" target="_blank" title="Cetak Kwitansi"><div class="btn-check"></div></a-->
					</td>
                </tr>
                <?php endforeach;?>
                <tr>
                    <td colspan="4" align="right"><b>TOTAL</b></td>
                    <td><?= fCurrency($total)?></td>
                    <td></td>
                </tr>
                <!--tr>
                    <td colspan="4" align="right"><b>SISA pembelian</b></td>
                    <td><b><?= fCurrency($row['TOTALBAYAR'] - $total)?></b></td>
                    <td></td>
                </tr-->
            </tbody>
        </table>
      <?php endif;?>
    </form>
</article>