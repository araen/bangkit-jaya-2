<script type="text/javascript" src="<?php echo base_url()?>templates/admin/default/js/jquery.xautox.js"></script> 
<style type="text/css">
    table.filter {margin : auto}
    table.filter td{padding : 5px}
</style>
<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >	
	<h1><?php echo @$page_title?></h1>
	
	<form id="form" action="<?php echo base_url().$cpanel_dir.'invoice'?>" method="post">
		<div class="tablefooter clearfix">
			<div class="actions" style="width: 100%;text-align: center;">
                <table class="filter" style="width: 500px;">
                    <!--<tr>
                        <td width="125" align="left"><b>Sales</b></td>
                        <td align="left">
                            <?= fInputText('sales_format',array('style'=>'width:250px','class'=>'validate[required]','readonly'=>'readonly',$disabled=>$disabled),$sales[$slsno])?>
                            <input type="hidden" id="sales" name="sales" class="sales" >
                            <a class="nyromodal" href="<?= base_url()."ajax/load_sales"?>"><img src="<?= base_url()."templates/admin/default/images/icons/information-octagon.png"?>"></a>                        </td>                
                    </tr>-->
                    <tr>
                        <td align="left"><b>Tanggal Invoice</b></td>
                        <td align="left"><input type="text" name="startdate" class="date" value="<?= !empty($startdate) ? $startdate : ""?>" /> <input type="text" name="enddate" class="date" value="<?= !empty($enddate) ? $enddate : ""?>" /></td>                
                  </tr>
                    <tr>
                        <td colspan="2">
                            <button type="submit" class="button green" name="cari">Cari</button>
                        </td>
                    </tr>
                </table>
          </div><br><br><br>
		<table id="table1" class="gtable sortable">
			<thead>
				<tr>
					<th>No Order</th>
                    <th>No Invoice</th>
					<th>Gudang</th>
					<th>Sales</th>
					<th>Outlet</th>
					<th>Tanggal Transaksi</th>
					<th>Total Order</th>
                    <th>Link</th>
				</tr>
			</thead>
			<tbody>
				<?php
                if(isset($rows['data'])): 
                $i=0;
                $day = get_hari_all();
                foreach($rows['data'] as $row): ?>
				<tr>
					<td width="1%"><?php echo $row['ORDERNO'] ?>
                        <input type="hidden" name="orderno[]" value="<?= $row['ORDERNO']?>">
                        <input type="hidden" name="invoice[]" value="<?= $row['INVNO']?>">
                    </td>
                    <td><?php echo $row['INVNO']?></td>
					<td><?php echo $row['GUDANG']?></td>
					<td><?php echo $row['SALES']?></td>
					<td><?php echo $row['OUTLET']?></td>
					<td><?php echo dateIndo($row['ORDERDATE'])?></td>
                    <td><?php echo fCurrency($row['SUBTOTAL'])?></td>
                    <td>
                        <a href="<?php echo base_url()."prosesorder/form/$row[ORDERNO]"?>" target="_blank" title="Edit" ><div class="btn-edit"></div></a>
                    </td>
				</tr>
				<?php endforeach; endif; ?>
                <?php if($rows['total'] > 0 and empty($invno) and $c_create){?>
                <tr>
                    <td colspan="8" align="center"><button type="submit" class="button green" name="cetak">Cetak</button></td>
                </tr>
                <?php }?>
			</tbody>
		</table>
  </form>
</article>

<script>
    jQuery(document).ready(function(){
        jQuery("#sales_format").xautox('<?= base_url()."ajax/sales"?>', {targetid: "sales"});    
    });
</script>