<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >    
    <h1><?php echo @$page_title?></h1>
    
    <?php if( $isopname ) {?>
        <div class="alert">Tidak dapat melakukan transaksi pembelian karena sedang dilakukan proses opname</div>
    <?php }?>
    
    <form id="form" action="" class="uniform" method="post">
        <fieldset>
            <legend><?php echo @$page_title?></legend>
            <table width="100%" border="0" class="form">
                <tr>
                  <td class="label"><label for="wrcode">No Dokumen</label></td>
                    <td><?php if($idbeli){$disabled = "disabled";echo "<input type='hidden' name='data[docno]' value='$row[DOCNO]'/>";}?>
                      <?= fInputText('data[docno]',array('style'=>'width:200px','class'=>'validate[required]','autocomplete'=>'off',$disabled=>$disabled),$row['DOCNO'])?></td>
                  <td class="label"><label for="wrcode">No Kendaraan</label></td>
                    <td><?= fInputText('data[vanregister]',array(),$row['VANREGISTER'])?></td>
                </tr>
                <tr>
                  <td class="label"><label for="wrcode">No Faktur</label></td>
                    <td><?php if($idbeli){$disabled = "disabled";echo "<input type='hidden' name='data[fakturno]' value='$row[FAKTURNO]'/>";}?>
                      <?= fInputText('data[fakturno]',array('style'=>'width:200px','class'=>'validate[required]','autocomplete'=>'off',$disabled=>$disabled),$row['FAKTURNO'])?></td>
                  <td class="label"><label for="label">Tanggal Terima</label></td>
                    <td><input type="text" id="tglbeli" name="data[tglbeli]" class="date" autocomplete="off" value="<?php echo !empty($row['TGLBELI']) ? date('d-m-Y',strtotime($row['TGLBELI'])) : date('d-m-Y')?>" size="25" /></td>
                </tr>
                <tr>
                  <td class="label"><label for="wrcode">Gudang / Warehouse</label></td>
                    <td><?php if($idbeli){$disabled = "disabled";}?>
                      <!--<?= fSelect('data[wrcode]',array('style'=>'width:200px',$disabled=>$disabled),$warehouse,$row['WRCODE'])?>-->
                      <?= fInputText('warehouse_format',array('style'=>'width:200px','class'=>'validate[required]','readonly'=>'readonly',$disabled=>$disabled),$warehouse[$row['WRCODE']])?>
                      <input type="hidden" id="data[wrcode]" name="data[wrcode]" value="<?= $row['WRCODE']?>" class="warehouse" />
                      <a class="nyromodal" href="<?= base_url()."ajax/load_warehouse"?>"><img src="<?= base_url()."templates/admin/default/images/icons/information-octagon.png"?>" /></a></td>
                    <td class="label">Total Pembelian</td>
                    <td><input type="text" id="totalbeli" name="totalbeli" autocomplete="off" value="<?php echo fCurrency(@$row['TOTALBELI'])?>" size="15" disabled="disabled"/></td>
                </tr>
                <tr>
                  <td class="label"><label for="wrcode">Supplier</label></td>
                    <td><?php if($idbeli){$disabled = "disabled";}?>
                      <!--<?= fSelect('data[suppno]',array('style'=>'width:200px',$disabled=>$disabled),$supplier,$row['SUPPNO'])?>-->
                      <?= fInputText('supplier_format',array('style'=>'width:200px','class'=>'validate[required]','readonly'=>'readonly',$disabled=>$disabled),$supplier[$row['SUPPNO']])?>
                      <input type="hidden" id="data[suppno]" name="data[suppno]" value="<?= $row['SUPPID']?>" class="supplier" />
                      <a class="nyromodal" href="<?= base_url()."ajax/load_supplier"?>"><img src="<?= base_url()."templates/admin/default/images/icons/information-octagon.png"?>" /></a></td>
                    <td class="label">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                <td colspan="4">
                    <div class="buttons">
                        <?php if ( !$idbeli and !$isopname ) {?>
                            <button name="save" class="button green" onclick="Submit()">Simpan</button>
                        <?php }?>
                        <a href="<?php echo $this->config->item('base_url')?>returnsupp" class="button white" >Kembali</a>
                    </div>
                </td>
            </tr>
        </table>
        </fieldset>
				
        <?php if ( $idbeli ):?>
        <table id="table1" class="gtable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>PCODE</th>
                    <th>Nama Barang</th>
                    <th>Qty/m3</th>
                    <th>Jumlah</th>
                    <th>Qty/m3 Ret</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $i=0;
                $total=0;
                foreach ( $detail as $rec ):
                $total += $rec['JMLBELI'];
                ?>
                <tr>
                    <td><?= ++$i?></td>
                    <td><?= $rec['PCODE']?></td>
                    <td><?= $rec['PNAME']?></td>
                    <td><?= $rec['QTYBELI']?></td>
                    <td><?= fCurrency($rec['JMLBELI'])?></td>
                    <td><?= fInputText("qtyret[$rec[IDDBELI]][1]", array('style'=>'width:50px'), $rec['QTYRET'])?></td>
                </tr>
                <?php endforeach;?>
                <?php if(empty($row['RETLOCK'])) {?>
                <tr>
                    <td colspan="12" align="center">
                        <button name="save" type="submit" class="button green">Simpan</button>
                    </td>
                </tr>
                <?PHP }?>
            </tbody>
        </table>
      <?php endif;?>
    </form>
</article>
<script>
    jQuery(document).ready(function(){
        jQuery(".gridinput").removeAttr('readonly');
    });
</script>