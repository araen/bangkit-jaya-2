		<article id="dashboard">
				<h1>Dashboard</h1>
			<script>
				function createForm(id){
					jQuery("#form"+id).remove();
					jQuery("#container_form_"+id).append(""
					+ "<form id=\"form"+id+"\" action=\"comment/reply/"+id+"\" class=\"uniform\" method=\"post\">"
					+	"<dl>"
					+		"<dt><label for=\"newscontent\">Reply</label></dt>"
					+		"<dd>"
					+			"<textarea name=\"data[content]\" id=\"comment_"+id+"\" class=\"big\" cols=\"75\" rows=\"5\"></textarea>"
					+		"</dd>"
					+	"</dl>"
					+	"<p>"
					+		"<button type=\"submit\" name=\"save\" class=\"button\">SAVE COMMENT</button>"
					+		"<a href=\"javascript:\" class=\"button white\" onClick=\"jQuery('#container_form_"+id+"').fadeOut()\" >CANCEL</a>"
					+	"</p>"
					+ "</form>"
					+"");
					
					jQuery('#comment_'+id).wysiwyg();
					jQuery("#container_form_"+id).fadeIn();
				}
			</script>
			
			<script type="text/javascript">
			jQuery(function($) {
					
				/* flot
				------------------------------------------------------------------------- */
				var d1 = [[1293814800000, 17], [1293901200000, 29], [1293987600000, 34], [1294074000000, 46], [1294160400000, 36], [1294246800000, 16], [1294333200000, 36]];
				var d2 = [[1293814800000, 20], [1293901200000, 75], [1293987600000, 44], [1294074000000, 49], [1294160400000, 56], [1294246800000, 23], [1294333200000, 46]];
				var d3 = [[1293814800000, 32], [1293901200000, 42], [1293987600000, 59], [1294074000000, 57], [1294160400000, 47], [1294246800000, 56], [1294333200000, 59]];

				$.plot($('#pageviews'), [
					{ label: 'Unique',  data: d1},
					{ label: 'Pages',  data: d2},
					{ label: 'Hits',  data: d3}
				], {
					series: {
						lines: { show: true },
						points: { show: true }
					},
					xaxis: {
						mode: 'time',
						timeformat: '%b %d'
					}
				});

			});
			</script>
					
			</article>