<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>  
<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >    
    <h1><?php echo @$page_title?></h1>
    
    <form id="form" action="" class="uniform" method="post">
        <fieldset>
            <legend><?php echo @$page_title?></legend>
            <dl class="inline">
				<dt><label for="nama">Nama</label></dt>
                <dd>
					<input type="hidden" name="ID" value="<?php echo @$row['id']?>" />
                    <input type="text" id="nama" name="data[nama]" autocomplete="off" value="<?php echo @$row['NAMA']?>" size="50" />
                </dd>
                                
                <div class="buttons">
                    <button name="save" class="button green" onclick="Submit()">Save</button>
                    <a href="<?php echo $this->config->item('base_url').$cpanel_dir?>stand" class="button white" >Cancel</a>
                </div>
        </fieldset>
    </form>
</article>