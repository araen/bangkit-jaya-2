<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >	
	<h1><?php echo @$page_title?></h1>
	
	<form id="form" action="<?php echo base_url().$cpanel_dir.'produk'?>" method="post" enctype="multipart/form-data">
		<div class="tablefooter clearfix">
			<div class="actions">
				<b>Cari :</b>
				<input type="text" name="q" id="q" value="<?php echo $q ?>" autocomplete="off" />
                <?php echo str_repeat("&nbsp;",3)?>
                <b>Page :</b>
				<?php echo fPaging($page,$limit,$rows['total'],array('name'=>'page','onchange'=>'jQuery("#form").submit()'))?>
				<?php echo str_repeat("&nbsp;",3)?>
				<input type="submit" class="button blue" name="filter" value="Filter">
				<?php echo str_repeat("&nbsp;",3)?>
                <?php if($c_create){?>
				<a href="<?php echo "$cls/form"?>" class="button green" >Tambah</a><?php echo str_repeat("&nbsp;",3)?>
			    <?php }?>
            </div>
		</div>
        <?= $transaction_message?>
		<br />
		<table id="table1" class="gtable sortable">
			<thead>
				<tr>
                    <th>No</th>
					<th>Nama Promo</th>
                    <th>Deskripsi</th>
					<th>Tgl Mulai</th>
					<th>Tgl Selesai</th>
                    <th>Aktif</th>
                    <th>Item Disc</th>
                    <th>Total Disc</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php
                if(isset($rows['data'])): 
                $i=0;
                foreach($rows['data'] as $row): ?>
				<tr>
					<td width="1%"><?php echo ++$i?></td>
                    <td><?php echo $row['TITLE']?></td>
                    <td><?php echo $row['DESCRIPTION']?></td>
                    <td><?php echo date('d-m-Y', strtotime($row['STARTDATE']))?></td>
                    <td><?php echo date('d-m-Y', strtotime($row['DUEDATE']))?></td>
                    <td>
                        <?php if (strtotime(date('Y-m-d')) >= strtotime($row['STARTDATE']) and strtotime(date('Y-m-d')) <= strtotime($row['DUEDATE']) ) {?>
                            <label class="available">Aktif</label>
                        <?php }else {?>
                            <label class="danger">Nonaktif</label>
                        <?php }?>
                    </td>
                    <td>
                        <?php if ( $row['ISITEMDISC'] ) {?>
                            <label class="available">Ya</label>
                        <?php }else {?>
                            <label class="warning">Tidak</label>
                        <?php }?>
                    </td>
					<td><?php echo $row['TOTALDISC']?>%</td>
                    <td width="10%">
                        <?php if($c_update){?>
                        <a href="<?php echo "$cls/form/".$row['IDPROMO']?>" title="Edit" ><div class="btn-edit"></div></a>
                        <?php }?>
                        <?php if($c_delete){?>
                        <a href="<?php echo "$cls/delete/".$row['IDPROMO']?>" title="Delete" onclick="return confirm('Apakah yakin hapus data ini?');"><div class="btn-delete"></div></a>
                        <?php }?>
                    </td>
				</tr>
				<?php endforeach; endif; ?>
			</tbody>
		</table>
	</form>
</article>
<script type="text/javascript">
    $ = jQuery;
    $('#link_template').click(function(){
        var href = $(this).attr('href');
        
        $('#uptemplate').show();
        $('#upload').show();
        $(this).hide();
    });
</script>