<style>
.label{}
</style>
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/default/js/jquery.xautox.js"></script>    
<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >    
    <h1><?php echo @$page_title?></h1>

    <?= $transaction_message?>
    
    <form id="form" action="" class="uniform" method="post">
        <fieldset>
            <legend><?php echo @$page_title?></legend>
            <table width="100%" border="0" class="form">
                <tr>
                    <td class="label">Nama Promo</td>
                    <td><input type="text" name="data[title]" style="width:200px" value="<?= $row['TITLE']?>" /></td>
                  <td class="label"><label for="label">Item Disc?</label></td>
                    <td><input type="checkbox" name="data[isitemdisc]" style="width:200px" value="1" <?= ($row['ISITEMDISC']) ? "checked = checked" :""?> /></td>
              </tr>
                <tr>
                    <td class="label" style="vertical-align:top">Deskripsi</td>
                    <td><textarea name="data[description]" style="width:200px"><?= $row['DESCRIPTION']?></textarea></td>
                  <td class="label" style="vertical-align:top"><label for="label">Total Disc</label></td>
                    <td style="vertical-align:top"><input type="text" name="data[totaldisc]" maxlength="3" style="width:20px;" value="<?= $row['TOTALDISC']?>" /> 
                    %</td>
              </tr>
                <tr>
                  <td class="label"><span class="label">Tanggal Mulai</span></td>
                  <td><input type="text" name="data[startdate]" style="width:200px" value="<?= !empty($row['STARTDATE']) ? date('d-m-Y', strtotime($row['STARTDATE'])) : ""?>" class="date" /></td>
					<td class="label">Minum Pembelian</td>
                  <td class="label"><input type="text" name="data[minamount]" style="width:200px" value="<?= $row['MINAMOUNT']?>" /></td>
                </tr>
                <tr>
                    <td class="label warehouse-field">Tanggal Selesai</td>
                    <td class="warehouse-field"><input type="text" style="width:200px" name="data[duedate]" class="date" value="<?= !empty($row['DUEDATE']) ? date('d-m-Y', strtotime($row['DUEDATE'])) : ""?>" /></td>
                        <td class="label">&nbsp;</td>
                        <td>&nbsp;</td>
                </tr>
                
                <tr>
                    <td colspan="4">
                        <div class="buttons">
                        <button name="save" class="button green" type="submit">Simpan</button>
                        <a href="<?php echo $cls?>" class="button white" >Kembali</a></div></td>
                </tr>
            </table>
      </fieldset>
				
		<?php if ( $idpromo ):?>
		<fieldset>
			<legend>Detail Outlet Type</legend>
			<dl class="inline">
				<dt><label for="pcode">Outlet Type</label></dt>
                <dd>
                    <?= fInputText('ott_format',array('style'=>'width:200px','readonly'=>'readonly'))?>
                    <input type="hidden" id="ottcode" name="ottcode" class="ott" >
                    <a class="nyromodal" href="<?= base_url()."ajax/load_outlet_type"?>"><img src="<?= base_url()."templates/admin/default/images/icons/information-octagon.png"?>"></a>
                </dd>
		</fieldset>
        
        <?php if($outletpromo) {?>
        <table id="table1" class="gtable sortable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Outlet Type</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $i=0;
                foreach ( $outletpromo as $rec ):?>
                <tr>
                    <td><?= ++$i?></td>
                    <td><?= $jnsoutlet[$rec['ATTCODE']]?></td>
                    <td>
                        <a href="<?php echo base_url().$cpanel_dir."promo/delete_outlet_type/$idpromo/$rec[ATTCODE]"?>" title="Delete" onclick="return confirm('Apakah yakin hapus data ini?');"><div class="btn-delete"></div></a>
                    </td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
        <?php }?>
		<?php endif;?>
    </form>
</article>

<script>
    $ = jQuery;
    
    $(document).ready(function(){
        $("#pcode_format").xautox('<?= base_url()."ajax/product"?>', {targetid: "pcode"});    
        
        $("#outno_format").xautox('<?= base_url()."ajax/outlet"?>', {targetid: "outno"});    
    });
    
    $('#proses').click(function(){
        $('#product_format').removeClass('validate[required]');        
    });
    
    $('#salestype').change(function(){
        console.log($(this).val());    
    });
    
    $('#invoice').click(function(){
        var orderno = $(this).attr('data-value');
        var status = $(this).attr('data-status');
        
        if(status == '1') {
            $('a#link-login').trigger('click');
        } else
            location.href = '<?= base_url()."order/invoice/"?>' + orderno;
        return false;    
    });
</script>
