<script type="text/javascript" src="<?php echo base_url()?>templates/admin/default/js/jquery.xautox.js"></script> 
<style type="text/css">
    table.filter {margin : auto}
    table.filter td{padding : 5px}
</style>
<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >	
	<h1><?php echo @$page_title?></h1>
	
	<form id="form" action="<?php echo base_url().$cpanel_dir.'closing'?>" method="post">
		<div class="tablefooter clearfix">
			<div class="actions" style="width: 100%;text-align: center;">
                <table class="filter" style="width: 500px;">
                    <tr>
                        <td width="125" align="left"><b>Tanggal Gudang</b></td>
                        <td align="left"><input type="text" name="tglgudang" class="date" value="<?= date('d-m-Y', strtotime($setting['WRDATE']))?>"></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <?php if($c_create){?>
                            <input type="hidden" name="closing">
                            <button type="button" name="closing" class="button green">Closing</button>
                            <?php }?>
                        </td>
                    </tr>
                </table>
          </div>
  </form>
</article>

<script type="text/javascript">
    $ = jQuery;
    
    $('button[name="closing"]').click(function(){
        if(confirm('Anda yakin akan melakukan closing harian?')) {
            $('form#form').submit();
        }
        return false;
    });
    
    function dailyClosing() {
        if ( confirm('Anda yakin akan closing? proses ini dilakukan diakhir hari') ) {
            $('#form').submit();
        }
    }
</script>