<article id="grafik">
	<h1>Grafik</h1>
    <div id="chart_omsetsalesposting"></div><br>
    <div id="chart_omsetsalesunpost"></div><br>
    <div id="chart_omsetproduct"></div><br>
    <div id="chart_stock"></div><br>

<script type="text/javascript" src="<?= base_url()?>public/highcharts/highcharts.js"></script>
<script type="text/javascript" src="<?= base_url()?>public/highcharts/modules/exporting.js"></script>
<script type="text/javascript">
 
$ = jQuery;

$(function () {
    // Set up the chart
    var chart = new Highcharts.Chart({
        chart: {
            renderTo: 'chart_omsetsalesposting',
            type: 'column',
            margin: 75,
            options3d: {
                enabled: true,
                alpha: 15,
                beta: 15,
                depth: 50,
                viewDistance: 25
            }
        },
        xAxis: {
            categories: [
                'Jan','Feb','Mar','Apr','May','Jun',
                'Jul','Aug','Sep','Oct','Nov','Dec'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Omset'
            }
        },
        title: {
            text: 'Grafik Omset Per Salesman'
        },
        plotOptions: {
            column: {
                depth: 25
            }
        },
        series: [
        <?php foreach ( $rekapsalesposting['salesman'] as $key => $val ) {?>
        {
            name : '<?= $val?>',
            data : [<?= implode(',', $rekapsalesposting['omset'][$key])?>],        
        },
        <?php }?>
        ]
    });
    
    var chart = new Highcharts.Chart({
        chart: {
            renderTo: 'chart_omsetsalesunpost',
            type: 'column',
            margin: 75,
            options3d: {
                enabled: true,
                alpha: 15,
                beta: 15,
                depth: 50,
                viewDistance: 25
            }
        },
        xAxis: {
            categories: [
                'Jan','Feb','Mar','Apr','May','Jun',
                'Jul','Aug','Sep','Oct','Nov','Dec'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Omset'
            }
        },
        title: {
            text: 'Grafik Order Belum Terproses'
        },
        plotOptions: {
            column: {
                depth: 25
            }
        },
        series: [
        <?php foreach ( $rekapsalesunpost['salesman'] as $key => $val ) {?>
        {
            name : '<?= $val?>',
            data : [<?= implode(',', $rekapsalesunpost['omset'][$key])?>],        
        },
        <?php }?>
        ]
    });
    
    var chart = new Highcharts.Chart({
        chart: {
            renderTo: 'chart_omsetproduct',
            type: 'column',
            margin: 75,
            options3d: {
                enabled: true,
                alpha: 15,
                beta: 15,
                depth: 50,
                viewDistance: 25
            }
        },
        xAxis: {
            categories: [
                'Jan','Feb','Mar','Apr','May','Jun',
                'Jul','Aug','Sep','Oct','Nov','Dec'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Omset'
            }
        },
        title: {
            text: 'Grafik Omset Per Produk'
        },
        plotOptions: {
            column: {
                depth: 25
            }
        },
        series: [
        <?php foreach ( $rekapproduct['product'] as $key => $val ) {?>
        {
            name : '<?= $key?>',
            data : [<?= implode(',', $rekapproduct['omset'][$key])?>],        
        },
        <?php }?>
        ]
    });
    
    var chart = new Highcharts.Chart({
        chart: {
            renderTo: 'chart_stock',
            type: 'column',
            margin: 75
        },
        xAxis: {
            categories: ['Product'],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Jumlah Stok'
            }
        },
        credits: {
            enabled: false
        },
        title: {
            text: 'Grafik Stock Per Produk'
        },
        series: [
        <?php foreach ( $rekapstock['product'] as $key => $val ) {?>
        {
            name : '<?= $key?>',
            data : [<?= $rekapstock['qty'][$key]?>],        
        },
        <?php }?>
        ]
    });

});
</script>
</article>