<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>  
<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >    
    <h1><?php echo @$page_title?></h1>
    
    <form id="form" action="" class="uniform" method="post">
        <fieldset>
            <legend><?php echo @$page_title?></legend>
            <dl class="inline">
				<dt><label for="nama">Nama</label></dt>
                <dd>
					<input type="hidden" name="ID" value="<?php echo @$row['id']?>" />
                    <input type="text" id="nama" name="data[nama]" autocomplete="off" value="<?php echo @$row['NAMA']?>" size="50" />
                </dd>
                
                <dt><label for="nik">NIK</label></dt>
                <dd>
                    <input type="text" id="nik" name="data[nik]" autocomplete="off" value="<?php echo @$row['NIK']?>" size="40" />
                </dd>
				
				<dt><label for="jeniskelamin">Jenis Kelamin</label></dt>
                <dd>
                    <input type="text" id="jeniskelamin" name="data[jeniskelamin]" autocomplete="off" value="<?php echo @$row['JENISKELAMIN']?>" size="3" />
                </dd>
								
				<dt><label for="tmplahir">Tempat Lahir</label></dt>
                <dd>
                    <input type="text" id="tmplahir" name="data[tmplahir]" autocomplete="off" value="<?php echo @$row['TMPLAHIR']?>" size="25" />
                </dd>
				
				<dt><label for="tgllahir">Tanggal Lahir</label></dt>
                <dd>
                    <input type="text" id="tgllahir" name="data[tgllahir]" class="date" autocomplete="off" value="<?php echo date('d-m-Y',strtotime($row['TGLLAHIR']))?>" size="25" />
                </dd>
				
				<dt><label for="alamatktp">Alamat KTP</label></dt>
                <dd>
                    <textarea cols="50" name="data[alamatktp]" rows="4"><?php echo @$row['ALAMATKTP']?></textarea>
                </dd>
				
				<dt><label for="alamat">Alamat</label></dt>
                <dd>
                    <textarea cols="50" name="data[alamat]" rows="4"><?php echo @$row['ALAMAT']?></textarea>
                </dd>
				
				<dt><label for="kodepos">Kode Pos</label></dt>
                <dd>
                    <input type="text" id="kodepos" name="data[kodepos]" autocomplete="off" value="<?php echo @$row['KODEPOS']?>" size="5" />
                </dd>
				
				<dt><label for="telepon">Telepon</label></dt>
                <dd>
                    <input type="text" id="telepon" name="data[telepon]" autocomplete="off" value="<?php echo @$row['TELEPON']?>" size="20" />
                </dd>
				
				<dt><label for="nohp">No. HP</label></dt>
                <dd>
                    <input type="text" id="nohp" name="data[nohp]" autocomplete="off" value="<?php echo @$row['NOHP']?>" size="20" />
                </dd>
				
				<dt><label for="idblok">Blok Stand</label></dt>
                <dd>
					<?= fSelect('data[idblok]',array(),$blok,$row['IDBLOK'])?>
                </dd>
                
                <div class="buttons">
                    <button name="save" class="button green" onclick="Submit()">Simpan</button>
                    <a href="<?php echo $this->config->item('base_url').$cpanel_dir?>penyewa" class="button white" >Batal</a>
                </div>
        </fieldset>
    </form>
</article>