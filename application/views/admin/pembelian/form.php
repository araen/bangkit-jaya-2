<script type="text/javascript" src="<?php echo base_url()?>templates/admin/default/js/jquery.xautox.js"></script> 
<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >    
    <h1><?php echo @$page_title?></h1>
    
    <?php if( $isopname ) {?>
        <div class="alert">Tidak dapat melakukan transaksi pembelian karena sedang dilakukan proses opname</div>
    <?php }?>
    
    <?= $transaction_message?>
    
    <form id="form" action="" class="uniform" method="post">
        <table width="100%">
            <tr>
                <td width="60%">
                    <table width="100%">
                        <tr>
                            <td width="60%">
                                <?php if ( empty($idbeli) ) {?>
                                    <fieldset>
                                        <legend>Detail Barang</legend>
                                        <table>
                                            <tr>
                                                <td style="padding:15px 10px 15px 0;">
                                                    <label for="pcode"><b>No. Dokumen</b></label>
                                                </td>
                                                <td>
                                                    <?= fInputText('document_format',array('style'=>'width:300px'))?>
                                                    <input type="hidden" id="data[pcode]" name="data[pcode]" class="product" >
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding:15px 10px 15px 0;">
                                                    <label for="pcode"><b>Nama Barang</b></label>
                                                </td>
                                                <td>
                                                    <?= fInputText('product_format',array('style'=>'width:300px'))?>
                                                    <input type="hidden" id="data[pcode]" name="data[pcode]" class="product" >
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="justify" colspan="2">
                                                    <span style="color: red;"><i># Untuk jumlah barang = 1 masukkan nama barang, pilih kemudian tekan enter</i></span><br>
                                                    <span style="color: red;"><i># Untuk jumlah barang > 1 masukkan quantity, (*) dan nama barang (contoh : 10*Nama Barang) - tanpa spasi</i></span><br>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                <?php } else {?>
                                    <fieldset>
                                        <legend>Detail Penerimaan</legend>
                                        <table>
                                            <tr>
                                                <td style="padding: 10px;"><h3>No. Dokumen</h3></td>
                                                <td style="padding: 10px;"><h3><?= $row['DOCNO']?></h3></td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                <?php }?>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <a class="button blue" href="<?= base_url().'pembelian'?>">Daftar</a>
                                <?php if ( empty($idbeli) ) {?>
                                <a id="reset_cart" class="button red" href="<?= base_url().'cart/reset_chart/R'?>" onclick="return confirm('Anda yakin akan mereset chart?')">Reset</a>
                                <?php }?>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <table id="table1" class="gtable sortable">
                                    <thead>
                                        <tr id="head">
                                            <th>NO</th>
                                            <th>PCODE</th>
                                            <th>NAMA BARANG</th>
                                            <th>HARGA SATUAN</th>
                                            <th>QTY/m3</th>
                                            <th>TOTAL</th>
                                            <?php if ( empty($idbeli) ) {?>
                                            <th>AKSI</th>
                                            <?php }?>
                                        </tr>
                                        <?php if($detail) {
                                            $i = 0;
                                            $total = 0;
                                            foreach ( $detail as $dt ) {
                                                $total += $dt['JMLBELI'];   
                                        ?>
                                        <tr>
                                            <td><?= ++$i?></td>    
                                            <td><?= $dt['PCODE']?></td>    
                                            <td><?= $dt['PNAME']?></td>    
                                            <td align="right"><?= fCurrency($dt['AMOUNTITEM'])?></td>    
                                            <td align="center"><?= $dt['QTYBELI']?></td>    
                                            <td align="right"><?= fCurrency($dt['JMLBELI'])?></td>    
                                        </tr>
                                        <?php }
                                        }?>
                                    </thead>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="35%">
                    <table style="width: 100%;">
                        <tr>
                            <td>
                                <fieldset>
                                    <legend>Supplier</legend>
                                    <table>
                                        <tr>
                                            <td>
                                                <label for="pcode"><b>Supplier</b></label>
                                            </td>
                                            <td>
                                                <?php if(empty($idbeli)){?>
                                                <?= fInputText('supplier_format',array('style'=>'width:200px'))?>
                                                <input type="hidden" id="suppno" name="data[suppno]" class="product" >
                                                <?php } else {?>
                                                <?= $supplier[$row['SUPPNO']]['SUPPNAME']?>
                                                <?php }?>
                                            </td>
                                        </tr>
                                        <tr <?= (empty($idbeli) or empty($row['SUPPNO'])) ? 'style="display: none;"' : '' ?> class="suppdetail">
                                            <td>
                                                <label for="pcode"><b>Alamat</b></label>
                                            </td>
                                            <td>
                                                <label id="address" for="pcode"><?= $supplier[$row['SUPPNO']]['SUPPADD']?></label>
                                            </td>
                                        </tr>
                                        <tr <?= (empty($idbeli) or empty($row['SUPPNO'])) ? 'style="display: none;"' : '' ?> class="suppdetail">
                                            <td>
                                                <label for="pcode"><b>Telp</b></label>
                                            </td>
                                            <td>
                                                <label id="phone" for="pcode"><?= $supplier[$row['SUPPNO']]['SUPPPHONE']?></label>
                                            </td>
                                        </tr>
                                        <tr id="edit" style="display: none;" class="suppdetail">
                                            <td colspan="2">
                                                <a name="save" class="button orange" onclick="Submit()" style="margin-top: 5px;width: 20%;">Ubah</a>
                                                <a name="save" class="button red" onclick="Submit()" style="margin-top: 5px;width: 20%;">Batalkan</a>
                                            </td>
                                        </tr>
                                        <tr id="save">
                                            <td width="80%" colspan="2">
                                                <!--<a name="save" class="button green" onclick="Submit()" style="margin-top: 5px;width: 95%;">+ Supplier Baru</a>-->
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <fieldset>
                                    <legend>PEMBAYARAN</legend>
                                    <table width="100%">
                                        <tr>
                                            <td style="font-size: 20px;"><b>TOTAL</b></td>
                                            <td style="font-size: 20px;" align="right"><b><span id="total">Rp. <?= ($total > 0) ? fCurrency($total) : 0?></span></b></td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <fieldset>
                                    <legend>Catatan</legend>
                                    <?php if(empty($idbeli) and !$isopname) {?>
                                    <textarea style="width: 95%;" name="note"></textarea>
                                    <input type="submit" name="submit" class="button green" style="width: 100%;" value="SELESAI">
                                    <?php } else {?>
                                    <?= $row['NOTE']?>
                                    <?php }?>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table> 
    </form>
</article>

<script type="text/javascript">
    $ = jQuery;
    
    var qty = 1;
    var query = null;
    var opname = <?= $isopname?>;
    $(document).ready(function(){
        <?php if(empty($idbeli)){?>
        loadCart();
        <?php }?>
        
        $('#product_format').focus();
        
        $("#product_format").autocomplete({
            source: function( request, response ) {
            var key = request.term.split('*');
            var docno = $('#document_format').val();
            
            if(key.length > 1) {
                qty = key[0];
                query = key[1];
            } else {
                qty = 1;
                query = key[0];
            }
            
            $.ajax( {
                  url: '<?php echo base_url().'ajax/product/U'?>',
                  dataType: "json",
                  data: {
                    q : query,
                    d : docno
                  },
                  success: function( data ) {
                    response( data );
                    
                    if ( data.length == 1 ) {
                        $('li.ui-menu-item').eq(0).click();
                    }    
                    
                    $(this).val('');
                  }
                });
            },
            minLength: 2,
            select: function( event, ui ) {
                bindResponse(ui.item, qty);    
            }
        });
        
        $("#document_format").autocomplete({
            source: function( request, response ) {

            $.ajax( {
                  url: '<?php echo base_url().'ajax/rsvdoc'?>',
                  dataType: "json",
                  data: {
                    q : request.term
                  },
                  success: function( data ) {
                    response( data );
                  }
                });
            },
            minLength: 2,
            select: function( event, ui ) {
                    
            }
        });
        
        $("#supplier_format").autocomplete({
            source: function( request, response ) {
            $.ajax( {
                  url: '<?php echo base_url().'ajax/supplier'?>',
                  dataType: "json",
                  data: {
                    q : request.term
                  },
                  success: function( data ) {
                    response( data );  

                  }
                });
            },
            minLength: 2,
            select: function( event, ui ) {
                $("#suppno").val(ui.item.key);
                $("tr.suppdetail").show();           
                $("label#address").text(ui.item.address);           
                $("label#phone").text(ui.item.phone);           
                $("tr#save").hide();           
            }
        });
        
        $('#form').submit(function(){
            if(opname) {
                alert('Pembelian tidak dapat diproses, sedang dilakukan opname');
                return false;
            }
            
            if(!confirm('Apakah anda yakin akan menyimpan data ini?')) {
                return false;    
            }
        });
    });
    
    function initGrid() {
        $("#table1 input[id^='pprice'], #table1 input[id^='pqty']").focusout(function(){
            var tr = $(this).parents('tr').eq(0);
            var pcode = tr.find("input[id^='pcode']").val();
            var pprice = tr.find("input[id^='pprice']").val();
            var pqty = tr.find("input[id^='pqty']").val();
            
            $.ajax({
                url   : '<?php echo base_url().'ajax/cart'?>',
                type  : 'post',
                data  : {
                    mode : 'update',
                    p : 'R',
                    q : new Array(pcode, pprice, pqty)
                },
                success : function(data) {
                    loadCart();
                }    
            })    
        });
    }
    
    function bindResponse(data, qty) {
        $.ajax({
          url   : '<?php echo base_url().'ajax/cart'?>',
          type  : 'post',
          data  : {
              mode : 'insert',
              t : qty,
              p : 'R',
              q : data
          },
          success : function(data) {
              $('#product_format').val('');
              loadCart();
          }
        });
    }
    
    function loadCart() {
        $.ajax({
          url   : '<?php echo base_url().'ajax/loadcart'?>',
          type  : 'POST',
          dataType: "json",
          data  : {
            q : 'R',
            p : '2'
          },
          success : function ( data ) {
              $('#table1 tr').not('tr#head').remove();
              $('#table1').append(data.html);
              $('span#total').text(data.total);
              initGrid();
          }
        });    
    }
</script>
