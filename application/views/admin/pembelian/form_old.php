<script type="text/javascript" src="<?php echo base_url()?>templates/admin/default/js/jquery.xautox.js"></script> 
<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >    
    <h1><?php echo @$page_title?></h1>
    
    <?php if( $isopname ) {?>
        <div class="alert">Tidak dapat melakukan transaksi pembelian karena sedang dilakukan proses opname</div>
    <?php }?>
    
    <?= $transaction_message?>
    
    <form id="form" action="" class="uniform" method="post">
        <fieldset>
            <legend><?php echo @$page_title?></legend>
            <table width="100%" border="0" class="form">
                <tr>
                  <td class="label"><label for="wrcode">No Dokumen</label></td>
                    <td><?php if($idbeli){$disabled = "disabled";echo "<input type='hidden' name='data[docno]' value='$row[DOCNO]'/>";}?>
                      <?= fInputText('data[docno]',array('style'=>'width:200px','class'=>'validate[required]','autocomplete'=>'off',$disabled=>$disabled),$row['DOCNO'])?></td>
                  <td class="label"><label for="wrcode">No Kendaraan</label></td>
                    <td><?= fInputText('data[vanregister]',array(),$row['VANREGISTER'])?></td>
                </tr>
                <tr>
                  <td class="label"><label for="wrcode">No Faktur</label></td>
                    <td><?php if($idbeli){$disabled = "disabled";echo "<input type='hidden' name='data[fakturno]' value='$row[FAKTURNO]'/>";}?>
                      <?= fInputText('data[fakturno]',array('style'=>'width:200px','class'=>'validate[required]','autocomplete'=>'off',$disabled=>$disabled),$row['FAKTURNO'])?></td>
                  <td class="label"><label for="label">Tanggal Terima</label></td>
                    <td><input type="text" id="tglbeli" name="data[tglbeli]" readonly="readonly" autocomplete="off" value="<?php echo !empty($row['TGLBELI']) ? date('d-m-Y',strtotime($row['TGLBELI'])) : date('d-m-Y', strtotime($setting['WRDATE']))?>" size="25" /></td>
                </tr>
                <tr>
                  <td class="label"><label for="wrcode">Gudang / Warehouse</label></td>
                    <td><?php if($idbeli){$disabled = "disabled";}?>
                      <!--<?= fSelect('data[wrcode]',array('style'=>'width:200px',$disabled=>$disabled),$warehouse,$row['WRCODE'])?>-->
                      <?= fInputText('warehouse_format',array('style'=>'width:200px','class'=>'validate[required]','readonly'=>'readonly',$disabled=>$disabled),$warehouse[$row['WRCODE']])?>
                      <input type="hidden" id="data[wrcode]" name="data[wrcode]" value="<?= $row['WRCODE']?>" class="warehouse" />
                      <a class="nyromodal" href="<?= base_url()."ajax/load_warehouse"?>"><img src="<?= base_url()."templates/admin/default/images/icons/information-octagon.png"?>" /></a></td>
                    <td class="label">Total Pembelian</td>
                    <td><input type="text" id="totalbeli" name="totalbeli" autocomplete="off" value="<?php echo fCurrency(@$row['TOTALBELI'])?>" size="15" disabled="disabled"/></td>
                </tr>
                <tr>
                  <td class="label"><label for="wrcode">Supplier</label></td>
                    <td><?php if($idbeli){$disabled = "disabled";}?>
                      <!--<?= fSelect('data[suppno]',array('style'=>'width:200px',$disabled=>$disabled),$supplier,$row['SUPPNO'])?>-->
                      <?= fInputText('supplier_format',array('style'=>'width:200px','class'=>'validate[required]','readonly'=>'readonly',$disabled=>$disabled),$supplier[$row['SUPPNO']])?>
                      <input type="hidden" id="data[suppno]" name="data[suppno]" value="<?= $row['SUPPID']?>" class="supplier" />
                      <a class="nyromodal" href="<?= base_url()."ajax/load_supplier"?>"><img src="<?= base_url()."templates/admin/default/images/icons/information-octagon.png"?>" /></a></td>
                    <td class="label">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                <td colspan="4">
                    <div class="buttons">
                        <?php if ( !$idbeli and !$isopname ) {?>
                            <button name="save" class="button green" onclick="Submit()">Simpan</button>
                        <?php }?>
                        <a href="<?php echo $this->config->item('base_url')?>pembelian" class="button white" >Kembali</a>
                    </div>
                </td>
            </tr>
        </table>
        </fieldset>
				
		<?php if ( $idbeli ):?>
		<fieldset>
			<legend>Detail Barang</legend>
			<dl class="inline">
				<dt><label for="pcode">Nama Barang</label></dt>
                <dd>
                    <?= fInputText('product_format',array('style'=>'width:200px','readonly'=>'readonly','class'=>'validate[required]'))?>
                    <input type="hidden" id="data[pcode]" name="data[pcode]" class="product" >
                    <a class="nyromodal" href="<?= base_url()."ajax/load_product/$row[WRCODE]"?>"><img src="<?= base_url()."templates/admin/default/images/icons/information-octagon.png"?>"></a>
                </dd>
				
				<dt><label for="qtybeli">Quantity</label></dt>
                <dd>
                    <input type="text" id="qtybeli1" name="data[qtybeli1]" autocomplete="off" value="<?php echo fCurrency(@$row['QTYBELI1'])?>" size="15" />
                    <input type="text" id="qtybeli2" name="data[qtybeli2]" autocomplete="off" value="<?php echo fCurrency(@$row['QTYBELI2'])?>" size="15" />
                    <input type="text" id="qtybeli3" name="data[qtybeli3]" autocomplete="off" value="<?php echo fCurrency(@$row['QTYBELI3'])?>" size="15" />
                </dd>
                
                <div class="buttons">
                    <?php if( $row['POSTSTATUS'] == 0 and !$isopname ) {?>
                        <button name="save" class="button green" onclick="Submit()">Simpan</button>
                    <?php }?>
                </div>
		</fieldset>
		<?php endif;?>
        <?php if ( $idbeli ):?>
        <table id="table1" class="gtable sortable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>PCODE</th>
                    <th>Nama Barang</th>
                    <th>Qty1</th>
                    <th>Qty2</th>
                    <th>Qty3</th>
                    <th>Jumlah</th>
                    <?php if( $row['POSTSTATUS'] == 0 ) { ?>
                    <th>Aksi</th>
                    <?php }?> 
                </tr>
            </thead>
            <tbody>
                <?php 
                $i=0;
                $total=0;
                foreach ( $detail as $rec ):
                $total += $rec['JMLBELI'];
                
                $conv = rekonversi($konversi, $rec['PCODE'], $rec['QTYBELI']);
                ?>
                <tr>
                    <td><?= ++$i?></td>
                    <td><?= $rec['PCODE']?></td>
                    <td><?= $rec['PNAME']?></td>
                    <td><?= $conv['QTY1']?></td>
                    <td><?= $conv['QTY2']?></td>
                    <td><?= $conv['QTY3']?></td>
                    <td><?= fCurrency($rec['JMLBELI'])?></td>
                    <?php if( $row['POSTSTATUS'] == 0 ) { ?>
                    <td>
                        <a href="<?php echo base_url().$cpanel_dir."pembelian/delete_detail/$idbeli/$rec[IDDBELI]"?>" title="Delete" onclick="return confirm('Apakah yakin hapus data ini?');"><div class="btn-delete"></div></a>
					</td>
                    <?php }?>
                </tr>
                <?php endforeach;?>
                <tr>
                    <td colspan="<?= ($row['POSTSTATUS'] == 0) ? 6 : 5 ?>" align="right"><b>TOTAL</b></td>
                    <td><?= fCurrency($total)?><input type="hidden" name="total" value="<?= $total?>"></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="<?= ($row['POSTSTATUS'] == 0) ? 8 : 7 ?>" align="center">
                        <?php if( $row['POSTSTATUS'] == 0 and !$isopname ) {?>
                            <button id="sahkan" name="sahkan" type="submit" class="button green">Sahkan</button>
                        <?php }else {?>
                            <button id="cetakfaktur" name="cetakfaktur" type="submit" class="button green" data-value="<?= $row['IDBELI']?>">Cetak Faktur Penerimaan</button>
                        <?php }?>
                    </td>
                </tr>
            </tbody>
        </table>
      <?php endif;?>
    </form>
</article>

<script>
    $ = jQuery;
    
    $(document).ready(function(){
        $("#pcode_format").xautox('<?= base_url()."ajax/product"?>', {targetid: "pcode"});    
    });
    
    $('#sahkan').click(function(){
        $('#product_format').removeClass('validate[required]');        
    });
    
    $('#cetakfaktur').click(function(){
        var idbeli = $(this).attr('data-value');
        
        location.href = '<?= base_url()."pembelian/faktur/"?>' + idbeli;
    })
</script>