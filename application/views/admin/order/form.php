<script type="text/javascript" src="<?php echo base_url()?>templates/admin/default/js/jquery.xautox.js"></script> 
<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >    
    <h1><?php echo @$page_title?></h1>
    
    <?php if( $isopname ) {?>
        <div class="alert">Tidak dapat melakukan transaksi order karena sedang dilakukan proses opname</div>
    <?php }?>
    
    <?= $transaction_message?>
    
    <form id="form" action="" class="uniform" method="post">
        <table width="100%">
            <tr>
                <td width="60%">
                    <table width="100%">
                        <tr>
                            <td width="60%">
                                <?php if ( empty($orderno) ) {?>
                                    <fieldset>
                                        <legend>Detail Barang</legend>
                                        <table>
                                            <tr>
                                                <td width="80%" style="padding:15px 10px 15px 0;">
                                                    <label for="pcode"><b>Nama Barang</b></label>
                                                    <?= fInputText('product_format',array('style'=>'width:300px'))?>
                                                    <input type="hidden" id="data[pcode]" name="data[pcode]" class="product" >
                                                </td>
                                                <td>
                                                    <!--<a name="save" class="button green" onclick="Submit()">+ Item Baru</a>-->
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="justify" colspan="2">
                                                    <span style="color: red;"><i># Untuk jumlah barang = 1 masukkan nama barang, pilih kemudian tekan enter</i></span><br>
                                                    <span style="color: red;"><i># Untuk jumlah barang > 1 masukkan quantity, (*) dan nama barang (contoh : 10*Nama Barang) - tanpa spasi</i></span><br>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                <?php } else {?>
                                    <fieldset>
                                        <legend>Detail Penerimaan</legend>
                                        <table>
                                            <tr>
                                                <td style="padding: 10px;"><h3>No. Invoice</h3></td>
                                                <td style="padding: 10px;"><h3><?= $row['INVNO']?></h3></td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                <?php }?>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <a class="button blue" href="<?= base_url().'order'?>">Daftar</a>
                                <?php if ( empty($orderno) ) {?>
                                <a class="button red" href="<?= base_url().'cart/reset_chart/S'?>" onclick="return confirm('Anda yakin akan mereset chart?')">Reset</a>
                                <?php }?>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <table id="table1" class="gtable sortable">
                                    <thead>
                                        <tr id="head">
                                            <th>NO</th>
                                            <th>PCODE</th>
                                            <th>NAMA BARANG</th>
                                            <th>HARGA SATUAN</th>
                                            <th>QTY/m3</th>
                                            <th>DISC</th>
                                            <th>TOTAL</th>
                                            <?php if ( empty($orderno) ) {?>
                                            <th>AKSI</th>
                                            <?php }?>
                                        </tr>
                                        <?php if($detail) {
                                            $i = 0;
                                            $total = 0;
                                            foreach ( $detail as $dt ) {
                                                $total += $dt['JMLBELI'];   
                                        ?>
                                        <tr>
                                            <td><?= ++$i?></td>    
                                            <td><?= $dt['PCODE']?></td>    
                                            <td><?= $dt['PNAME']?></td>    
                                            <td align="right"><?= fCurrency($dt['AMOUNTITEM'])?></td>    
                                            <td align="center"><?= $dt['QTY']?></td>    
                                            <td align="center"><?= $dt['DISC']?></td>    
                                            <td align="right"><?= fCurrency($dt['AMOUNT'])?></td>    
                                        </tr>
                                        <?php }
                                        }?>
                                    </thead>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="35%">
                    <table style="width: 100%;">
                        <tr>
                            <td>
                                <fieldset>
                                    <legend>Customer</legend>
                                    <table>
                                        <tr>
                                            <td width="80%">
                                                <label for="pcode"><b>Customer</b></label>
                                            </td>
                                            <td>
                                                <?php if(empty($orderno)){?>
                                                <?= fInputText('customer_format',array('style'=>'width:200px'))?>
                                                <input type="hidden" id="outno" name="data[outno]" class="product" >
                                                <?php } else {?>
                                                <?= $a_outlet[$row['OUTNO']]['OUTNAME']?>
                                                <?php }?>
                                            </td>
                                        </tr>
                                        <tr <?= (empty($orderno) or empty($row['OUTNO'])) ? 'style="display: none;"' : '' ?> class="suppdetail">
                                            <td>
                                                <label for="pcode"><b>Alamat</b></label>
                                            </td>
                                            <td>
                                                <label id="address" for="pcode"><?= $a_outlet[$row['OUTNO']]['OUTADD']?></label>
                                            </td>
                                        </tr>
                                        <tr <?= (empty($orderno) or empty($row['OUTNO'])) ? 'style="display: none;"' : '' ?> class="suppdetail">
                                            <td>
                                                <label for="pcode"><b>Telp</b></label>
                                            </td>
                                            <td>
                                                <label id="phone" for="pcode"><?= $a_outlet[$row['OUTNO']]['OCITYa']?></label>
                                            </td>
                                        </tr>
                                        <tr class="discount" style="display: none;">
                                            <td colspan="2" align="center"><b>DISCOUNT</b></td>
                                        </tr>
                                        <tr id="save">
                                            <td width="80%" colspan="2">
                                                <!--<a name="save" class="button green" onclick="Submit()" style="margin-top: 5px;width: 95%;">+ Supplier Baru</a>-->
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <fieldset>
                                    <legend>PEMBAYARAN</legend>
                                    <table width="100%">
                                        <tr>
                                            <td style="font-size: 20px;"><b>TOTAL</b></td>
                                            <td style="font-size: 20px;" align="right">
                                                <b><span id="total">Rp. <?= ($row['SUBTOTAL'] > 0) ? fCurrency($row['SUBTOTAL']) : 0?></span></b>
                                                <input type="hidden" id="total" name="total">
                                            </td>
                                        </tr>
                                        <?php if(empty($orderno)) {?>
                                        <tr>
                                            <td colspan="2"><b>Pembayaran</b></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"><input style="width: 100%;height:25px;text-align: right;font-size: 18px;" id="payment" name="payment" value="" /></td>
                                        </tr>
                                        <?php }?>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <fieldset>
                                    <legend>Catatan</legend>
                                    <?php if(empty($orderno) and !$isopname) {?>
                                    <textarea style="width: 95%;" name="note"></textarea>
                                    <input type="submit" name="submit" class="button green" style="width: 100%;" value="SELESAI">
                                    <?php } else {?>
                                    <?= $row['NOTE']?>
                                    <?php }?>
                                </fieldset>
                            </td> 
                        </tr>
                        <?php if(!empty($orderno)) {?>
                        <tr>
                            <td colspan="<?= ( $row['OSTATUSCODE'] == 2 ) ? 9 : 7;?>" align="center">
                                <?php if ( !$isOpname ) {?>
                                    <?php if( $row['OSTATUSCODE'] == 2 ) {?>
                                        <button id="proses" name="proses" type="submit" class="button green">Proses</button>
                                    <?php }else {?>
                                        <button id="invoice" data-value='<?= $orderno?>' data-status='<?= $row['PRINTINV']?>' name="invoice" type="submit" class="button green <?= ($row['PRINTINV']) ? "nyromodal" : ""?>">Cetak Invoice</button>
                                        <a id="link-login" class="nyromodal" href="<?= base_url()."ajax/login/$row[ORDERNO]"?>" style="display: none;"><img src="<?= base_url()."templates/admin/default/images/icons/information-octagon.png"?>"></a>
                                        <button id="suratjalan" data-value='<?= $orderno?>' data-status='<?= $row['PRINTINV']?>' name="suratjalan" type="submit" class="button green <?= ($row['PRINTINV']) ? "nyromodal" : ""?>">Cetak Surat Jalan</button>
                                        <a id="link-login-suratjalan" class="nyromodal" href="<?= base_url()."ajax/login/$row[ORDERNO]/0"?>" style="display: none;"><img src="<?= base_url()."templates/admin/default/images/icons/information-octagon.png"?>"></a>
                                    <?php }
                                }?>
                            </td>
                        </tr>
                        <?php }?>
                    </table>
                </td>
            </tr>
        </table> 
    </form>
</article>

<script type="text/javascript">
    $ = jQuery;
    
    var qty = 1;
    var query = null;
    $(document).ready(function(){
        <?php if(empty($orderno)){?>
        loadCart();
        <?php }?>
        
        $('#product_format').focus();
        
        $("#product_format").autocomplete({
            source: function( request, response ) {
            var key = request.term.split('*');
            
            if(key.length > 1) {
                qty = key[0];
                query = key[1];
            } else {
                qty = 1;
                query = key[0];
            }
            
            $.ajax( {
                  url: '<?php echo base_url().'ajax/product/U'?>',
                  dataType: "json",
                  data: {
                    q : query
                  },
                  success: function( data ) {
                    response( data );
                    
                    if ( data.length == 1 ) {
                        $('li.ui-menu-item').eq(0).click();
                    }    
                    
                    $(this).val('');
                  }
                });
            },
            minLength: 2,
            select: function( event, ui ) {
                bindResponse(ui.item, qty);    
            }
        });
        
        $("#customer_format").autocomplete({
            source: function( request, response ) {
            $.ajax( {
                  url: '<?php echo base_url().'ajax/outlet'?>',
                  dataType: "json",
                  data: {
                    q : request.term
                  },
                  success: function( data ) {
                    response( data );  
                  }
                });
            },
            minLength: 2,
            select: function( event, ui ) {
                $("#outno").val(ui.item.key);
                $("tr.suppdetail").show();           
                $("label#address").text(ui.item.address);           
                $("label#phone").text(ui.item.phone);           
                $("tr#save").hide();
                
                bindCustomer(ui.item.key);           
            }
        });
        
        $('#form').submit(function(){
            if(!confirm('Apakah anda yakin akan menyimpan data ini?')) {
                return false;    
            }
        });
    });
    
    function initGrid() {
        $("#table1 input[id^='pprice'], #table1 input[id^='pqty'], #table1 input[id^='pdisc']").focusout(function(){
            var tr = $(this).parents('tr').eq(0);
            var pcode = tr.find("input[id^='pcode']").val();
            var pprice = tr.find("input[id^='pprice']").val();
            var pqty = tr.find("input[id^='pqty']").val();
            var pdisc = tr.find("input[id^='pdisc']").val();
            
            $.ajax({
                url   : '<?php echo base_url().'ajax/cart'?>',
                type  : 'post',
                data  : {
                    mode : 'update',
                    p : 'S',
                    q : new Array(pcode, pprice, pqty, pdisc)
                },
                success : function(data) {
                    loadCart();
                }    
            })    
        });
    }
    
    function bindResponse(data, qty) {
        $.ajax({
          url   : '<?php echo base_url().'ajax/cart'?>',
          type  : 'post',
          data  : {
              mode : 'insert',
              t : qty,
              p : 'S',
              q : data
          },
          success : function(data) {
              $('#product_format').val('');
              loadCart();
          }
        });
    }
    
    function bindCustomer(outno) {
        $.ajax({
          url   : '<?php echo base_url().'ajax/outlet_promo'?>',
          type  : 'post',
          dataType : 'json',
          data  : {
              q : outno
          },
          success : function(data) {
              $('tr#detail_disc').remove();
              
              if( data.count > 0 ) {
                $("tr.discount").show();    
                $("tr.discount").after('<tr id="detail_disc"><td colspan="2">'+ data.html +'</td></tr>');    
              }
          }
        });
    }
    
    function loadCart() {
        $.ajax({
          url   : '<?php echo base_url().'ajax/loadcart'?>',
          type  : 'POST',
          dataType: "json",
          data  : {
            q : 'S',
          },
          success : function ( data ) {
              $('#table1 tr').not('tr#head').remove();
              $('#table1').append(data.html);
              $('span#total').text(data.total);
              $('input#total').val(data.numtotal);
              $('input#payment').val(data.numtotal);
              initGrid();
          }
        });    
    }
    
    $('#invoice').click(function(){
        var orderno = $(this).attr('data-value');
        var status = $(this).attr('data-status');
        
        if(status == '1') {
            $('a#link-login').trigger('click');
        } else
            location.href = '<?= base_url()."order/invoice/"?>' + orderno;
        return false;    
    });
    
    $('#suratjalan').click(function(){
        var orderno = $(this).attr('data-value');
        var status = $(this).attr('data-status');
        
        $('a#link-login-suratjalan').trigger('click');
        
        return false;    
    });
    
    function usediscount(elem) {
        var href = $(elem).attr('href');
        
        $.ajax({
          url   : href,
          type  : 'POST',
          success : function ( data ) {
            loadCart();
            
            $('tr.discount').hide();  
            $('tr#detail_disc').remove();  
          }
        });
    }
</script>
