<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >	
	<h1><?php echo @$page_title?></h1>
	
	<form id="form" action="<?php echo base_url().$cpanel_dir.'order'?>" method="post">
		<div class="tablefooter clearfix">
			<div class="actions">
				<b>Cari :</b>
				<input type="text" name="q" id="q" value="<?php echo $q ?>" autocomplete="off" />
                <?php echo str_repeat("&nbsp;",3)?>
                <b>Page :</b>
				<?php echo fPaging($page,$limit,$rows['total'],array('name'=>'page','onchange'=>'jQuery("#form").submit()'))?>
				<?php echo str_repeat("&nbsp;",3)?>
				<input type="submit" class="button blue" value="Filter">
				<?php if($c_create){?>
                <?php echo str_repeat("&nbsp;",3)?>
				<a href="<?php echo base_url()?>order/form" class="button green" >Tambah</a>
			    <?php echo str_repeat("&nbsp;",3)?>
                <?php }?>
            </div>
		</div>
		<br />
		<table id="table1" class="gtable sortable">
			<thead>
				<tr>
					<th>No Order</th>
					<th>Gudang/Van</th>
					<th>Sales</th>
					<th>Outlet</th>
					<th>Tanggal Transaksi</th>
                    <th>Total Order</th>
                    <th>Status</th>
					<th>Print Inv</th>
                    <th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php
                if(isset($rows['data'])): 
                $i=0;
                $day = get_hari_all();
                foreach($rows['data'] as $row): ?>
				<tr>
					<td width="1%"><?php echo $row['ORDERNO'] ?></td>
					<td><?php echo !empty($row['GUDANG']) ? $row['GUDANG'] : $row['VANNAME']?></td>
					<td><?php echo $row['SALES']?></td>
					<td><?php echo substr($row['OUTLET'],0,15)?></td>
					<td><?php echo dateIndo($row['ORDERDATE'])?></td>
                    <td><?php echo fCurrency($row['SUBTOTAL'])?></td>
					<td>
                        <?php if ( $row['OSTATUSCODE'] == 2 ) {?>
                            <label class="warning">Diajukan</label>
                        <?php }elseif ( $row['OSTATUSCODE'] == 3 ) {?>
                            <label class="available">Diproses</label>
                        <?php }?>
                    </td>
                    <td>
                        <?php if ( $row['PRINTINV'] == 0 ) {?>
                            <label class="warning">Belum</label>
                        <?php }elseif ( $row['PRINTINV'] == 1 ) {?>
                            <label class="available">Tercetak</label>
                        <?php }?>
                    </td>
                    <td>
                        <?php if($c_update){?>
                        <a href="<?php echo base_url()."order/form/$row[ORDERNO]"?>" title="Edit" ><div class="btn-edit"></div></a>
                        <?php }?>
                        <?php if ( $row['OSTATUSCODE'] == 2 and $c_delete){?>    
                        <a href="<?php echo base_url()."order/delete/$row[ORDERNO]"?>" title="Delete" onclick="return confirm('Apakah yakin hapus data ini?');"><div class="btn-delete"></div></a>                    
                        <?php }?>
                    </td>
				</tr>
				<?php endforeach; endif; ?>
			</tbody>
		</table>
  </form>
</article>
<script type="text/javascript">
function goCetak() {
    var smt = jQuery('#semester').val();
    var thn = jQuery('#tahun').val();
    window.open('<?php echo base_url()."$cpanel_dir/order/rekap/"?>'+smt+"/"+thn);
}
</script>