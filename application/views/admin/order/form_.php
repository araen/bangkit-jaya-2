<style>
.label{}
</style>
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/default/js/jquery.xautox.js"></script>    
<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >    
    <h1><?php echo @$page_title?></h1>
    
    <?php if( $isopname ) {?>
        <div class="alert">Tidak dapat melakukan transaksi order karena sedang dilakukan proses opname</div>
    <?php }?>
    
    <?= $transaction_message?>
    
    <form id="form" action="" class="uniform" method="post">
        <fieldset>
            <legend><?php echo @$page_title?></legend>
            <table width="100%" border="0" class="form">
                <tr>
                    <td class="label">Nomor Order</td>
                    <td><?php if($orderno){$disabled = "disabled";echo "<input type='hidden' name='orderno' value='$orderno'/>";}?>
                    <input type="text" id="orderno2" name="orderno" autocomplete="off" value="<?php echo @$row['ORDERNO']?>" size="15" disabled="disabled" style="width: 200px;"/></td>
                    <td class="label"><label for="label">Tanggal Transaksi</label></td>
                    <td><input type="text" id="orderdate" name="data[orderdate]" readonly="readonly" autocomplete="off" value="<?php echo !empty($row['ORDERDATE']) ? date('d-m-Y',strtotime($row['ORDERDATE'])) : date('d-m-Y', strtotime($setting['WRDATE']))?>" size="25" /></td>
                </tr>
                <tr>
                    <td class="label">Sales</td>
                    <td>
                        <?php if($orderno){$disabled = "disabled";}?>
                        <?= fInputText('sales_format',array('style'=>'width:200px','class'=>'validate[required]','readonly'=>'readonly',$disabled=>$disabled),$sales[$row['SLSNO']])?>
                        <input type="hidden" id="data[slsno]" name="data[slsno]" value="<?= $row['SLSNO']?>" class="sales" />
                        <input type="hidden" id="slstype" class="slstype" name="slstype" value="<?= $row['STCODE']?>" />
                        <?php if ( !$orderno and !$isopname ) {?>
                        <a class="nyromodal" href="<?= base_url()."ajax/load_sales"?>"><img src="<?= base_url()."templates/admin/default/images/icons/information-octagon.png"?>" /></a>
                        <?php }?>                    </td>
                    <td class="label"><label for="label">Total Order</label></td>
                    <td><input type="text" id="subtotal" name="subtotal" autocomplete="off" value="<?php echo fCurrency(@$row['SUBTOTAL'])?>" size="25" disabled="disabled"/></td>
                </tr>
                <tr>
                  <td class="label"><span class="label">Outlet</span></td>
                  <td><?php if($orderno){$disabled = "disabled";}?>
                    <?= fInputText('outlet_format',array('style'=>'width:200px','class'=>'validate[required]','readonly'=>'readonly',$disabled=>$disabled),$outlet[$row['OUTNO']])?>
                    <input type="hidden" id="data[outno]" name="data[outno]" value="<?= $row['OUTNO']?>" class="outlet" />
                    <?php if ( !$orderno and !$isopname ) {?>
                    <a class="nyromodal" href="<?= base_url()."ajax/load_outlet"?>"><img src="<?= base_url()."templates/admin/default/images/icons/information-octagon.png"?>" /></a></td>
                    <?php }?></td>
                  <td class="label">&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                    <?php if( empty($orderno) or $row['STCODE'] == 'TO'  ) {?>
                    <td class="label warehouse-field">Gudang</td>
                    <td class="warehouse-field">
                        <?php if($orderno){$disabled = "disabled";}?>
                        <?= fInputText('warehouse_format',array('style'=>'width:200px','class'=>'validate[required]','readonly'=>'readonly',$disabled=>$disabled),$warehouse[$row['WRCODE']])?>
                        <input type="hidden" id="data[wrcode]" name="data[wrcode]" value="<?= $row['WRCODE']?>" class="warehouse" />
                        <?php if ( !$orderno and !$isopname ) {?>
                            <a class="nyromodal" href="<?= base_url()."ajax/load_warehouse"?>"><img src="<?= base_url()."templates/admin/default/images/icons/information-octagon.png"?>" /></a>
                        <?php }?>                    
                    </td>
                    <?php } else {?>
                        <td class="label">Van</td>
                        <td>
                            <?php if($idbeli){$disabled = "disabled";}?>
                            <?= fInputText('van_format',array('style'=>'width:200px','class'=>'validate[required]','readonly'=>'readonly',$disabled=>$disabled),$van[$row['VANCODE']])?>
                            <input type="hidden" id="data[vancode]" name="data[vancode]" value="<?= $row['VANCODE']?>" class="warehouse" />
                            <?php if ( !$orderno and !$isopname ) {?>
                                <a class="nyromodal" href="<?= base_url()."ajax/load_warehouse"?>"><img src="<?= base_url()."templates/admin/default/images/icons/information-octagon.png"?>" /></a>
                            <?php }?>                    
                        </td>
                    <?php }?>
                    <td class="label">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                
                <tr>
                    <td colspan="4">
                        <div class="buttons">
                        <?php if ( !$orderno and !$isopname ) {?>
                        <button name="save" class="button green" onclick="Submit()">Simpan</button>
                        <?php }?>
                        <a href="<?php echo $this->config->item('base_url')?>order" class="button white" >Kembali</a>                        </div>                    </td>
                </tr>
            </table>
      </fieldset>
				
		<?php if ( $orderno ):?>
		<fieldset>
			<legend>Detail Barang</legend>
			<dl class="inline">
				<dt><label for="pcode">Nama Barang</label></dt>
                <dd>
                    <?= fInputText('product_format',array('style'=>'width:200px','readonly'=>'readonly','class'=>'validate[required]'))?>
                    <input type="hidden" id="data[pcode]" name="data[pcode]" class="product" >
                    <?php if( $row['STCODE'] == 'TO' ) {?>
                    <a class="nyromodal" href="<?= base_url()."ajax/load_product_gudang/$row[WRCODE]"?>"><img src="<?= base_url()."templates/admin/default/images/icons/information-octagon.png"?>"></a>
                    <?php }else{?>
                    <a class="nyromodal" href="<?= base_url()."ajax/load_product_van/$row[VANCODE]"?>"><img src="<?= base_url()."templates/admin/default/images/icons/information-octagon.png"?>"></a>
                    <?php }?>
                </dd>
				
				<dt><label for="qty">Quantity</label></dt>
                <dd>
                    <input type="text" id="qty1" name="data[qty1]" autocomplete="off" value="<?php echo fCurrency(@$row['QTY1'])?>" size="15" />
                    <input type="text" id="qty2" name="data[qty2]" autocomplete="off" value="<?php echo fCurrency(@$row['QTY2'])?>" size="15" />
                    <input type="text" id="qty3" name="data[qty3]" autocomplete="off" value="<?php echo fCurrency(@$row['QTY3'])?>" size="15" />
                </dd>
				
				<!--<dt><label for="jmlbeli">Sub Total</label></dt>
                <dd>
                    <input type="text" id="jmlbeli" name="data[jmlbeli]" autocomplete="off" value="<?php echo fCurrency(@$row['JMLBELI'])?>" size="15" />
                </dd>-->
                <?php if($c_create){?>
                <div class="buttons">
                    <?php if( $row['OSTATUSCODE'] == 2 and !$isopname ) {?>
                        <button name="save" class="button green" onclick="Submit()">Simpan</button>
                    <?php }?>
                </div>
                <?php }?>
		</fieldset>
		<?php endif;?>
        <?php if ( $orderno ):?>
        <table id="table1" class="gtable sortable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>PCODE</th>
                    <th>Nama Barang</th>
                    <th>Qty1</th>
                    <th>Qty2</th>
                    <th>Qty3</th>
                    <th>Jumlah</th>
                    <?php if( $row['OSTATUSCODE'] == 2 ) {?>
                    <th>Stock</th>
                    <th>Aksi</th>
                    <?php }?>
                </tr>
            </thead>
            <tbody>
                <?php 
                $i=0;
                $total=0;
                $outstock = 0;
                $statusstock = true;
                foreach ( $detail as $rec ):
                    if ( $rec['QTY'] > $rec['STOCK'] ) {
                        $statusstock = false;
                        ++$outstock;
                    }
                    $conv = rekonversi($konversi, $rec['PCODE'], $rec['QTY']);    
                    $total += $rec['AMOUNT'];
                ?>
                <tr>
                    <td><?= ++$i?></td>
                    <td><?= $rec['PCODE']?></td>
                    <td><?= $rec['PNAME']?></td>
                    <td><?= $conv['QTY1']?></td>
                    <td><?= $conv['QTY2']?></td>
                    <td><?= $conv['QTY3']?></td>
                    <td><?= fCurrency($rec['AMOUNT'])?></td>
                    <?php if( $row['OSTATUSCODE'] == 2 ){?>
                    <td>
                        <?php if ( $statusstock ) {?>
                            <label class="available">Available</label>
                        <?php } else {?>
                            <label class="danger">Out of Stock</label>
                        <?php }?>
                    </td>
                    <td>
                        <a href="<?php echo base_url().$cpanel_dir."order/delete_detail/$orderno/$rec[DORDERNO]"?>" title="Delete" onclick="return confirm('Apakah yakin hapus data ini?');"><div class="btn-delete"></div></a>
                    </td>
                    <?php }?>
                </tr>
                <?php endforeach;?>
                <tr>
                    <td colspan="<?= ( $row['OSTATUSCODE'] == 2 ) ? 7 : 6;?>" align="right"><b>TOTAL</b></td>
                    <td colspan="2"><b><?= fCurrency($total)?><input type="hidden" name="total" value="<?= $total?>"></b></td>
                </tr>
                <tr>
                    <td colspan="<?= ( $row['OSTATUSCODE'] == 2 ) ? 9 : 7;?>" align="center">
                        <?php if ( !$isOpname ) {?>
                            <?php if( $outstock == 0 and  $row['OSTATUSCODE'] == 2 ) {?>
                                <button id="proses" name="proses" type="submit" class="button green">Proses</button>
                            <?php }else {?>
                                <button id="invoice" data-value='<?= $orderno?>' data-status='<?= $row['PRINTINV']?>' name="invoice" type="submit" class="button green <?= ($row['PRINTINV']) ? "nyromodal" : ""?>">Cetak Invoice</button>
                                <a id="link-login" class="nyromodal" href="<?= base_url()."ajax/login/$row[ORDERNO]"?>" style="display: none;"><img src="<?= base_url()."templates/admin/default/images/icons/information-octagon.png"?>"></a>
                            <?php }
                        }?>
                    </td>
                </tr>
            </tbody>
        </table>
      <?php endif;?>
    </form>
</article>

<script>
    $ = jQuery;
    
    $(document).ready(function(){
        $("#pcode_format").xautox('<?= base_url()."ajax/product"?>', {targetid: "pcode"});    
        
        $("#outno_format").xautox('<?= base_url()."ajax/outlet"?>', {targetid: "outno"});    
    });
    
    $('#proses').click(function(){
        $('#product_format').removeClass('validate[required]');        
    });
    
    $('#salestype').change(function(){
        console.log($(this).val());    
    });
    
    $('#invoice').click(function(){
        var orderno = $(this).attr('data-value');
        var status = $(this).attr('data-status');
        
        if(status == '1') {
            $('a#link-login').trigger('click');
        } else
            location.href = '<?= base_url()."order/invoice/"?>' + orderno;
        return false;    
    });
</script>
