<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >	
	<h1><?php echo @$page_title?></h1>
	
	<form id="form" action="<?php echo base_url().$cpanel_dir.'penyewa'?>" method="post">
		<div class="tablefooter clearfix">
			<div class="actions">
				<b>Cari :</b>
				<input type="text" name="q" id="q" value="<?php echo $q ?>" autocomplete="off" />
                <?php echo str_repeat("&nbsp;",3)?>
                <b>Page :</b>
				<?php echo fPaging($page,$limit,$rows['total'],array('name'=>'page','onchange'=>'jQuery("#form").submit()'))?>
				<?php echo str_repeat("&nbsp;",3)?>
				<input type="submit" class="button blue" value="Filter">
				<?php echo str_repeat("&nbsp;",3)?>
				<a href="<?php echo base_url().$cpanel_dir?>pembayaran/form" class="button green" >Tambah</a>
			    <?php echo str_repeat("&nbsp;",3)?>
                <?= fSelect('semester',array(),array('1'=>'Semester 1','2'=>'Semester 2'),$row['IDBLOK'])?>
                <?= fSelect('tahun',array(),array(date('Y')=>date('Y'),date('Y')+1=>date('Y')+1),$row['IDBLOK'])?>
                <input type="button" class="button green" name="rekap" value="Cetak Rekap" onclick="goCetak()">
            </div>
		</div>
		<br />
		<table id="table1" class="gtable sortable">
			<thead>
				<tr>
					<th>No</th>
                    <th>Nama</th>
					<th>Blok</th>
					<th>Total Pembayaran</th>
                    <th>Tgl Bayar Terakhir</th>
                    <th>Sisa Bayar</th>
                    <th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php
                if(isset($rows['data'])): 
                $i=0;
                $day = get_hari_all();
                foreach($rows['data'] as $row): ?>
				<tr>
					<td width="1%"><?php echo ++$i?></td>
					<td><?php echo $row['NAMA']?></td>
					<td><?php echo $row['BLOK']?></td>
					<td><?php echo fCurrency($row['TOTALBAYAR'])?></td>
                    <td><?php echo dateIndo($row['TGLBAYAR'])?></td>
                    <td><?php echo fCurrency($row['TOTALBAYAR'] - $row['JMLBAYAR'])?></td>
                    <td>
                        <a href="<?php echo base_url().$cpanel_dir."pembayaran/form/$row[ID]"?>" title="Edit" ><div class="btn-edit"></div></a>
                        <a href="<?php echo base_url().$cpanel_dir."pembayaran/delete/$row[ID]"?>" title="Delete" onclick="return confirm('Apakah yakin hapus data ini?');"><div class="btn-delete"></div></a>                    
                    </td>
				</tr>
				<?php endforeach; endif; ?>
			</tbody>
		</table>
  </form>
</article>
<script type="text/javascript">
function goCetak() {
    var smt = jQuery('#semester').val();
    var thn = jQuery('#tahun').val();
    window.open('<?php echo base_url()."$cpanel_dir/pembayaran/rekap/"?>'+smt+"/"+thn);
}
</script>