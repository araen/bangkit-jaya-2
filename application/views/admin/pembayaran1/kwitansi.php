<html>
<head>
<title>Kwitansi Pembayaran</title>  
<style type="text/css">
#main_wrapper {
	margin:auto;
	font-family:Arial, Helvetica, sans-serif;
	width:18cm;
}

table{font-size:12px;border-collapse:collapse;}

.style1 {
	font-size: 24px;
	font-weight: bold;
}

th {border-top:1px solid;border-bottom:1px solid}
th,td {padding-top:5px;padding-bottom:5px;}
#rounded-box{border:1px dashed;border-radius: 25px;padding:5px;}
.fill {background-color:#CCCCCC}
</style>
</head>
<body>
<div id="main_wrapper">
<table border="0" width="100%">
  <tr>
    <td colspan="2"><strong>KANTOR DESA PEPE</strong></td>
    <td rowspan="3"><div align="center" class="style1">KWITANSI</div></td>
    <td>Tgl Angsuran</td>
    <td>: <?php echo dateIndo($row['TGLBAYAR'])?></td>
  </tr>
  <tr>
    <td colspan="2">Jl. H. Sulaiman No 1 Betro Sedati Sidoarjo</td>
    <td>No. Penyewa</td>
    <td>: <span style="width:20%"><?php echo $batch['NIK']?></span></td>
  </tr>
  <tr>
    <td colspan="2">Telp. (031) 8910658</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5"><hr /></td>
    </tr>
  <tr>
    <td style="width:15%">Telah terima dari</td>
    <td style="width:20%">: <?php echo $batch['NAMA']?></td>
    <td>&nbsp;</td>
    <td style="width:12%">&nbsp;</td>
    <td style="width:23%">&nbsp;</td>
  </tr>
  <tr>
    <td>Sejumlah uang</td>
    <td>: <span style="width:20%"><?php echo fCurrency($row['JMLBAYAR'])?></span></td>
    <td colspan="3"><div id="rounded-box"><?php echo terbilang($row['JMLBAYAR'])?></div></td>
    </tr>
  
  <tr>
    <td>Untuk keperluan</td>
    <td colspan="4"><div id="rounded-box" class="fill"><em><strong><?php echo $row['KETERANGAN']?></strong></em></div></td>
    </tr>
  
  <?php 
  $i=0;
  foreach ( $detail as $rec ):
  $total += $rec['JMLBAYAR'];
  ?>
  <?php endforeach;?>
  <tr>
    <td colspan="5"><hr /></td>
    </tr>
  
  
  <tr>
    <td>Total Pembayaran</td>
    <td>: <span class="content">
    <?= fCurrency($batch['TOTALBAYAR'])?>
    </span></td>
    <td><em><strong>Perhatian :</strong></em></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Total Angsuran</td>
    <td>: <span class="content">
      <?= fCurrency($total)?>
    </span></td>
    <td rowspan="3" style="border:1px solid">&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right"><?php echo dateIndo(date('d-m-Y'))?></td>
  </tr>
  <tr>
    <td>Sisa Angsuran</td>
    <td>: <span class="content">
      <?= fCurrency($batch['TOTALBAYAR'] - $total)?>
    </span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Status</td>
    <td>:<span class="content">
      <?php echo (($batch['TOTALBAYAR'] - $total) == 0) ? "Lunas":"Belum Lunas"?>
    </span></td>
    <td>&nbsp;</td>
    <td><div align="right">Kepala Adm. Pasar Pepe</div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</div>
</body>
</html>