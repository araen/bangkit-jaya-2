<html>
<head>
<title>Rekapitulasi Pembayaran Angsuran</title>
<style type="text/css">
#main_wrapper{width:1100px;margin:auto;font-family:Arial, Helvetica, sans-serif}
table{border-collapse:collapse}
.content{border:1px solid;font-size: 11px;}
.style1 {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>
<body>
<div id="main_wrapper">
<table width="100%" border="0">
  <tr>
    <td colspan="11"><div align="center" class="style1">LAPORAN TAHUNAN PEMBAYARAN ANGSURAN PASAR TRADISIONAL DESA PEPE SEMESTER <?= $nsemester?> TAHUN <?= $ntahun?></div></td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th class="content">NO</th>
    <th class="content">NAMA LENGKAP</th>
    <th class="content" style="width:8%">BLOK</th>
    <th class="content" style="width:8%">CASH</th>
    <th class="content" style="width:8%">UANG MUKA</th>
    <?php foreach ( $semester as $key => $val ):?>
    <th class="content" style="width:8%"><?= $val?></th>
    <?php endforeach;?>
  </tr>
  <?php 
  $i=0;
  $um = 0;
  $cash = 0;
  foreach ( $rows as $row ):
  $um += $row['JNSBAYAR']['UM']['JMLBAYAR'];
  $cash += $row['JNSBAYAR']['CS']['JMLBAYAR'];
  ?>
  <tr>
    <td class="content" align="center"><?= ++$i?></td>
    <td class="content"><?= $row['NAMA']?></td>
    <td class="content" align="center"><?= $row['BLOK']?></td>
    <td class="content" align="right"><?= fCurrency($row['JNSBAYAR']['CS']['JMLBAYAR'])?></td>
    <td class="content" align="right"><?= fCurrency($row['JNSBAYAR']['UM']['JMLBAYAR'])?></td>
    <?php foreach ( $semester as $key => $val ):?>
    <td class="content" align="right"><?= fCurrency($row['JNSBAYAR']['AG'][str_pad($key,2,'0',STR_PAD_LEFT)]['JMLBAYAR'])?></td>
    <?php endforeach;?>    
  </tr>
  <?php endforeach;?>
  <tr>
    <td class="content">&nbsp;</td>
    <td class="content">&nbsp;</td>
    <td class="content">&nbsp;</td>
    <td class="content">&nbsp;</td>
    <td class="content">&nbsp;</td>
    <?php foreach ( $semester as $key => $val ):?>
    <td class="content">&nbsp;</td>
    <?php endforeach;?>
  </tr>
  <tr>
    <td class="content">&nbsp;</td>
    <td class="content"><div align="center"><strong>TOTAL</strong></div></td>
    <td class="content">&nbsp;</td>
    <td class="content" align="right"><strong>
      <?= fCurrency($cash)?>
    </strong></td>
    <td class="content" align="right"><strong>
      <?= fCurrency($um)?>
    </strong></td>
    <?php foreach ( $semester as $key => $val ):?>
    <td class="content">&nbsp;</td>
    <?php endforeach;?>
  </tr>
</table>
</div>
</body>
</html>
