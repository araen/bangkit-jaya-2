<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>  
<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >    
    <h1><?php echo @$page_title?></h1>
    
    <form id="form" action="" class="uniform" method="post">
        <fieldset>
            <legend><?php echo @$page_title?></legend>
            <dl class="inline">
				<dt><label for="nama">Nama</label></dt>
                <dd>
                	<?php if($id){$disabled = "disabled";echo "<input type='hidden' name='idbatch' value='$id'/>";}?>
					<?= fSelect('data[idpenyewa]',array('style'=>'width:200px',$disabled=>$disabled),$penyewa,$row['IDPENYEWA'])?>
                </dd>
                
                <dt><label for="idblok">Blok Stand</label></dt>
                <dd>
                    <?= fSelect('data[idblok]',array($disabled=>$disabled),$blok,$row['IDBLOK'])?>
                </dd>
                
                <dt>
		          <label for="tgllahir">Tanggal Bayar</label>
		        </dt>
                <dd>
                    <input type="text" id="tglbayar" name="data[tglbayar]" class="date" autocomplete="off" value="<?php echo (!empty($row['TGLBAYAR']) ? date('d-m-Y',strtotime($row['TGLLAHIR'])) :"" )?>" size="25" />
                </dd>
                
                <dt>
                  <label for="tgllahir">Jumlah Bayar</label>
                </dt>
                <dd>
                    <input type="text" id="jmlbayar" name="data[jmlbayar]" autocomplete="off" value="<?php echo (!empty($row['TGLBAYAR']) ? date('d-m-Y',strtotime($row['TGLLAHIR'])) :"" )?>" size="25" />
                </dd>
                
                <dt>
                  <label for="tgllahir">Jenis Pembayaran</label>
                </dt>
                <dd>
                    <?= fSelect('data[jnsbayar]',array(),array("CS"=>"Cash","UM"=>"Uang Muka","AG"=>"Angsuran"),$row['JNSBAYAR'])?>
                </dd>
                
                <dt>
                  <label for="tgllahir">Keterangan</label>
                </dt>
                <dd>
                    <textarea cols="50" rows="5" name="data[keterangan]"></textarea>
                </dd>

                <div class="buttons">
                    <button name="save" class="button green" onclick="Submit()">Simpan</button>
                    <a href="<?php echo $this->config->item('base_url').$cpanel_dir?>pembayaran" class="button white" >Batal</a>
                </div>
        </fieldset>
        <?php if ( $id ):?>
        <table id="table1" class="gtable sortable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Tgl Bayar</th>
                    <th>Jns Bayar</th>
                    <th>Keterangan</th>
                    <th>Jumlah</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $i=0;
                $total=0;
                foreach ( $detail as $rec ):
                $total += $rec['JMLBAYAR'];
                ?>
                <tr>
                    <td><?= ++$i?></td>
                    <td><?= dateIndo($rec['TGLBAYAR'])?></td>
                    <td><?php
                    switch($rec['JNSBAYAR']){
                        case "CS" : echo "Cash";break;
                        case "UM" : echo "Uang Muka";break;
                        case "AG" : echo "Angsuran";break;
                    }
                    ?></td>
                    <td><?= $rec['KETERANGAN']?></td>
                    <td><?= fCurrency($rec['JMLBAYAR'])?></td>
                    <td>
                        <a href="<?php echo base_url().$cpanel_dir."pembayaran/delete_detail/$id/$rec[ID]"?>" title="Delete" onclick="return confirm('Apakah yakin hapus data ini?');"><div class="btn-delete"></div></a>
                        <a href="<?php echo base_url().$cpanel_dir."pembayaran/kwitansi/$id/$rec[ID]"?>" target="_blank" title="Cetak Kwitansi"><div class="btn-check"></div></a>                    </td>
                </tr>
                <?php endforeach;?>
                <tr>
                    <td colspan="4" align="right"><b>TOTAL</b></td>
                    <td><?= fCurrency($total)?></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="4" align="right"><b>SISA PEMBAYARAN</b></td>
                    <td><b><?= fCurrency($row['TOTALBAYAR'] - $total)?></b></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
      <?php endif;?>
    </form>
</article>