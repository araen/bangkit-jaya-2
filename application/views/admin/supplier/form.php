<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>  
<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >    
    <h1><?php echo @$page_title?></h1>
    
    <form id="form" action="" class="uniform" method="post">
        <fieldset>
            <legend><?php echo @$page_title?></legend>
            <dl class="inline">
				<dt><label for="suppno">Kode Supplier</label></dt>
                <dd>
                    <input type="text" id="suppno" name="data[suppno]" autocomplete="off" value="<?php echo @$row['SUPPNO']?>" size="40" />
                </dd>
				
				<dt><label for="suppname">Nama Supplier</label></dt>
                <dd>
                    <input type="text" id="suppname" name="data[suppname]" autocomplete="off" value="<?php echo @$row['SUPPNAME']?>" size="50" />
                </dd>
				
				<dt><label for="suppadd">Alamat</label></dt>
                <dd>
                    <textarea cols="50" name="data[suppadd]" rows="4"><?php echo @$row['SUPPADD']?></textarea>
                </dd>
				
				<dt><label for="suppphone">Telephone</label></dt>
                <dd>
                    <input type="text" id="suppphone" name="data[suppphone]" autocomplete="off" value="<?php echo @$row['SUPPPHONE']?>" size="50" />
                </dd>
				
				<dt><label for="suppfax">No. Fax</label></dt>
                <dd>
                    <input type="text" id="suppfax" name="data[suppfax]" autocomplete="off" value="<?php echo @$row['SUPPFAX']?>" size="50" />
                </dd>
				
				<dt><label for="suppcontact">No. Contact</label></dt>
                <dd>
                    <input type="text" id="suppcontact" name="data[suppcontact]" autocomplete="off" value="<?php echo @$row['SUPPCONTACT']?>" size="50" />
                </dd>
				
				<dt><label for="supptype">Tipe Supplier</label></dt>
                <dd>
                    <input type="text" id="supptype" name="data[supptype]" autocomplete="off" value="<?php echo @$row['SUPPTYPE']?>" size="50" />
                </dd>
				
				<dt><label for="accno">No. ACC</label></dt>
                <dd>
                    <input type="text" id="accno" name="data[accno]" autocomplete="off" value="<?php echo @$row['ACCNO']?>" size="50" />
                </dd>
                <?php if($c_create){?>
                <div class="buttons">
                    <button name="save" class="button green" onclick="Submit()">Simpan</button>
                    <a href="<?php echo $this->config->item('base_url')?>supplier" class="button white" >Batal</a>
                </div>
                <?php }?>
        </fieldset>
    </form>
</article>