<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>  
<?php $cpanel_dir = $this->config->item('cpanel_dir');?>
<article >    
    <h1><?php echo @$page_title?></h1>
    
    <form id="form" action="" class="uniform" method="post">
        <fieldset>
            <legend><?php echo @$page_title?></legend>
            <dl class="inline">
				<dt><label for="stcode">Kode Jenis Sales</label></dt>
                <dd>
                    <input type="text" id="stcode" name="data[stcode]" autocomplete="off" value="<?php echo @$row['STCODE']?>" size="40" />
                </dd>
				
				<dt><label for="stname">Nama Jenis Sales</label></dt>
                <dd>
                    <input type="text" id="stname" name="data[stname]" autocomplete="off" value="<?php echo @$row['STNAME']?>" size="50" />
                </dd>
                
                <div class="buttons">
                    <button name="save" class="button green" onclick="Submit()">Simpan</button>
                    <a href="<?php echo $this->config->item('base_url')?>jnssales" class="button white" >Batal</a>
                </div>
        </fieldset>
    </form>
</article>