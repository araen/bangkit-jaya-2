<?php $cpanel_dir = $this->config->item('cpanel_dir')?>
<article >	
	<h1>File Manager</h1>
	<script>
		function folder(path){
			path = jQuery("#path").val() + "/" + path;
			jQuery("#path").val(path);
			jQuery("#view").submit();
		}
		
		function back(path){
			jQuery("#path").val(path);
			jQuery("#dir").val('');
			jQuery("#view").submit();
		}

		function del(file){
			var conf = confirm("File or Folder will be deleted. Are you sure ?");
			if(conf){
				jQuery("#dir").val('');
				jQuery("#del").val(file);
				jQuery("#view").submit();
			}
		}
		
		jQuery(document).ready(function(){
			jQuery("#form").validationEngine();
		});
	</script>
	<?php if($msg != ''){?>
	<div class="information msg"><?php echo $msg?></div>
	<?php }?>
	<form id="view" action="<?php echo base_url().$cpanel_dir."file/file_url"?>" method="post" class="uniform nyromodal" >
		<?php 
		if($path != 'data'){
			$exp = explode("/",$path);
			unset($exp[count($exp)-1]);
			$paths = implode("/",$exp);
			echo "<input type=\"button\" class=\"button green\" value=\"BACK\" onClick=\"back('$paths')\" />";
		} 
		?>
		<input type="text" id="path" name="path" class="medium" readonly="readonly" value="<?php echo $path?>" />
		&nbsp;/&nbsp;
		<input type="text" id="dir" name="dir" />
		<input type="hidden" id="del" name="del" />
		<button type="submit" class="button orange">Create Folder</button>
	</form>
	<br />
	<ul class="photos">
			<?php
			$base = $this->config->item('base_url');
			foreach($file as $row){
				if(isset($row['name'])){
					echo "<li>";
					if(isset($row['type']) && $row['type']== 'file'){
						echo "<a href=\"javascript:\" onClick=\"setUrl('".$path.'/'.$row['name']."')\" title=\"$row[name]\"><img src=\"".$base.$path.'/'.$row['name']."\" border=0 width=32 height=32 /><div class=\"links\">$row[name]</div></a>";
					}else if(isset($row['type']) && $row['type'] == 'dir'){
						echo "<a href=\"javascript:\" onClick=\"folder('$row[name]')\" ><img src=\"$base/admin/templates/default/images/file-extension/folder.png\" border=0 /><div class=\"links\">$row[name]</div></a>";
					}
					echo "</li>";
				}
			}
			?>
	</ul>
	<br />
	<h2>Upload File (MAX 10M)</h2>
	<hr />
	<form id="form" class="uniform nyromodal" action="<?php echo base_url().$cpanel_dir."file/file_url"?>" method="post" enctype="multipart/form-data" >
		<div class="tablefooter clearfix">
			<div class="actions">
				<label for="photo_file">Upload File :</label>
				<input type="file" id="file" name="userfile" class="validate[required]" />
				<input type="hidden" name="path" value="<?php echo $path?>" />
				<button type="submit" class="button">Upload</button>
			</div>
		</div>
	</form>
</article>

<script type="text/javascript" >
	jQuery(function($) {					
  		$('.nyromodal').nyroModal();
	});
	
	var setUrl = function(url){
		jQuery("#file_url").val(url);
		jQuery.nmTop().close();
	}
</script>