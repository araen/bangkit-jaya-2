<?php $cpanel_dir = $this->config->item('cpanel_dir')?>   
<article >	
	<h1>Image Manager</h1>
	<script>
		function folder(path){
			path = jQuery("#path").val() + "/" + path;
			$("#path").val(path);
			$("#view").submit();
		}
		
		function back(path){
			$("#path").val(path);
			$("#dir").val('');
			$("#view").submit();
		}

		function del(file){
			var conf = confirm("File or Folder will be deleted. Are you sure ?");
			if(conf){
				$("#dir").val('');
				$("#del").val(file);
				$("#view").submit();
			}
		}
		
		$(document).ready(function(){
			$("#form").validationEngine();
		});
	</script>
	<?php if($msg != ''){?>
	<div class="information msg"><?php echo $msg?></div>
	<?php }?>
	<div class="accordion">
	<h3 id="chose">Chose Image</h3>
	<div>
	<form id="view" action="<?php echo base_url().$cpanel_dir."file/image_content"?>" method="post" class="uniform nyromodal" >
		<?php 
		if($path != 'data'){
			$exp = explode("/",$path);
			unset($exp[count($exp)-1]);
			$paths = implode("/",$exp);
			echo "<input type=\"button\" class=\"button green\" value=\"BACK\" onClick=\"back('$paths')\" />";
		} 
		?>
		<input type="text" id="path" name="path" class="medium" readonly="readonly" value="<?php echo $path?>" />
		&nbsp;/&nbsp;
		<input type="text" id="dir" name="dir" />
		<input type="hidden" id="del" name="del" />
		<button type="submit" class="button orange">Create Folder</button>
	</form>
	<br />
	<ul class="photos">
			<?php
			$base = $this->config->item('base_url');
			foreach($file as $row){
				if(isset($row['name'])){
					echo "<li>";
					if(isset($row['type']) && $row['type']== 'file'){
						echo "<a href=\"javascript:\" onClick=\"setUrl('".$base.$path.'/'.$row['name']."')\" title=\"$row[name]\"><img src=\"".$base.$path.'/'.$row['name']."\" border=0 width=32 height=32 /><div class=\"links\">$row[name]</div></a>";
					}else if(isset($row['type']) && $row['type'] == 'dir'){
						echo "<a href=\"javascript:\" onClick=\"folder('$row[name]')\" ><img src=\"$base/admin/templates/default/images/file-extension/folder.png\" border=0 /><div class=\"links\">$row[name]</div></a>";
					}
					echo "</li>";
				}
			}
			?>
	</ul>
	<br />
	<h2>Upload File (MAX 10M)</h2>
	<hr />
	<form id="form" class="uniform nyromodal" action="<?php echo base_url().$cpanel_dir."file/image_content"?>" method="post" enctype="multipart/form-data" >
		<div class="tablefooter clearfix">
			<div class="actions">
				<label for="photo_file">Upload File :</label>
				<input type="file" id="file" name="userfile" class="validate[required]" />
				<input type="hidden" name="path" value="<?php echo $path?>" />
				<button type="submit" class="button">Upload</button>
			</div>
		</div>
	</form>
	</div>
	
	<h3 id="setting">Setting Image</h3>
	<div>
	<form id="form" class="uniform nyromodal" action="<?php echo base_url().$cpanel_dir."file/image"?>" method="post" enctype="multipart/form-data" >
		<dl>
			<dt><label for="newstitle">Image URL :</label></dt>
			<dd><input type="text" id="url" class="medium" readonly="readonly" /></dd>
			<dt><label for="newstitle">Image Title :</label></dt>
			<dd><input type="text" id="title" class="medium" /></dd>
			<dt><label for="newstitle">Image Description :</label></dt>
			<dd><textarea id="desc" class="medium"></textarea></dd>
			<dt><label for="newstitle">Image W x H :</label></dt>
			<dd><input type="text" id="w" />&nbsp;X&nbsp;<input type="text" id="h" /></dd>
			<dt><label for="newstitle">Float :</label></dt>
			<dd>
				<select id="float">
					<option value="">None</option>
					<option value="right">Right</option>
					<option value="left">Left</option>
				</select>
			</dd>
		</dl>
		<p>
		<button type="button" id="insert" class="button">Insert</button>
		</p>
	</form>
	</div>
	</div>
</article>

<script type="text/javascript" >
	$(function($) {					
  		$('.nyromodal').nyroModal();
		$('.accordion > h3:first-child').addClass('active');
		$('.accordion > div').hide();
		$('.accordion > h3:first-child').next().show();
		$('.accordion > h3').click(function(){
			if($(this).hasClass('active')){
				return false
			}
			$(this).parent().children('h3').removeClass('active');
			$(this).addClass('active');
			$(this).parent().children('div').slideUp(200);
			$(this).next().slideDown(200)
		});
		
		$('#insert').click(function(){
			var css="";
			var cls="";

			if ($("#w").val().toString().match(/^[0-9]+(px|%)?$/)) {
				css += "width:"+$("#w").val()+"px;";
			}
			
			if ($("#h").val().toString().match(/^[0-9]+(px|%)?$/)) {
				css += "height:"+$("#h").val()+"px;";
			}
			
			if ($("#float").val() != "") {
				css += "float:"+$("#float").val()+";";
				cls += 'float_'+$("#float").val();
			}

			$('#newscontent').wysiwyg('insertImage', $("#url").val(), { 'title': $("#title").val(),'alt':$("#desc").val(),'style':css,'class':cls});
			$.nmTop().close();
		})
	});
	
	var setUrl = function(url){
		$("#url").val(url);
		
		$("#setting").parent().children('h3').removeClass('active');
		$("#setting").addClass('active');
		$("#setting").parent().children('div').slideUp(200);
		$("#setting").next().slideDown(200)
	}
</script>