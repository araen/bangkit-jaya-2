<?php $cpanel_dir = $this->config->item('cpanel_dir')?>       
<article >	
	<h1>File Manager</h1>
	<script>
		function folder(path){
			path = jQuery("#path").val() + "/" + path;
			jQuery("#path").val(path);
			jQuery("#view").submit();
		}
		
		function back(path){
			jQuery("#path").val(path);
			jQuery("#dir").val('');
			jQuery("#view").submit();
		}

		function del(file){
			var conf = confirm("File or Folder will be deleted. Are you sure ?");
			if(conf){
				jQuery("#dir").val('');
				jQuery("#del").val(file);
				jQuery("#view").submit();
			}
		}
		
		jQuery(document).ready(function(){
			jQuery("#form").validationEngine();
		});
	</script>
	<?php if($msg != ''){?>
	<div class="information msg"><?php echo $msg?></div>
	<?php }?>
	<form id="view" action="" method="post" class="uniform" >
		<?php 
		if($path != 'data'){
			$exp = explode("/",$path);
			unset($exp[count($exp)-1]);
			$paths = implode("/",$exp);
			echo "<input type=\"button\" class=\"button green\" value=\"BACK\" onClick=\"back('$paths')\" />";
		} 
		?>
		Directory&nbsp;:&nbsp;
		<input type="text" id="path" name="path" class="medium" readonly="readonly" value="<?php echo $path?>" />
		&nbsp;/&nbsp;
		<input type="text" id="dir" class="small" name="dir" />
		<input type="hidden" id="del" name="del" />
		<button type="submit" class="button orange">Create Folder</button>
	</form>
	<br />
	<table class="gtable" width="100%">
		<thead>
			<tr>
				<th align="center">Name</th>
				<th align="center">Size</th>
				<th align="center">Action</th>
			</tr>
		</thead>
		<tbody>
			<?php //echo "<pre>";var_dump($file);
			function _format_bytes($a_bytes)
			{
				if($a_bytes == ''){
					return '';
				} elseif ($a_bytes < 1024) {
					return $a_bytes .' B';
				} elseif ($a_bytes < 1048576) {
					return round($a_bytes / 1024, 2) .' KiB';
				} elseif ($a_bytes < 1073741824) {
					return round($a_bytes / 1048576, 2) . ' MiB';
				} elseif ($a_bytes < 1099511627776) {
					return round($a_bytes / 1073741824, 2) . ' GiB';
				} elseif ($a_bytes < 1125899906842624) {
					return round($a_bytes / 1099511627776, 2) .' TiB';
				} elseif ($a_bytes < 1152921504606846976) {
					return round($a_bytes / 1125899906842624, 2) .' PiB';
				} elseif ($a_bytes < 1180591620717411303424) {
					return round($a_bytes / 1152921504606846976, 2) .' EiB';
				} elseif ($a_bytes < 1208925819614629174706176) {
					return round($a_bytes / 1180591620717411303424, 2) .' ZiB';
				} else {
					return round($a_bytes / 1208925819614629174706176, 2) .' YiB';
				}
			}
			
			$base = $this->config->item('base_url');
			foreach($file as $row){
				if(isset($row['name'])){
				echo "<tr>";
				if(isset($row['type']) && $row['type']== 'file'){
					$exp = explode(".",$row['name']);
					$type = array('png','gif','jpg','jpeg');
					if(in_array(strtolower($exp[count($exp)-1]),$type)){
						echo "<td><a href=\"".$base.$path.'/'.$row['name']."\" rel=\"gal\" title=\"$row[name]\" class=\"nyromodal\"><img src=\"".$base."templates/admin/default/images/file-extension/".strtolower($exp[count($exp)-1]).".png\" border=0 />$row[name]</a></td>";
					}else{
						echo "<td><a href=\"".$base.$path.'/'.$row['name']."\"  title=\"$row[name]\"><img src=\"".$base."templates/admin/default/images/file-extension/".strtolower($exp[count($exp)-1]).".png\" border=0 />$row[name]</a></td>";
					}
				}else if(isset($row['type']) && $row['type'] == 'dir'){
					echo "<td><a href=\"javascript:\" onClick=\"folder('$row[name]')\" ><img src=\"".$base."templates/admin/default/images/file-extension/folder.png\" border=0 />$row[name]</a></td>";
				}
				echo "<td>"._format_bytes($row['size'])."</td>";
				echo "<td><a href='javascript:' onClick=\"del('$row[name]')\" title='Delete'><div class='btn-delete'></div></a></td>";
				echo "</tr>";
				}
			}
			?>	
				
		</tbody>
	</table>

	<br />
	<h2>Upload File (MAX 10M)</h2>
	<hr />
	<form id="form" class="uniform" action="" method="post" enctype="multipart/form-data" >
		<div class="tablefooter clearfix">
			<div class="actions">
				<label for="photo_file">Upload File :</label>
				<input type="file" id="file" name="userfile" class="validate[required]" />
				<input type="hidden" name="path" value="<?php echo $path?>" />
				<button type="submit" class="button">Upload</button>
			</div>
		</div>
	</form>
</article>