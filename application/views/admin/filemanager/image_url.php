<?php $cpanel_dir = $this->config->item('cpanel_dir')?>
<script type="text/javascript" src="<?php echo base_url()?>templates/admin/plugins/jquery/jquery-1.7.1.min.js"></script>
<article >	
	<h1>Image Manager</h1>
	<script>
	    jQuery(document).ready(function(){
            jQuery('.nyromodal').nyroModal();
            jQuery("#form").validationEngine();
        });
        
        var setUrl = function(url){
            jQuery("#image_url").val(url);
            jQuery.nmTop().close();
        }
        function folder(path){
			path = jQuery("#path").val() + "/" + path;
            jQuery("#path").empty();
			jQuery("#path").val(path);
            jQuery("#view").submit();
            /*jQuery.ajax({
                url : jQuery('#view').attr('action'),
                type: 'post',
                data: jQuery('#view').serialize(),
                success : function(respon)
                {
                    jQuery('ul.photos').html(respon);
                }    
            });
            return false;*/ 
		}
		
		function back(path){
			jQuery("#path").val(path);
			jQuery("#dir").val('');
			jQuery("#view").submit();
		}

		function del(file){
			var conf = confirm("File or Folder will be deleted. Are you sure ?");
			if(conf){
				jQuery("#dir").val('');
				jQuery("#del").val(file);
				jQuery("#view").submit();
			}
		}
	</script>
	<?php if($msg != ''){?>
	<div class="information msg"><?php echo $msg?></div>
	<?php }?>
	<form id="view" action="<?php echo base_url().$cpanel_dir."file/image_url"?>" method="post" class="uniform nyromodal" >
		<?php 
		if($path != 'data'){
			$exp = explode("/",$path);
			unset($exp[count($exp)-1]);
			$paths = implode("/",$exp);
			echo "<input type=\"button\" class=\"button green\" value=\"BACK\" onClick=\"back('$paths')\" />";
		} 
		?>
		<input type="text" id="path" name="path" class="medium" readonly="readonly" value="<?php echo $path?>" />
		&nbsp;/&nbsp;
		<input type="text" id="dir" name="dir" />
		<input type="hidden" id="del" name="del" />
		<button type="submit" class="button orange">Create Folder</button>
	</form>
	<br />
	<ul class="photos">
			<?php
			$base = $this->config->item('base_url');
			foreach($file as $row){
				if(isset($row['name'])){
					echo "<li>";
					if(isset($row['type']) && $row['type']== 'file'){
						echo "<a href=\"javascript:\" onClick=\"setUrl('".$path.'/'.$row['name']."')\" title=\"$row[name]\"><img src=\"".$base.$path.'/'.$row['name']."\" border=0 width=32 height=32 /><div class=\"links\">$row[name]</div></a>";
					}else if(isset($row['type']) && $row['type'] == 'dir'){
						echo "<a href=\"javascript:\" onClick=\"folder('$row[name]')\" ><img src=\"".$base."templates/admin/default/images/file-extension/folder.png\" border=0 /><div class=\"links\">$row[name]</div></a>";
					}
					echo "</li>";
				}
			}
			?>
	</ul>
	<br />
	<h2>Upload File (MAX 10M)</h2>
	<hr />
	<form id="form" class="uniform nyromodal" action="<?php echo base_url().$cpanel_dir."file/image_url"?>" method="post" enctype="multipart/form-data" >
		<div class="tablefooter clearfix">
			<div class="actions">
				<label for="photo_file">Upload File :</label>
				<input type="file" id="file" name="userfile" class="validate[required]" />
				<input type="hidden" name="path" value="<?php echo $path?>" />
				<button type="submit" class="button">Upload</button>
			</div>
		</div>
	</form>
</article>