<style type="text/css">
    table td {padding:5px}
</style>

<article id="stockgudang">
<h1 align="center"><?php echo @$page_title?></h1>
	<?php $data = array('html' => 'HTML', 'xls' => 'EXCEL'); ?>
	<div class="tablefooter clearfix">
		<form id="form" action="<?= $cls."/rep_audittrailgudang"?>" class="uniform" method="post" target="_blank">
            <div style="padding: 5px;">
                <table align="center" style="margin: auto;width: 400px;" cellpadding="5">
                    <tr>
                      <td width="69"><label for="wrcode"><b>Gudang</b></label></td>
                      <td width="305" align="left">
                            <?= fInputText('warehouse_format',array('style'=>'width:200px','readonly'=>'readonly','disabled'=>'disabled'),$outlet[$row['OUTNO']])?>
                            <input type="hidden" id="data[wrcode]" name="data[wrcode]" value="<?= $row['OUTNO']?>" class="warehouse" >
                            <a class="nyromodal" href="<?= base_url()."ajax/load_warehouse"?>"><img src="<?= base_url()."templates/admin/default/images/icons/information-octagon.png"?>"></a>                      
                      </td>
                  </tr>
                  <tr>
                    <td><b>Tanggal</b></td>
                    <td><input type="text" name="data[tglmulai]" class="date" /> - <input type="text" name="data[tglselesai]" class="date" /> </td>
                  </tr>
                </table>
            </div>
            <div style="text-align: center;padding: 5px;">
                <input type="submit" class="button green" name="stockgudang" value="Cetak Stok Gudang">
            </div>
        </form>
	</div>
</article>
<script type="text/javascript">
function goCetak() {
    window.open('<?php echo base_url()."stockgudang/rep_stockgudang/"?>');
}
</script>

