<html>
<head>
<title>Laporan Audit Trail Gudang</title>
<style type="text/css">
#main_wrapper {
	margin:auto;
	font-family:Arial, Helvetica, sans-serif;
	width:18cm;
}

table{font-size:12px;border-collapse:collapse;}

.style1 {
	font-size: 24px;
	font-weight: bold;
}

th {border-top:1px solid;border-bottom:1px solid}
th,td {padding-top:5px;padding-bottom:5px;}
#rounded-box{border:1px dashed;border-radius: 25px;padding:5px;}
.fill {background-color:#CCCCCC}

tr.content-header td{background-color: #F0F0F0;}
</style>
</head>
<body>
<div id="main_wrapper">
  <table border="0" width="100%">
    <tr>
      <td colspan="3"><b><?= $setting['CORPORATE_NAME']?></b></td>
      <td colspan="8" rowspan="3"><div align="center" class="style1">AUDIT TRAIL GUDANG</div></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="3"><?= $setting['ADDRESS']?></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3">Telp. 
      <?= $setting['PHONE']?></td>
      <td colspan="2">Tgl.
      <?= dateIndo(dGetDate($tglmulai)).' - '.dateIndo(dGetDate($tglselesai))?></td>
    </tr>
    <tr>
      <td colspan="8"></td>
    </tr>
    <tr bgcolor="#CCCCCC">
      <th >No.</th>
      <th>Tgl</th>
      <th>PCODE</th>
      <th>Barang</th>
      <th>Rekanan</th>
      <th colspan="3">Jumlah Masuk</th>
      <th>Nilai (Rp)</th>
      <th colspan="3">Jumlah Keluar</th>
      <th>Nilai (Rp)</th>
    </tr>
    <tr bgcolor="#CCCCCC">
      <td rowspan="2" align="center">1</td>
      <td rowspan="2" align="center">2</td>
      <td rowspan="2" align="center">3</td>
      <td rowspan="2" align="center">4</td>
      <td rowspan="2" align="center">5</td>
      <td colspan="3" align="center">6</td>
      <td rowspan="2" align="center">7</td>
      <td colspan="3" align="center">8</td>
      <td rowspan="2" align="center">9</td>
    </tr>
    <tr bgcolor="#CCCCCC">
      <td align="center">JML1</td>
      <td align="center">JML2</td>
      <td align="center">JML3</td>
      <td align="center">JML1</td>
      <td align="center">JML2</td>
      <td align="center">JML3</td>
    </tr>
    <?php 
	  $i=0;
	  foreach ( $rows as $rec ):
      ?>
    <tr valign="top" class="content-header">
      <td align="center"><b><?= $rec['WRCODE']?></b></td>
      <td colspan="12"><b><?= $rec['WRNAME']?></b></td>
    </tr>
    <?php foreach ( $rec['DETAIL'] as $row ) :?>
    <tr>
      <td align="right"><?= ++$i?></td>
      <td align="center"><?= date('d/m/Y', strtotime($row['TGL']))?></td>
      <td align="center"><?= $row['PCODE']?></td>
      <td><?= $row['PNAME']?></td>
      <td><?= strtoupper($thpart[$row['THPART']])?></td>
      <?php 
	    if($row['TYPE'] == 'M'){
            $totin1 += $row['QTY'];
            
            $totamountin += $row['AMOUNT'];
	  ?>
      <td align="right"><?= $row['QTY']?></td>
      <td align="right">-</td>
      <td align="right">-</td>
      <td align="right"><?= fCurrency($row['AMOUNT'])?></td>
      <?php } else {?>
      <td align="right">-</td>
      <td align="right">-</td>
      <td align="right">-</td>
      <td align="right">-</td>
      <?php }?>
      
      <?php 
        if($row['TYPE'] == 'K'){
            $totout1 += $row['QTY'];
            
            $totamountout += $row['AMOUNT'];
      ?>
      <td align="right"><?= $row['QTY']?></td>
      <td align="right">-</td>
      <td align="right">-</td>
      <td align="right"><?= fCurrency($row['AMOUNT'])?></td>
      <?php } else {?>
      <td align="right">-</td>
      <td align="right">-</td>
      <td align="right">-</td>
      <td align="right">-</td>
      <?php }?>
    </tr>
    <?php endforeach;?>
    <?php endforeach;?> 
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>TOTAL</td>
      <td align="right"><b>
        <?= fCurrency($totin1) ?>
      </b></td>
      <td align="right"><b>
        <?= fCurrency($totin2) ?>
      </b></td>
      <td align="right"><b>
        <?= fCurrency($totin3) ?>
      </b></td>
      <td align="right"><b>
        <?= fCurrency($totamountin) ?>
      </b></td>
      <td align="right"><b>
        <?= fCurrency($totout1) ?>
      </b></td>
      <td align="right"><b>
        <?= fCurrency($totout2) ?>
      </b></td>
      <td align="right"><b>
        <?= fCurrency($totout3) ?>
      </b></td>
      <td align="right"><b>
        <?= fCurrency($totamountout) ?>
      </b></td>
    </tr>
  </table>
</div>
</div>
</body>
</html>
