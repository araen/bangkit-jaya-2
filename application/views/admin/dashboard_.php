		<article id="dashboard">
				<h1>Dashboard</h1>
				
					<!--<h2>Statistics</h2>
					<div class="statistics">
						<table>
							<tr>
								<td>Users</td>
								<td><a href="#"><?php echo $users?></a></td>
							</tr>
							<tr>
								<td>Posts</td>
								<td><a href="#"><?php echo $posts?></a></td>
							</tr>
							<tr>
								<td>Pages</td>
								<td><a href="#"><?php echo $pages?></a></td>
							</tr>
							<tr>
								<td>Categories</td>
								<td><a href="#"><?php echo $categories?></a></td>
							</tr>
							<tr>
								<td>Comments</td>
								<td><a href="#"><?php //echo $comments?></a></td>
							</tr>
							<tr>
								<td>Page Views</td>
								<td><a href="#"><?php echo $views?></a></td>
							</tr>
						</table>
					</div>-->
                    <!--<div id="pageviews" style="width:420px;height:250px;"></div>-->
                    <div class="clear"></div>
					<!--h2>Quick Links</h2-->
                    <section class="icons">
                        <ul>
                            <li>
                                <a href="<?php echo base_url().$cpanel_dir."main.html"?>">
                                    <div class="dash home"></div>
                                    <span>Home</span>
                                </a>
                            </li>
							<li>
                                <a href="<?php echo base_url().$cpanel_dir."blok.html"?>">
                                    <div class="dash blok"></div>
                                    <span>Blok Stand</span>
                                </a>
                            </li>
							<li>
                                <a href="<?php echo base_url().$cpanel_dir."stand.html"?>">
                                    <div class="dash stand"></div>
                                    <span>Jenis Stand</span>
                                </a>
                            </li>
							<li>
                                <a href="<?php echo base_url().$cpanel_dir."penyewa.html"?>">
                                    <div class="dash penyewa"></div>
                                    <span>Penyewa</span>
                                </a>
                            </li>
							<li>
                                <a href="<?php echo base_url().$cpanel_dir."user.html"?>">
                                    <div class="dash user"></div>
                                    <span>User</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url().$cpanel_dir."main/logout.html"?>">
                                    <div class="dash logout"></div>
                                    <span>Logout</span>
                                </a>
                            </li>
                        </ul>
                    </section>
					<div class="clear"></div>
					
                    <?php if (isset($message)):?>
					<h2>Recent Message</h2>
					<ul class="comments">
                        <?php foreach($message['data'] as $row){?>
						<li>
							<div class="comment-body clearfix">
                                <div class="dash dummy"></div>
								<b><?php echo ucwords($row['AUTHOR_NAME'])?></b> on <a href="<?php echo base_url()."guest/form/$row[ID]"?>"><?php echo ucwords($row['SUBJECT'])?></a>:
							    <div class="comment-content"><?php echo $row['CONTENT']?></div>
                            </div>
                            <div class="links">
                                <span><?php echo dateIndo($row['CREATED'],true)?></span>
                                <!--<a href="javascript:" onClick="createForm(<?php echo $row['ID']?>)" class="reply">Reply</a>-->
                                <a href="<?php echo base_url()."admin/guest/form/$row[ID]"?>" class="reply">Reply</a>
                                <a href="<?php echo base_url()."admin/guest/delete/$row[ID]"?>" onClick="return confirm('Data will be deleted. Are you sure?')" class="delete">Delete</a>
                            </div>
                        </li>
						<?php } ?>
					</ul>
					<div class="links">
						<a class="button" href="guest">View All</a>
					</div> 
                    <?php endif;?>
			<script>
				function createForm(id){
					jQuery("#form"+id).remove();
					jQuery("#container_form_"+id).append(""
					+ "<form id=\"form"+id+"\" action=\"comment/reply/"+id+"\" class=\"uniform\" method=\"post\">"
					+	"<dl>"
					+		"<dt><label for=\"newscontent\">Reply</label></dt>"
					+		"<dd>"
					+			"<textarea name=\"data[content]\" id=\"comment_"+id+"\" class=\"big\" cols=\"75\" rows=\"5\"></textarea>"
					+		"</dd>"
					+	"</dl>"
					+	"<p>"
					+		"<button type=\"submit\" name=\"save\" class=\"button\">SAVE COMMENT</button>"
					+		"<a href=\"javascript:\" class=\"button white\" onClick=\"jQuery('#container_form_"+id+"').fadeOut()\" >CANCEL</a>"
					+	"</p>"
					+ "</form>"
					+"");
					
					jQuery('#comment_'+id).wysiwyg();
					jQuery("#container_form_"+id).fadeIn();
				}
			</script>
			
			<script type="text/javascript">
			jQuery(function($) {
					
				/* flot
				------------------------------------------------------------------------- */
				var d1 = [[1293814800000, 17], [1293901200000, 29], [1293987600000, 34], [1294074000000, 46], [1294160400000, 36], [1294246800000, 16], [1294333200000, 36]];
				var d2 = [[1293814800000, 20], [1293901200000, 75], [1293987600000, 44], [1294074000000, 49], [1294160400000, 56], [1294246800000, 23], [1294333200000, 46]];
				var d3 = [[1293814800000, 32], [1293901200000, 42], [1293987600000, 59], [1294074000000, 57], [1294160400000, 47], [1294246800000, 56], [1294333200000, 59]];

				$.plot($('#pageviews'), [
					{ label: 'Unique',  data: d1},
					{ label: 'Pages',  data: d2},
					{ label: 'Hits',  data: d3}
				], {
					series: {
						lines: { show: true },
						points: { show: true }
					},
					xaxis: {
						mode: 'time',
						timeformat: '%b %d'
					}
				});

			});
			</script>
					
			</article>