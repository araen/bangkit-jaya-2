<?php echo $this->load->view('admin/header')?>
<?php $cpanel_dir = $this->config->item('cpanel_dir');?> 
<header id="top">
	<div class="container_12 clearfix">
		<div id="logo" class="grid_5">
			<!-- replace with your website title or logo -->
			<!--a id="site-title" href="dashboard.html"><span>Administrator</span></a-->
		</div>

		<div id="userinfo" class="grid_7">
			Selamat Datang, <a href="<?php echo base_url()."user/form/".$this->session->userdata('id_user')?>"><?php echo $this->session->userdata('username')?></a>
		</div>
	</div>
</header>

<nav id="topmenu">
	<div class="container_12 clearfix">
		<div class="grid_12">
			<!--?php $menu = $this->auth->get_menu_admin();?-->
			<?= $this->htmlmenu?>
			<ul id="usermenu">
                <!--<li><a href="" class="comment" title="comment">1</a></li>
                <li><a href="" class="message" title="message">1</a></li>-->
				<li><a href="<?php echo $this->config->item('base_url').$cpanel_dir?>main/logout.html">Logout</a></li>
			</ul>
		</div>
	</div>
</nav>

<section id="content">
	<section class="container_12 clearfix">
		<section id="main" class="grid_12">
			<?php echo !empty($content)?$content:''; ?>
		</section>
	</section>
</section>

<?php echo $this->load->view('admin/footer')?>
