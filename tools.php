<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tools extends MY_Controller {
    		
    function __construct()
    {
        parent::__construct();
        $this->controller = "tools";
    }
	
	public function index()
	{
		if($this->session->userdata('login') == TRUE){
            $this->data['page_title'] = "Tools";

            $this->data['content'] = $this->load->view('admin/tools',$this->data,TRUE);
			parent::view("admin/tools/grid");
		}else{
			parent::view("admin/login");
		}
	}
	
	function backup()
	{	
		if($this->session->userdata('login') == TRUE){
			$this->load->dbutil();
			$prefs = array(
				'format'      => 'txt',            
				'add_drop'    => FALSE,            
				'add_insert'  => TRUE,             
				'newline'     => "\n"              
			);
			$backup = &$this->dbutil->backup($prefs);
			$backup = 'SET FOREIGN_KEY_CHECKS = 0;'."\n".$backup."\n".'SET FOREIGN_KEY_CHECKS = 1;';
			force_download('sds_'.date("Y-m-d").'.sql', $backup);
		}else{
			parent::view("admin/login");
		}
	}
    
	function optimize()
	{
		if($this->session->userdata('login') == TRUE){
			$this->load->dbutil();
			$result = $this->dbutil->optimize_database();
			if ($result !== FALSE)
			{
				print_r($result);
			}
		}else{
			parent::view("admin/login");
		}
	}
}

/* End of file main.php */
/* Location: ./application/controllers/tools.php */
