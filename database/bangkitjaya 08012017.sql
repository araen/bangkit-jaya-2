/*
SQLyog Community v10.3 
MySQL - 5.1.37 : Database - bangkitjaya
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`bangkitjaya` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `bangkitjaya`;

/*Table structure for table `m_access` */

DROP TABLE IF EXISTS `m_access`;

CREATE TABLE `m_access` (
  `IDROLE` int(11) NOT NULL,
  `CMNCODE` int(11) NOT NULL,
  `ISCREATE` char(1) DEFAULT '0',
  `ISREAD` char(1) DEFAULT '0',
  `ISUPDATE` char(1) DEFAULT '0',
  `ISDELETE` char(1) DEFAULT '0',
  PRIMARY KEY (`IDROLE`,`CMNCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_access` */

insert  into `m_access`(`IDROLE`,`CMNCODE`,`ISCREATE`,`ISREAD`,`ISUPDATE`,`ISDELETE`) values (1,101,'1','1','1','1'),(1,103,'1','1','1','1'),(1,104,'1','1','1','1'),(1,105,'1','1','1','1'),(1,106,'1','1','1','1'),(1,107,'1','1','1','1'),(1,108,'1','1','1','1'),(1,110,'1','1','1','1'),(1,201,'1','1','1','1'),(1,202,'1','1','1','1'),(1,203,'1','1','1','1'),(1,301,'1','1','1','1'),(1,304,'1','1','1','1'),(1,401,'1','1','1','1'),(1,403,'1','1','1','1'),(1,501,'1','1','1','1'),(1,502,'1','1','1','1'),(1,503,'1','1','1','1'),(1,505,'1','1','1','1'),(1,507,'1','1','1','1'),(1,508,'1','1','1','1'),(1,509,'1','1','1','1'),(1,601,'1','1','1','1'),(1,701,'1','1','1','1'),(1,702,'1','1','1','1'),(1,703,'1','1','1','1'),(1,704,'1','1','1','1'),(2,102,'0','1','1','0'),(2,301,'1','1','1','1');

/*Table structure for table `m_cmenu` */

DROP TABLE IF EXISTS `m_cmenu`;

CREATE TABLE `m_cmenu` (
  `CMNCODE` varchar(5) NOT NULL,
  `MNCODE` varchar(3) DEFAULT NULL,
  `CMNNAME` varchar(25) DEFAULT NULL,
  `CMNURL` varchar(30) DEFAULT NULL,
  `ISACTIVE` char(1) DEFAULT NULL,
  PRIMARY KEY (`CMNCODE`),
  UNIQUE KEY `M_CMENU_PK` (`CMNCODE`),
  KEY `CHILD_MENU_FK` (`MNCODE`),
  CONSTRAINT `FK_M_CMENU_CHILD_MEN_M_MENU` FOREIGN KEY (`MNCODE`) REFERENCES `m_menu` (`MNCODE`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_cmenu` */

insert  into `m_cmenu`(`CMNCODE`,`MNCODE`,`CMNNAME`,`CMNURL`,`ISACTIVE`) values ('101','100','Jenis Customer','jnsoutlet','1'),('102','100','Sales','sales','1'),('103','100','Customer','outlet','1'),('104','100','Jenis Sales','jnssales','0'),('105','100','Merk','brand','1'),('106','100','Produk','produk','1'),('107','100','Supplier','supplier','1'),('108','100','Gudang','warehouse','1'),('109','100','Van','van','1'),('110','100','Promo','promo','1'),('201','200','Penerimaan Barang (F1)','pembelian/form','1'),('202','200','Opname Gudang','opname','1'),('203','200','Retur Supplier','returnsupp','1'),('204','200','Penambahan Barang Van','vanload','1'),('205','200','Penurunan Barang Van','vandrop','1'),('301','300','Input Data Order (F2)','order/form','1'),('302','300','Proses Data Order','prosesorder','1'),('304','300','Retur Penjualan','returnorder','1'),('305','300','Cetak Invoice','invoice','1'),('401','400','Pembayaran','pembayaran','1'),('403','400','Auto Bill','autobill','1'),('501','500','Rekap Data Per Outlet','rekaporder/outlet','1'),('502','500','Rekap Data Per Produk','rekaporder/product','1'),('503','500','Stock Gudang','stockgudang','1'),('504','500','Stock Van','stockvan','1'),('505','500','Audit Trail Gudang','audittrail/warehouse','1'),('506','500','Audit Trail Van','audittrail/van','1'),('507','500','Penjualan','reppenjualan','1'),('508','500','Retur','retur','1'),('509','500','Pelunasan','reppayment','1'),('601','600','Closing','closing','1'),('701','700','User','user','1'),('702','700','Contact','setting','1'),('703','700','Menu','menu','1'),('704','700','Role','role','1');

/*Table structure for table `m_collector` */

DROP TABLE IF EXISTS `m_collector`;

CREATE TABLE `m_collector` (
  `COLCODE` varchar(5) NOT NULL,
  `COLNAME` varchar(32) DEFAULT NULL,
  `COLADD` varchar(50) DEFAULT NULL,
  `COLCITY` varchar(25) DEFAULT NULL,
  `BIRTHDATE` timestamp NULL DEFAULT NULL,
  `CWORKDATE` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`COLCODE`),
  UNIQUE KEY `M_COLLECTOR_PK` (`COLCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_collector` */

/*Table structure for table `m_debt_reason` */

DROP TABLE IF EXISTS `m_debt_reason`;

CREATE TABLE `m_debt_reason` (
  `DEBTREASONCODE` varchar(2) NOT NULL,
  `DEBTREASONNAME` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`DEBTREASONCODE`),
  UNIQUE KEY `M_DEBT_REASON_PK` (`DEBTREASONCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_debt_reason` */

/*Table structure for table `m_discount` */

DROP TABLE IF EXISTS `m_discount`;

CREATE TABLE `m_discount` (
  `CDISC` varchar(10) NOT NULL,
  `MINRANGE` timestamp NULL DEFAULT NULL,
  `MAXRANGE` timestamp NULL DEFAULT NULL,
  `PERCENT` decimal(3,0) DEFAULT NULL,
  `DISCTYPE` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`CDISC`),
  UNIQUE KEY `M_DISCOUNT_PK` (`CDISC`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_discount` */

/*Table structure for table `m_menu` */

DROP TABLE IF EXISTS `m_menu`;

CREATE TABLE `m_menu` (
  `MNCODE` varchar(3) NOT NULL,
  `MNNAME` varchar(25) DEFAULT NULL,
  `ISACTIVE` char(1) DEFAULT NULL,
  PRIMARY KEY (`MNCODE`),
  UNIQUE KEY `M_MENU_PK` (`MNCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_menu` */

insert  into `m_menu`(`MNCODE`,`MNNAME`,`ISACTIVE`) values ('100','Master','1'),('200','Inventory','1'),('300','Sales','1'),('400','Account Receivable','1'),('500','Reporting','1'),('600','End of Day','1'),('700','Setting','1');

/*Table structure for table `m_ostatus` */

DROP TABLE IF EXISTS `m_ostatus`;

CREATE TABLE `m_ostatus` (
  `OSTATUSCODE` char(1) NOT NULL,
  `OSTATUSNAME` varchar(20) DEFAULT NULL,
  `OSTATUSVALUE` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`OSTATUSCODE`),
  UNIQUE KEY `M_OSTATUS_PK` (`OSTATUSCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_ostatus` */

insert  into `m_ostatus`(`OSTATUSCODE`,`OSTATUSNAME`,`OSTATUSVALUE`) values ('1','OPNAME','0'),('2','ORDER',NULL),('3','POSTING',NULL);

/*Table structure for table `m_outlet` */

DROP TABLE IF EXISTS `m_outlet`;

CREATE TABLE `m_outlet` (
  `OUTNO` varchar(5) NOT NULL,
  `ATTCODE` varchar(2) DEFAULT NULL,
  `OUTNAME` varchar(32) DEFAULT NULL,
  `OUTADD` varchar(50) DEFAULT NULL,
  `OCITY` varchar(20) DEFAULT NULL,
  `OPHONE` varchar(20) DEFAULT NULL,
  `OPOST` decimal(5,0) DEFAULT NULL,
  `OCONTACT` varchar(32) DEFAULT NULL,
  `FLIMIT` decimal(10,0) DEFAULT NULL,
  `FIRSTTRANS` timestamp NULL DEFAULT NULL,
  `REGDATE` timestamp NULL DEFAULT NULL,
  `NPWP` varchar(20) DEFAULT NULL,
  `FIRSTOPEN` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`OUTNO`),
  UNIQUE KEY `M_OUTLET_PK` (`OUTNO`),
  KEY `OUTLET_TYPE_FK` (`ATTCODE`),
  CONSTRAINT `FK_M_OUTLET_OUTLET_TY_M_OUTLET` FOREIGN KEY (`ATTCODE`) REFERENCES `m_outlet_attribute` (`ATTCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_outlet` */

insert  into `m_outlet`(`OUTNO`,`ATTCODE`,`OUTNAME`,`OUTADD`,`OCITY`,`OPHONE`,`OPOST`,`OCONTACT`,`FLIMIT`,`FIRSTTRANS`,`REGDATE`,`NPWP`,`FIRSTOPEN`) values ('101','G','PT. Test','Surabaya','08123456','','0','','0','0000-00-00 00:00:00','0000-00-00 00:00:00','','0000-00-00 00:00:00'),('102','P','PT Tes2','','','','0','','0','0000-00-00 00:00:00','0000-00-00 00:00:00','','0000-00-00 00:00:00');

/*Table structure for table `m_outlet_attribute` */

DROP TABLE IF EXISTS `m_outlet_attribute`;

CREATE TABLE `m_outlet_attribute` (
  `ATTCODE` varchar(2) NOT NULL,
  `ATTNAME` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`ATTCODE`),
  UNIQUE KEY `M_OUTLET_ATTRIBUTE_PK` (`ATTCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_outlet_attribute` */

insert  into `m_outlet_attribute`(`ATTCODE`,`ATTNAME`) values ('G','Galangan'),('P','Proyek'),('R','Retail');

/*Table structure for table `m_pbrand` */

DROP TABLE IF EXISTS `m_pbrand`;

CREATE TABLE `m_pbrand` (
  `PBCODE` varchar(2) NOT NULL,
  `PBNAME` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`PBCODE`),
  UNIQUE KEY `M_PBRAND_PK` (`PBCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_pbrand` */

insert  into `m_pbrand`(`PBCODE`,`PBNAME`) values ('AB','Albasia'),('AR','Aren'),('BK','Bangkirai'),('CD','Cendana'),('EB','Eboni'),('JT','Jati'),('KL','Kelapa'),('KM','Kamper'),('MB','Merbau'),('MH','Mahoni'),('MR','Meranti'),('PN','Pinus'),('SK','Sungkai'),('SN','Sonokeling'),('TR','Trembesi'),('UL','Ulin');

/*Table structure for table `m_product` */

DROP TABLE IF EXISTS `m_product`;

CREATE TABLE `m_product` (
  `PCODE` varchar(10) NOT NULL,
  `SUPPNO` varchar(5) DEFAULT NULL,
  `PBCODE` varchar(2) DEFAULT NULL,
  `PNAME` varchar(50) DEFAULT NULL,
  `PGROUP` char(1) DEFAULT NULL COMMENT 'L:Log; U:Unit',
  `PWEIGHT` decimal(3,0) DEFAULT NULL,
  `PLAUNCH` timestamp NULL DEFAULT NULL,
  `PSTATUS` char(1) DEFAULT NULL,
  `PPRICE1` decimal(20,0) DEFAULT NULL,
  `PPRICE2` decimal(20,0) DEFAULT NULL,
  `PPRICE3` decimal(20,0) DEFAULT NULL,
  `CONV1` decimal(2,0) DEFAULT NULL,
  `CONV2` decimal(2,0) DEFAULT NULL,
  `CONV3` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`PCODE`),
  UNIQUE KEY `M_PRODUCT_PK` (`PCODE`),
  KEY `PRODUCT_BRAND_FK` (`PBCODE`),
  KEY `PRODUCT_SUPPLIER_FK` (`SUPPNO`),
  CONSTRAINT `FK_M_PRODUC_PRODUCT_B_M_PBRAND` FOREIGN KEY (`PBCODE`) REFERENCES `m_pbrand` (`PBCODE`),
  CONSTRAINT `FK_M_PRODUC_PRODUCT_S_M_SUPPLI` FOREIGN KEY (`SUPPNO`) REFERENCES `m_supplier` (`SUPPNO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_product` */

insert  into `m_product`(`PCODE`,`SUPPNO`,`PBCODE`,`PNAME`,`PGROUP`,`PWEIGHT`,`PLAUNCH`,`PSTATUS`,`PPRICE1`,`PPRICE2`,`PPRICE3`,`CONV1`,`CONV2`,`CONV3`) values ('AB01','101','AB','ALBASIA LOG','L',NULL,'2016-11-24 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL),('AB02','101','AB','ALBASIA UNIT','U','0','2016-11-24 00:00:00','','100000','0','0','1','0','0'),('AR01','101','AR','AREN LOG','L',NULL,'2016-11-25 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL),('AR02','101','AR','AREN UNIT','U','0','2016-11-25 00:00:00','','100000','0','0','1','0','0'),('BK01','101','BK','BANGKIRAI LOG','L',NULL,'2016-11-26 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL),('BK02','101','BK','BANGKIRAI UNIT','U','0','2016-11-26 00:00:00','','100000','0','0','1','0','0'),('CD01','101','CD','CENDANA LOG','L',NULL,'2016-11-27 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL),('CD02','101','CD','CENDANA UNIT','U','0','2016-11-27 00:00:00','','100000','0','0','1','0','0'),('EB01','101','EB','EBONI LOG','L',NULL,'2016-11-28 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL),('EB02','101','EB','EBONI UNIT','U','0','2016-11-28 00:00:00','','100000','0','0','1','0','0'),('JT01','101','JT','JATI LOG','L',NULL,'2016-11-29 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL),('JT02','101','JT','JATI UNIT','U','0','2016-11-29 00:00:00','','100000','0','0','1','0','0'),('KL01','101','KL','KELAPA LOG','L',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL),('KL02','101','KL','KELAPA UNIT','U','0','0000-00-00 00:00:00','','100000','0','0','1','0','0'),('KM01','101','KM','KAMPER LOG','L',NULL,'2016-11-30 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL),('KM02','101','KM','KAMPER UNIT','U','0','2016-11-30 00:00:00','','100000','0','0','1','0','0'),('MB01','101','MB','MERBAU LOG','L',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL),('MB02','101','MB','MERBAU UNIT','U','0','0000-00-00 00:00:00','','100000','0','0','1','0','0'),('MH01','101','MH','MAHONI LOG','L',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL),('MH02','101','MH','MAHONI UNIT','U',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,NULL,NULL,NULL),('MR01','101','MR','MERANTI LOG','L',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL),('MR02','101','MR','MERANTI UNIT','U',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,NULL,NULL,NULL),('PN01','101','PN','PINUS LOG','L',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL),('PN02','101','PN','PINUS UNIT','U',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,NULL,NULL,NULL),('SK01','101','SK','SUNGKAI LOG','L',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL),('SK02','101','SK','SUNGKAI UNIT','U',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,NULL,NULL,NULL),('SN01','101','SN','SONOKELING LOG','L',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL),('SN02','101','SN','SONOKELING UNIT','U',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,NULL,NULL,NULL),('TR01','101','TR','TREMBESI LOG','L',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL),('TR02','101','TR','TREMBESI UNIT','U',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,NULL,NULL,NULL),('UL01','101','UL','ULIN LOG','L',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL),('UL02','101','UL','ULIN UNIT','U',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `m_profile` */

DROP TABLE IF EXISTS `m_profile`;

CREATE TABLE `m_profile` (
  `PROFCD` varchar(5) NOT NULL,
  `PROFNAME` varchar(20) DEFAULT NULL,
  `PROFADD` varchar(25) DEFAULT NULL,
  `PROFNPWP` varchar(20) DEFAULT NULL,
  `PROFCITY` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`PROFCD`),
  UNIQUE KEY `M_PROFILE_PK` (`PROFCD`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_profile` */

/*Table structure for table `m_promo` */

DROP TABLE IF EXISTS `m_promo`;

CREATE TABLE `m_promo` (
  `IDPROMO` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(100) DEFAULT NULL,
  `DESCRIPTION` text,
  `STARTDATE` date DEFAULT NULL,
  `DUEDATE` date DEFAULT NULL,
  `ISITEMDISC` char(1) DEFAULT NULL,
  `TOTALDISC` decimal(3,0) DEFAULT NULL,
  `MINAMOUNT` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`IDPROMO`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `m_promo` */

insert  into `m_promo`(`IDPROMO`,`TITLE`,`DESCRIPTION`,`STARTDATE`,`DUEDATE`,`ISITEMDISC`,`TOTALDISC`,`MINAMOUNT`) values (1,'Customer Proyek','Diskon untuk customer proyek','2016-12-25','2017-12-25','','5','0'),(2,'Customer Galangan','Diskon untuk customer dengan jenis galangan','2016-12-25','2017-01-09','','10','0'),(3,'Customer Retail','Diskon untuk pelanggan retail','2016-12-25','2017-12-25','','12','0');

/*Table structure for table `m_role` */

DROP TABLE IF EXISTS `m_role`;

CREATE TABLE `m_role` (
  `IDROLE` int(11) NOT NULL AUTO_INCREMENT,
  `NAMA` varchar(20) DEFAULT NULL,
  `ISACTIVE` int(11) DEFAULT '1',
  PRIMARY KEY (`IDROLE`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `m_role` */

insert  into `m_role`(`IDROLE`,`NAMA`,`ISACTIVE`) values (1,'Admin',1),(2,'Sales',1);

/*Table structure for table `m_sales` */

DROP TABLE IF EXISTS `m_sales`;

CREATE TABLE `m_sales` (
  `SLSNO` varchar(5) NOT NULL,
  `VANCODE` varchar(3) DEFAULT NULL,
  `STCODE` varchar(5) DEFAULT NULL,
  `SLSNAME` varchar(32) DEFAULT NULL,
  `SLSADD` varchar(50) DEFAULT NULL,
  `SLSCITY` varchar(25) DEFAULT NULL,
  `WORKDATE` timestamp NULL DEFAULT NULL,
  `TRANSDATE` timestamp NULL DEFAULT NULL,
  `BIRTHDATE` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`SLSNO`),
  UNIQUE KEY `M_SALES_PK` (`SLSNO`),
  KEY `SALES_TYPE_FK` (`STCODE`),
  KEY `VANCODE` (`VANCODE`),
  CONSTRAINT `FK_M_SALES_SALES_TYP_M_SALES_` FOREIGN KEY (`STCODE`) REFERENCES `m_sales_type` (`STCODE`),
  CONSTRAINT `m_sales_ibfk_2` FOREIGN KEY (`VANCODE`) REFERENCES `m_van` (`VANCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_sales` */

insert  into `m_sales`(`SLSNO`,`VANCODE`,`STCODE`,`SLSNAME`,`SLSADD`,`SLSCITY`,`WORKDATE`,`TRANSDATE`,`BIRTHDATE`) values ('00001',NULL,'TO','ANANG PRIBADI','PETUNG','PENAJAM','2015-02-01 21:59:36','2015-02-01 21:59:36','1994-06-05 23:59:42'),('00002','01','CVS','YOGA','PETUNG','PENAJAM','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00');

/*Table structure for table `m_sales_type` */

DROP TABLE IF EXISTS `m_sales_type`;

CREATE TABLE `m_sales_type` (
  `STCODE` varchar(5) NOT NULL,
  `STNAME` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`STCODE`),
  UNIQUE KEY `M_SALES_TYPE_PK` (`STCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_sales_type` */

insert  into `m_sales_type`(`STCODE`,`STNAME`) values ('CVS','Canvas'),('TO','Taking Order');

/*Table structure for table `m_setting` */

DROP TABLE IF EXISTS `m_setting`;

CREATE TABLE `m_setting` (
  `KEY` varchar(20) NOT NULL DEFAULT '',
  `VALUE` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_setting` */

insert  into `m_setting`(`KEY`,`VALUE`) values ('ADDRESS','Jl.Propinsi Km 1'),('CORPORATE_NAME','UD. Bangkit Jaya'),('EMAIL_CONTACT',''),('MOBILE',''),('PHONE','0542-7200168'),('VANDATE','2015-12-25'),('WRDATE','2017-01-08');

/*Table structure for table `m_supplier` */

DROP TABLE IF EXISTS `m_supplier`;

CREATE TABLE `m_supplier` (
  `SUPPNO` varchar(5) NOT NULL,
  `SUPPNAME` varchar(50) DEFAULT NULL,
  `SUPPADD` varchar(50) DEFAULT NULL,
  `SUPPPHONE` varchar(20) DEFAULT NULL,
  `SUPPFAX` varchar(20) DEFAULT NULL,
  `SUPPCONTACT` varchar(25) DEFAULT NULL,
  `SUPPTYPE` varchar(2) DEFAULT NULL,
  `ACCNO` decimal(20,0) DEFAULT NULL,
  PRIMARY KEY (`SUPPNO`),
  UNIQUE KEY `M_SUPPLIER_PK` (`SUPPNO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_supplier` */

insert  into `m_supplier`(`SUPPNO`,`SUPPNAME`,`SUPPADD`,`SUPPPHONE`,`SUPPFAX`,`SUPPCONTACT`,`SUPPTYPE`,`ACCNO`) values ('101','ALPUKATSOFT','Surabaya','0812345678','','','','0');

/*Table structure for table `m_user` */

DROP TABLE IF EXISTS `m_user`;

CREATE TABLE `m_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USCODE` varchar(25) DEFAULT NULL,
  `USNAME` varchar(25) DEFAULT NULL,
  `FULLNAME` varchar(100) DEFAULT NULL,
  `CDLEVEL` decimal(1,0) DEFAULT NULL,
  `CODE` varchar(5) DEFAULT NULL,
  `USPASSWORD` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `M_USER_PK` (`USCODE`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `m_user` */

insert  into `m_user`(`ID`,`USCODE`,`USNAME`,`FULLNAME`,`CDLEVEL`,`CODE`,`USPASSWORD`) values (1,NULL,'admin','Administrator',NULL,NULL,'21232f297a57a5a743894a0e4a801fc3'),(2,NULL,'sales','Salesman',NULL,NULL,'9ed083b1436e5f40ef984b28255eef18'),(3,NULL,'yoga','Azmi Yoga',NULL,NULL,'807659cd883fc0a63f6ce615893b3558');

/*Table structure for table `m_userrole` */

DROP TABLE IF EXISTS `m_userrole`;

CREATE TABLE `m_userrole` (
  `IDROLE` int(11) NOT NULL,
  `IDUSER` int(11) NOT NULL,
  KEY `IDROLE` (`IDROLE`),
  KEY `IDUSER` (`IDUSER`),
  CONSTRAINT `m_userrole_ibfk_1` FOREIGN KEY (`IDROLE`) REFERENCES `m_role` (`IDROLE`),
  CONSTRAINT `m_userrole_ibfk_2` FOREIGN KEY (`IDUSER`) REFERENCES `m_user` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_userrole` */

insert  into `m_userrole`(`IDROLE`,`IDUSER`) values (1,1),(2,2),(1,3);

/*Table structure for table `m_van` */

DROP TABLE IF EXISTS `m_van`;

CREATE TABLE `m_van` (
  `VANCODE` varchar(5) NOT NULL,
  `VANNAME` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`VANCODE`),
  UNIQUE KEY `M_VAN_TYPE_PK` (`VANCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_van` */

insert  into `m_van`(`VANCODE`,`VANNAME`) values ('01','VAN 1'),('02','VAN 2');

/*Table structure for table `m_warehouse` */

DROP TABLE IF EXISTS `m_warehouse`;

CREATE TABLE `m_warehouse` (
  `WRCODE` varchar(2) NOT NULL,
  `PCODE` varchar(5) DEFAULT NULL,
  `WRNAME` varchar(25) DEFAULT NULL,
  `WRDATE` timestamp NULL DEFAULT NULL,
  `WRSTOCK` decimal(20,0) DEFAULT NULL,
  `WRTYPE` char(1) DEFAULT 'L',
  PRIMARY KEY (`WRCODE`),
  UNIQUE KEY `M_WAREHOUSE_PK` (`WRCODE`),
  KEY `PRODUCT_STOCK_FK` (`PCODE`),
  CONSTRAINT `FK_M_WAREHO_PRODUCT_S_M_PRODUC` FOREIGN KEY (`PCODE`) REFERENCES `m_product` (`PCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_warehouse` */

insert  into `m_warehouse`(`WRCODE`,`PCODE`,`WRNAME`,`WRDATE`,`WRSTOCK`,`WRTYPE`) values ('01',NULL,'GUDANG LOG',NULL,NULL,'L'),('02',NULL,'GUDANG UNIT',NULL,NULL,'U');

/*Table structure for table `t_cart` */

DROP TABLE IF EXISTS `t_cart`;

CREATE TABLE `t_cart` (
  `SESSIONID` varchar(100) NOT NULL,
  `TRXCODE` char(1) NOT NULL COMMENT 'R:Receiving; S:Sales',
  `PCODE` varchar(10) NOT NULL,
  `SUPPNO` varchar(5) DEFAULT NULL,
  `OUTNO` varchar(5) DEFAULT NULL,
  `PNAME` varchar(30) DEFAULT NULL,
  `PPRICE` decimal(17,0) DEFAULT NULL,
  `QTY` decimal(5,0) DEFAULT NULL,
  `DISC` decimal(3,0) DEFAULT NULL,
  `TOTAL` decimal(17,0) DEFAULT NULL,
  PRIMARY KEY (`SESSIONID`,`TRXCODE`,`PCODE`),
  KEY `PCODE` (`PCODE`),
  KEY `SUPPNO` (`SUPPNO`),
  KEY `OUTNO` (`OUTNO`),
  CONSTRAINT `t_cart_ibfk_1` FOREIGN KEY (`PCODE`) REFERENCES `m_product` (`PCODE`),
  CONSTRAINT `t_cart_ibfk_2` FOREIGN KEY (`SUPPNO`) REFERENCES `m_supplier` (`SUPPNO`),
  CONSTRAINT `t_cart_ibfk_3` FOREIGN KEY (`OUTNO`) REFERENCES `m_outlet` (`OUTNO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_cart` */

insert  into `t_cart`(`SESSIONID`,`TRXCODE`,`PCODE`,`SUPPNO`,`OUTNO`,`PNAME`,`PPRICE`,`QTY`,`DISC`,`TOTAL`) values ('1','S','KL02',NULL,NULL,'KELAPA UNIT','100000','1','5','95000'),('1','S','MH02',NULL,NULL,'MAHONI UNIT','100000','1','5','95000'),('1','S','TR02',NULL,NULL,'TREMBESI UNIT','100000','1','5','95000');

/*Table structure for table `t_dbeli` */

DROP TABLE IF EXISTS `t_dbeli`;

CREATE TABLE `t_dbeli` (
  `IDDBELI` int(11) NOT NULL AUTO_INCREMENT,
  `IDBELI` int(11) NOT NULL,
  `PCODE` varchar(10) NOT NULL,
  `QTYBELI` decimal(10,0) DEFAULT NULL,
  `AMOUNTITEM` decimal(10,0) DEFAULT NULL,
  `JMLBELI` decimal(10,0) DEFAULT NULL,
  `RETDATE` date DEFAULT NULL,
  `QTYRET` decimal(10,0) DEFAULT NULL,
  `AMOUNTRET` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`IDDBELI`),
  KEY `FK_T_DBELI_IDBELI_S_T_HBELI` (`IDBELI`),
  KEY `FK_T_DBELI_PCODE_S_M_PRODUCT` (`PCODE`),
  CONSTRAINT `FK_T_DBELI_IDBELI_S_T_HBELI` FOREIGN KEY (`IDBELI`) REFERENCES `t_hbeli` (`IDBELI`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_T_DBELI_PCODE_S_M_PRODUCT` FOREIGN KEY (`PCODE`) REFERENCES `m_product` (`PCODE`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=latin1;

/*Data for the table `t_dbeli` */

insert  into `t_dbeli`(`IDDBELI`,`IDBELI`,`PCODE`,`QTYBELI`,`AMOUNTITEM`,`JMLBELI`,`RETDATE`,`QTYRET`,`AMOUNTRET`) values (102,44,'AB01','72','100000','7200000',NULL,NULL,NULL),(103,44,'AR01','56','100000','5600000',NULL,NULL,NULL),(104,44,'KL01','76','100000','7600000',NULL,NULL,NULL),(105,44,'MH01','87','100000','8700000',NULL,NULL,NULL),(106,45,'BK01','31','100000','3100000',NULL,NULL,NULL),(107,45,'MH01','76','100000','7600000',NULL,NULL,NULL),(108,45,'PN01','99','100000','9900000',NULL,NULL,NULL),(109,46,'AR01','71','100000','7100000',NULL,NULL,NULL),(110,46,'MH01','254','100000','25400000',NULL,NULL,NULL),(111,46,'TR01','98','100000','9800000',NULL,NULL,NULL),(112,47,'AR01','1','100000','100000',NULL,NULL,NULL);

/*Table structure for table `t_debt` */

DROP TABLE IF EXISTS `t_debt`;

CREATE TABLE `t_debt` (
  `DEBTCODE` varchar(8) NOT NULL,
  `ORDERNO` varchar(5) DEFAULT NULL,
  `OUTNO` varchar(5) DEFAULT NULL,
  `DEBTREASONCODE` varchar(2) DEFAULT NULL,
  `COLCODE` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`DEBTCODE`),
  UNIQUE KEY `T_DEBT_PK` (`DEBTCODE`),
  KEY `OUTLET_DEBT_FK` (`OUTNO`),
  KEY `ORDER_DEBT_FK` (`ORDERNO`),
  KEY `DEBT_REASON_FK` (`DEBTREASONCODE`),
  KEY `DEBT_COLLECTOR_FK` (`COLCODE`),
  CONSTRAINT `FK_T_DEBT_DEBT_COLL_M_COLLEC` FOREIGN KEY (`COLCODE`) REFERENCES `m_collector` (`COLCODE`),
  CONSTRAINT `FK_T_DEBT_DEBT_REAS_M_DEBT_R` FOREIGN KEY (`DEBTREASONCODE`) REFERENCES `m_debt_reason` (`DEBTREASONCODE`),
  CONSTRAINT `FK_T_DEBT_ORDER_DEB_T_HORDER` FOREIGN KEY (`ORDERNO`) REFERENCES `t_horder` (`ORDERNO`),
  CONSTRAINT `FK_T_DEBT_OUTLET_DE_M_OUTLET` FOREIGN KEY (`OUTNO`) REFERENCES `m_outlet` (`OUTNO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_debt` */

/*Table structure for table `t_dorder` */

DROP TABLE IF EXISTS `t_dorder`;

CREATE TABLE `t_dorder` (
  `DORDERNO` int(11) NOT NULL AUTO_INCREMENT,
  `ORDERNO` varchar(15) DEFAULT NULL,
  `PCODE` varchar(10) DEFAULT NULL,
  `QTY` decimal(10,0) DEFAULT NULL,
  `AMOUNTITEM` decimal(10,0) DEFAULT NULL,
  `DISC` decimal(10,0) DEFAULT NULL,
  `AMOUNTDISC` decimal(10,0) DEFAULT NULL,
  `AMOUNT` decimal(10,0) DEFAULT NULL,
  `COSTVALUE` decimal(10,0) DEFAULT NULL,
  `RETTYPE` char(1) DEFAULT NULL,
  `RETDATE` date DEFAULT NULL,
  `QTYRET` decimal(10,0) DEFAULT NULL,
  `AMOUNTRET` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`DORDERNO`),
  KEY `DETAIL_ORDER_FK` (`ORDERNO`),
  KEY `FK_T_DORDER_DETAIL_OR_M_PRODUCT` (`PCODE`),
  CONSTRAINT `FK_T_DORDER_DETAIL_OR_M_PRODUCT` FOREIGN KEY (`PCODE`) REFERENCES `m_product` (`PCODE`),
  CONSTRAINT `FK_T_DORDER_DETAIL_OR_T_HORDER` FOREIGN KEY (`ORDERNO`) REFERENCES `t_horder` (`ORDERNO`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

/*Data for the table `t_dorder` */

insert  into `t_dorder`(`DORDERNO`,`ORDERNO`,`PCODE`,`QTY`,`AMOUNTITEM`,`DISC`,`AMOUNTDISC`,`AMOUNT`,`COSTVALUE`,`RETTYPE`,`RETDATE`,`QTYRET`,`AMOUNTRET`) values (33,'OR.1612.001','AR02','71','100000','10','710000','6390000',NULL,NULL,NULL,NULL,NULL),(34,'OR.1701.001','MH02','12','100000','0','0','1200000',NULL,NULL,NULL,NULL,NULL),(35,'OR.1701.001','TR02','14','100000','0','0','1400000',NULL,NULL,NULL,NULL,NULL),(36,'OR.1701.002','KL02','1','100000','0','0','100000',NULL,NULL,NULL,NULL,NULL),(37,'OR.1701.002','MH02','1','100000','0','0','100000',NULL,NULL,NULL,NULL,NULL),(38,'OR.1701.002','SN02','1','100000','0','0','100000',NULL,NULL,NULL,NULL,NULL),(39,'OR.1701.004','KL02','1','100000','10','10000','90000',NULL,NULL,NULL,NULL,NULL),(40,'OR.1701.004','MH02','1','100000','10','10000','90000',NULL,NULL,NULL,NULL,NULL),(41,'OR.1701.004','SN02','1','100000','10','10000','90000',NULL,NULL,NULL,NULL,NULL),(42,'OR.1701.004','TR02','1','100000','10','10000','90000',NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `t_dvan` */

DROP TABLE IF EXISTS `t_dvan`;

CREATE TABLE `t_dvan` (
  `IDDTVAN` int(11) NOT NULL AUTO_INCREMENT,
  `IDTVAN` int(11) NOT NULL,
  `PCODE` varchar(10) NOT NULL,
  `QTY` decimal(20,0) DEFAULT NULL,
  `AMOUNT` decimal(20,0) DEFAULT NULL,
  PRIMARY KEY (`IDDTVAN`),
  KEY `PCODE` (`PCODE`),
  KEY `IDTVAN` (`IDTVAN`),
  CONSTRAINT `t_dvan_ibfk_2` FOREIGN KEY (`PCODE`) REFERENCES `m_product` (`PCODE`),
  CONSTRAINT `t_dvan_ibfk_3` FOREIGN KEY (`IDTVAN`) REFERENCES `t_hvan` (`IDTVAN`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_dvan` */

/*Table structure for table `t_hbeli` */

DROP TABLE IF EXISTS `t_hbeli`;

CREATE TABLE `t_hbeli` (
  `IDBELI` int(11) NOT NULL AUTO_INCREMENT,
  `WRCODE` varchar(2) NOT NULL,
  `TOTALBELI` decimal(10,0) DEFAULT NULL,
  `TGLBELI` timestamp NULL DEFAULT NULL,
  `POSTSTATUS` decimal(10,0) DEFAULT '0',
  `DOCNO` varbinary(20) DEFAULT NULL,
  `SUPPNO` varchar(5) DEFAULT NULL,
  `VANREGISTER` varchar(10) DEFAULT NULL,
  `FAKTURNO` varchar(10) DEFAULT NULL,
  `RETLOCK` char(1) DEFAULT NULL,
  `NOTE` text,
  PRIMARY KEY (`IDBELI`),
  KEY `FK_T_HBELI_WAREHOU_S_M_WAREHOUSE` (`WRCODE`),
  KEY `SUPPNO` (`SUPPNO`),
  KEY `VANID` (`VANREGISTER`),
  CONSTRAINT `FK_T_HBELI_WAREHOU_S_M_WAREHOUSE` FOREIGN KEY (`WRCODE`) REFERENCES `m_warehouse` (`WRCODE`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

/*Data for the table `t_hbeli` */

insert  into `t_hbeli`(`IDBELI`,`WRCODE`,`TOTALBELI`,`TGLBELI`,`POSTSTATUS`,`DOCNO`,`SUPPNO`,`VANREGISTER`,`FAKTURNO`,`RETLOCK`,`NOTE`) values (44,'01','29100000','2016-12-26 00:00:00','1','RCV.1612.001','',NULL,NULL,NULL,NULL),(45,'01','20600000','2016-12-26 00:00:00','1','RCV.1612.002','101',NULL,NULL,NULL,NULL),(46,'01','42300000','2016-12-26 00:00:00','1','RCV.1612.003','101',NULL,NULL,NULL,NULL),(47,'01','100000','2016-12-26 00:00:00','1','RCV.1612.004','',NULL,NULL,NULL,'tes');

/*Table structure for table `t_horder` */

DROP TABLE IF EXISTS `t_horder`;

CREATE TABLE `t_horder` (
  `ORDERNO` varchar(15) NOT NULL,
  `WRCODE` varchar(2) DEFAULT NULL,
  `VANCODE` varchar(2) DEFAULT NULL,
  `USCODE` varchar(25) DEFAULT NULL,
  `SLSNO` varchar(5) DEFAULT NULL,
  `OUTNO` varchar(5) DEFAULT NULL,
  `CDISC` varchar(10) DEFAULT NULL,
  `TTYPECODE` char(1) DEFAULT NULL,
  `OSTATUSCODE` char(1) DEFAULT NULL,
  `DOUTNO` varchar(5) DEFAULT NULL,
  `ORDERDATE` timestamp NULL DEFAULT NULL,
  `DUEDATE` timestamp NULL DEFAULT NULL,
  `SUBTOTAL` decimal(10,0) DEFAULT NULL,
  `TOTDISC` decimal(10,0) DEFAULT NULL,
  `PPN` decimal(10,0) DEFAULT NULL,
  `INVNO` varchar(15) DEFAULT NULL,
  `PRINTINV` char(1) DEFAULT '0',
  `RETLOCK` char(1) DEFAULT NULL,
  `PRINTNUM` decimal(1,0) DEFAULT NULL,
  PRIMARY KEY (`ORDERNO`),
  UNIQUE KEY `T_HORDER_PK` (`ORDERNO`),
  KEY `TRANSACTION_TYPE_FK` (`TTYPECODE`),
  KEY `ORDER_STATUS_FK` (`OSTATUSCODE`),
  KEY `USER_ORDER_FK` (`USCODE`),
  KEY `INVOICE_DISCOUNT_FK` (`CDISC`),
  KEY `INVOICE_OUTLET_FK` (`OUTNO`),
  KEY `ORDER_STOCK_FK` (`WRCODE`),
  KEY `SALES_ORDER_FK` (`SLSNO`),
  KEY `VANCODE` (`VANCODE`),
  CONSTRAINT `FK_T_HORDER_INVOICE_D_M_DISCOU` FOREIGN KEY (`CDISC`) REFERENCES `m_discount` (`CDISC`),
  CONSTRAINT `FK_T_HORDER_ORDER_STA_M_OSTATU` FOREIGN KEY (`OSTATUSCODE`) REFERENCES `m_ostatus` (`OSTATUSCODE`),
  CONSTRAINT `FK_T_HORDER_ORDER_STO_M_WAREHO` FOREIGN KEY (`WRCODE`) REFERENCES `m_warehouse` (`WRCODE`),
  CONSTRAINT `FK_T_HORDER_SALES_ORD_M_SALES` FOREIGN KEY (`SLSNO`) REFERENCES `m_sales` (`SLSNO`),
  CONSTRAINT `FK_T_HORDER_TRANSACTI_T_TRANST` FOREIGN KEY (`TTYPECODE`) REFERENCES `t_transtype` (`TTYPECODE`),
  CONSTRAINT `FK_T_HORDER_USER_ORDE_M_USER` FOREIGN KEY (`USCODE`) REFERENCES `m_user` (`USCODE`),
  CONSTRAINT `t_horder_ibfk_1` FOREIGN KEY (`VANCODE`) REFERENCES `m_van` (`VANCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_horder` */

insert  into `t_horder`(`ORDERNO`,`WRCODE`,`VANCODE`,`USCODE`,`SLSNO`,`OUTNO`,`CDISC`,`TTYPECODE`,`OSTATUSCODE`,`DOUTNO`,`ORDERDATE`,`DUEDATE`,`SUBTOTAL`,`TOTDISC`,`PPN`,`INVNO`,`PRINTINV`,`RETLOCK`,`PRINTNUM`) values ('OR.1612.001','02',NULL,NULL,NULL,'',NULL,NULL,'3',NULL,'2016-12-28 00:00:00',NULL,'6390000',NULL,NULL,'INV.1612.003','1',NULL,'5'),('OR.1701.001','02',NULL,NULL,NULL,'',NULL,NULL,'3',NULL,'2017-01-05 00:00:00',NULL,'2600000',NULL,NULL,'INV.1701.001','1',NULL,'1'),('OR.1701.002','02',NULL,NULL,NULL,'101',NULL,NULL,'3',NULL,'2017-01-08 00:00:00',NULL,'300000',NULL,NULL,'INV.1701.002','1',NULL,NULL),('OR.1701.003','02',NULL,NULL,NULL,'',NULL,NULL,'3',NULL,'2017-01-08 00:00:00',NULL,'0',NULL,NULL,'INV.1701.003','1',NULL,NULL),('OR.1701.004','02',NULL,NULL,NULL,'101',NULL,NULL,'3',NULL,'2017-01-08 00:00:00',NULL,'360000',NULL,NULL,'INV.1701.004','1',NULL,NULL);

/*Table structure for table `t_hvan` */

DROP TABLE IF EXISTS `t_hvan`;

CREATE TABLE `t_hvan` (
  `IDTVAN` int(11) NOT NULL AUTO_INCREMENT,
  `VANCODE` varchar(2) DEFAULT NULL,
  `WRCODE` varchar(2) DEFAULT NULL,
  `SLSNO` varchar(5) DEFAULT NULL,
  `OSTATUSCODE` char(1) DEFAULT '2',
  `DAYIN` timestamp NULL DEFAULT NULL,
  `DAYOUT` timestamp NULL DEFAULT NULL,
  `SUBTOTAL` decimal(20,0) DEFAULT NULL,
  PRIMARY KEY (`IDTVAN`),
  KEY `FK_T_VAN_TRANS_VAN_M_WAREHO` (`WRCODE`),
  CONSTRAINT `FK_T_VAN_TRANS_VAN_M_WAREHO` FOREIGN KEY (`WRCODE`) REFERENCES `m_warehouse` (`WRCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_hvan` */

/*Table structure for table `t_invoice` */

DROP TABLE IF EXISTS `t_invoice`;

CREATE TABLE `t_invoice` (
  `INVNO` varchar(15) NOT NULL,
  `INVDATE` date DEFAULT NULL,
  PRIMARY KEY (`INVNO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_invoice` */

insert  into `t_invoice`(`INVNO`,`INVDATE`) values ('INV.0000.001','0000-00-00'),('INV.0000.002','0000-00-00'),('INV.1612.001','2016-12-28'),('INV.1612.002','2016-12-28'),('INV.1612.003','2016-12-28'),('INV.1701.001','2017-01-05'),('INV.1701.002','2017-01-08'),('INV.1701.003','2017-01-08'),('INV.1701.004','2017-01-08');

/*Table structure for table `t_outlet_promo` */

DROP TABLE IF EXISTS `t_outlet_promo`;

CREATE TABLE `t_outlet_promo` (
  `ATTCODE` varchar(2) NOT NULL,
  `IDPROMO` int(11) NOT NULL,
  PRIMARY KEY (`ATTCODE`,`IDPROMO`),
  KEY `IDPROMO` (`IDPROMO`),
  CONSTRAINT `t_outlet_promo_ibfk_1` FOREIGN KEY (`ATTCODE`) REFERENCES `m_outlet_attribute` (`ATTCODE`) ON UPDATE CASCADE,
  CONSTRAINT `t_outlet_promo_ibfk_2` FOREIGN KEY (`IDPROMO`) REFERENCES `m_promo` (`IDPROMO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_outlet_promo` */

insert  into `t_outlet_promo`(`ATTCODE`,`IDPROMO`) values ('P',1),('G',2),('R',3);

/*Table structure for table `t_payment` */

DROP TABLE IF EXISTS `t_payment`;

CREATE TABLE `t_payment` (
  `IDPAYMENT` int(11) NOT NULL AUTO_INCREMENT,
  `PAYNO` varchar(15) DEFAULT NULL,
  `INVNO` varchar(15) DEFAULT NULL,
  `PAYDATE` date DEFAULT NULL,
  `AMOUNT` decimal(20,0) DEFAULT NULL,
  PRIMARY KEY (`IDPAYMENT`),
  KEY `INVNO` (`INVNO`),
  CONSTRAINT `t_payment_ibfk_1` FOREIGN KEY (`INVNO`) REFERENCES `t_invoice` (`INVNO`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

/*Data for the table `t_payment` */

insert  into `t_payment`(`IDPAYMENT`,`PAYNO`,`INVNO`,`PAYDATE`,`AMOUNT`) values (24,'PY.1612.001','INV.1612.001','2016-12-28','1000'),(25,'PY.1612.002','INV.1612.002','2016-12-28','1000'),(26,'PY.1612.003','INV.1612.001','2016-12-28','147689000'),(27,'PY.1612.004','INV.1612.002','2016-12-28','13169000'),(28,'PY.1612.005','INV.1612.003','2016-12-28','1000'),(29,'PY.1701.001','INV.1612.003','2017-01-05','6389000'),(30,'PY.1701.002','INV.1701.001','2017-01-05','2600000'),(31,'PY.1701.003','INV.1701.002','2017-01-08','300000'),(32,'PY.1701.004','INV.1701.003','2017-01-08','0'),(33,'PY.1701.005','INV.1701.004','2017-01-08','360000');

/*Table structure for table `t_recap` */

DROP TABLE IF EXISTS `t_recap`;

CREATE TABLE `t_recap` (
  `RECAPCODE` varchar(8) NOT NULL,
  `RECAPDETAIL` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`RECAPCODE`),
  UNIQUE KEY `T_RECAP_PK` (`RECAPCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_recap` */

/*Table structure for table `t_retorder` */

DROP TABLE IF EXISTS `t_retorder`;

CREATE TABLE `t_retorder` (
  `DORDERNO` int(11) DEFAULT NULL,
  `RETTYPE` char(1) DEFAULT NULL,
  `RETDATE` date DEFAULT NULL,
  `QTYRET` decimal(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_retorder` */

/*Table structure for table `t_retsupp` */

DROP TABLE IF EXISTS `t_retsupp`;

CREATE TABLE `t_retsupp` (
  `IDDBELI` int(11) DEFAULT NULL,
  `QTYRET` decimal(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_retsupp` */

/*Table structure for table `t_stock` */

DROP TABLE IF EXISTS `t_stock`;

CREATE TABLE `t_stock` (
  `STOCKDATE` date NOT NULL,
  `PCODE` varchar(10) NOT NULL,
  `WRCODE` varchar(2) NOT NULL,
  `QTYSTART` decimal(10,0) DEFAULT NULL,
  `QTYIN` decimal(10,0) DEFAULT NULL,
  `QTYOUT` decimal(10,0) DEFAULT NULL,
  `QTYSTOCK` decimal(10,0) DEFAULT NULL,
  `SALDOSTART` decimal(10,0) DEFAULT NULL,
  `DEBIT` decimal(10,0) DEFAULT NULL,
  `KREDIT` decimal(10,0) DEFAULT NULL,
  `SALDOEND` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`STOCKDATE`,`PCODE`,`WRCODE`),
  KEY `FK_T_STOCK_PCODE_M_PRODUCT` (`PCODE`),
  KEY `FK_T_STOCK_WRCODE_M_WAREHOUSE` (`WRCODE`),
  CONSTRAINT `FK_T_STOCK_PCODE_M_PRODUCT` FOREIGN KEY (`PCODE`) REFERENCES `m_product` (`PCODE`),
  CONSTRAINT `FK_T_STOCK_WRCODE_M_WAREHOUSE` FOREIGN KEY (`WRCODE`) REFERENCES `m_warehouse` (`WRCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_stock` */

/*Table structure for table `t_stockmove` */

DROP TABLE IF EXISTS `t_stockmove`;

CREATE TABLE `t_stockmove` (
  `IDSTMV` int(11) NOT NULL AUTO_INCREMENT,
  `STMVCODE` char(2) DEFAULT NULL COMMENT 'I: Opname In; O:Opname Out; S:Sales; R:Receiving; D:Discount',
  `TRANSDATE` timestamp NULL DEFAULT NULL,
  `WRCODE` varchar(2) DEFAULT NULL,
  `PCODE` varchar(10) DEFAULT NULL,
  `STOCKNUM` decimal(10,0) DEFAULT NULL,
  `BALANCE` decimal(17,0) DEFAULT NULL,
  PRIMARY KEY (`IDSTMV`),
  KEY `WAREHOUSE_MOVE_FK` (`WRCODE`),
  KEY `PCODE` (`PCODE`),
  CONSTRAINT `FK_T_STOCKM_WAREHOUSE_M_WAREHO` FOREIGN KEY (`WRCODE`) REFERENCES `m_warehouse` (`WRCODE`),
  CONSTRAINT `t_stockmove_ibfk_1` FOREIGN KEY (`PCODE`) REFERENCES `m_product` (`PCODE`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=latin1;

/*Data for the table `t_stockmove` */

insert  into `t_stockmove`(`IDSTMV`,`STMVCODE`,`TRANSDATE`,`WRCODE`,`PCODE`,`STOCKNUM`,`BALANCE`) values (72,'R','2016-12-26 00:00:00','01','AB01','72','-7200000'),(73,'R','2016-12-26 00:00:00','01','AR01','56','-5600000'),(74,'R','2016-12-26 00:00:00','01','KL01','76','-7600000'),(75,'R','2016-12-26 00:00:00','01','MH01','87','-8700000'),(76,'R','2016-12-26 00:00:00','01','BK01','31','-3100000'),(77,'R','2016-12-26 00:00:00','01','MH01','76','-7600000'),(78,'R','2016-12-26 00:00:00','01','PN01','99','-9900000'),(79,'R','2016-12-26 00:00:00','01','AR01','71','-7100000'),(80,'R','2016-12-26 00:00:00','01','MH01','254','-25400000'),(81,'R','2016-12-26 00:00:00','01','TR01','98','-9800000'),(82,'R','2016-12-26 00:00:00','01','AR01','1','-100000'),(106,'S','2016-12-28 00:00:00','02','AR02','-876','87600000'),(107,'S','2016-12-28 00:00:00','02','TR02','-765','76500000'),(108,'S','2016-12-28 00:00:00','02','BK02','-63','6300000'),(109,'D','2016-12-28 00:00:00','02','BK02','0','-630000'),(110,'S','2016-12-28 00:00:00','02','KL02','-75','7500000'),(111,'S','2016-12-28 00:00:00','02','AR02','-71','7100000'),(112,'D','2016-12-28 00:00:00','02','AR02','0','-710000'),(113,'S','2017-01-05 00:00:00','02','MH02','-12','1200000'),(114,'S','2017-01-05 00:00:00','02','TR02','-14','1400000'),(115,'S','2017-01-08 00:00:00','02','KL02','-1','100000'),(116,'S','2017-01-08 00:00:00','02','MH02','-1','100000'),(117,'S','2017-01-08 00:00:00','02','SN02','-1','100000'),(118,'S','2017-01-08 00:00:00','02','KL02','-1','100000'),(119,'D','2017-01-08 00:00:00','02','KL02','0','-10000'),(120,'S','2017-01-08 00:00:00','02','MH02','-1','100000'),(121,'D','2017-01-08 00:00:00','02','MH02','0','-10000'),(122,'S','2017-01-08 00:00:00','02','SN02','-1','100000'),(123,'D','2017-01-08 00:00:00','02','SN02','0','-10000'),(124,'S','2017-01-08 00:00:00','02','TR02','-1','100000'),(125,'D','2017-01-08 00:00:00','02','TR02','0','-10000');

/*Table structure for table `t_transtype` */

DROP TABLE IF EXISTS `t_transtype`;

CREATE TABLE `t_transtype` (
  `TTYPECODE` char(1) NOT NULL,
  `TRANSNAME` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`TTYPECODE`),
  UNIQUE KEY `T_TRANSTYPE_PK` (`TTYPECODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_transtype` */

/*Table structure for table `t_van` */

DROP TABLE IF EXISTS `t_van`;

CREATE TABLE `t_van` (
  `IDSVAN` int(11) NOT NULL AUTO_INCREMENT,
  `VANCODE` varchar(2) DEFAULT NULL,
  `PCODE` varchar(5) DEFAULT NULL,
  `DAYIN` timestamp NULL DEFAULT NULL,
  `DAYOUT` timestamp NULL DEFAULT NULL,
  `QTYSTOCK` decimal(20,0) DEFAULT NULL,
  `TOTALBAYAR` decimal(20,0) DEFAULT NULL,
  `DAYBEG` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`IDSVAN`),
  KEY `VANID` (`VANCODE`),
  KEY `PCODE` (`PCODE`),
  CONSTRAINT `t_van_ibfk_1` FOREIGN KEY (`VANCODE`) REFERENCES `m_van` (`VANCODE`),
  CONSTRAINT `t_van_ibfk_3` FOREIGN KEY (`PCODE`) REFERENCES `m_product` (`PCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_van` */

/* Trigger structure for table `m_cmenu` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `tib_cmenu` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `tib_cmenu` BEFORE INSERT ON `m_cmenu` FOR EACH ROW BEGIN
	SET @iterate = (SELECT COALESCE(MAX(CMNCODE), 0) FROM m_cmenu WHERE MNCODE = NEW.MNCODE);
	
	SET NEW.CMNCODE := @iterate + 1;
    END */$$


DELIMITER ;

/* Trigger structure for table `m_menu` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `tib_menu` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `tib_menu` BEFORE INSERT ON `m_menu` FOR EACH ROW BEGIN
	SET @iterate = (SELECT COALESCE(MAX(MNCODE), 0) FROM m_menu);
		
	SET NEW.MNCODE := @iterate + 100;
    END */$$


DELIMITER ;

/* Trigger structure for table `t_hbeli` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `tib_hbelidocno` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `tib_hbelidocno` BEFORE INSERT ON `t_hbeli` FOR EACH ROW BEGIN
	SET @prefix = DATE_FORMAT(NEW.TGLBELI, '%y%m');
	SET @iterate = (SELECT COALESCE(MAX(SUBSTR(DOCNO, 10, 3)), 0) FROM t_hbeli WHERE SUBSTR(DOCNO, 5, 4) = @prefix);
	
	SET NEW.DOCNO := CONCAT('RCV.',@prefix,'.',LPAD(COALESCE(@iterate,0) + 1, 3, '0'));
    END */$$


DELIMITER ;

/* Trigger structure for table `t_horder` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `tib_horder` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `tib_horder` BEFORE INSERT ON `t_horder` FOR EACH ROW BEGIN
	SET @prefix = DATE_FORMAT(NEW.ORDERDATE, '%y%m');
	SET @iterate = (SELECT COALESCE(MAX(SUBSTR(ORDERNO, 9, 3)), 0) FROM t_horder WHERE SUBSTR(ORDERNO, 4, 4) = @prefix);
	
	SET NEW.ORDERNO := CONCAT('OR.',@prefix,'.',LPAD(@iterate + 1, 3, '0'));
    END */$$


DELIMITER ;

/* Trigger structure for table `t_invoice` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `tib_invoice` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `tib_invoice` BEFORE INSERT ON `t_invoice` FOR EACH ROW BEGIN
	SET @prefix = DATE_FORMAT(NEW.INVDATE, '%y%m');
	SET @iterate = (SELECT COALESCE(MAX(SUBSTR(INVNO, 10, 3)), 0) FROM t_invoice WHERE SUBSTR(INVNO, 5, 4) = @prefix);
	
	SET NEW.INVNO := CONCAT('INV.',@prefix,'.',LPAD(@iterate + 1, 3, '0'));
    END */$$


DELIMITER ;

/* Trigger structure for table `t_payment` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `tib_payment` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `tib_payment` BEFORE INSERT ON `t_payment` FOR EACH ROW BEGIN
	SET @prefix = DATE_FORMAT(NEW.PAYDATE, '%y%m');
	SET @iterate = (SELECT COALESCE(MAX(SUBSTR(PAYNO, 9, 3)), 0) FROM t_payment WHERE SUBSTR(PAYNO, 4, 4) = @prefix);
	
	SET NEW.PAYNO := CONCAT('PY.',@prefix,'.',LPAD(@iterate + 1, 3, '0'));
    END */$$


DELIMITER ;

/* Procedure structure for procedure `f_totalbeli` */

/*!50003 DROP PROCEDURE IF EXISTS  `f_totalbeli` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `f_totalbeli`(IN id INT)
BEGIN
	SET @t_total = 0;
	
	SELECT SUM(JMLBELI) INTO @t_total 
	FROM t_dbeli
	WHERE IDBELI = id
	GROUP BY IDBELI;
	
	UPDATE t_hbeli SET TOTALBELI = @t_total WHERE IDBELI = id;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `f_totalorder` */

/*!50003 DROP PROCEDURE IF EXISTS  `f_totalorder` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `f_totalorder`(IN id INT)
BEGIN
	SET @t_total = 0;
	
	SELECT SUM(AMOUNT) INTO @t_total 
	FROM t_dorder
	WHERE ORDERNO = id
	GROUP BY ORDERNO;
	
	UPDATE t_horder SET SUBTOTAL = @t_total WHERE ORDERNO = id;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `get_last_custom_error` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_last_custom_error` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_last_custom_error`()
    DETERMINISTIC
    SQL SECURITY INVOKER
BEGIN
  SELECT @error_code, @error_message;
END */$$
DELIMITER ;

/* Procedure structure for procedure `raise_application_error` */

/*!50003 DROP PROCEDURE IF EXISTS  `raise_application_error` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `raise_application_error`(IN CODE INTEGER, IN MESSAGE VARCHAR(255))
    DETERMINISTIC
    SQL SECURITY INVOKER
BEGIN
  CREATE TEMPORARY TABLE IF NOT EXISTS RAISE_ERROR(F1 INT NOT NULL);
  SELECT CODE, MESSAGE INTO @error_code, @error_message;
  INSERT INTO RAISE_ERROR VALUES(NULL);
END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
