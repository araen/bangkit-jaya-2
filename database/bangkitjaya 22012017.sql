/*
SQLyog Community v10.3 
MySQL - 5.1.37 : Database - bangkitjaya
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`bangkitjaya` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `bangkitjaya`;

/*Table structure for table `m_access` */

DROP TABLE IF EXISTS `m_access`;

CREATE TABLE `m_access` (
  `IDROLE` int(11) NOT NULL,
  `CMNCODE` int(11) NOT NULL,
  `ISCREATE` char(1) DEFAULT '0',
  `ISREAD` char(1) DEFAULT '0',
  `ISUPDATE` char(1) DEFAULT '0',
  `ISDELETE` char(1) DEFAULT '0',
  PRIMARY KEY (`IDROLE`,`CMNCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_access` */

insert  into `m_access`(`IDROLE`,`CMNCODE`,`ISCREATE`,`ISREAD`,`ISUPDATE`,`ISDELETE`) values (1,101,'1','1','1','1'),(1,103,'1','1','1','1'),(1,104,'1','1','1','1'),(1,105,'1','1','1','1'),(1,106,'1','1','1','1'),(1,107,'1','1','1','1'),(1,108,'1','1','1','1'),(1,110,'1','1','1','1'),(1,201,'1','1','1','1'),(1,202,'1','1','1','1'),(1,203,'1','1','1','1'),(1,204,'1','1','1','1'),(1,301,'1','1','1','1'),(1,304,'1','1','1','1'),(1,401,'1','1','1','1'),(1,403,'1','1','1','1'),(1,501,'1','1','1','1'),(1,502,'1','1','1','1'),(1,503,'1','1','1','1'),(1,505,'1','1','1','1'),(1,507,'1','1','1','1'),(1,508,'1','1','1','1'),(1,509,'1','1','1','1'),(1,601,'1','1','1','1'),(1,701,'1','1','1','1'),(1,702,'1','1','1','1'),(1,703,'1','1','1','1'),(1,704,'1','1','1','1'),(2,102,'0','1','1','0'),(2,301,'1','1','1','1');

/*Table structure for table `m_cmenu` */

DROP TABLE IF EXISTS `m_cmenu`;

CREATE TABLE `m_cmenu` (
  `CMNCODE` varchar(5) NOT NULL,
  `MNCODE` varchar(3) DEFAULT NULL,
  `CMNNAME` varchar(25) DEFAULT NULL,
  `CMNURL` varchar(30) DEFAULT NULL,
  `ISACTIVE` char(1) DEFAULT NULL,
  PRIMARY KEY (`CMNCODE`),
  UNIQUE KEY `M_CMENU_PK` (`CMNCODE`),
  KEY `CHILD_MENU_FK` (`MNCODE`),
  CONSTRAINT `FK_M_CMENU_CHILD_MEN_M_MENU` FOREIGN KEY (`MNCODE`) REFERENCES `m_menu` (`MNCODE`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_cmenu` */

insert  into `m_cmenu`(`CMNCODE`,`MNCODE`,`CMNNAME`,`CMNURL`,`ISACTIVE`) values ('101','100','Jenis Customer','jnsoutlet','1'),('102','100','Sales','sales','1'),('103','100','Customer','outlet','1'),('104','100','Jenis Sales','jnssales','0'),('105','100','Merk','brand','1'),('106','100','Produk','produk','1'),('107','100','Supplier','supplier','1'),('108','100','Gudang','warehouse','1'),('109','100','Van','van','1'),('110','100','Promo','promo','1'),('201','200','Penerimaan Barang Log','pembelianlog/form','1'),('202','200','Penerimaan Barang Unit','pembelian/form','1'),('203','200','Opname Gudang','opname','1'),('204','200','Retur Supplier','returnsupp','1'),('205','200','Penambahan Barang Van','vanload','1'),('206','200','Penurunan Barang Van','vandrop','1'),('301','300','Input Data Order (F2)','order/form','1'),('302','300','Proses Data Order','prosesorder','1'),('304','300','Retur Penjualan','returnorder','1'),('305','300','Cetak Invoice','invoice','1'),('401','400','Pembayaran','pembayaran','1'),('403','400','Auto Bill','autobill','1'),('501','500','Rekap Data Per Cust','rekaporder/outlet','1'),('502','500','Rekap Data Per Produk','rekaporder/product','1'),('503','500','Stock Gudang','stockgudang','1'),('504','500','Stock Van','stockvan','1'),('505','500','Audit Trail Gudang','audittrail/warehouse','1'),('506','500','Audit Trail Van','audittrail/van','1'),('507','500','Penjualan','reppenjualan','1'),('508','500','Retur','retur','1'),('509','500','Pelunasan','reppayment','1'),('601','600','Closing','closing','1'),('701','700','User','user','1'),('702','700','Contact','setting','1'),('703','700','Menu','menu','1'),('704','700','Role','role','1');

/*Table structure for table `m_collector` */

DROP TABLE IF EXISTS `m_collector`;

CREATE TABLE `m_collector` (
  `COLCODE` varchar(5) NOT NULL,
  `COLNAME` varchar(32) DEFAULT NULL,
  `COLADD` varchar(50) DEFAULT NULL,
  `COLCITY` varchar(25) DEFAULT NULL,
  `BIRTHDATE` timestamp NULL DEFAULT NULL,
  `CWORKDATE` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`COLCODE`),
  UNIQUE KEY `M_COLLECTOR_PK` (`COLCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_collector` */

/*Table structure for table `m_debt_reason` */

DROP TABLE IF EXISTS `m_debt_reason`;

CREATE TABLE `m_debt_reason` (
  `DEBTREASONCODE` varchar(2) NOT NULL,
  `DEBTREASONNAME` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`DEBTREASONCODE`),
  UNIQUE KEY `M_DEBT_REASON_PK` (`DEBTREASONCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_debt_reason` */

/*Table structure for table `m_discount` */

DROP TABLE IF EXISTS `m_discount`;

CREATE TABLE `m_discount` (
  `CDISC` varchar(10) NOT NULL,
  `MINRANGE` timestamp NULL DEFAULT NULL,
  `MAXRANGE` timestamp NULL DEFAULT NULL,
  `PERCENT` decimal(3,0) DEFAULT NULL,
  `DISCTYPE` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`CDISC`),
  UNIQUE KEY `M_DISCOUNT_PK` (`CDISC`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_discount` */

/*Table structure for table `m_menu` */

DROP TABLE IF EXISTS `m_menu`;

CREATE TABLE `m_menu` (
  `MNCODE` varchar(3) NOT NULL,
  `MNNAME` varchar(25) DEFAULT NULL,
  `ISACTIVE` char(1) DEFAULT NULL,
  PRIMARY KEY (`MNCODE`),
  UNIQUE KEY `M_MENU_PK` (`MNCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_menu` */

insert  into `m_menu`(`MNCODE`,`MNNAME`,`ISACTIVE`) values ('100','Master','1'),('200','Inventory','1'),('300','Sales','1'),('400','Account Receivable','1'),('500','Reporting','1'),('600','End of Day','1'),('700','Setting','1');

/*Table structure for table `m_ostatus` */

DROP TABLE IF EXISTS `m_ostatus`;

CREATE TABLE `m_ostatus` (
  `OSTATUSCODE` char(1) NOT NULL,
  `OSTATUSNAME` varchar(20) DEFAULT NULL,
  `OSTATUSVALUE` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`OSTATUSCODE`),
  UNIQUE KEY `M_OSTATUS_PK` (`OSTATUSCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_ostatus` */

insert  into `m_ostatus`(`OSTATUSCODE`,`OSTATUSNAME`,`OSTATUSVALUE`) values ('1','OPNAME','1'),('2','ORDER',NULL),('3','POSTING',NULL);

/*Table structure for table `m_outlet` */

DROP TABLE IF EXISTS `m_outlet`;

CREATE TABLE `m_outlet` (
  `OUTNO` varchar(5) NOT NULL,
  `ATTCODE` varchar(2) DEFAULT NULL,
  `OUTNAME` varchar(32) DEFAULT NULL,
  `OUTADD` varchar(50) DEFAULT NULL,
  `OCITY` varchar(20) DEFAULT NULL,
  `OPHONE` varchar(20) DEFAULT NULL,
  `OPOST` decimal(5,0) DEFAULT NULL,
  `OCONTACT` varchar(32) DEFAULT NULL,
  `FLIMIT` decimal(10,0) DEFAULT NULL,
  `FIRSTTRANS` timestamp NULL DEFAULT NULL,
  `REGDATE` timestamp NULL DEFAULT NULL,
  `NPWP` varchar(20) DEFAULT NULL,
  `FIRSTOPEN` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`OUTNO`),
  UNIQUE KEY `M_OUTLET_PK` (`OUTNO`),
  KEY `OUTLET_TYPE_FK` (`ATTCODE`),
  CONSTRAINT `FK_M_OUTLET_OUTLET_TY_M_OUTLET` FOREIGN KEY (`ATTCODE`) REFERENCES `m_outlet_attribute` (`ATTCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_outlet` */

insert  into `m_outlet`(`OUTNO`,`ATTCODE`,`OUTNAME`,`OUTADD`,`OCITY`,`OPHONE`,`OPOST`,`OCONTACT`,`FLIMIT`,`FIRSTTRANS`,`REGDATE`,`NPWP`,`FIRSTOPEN`) values ('101','G','PT. Test','Surabaya','08123456','','0','','0','0000-00-00 00:00:00','0000-00-00 00:00:00','','0000-00-00 00:00:00'),('102','P','PT Tes2','','','','0','','0','0000-00-00 00:00:00','0000-00-00 00:00:00','','0000-00-00 00:00:00');

/*Table structure for table `m_outlet_attribute` */

DROP TABLE IF EXISTS `m_outlet_attribute`;

CREATE TABLE `m_outlet_attribute` (
  `ATTCODE` varchar(2) NOT NULL,
  `ATTNAME` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`ATTCODE`),
  UNIQUE KEY `M_OUTLET_ATTRIBUTE_PK` (`ATTCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_outlet_attribute` */

insert  into `m_outlet_attribute`(`ATTCODE`,`ATTNAME`) values ('G','Galangan'),('P','Proyek'),('R','Retail');

/*Table structure for table `m_pbrand` */

DROP TABLE IF EXISTS `m_pbrand`;

CREATE TABLE `m_pbrand` (
  `PBCODE` varchar(2) NOT NULL,
  `PBNAME` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`PBCODE`),
  UNIQUE KEY `M_PBRAND_PK` (`PBCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_pbrand` */

insert  into `m_pbrand`(`PBCODE`,`PBNAME`) values ('AB','Albasia'),('AR','Aren'),('BK','Bangkirai'),('CD','Cendana'),('EB','Eboni'),('JT','Jati'),('KL','Kelapa'),('KM','Kamper'),('MB','Merbau'),('MH','Mahoni'),('MR','Meranti'),('PN','Pinus'),('SK','Sungkai'),('SN','Sonokeling'),('TR','Trembesi'),('UL','Ulin');

/*Table structure for table `m_product` */

DROP TABLE IF EXISTS `m_product`;

CREATE TABLE `m_product` (
  `PCODE` varchar(10) NOT NULL,
  `SUPPNO` varchar(5) DEFAULT NULL,
  `PBCODE` varchar(2) DEFAULT NULL,
  `PNAME` varchar(50) DEFAULT NULL,
  `PGROUP` char(1) DEFAULT NULL COMMENT 'L:Log; U:Unit',
  `PWEIGHT` decimal(3,0) DEFAULT NULL,
  `PLAUNCH` timestamp NULL DEFAULT NULL,
  `PSTATUS` char(1) DEFAULT NULL,
  `PPRICE1` decimal(20,0) DEFAULT NULL,
  `PPRICE2` decimal(20,0) DEFAULT NULL,
  `PPRICE3` decimal(20,0) DEFAULT NULL,
  `CONV1` decimal(2,0) DEFAULT NULL,
  `CONV2` decimal(2,0) DEFAULT NULL,
  `CONV3` decimal(3,0) DEFAULT NULL,
  `SPRICE1` decimal(20,0) DEFAULT NULL,
  PRIMARY KEY (`PCODE`),
  UNIQUE KEY `M_PRODUCT_PK` (`PCODE`),
  KEY `PRODUCT_BRAND_FK` (`PBCODE`),
  KEY `PRODUCT_SUPPLIER_FK` (`SUPPNO`),
  CONSTRAINT `FK_M_PRODUC_PRODUCT_B_M_PBRAND` FOREIGN KEY (`PBCODE`) REFERENCES `m_pbrand` (`PBCODE`),
  CONSTRAINT `FK_M_PRODUC_PRODUCT_S_M_SUPPLI` FOREIGN KEY (`SUPPNO`) REFERENCES `m_supplier` (`SUPPNO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_product` */

insert  into `m_product`(`PCODE`,`SUPPNO`,`PBCODE`,`PNAME`,`PGROUP`,`PWEIGHT`,`PLAUNCH`,`PSTATUS`,`PPRICE1`,`PPRICE2`,`PPRICE3`,`CONV1`,`CONV2`,`CONV3`,`SPRICE1`) values ('AB01','101','AB','ALBASIA LOG','L','0','2016-11-24 00:00:00','','100000','0','0','0','0','0','120000'),('AB02','101','AB','ALBASIA UNIT','U','0','2016-11-24 00:00:00','','100000','0','0','0','0','0','110000'),('AR01','101','AR','AREN LOG','L','0','2016-11-25 00:00:00','','100000','0','0','0','0','0','110000'),('AR02','101','AR','AREN UNIT','U','0','2016-11-25 00:00:00','','100000','0','0','0','0','0','110000'),('BK01','101','BK','BANGKIRAI LOG','L','0','2016-11-26 00:00:00','','100000','0','0','0','0','0','110000'),('BK02','101','BK','BANGKIRAI UNIT','U','0','2016-11-26 00:00:00','','100000','0','0','0','0','0','110000'),('CD01','101','CD','CENDANA LOG','L',NULL,'2016-11-27 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL,'110000'),('CD02','101','CD','CENDANA UNIT','U','0','2016-11-27 00:00:00','','100000','0','0','1','0','0','110000'),('EB01','101','EB','EBONI LOG','L',NULL,'2016-11-28 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL,'110000'),('EB02','101','EB','EBONI UNIT','U','0','2016-11-28 00:00:00','','100000','0','0','1','0','0','110000'),('JT01','101','JT','JATI LOG','L',NULL,'2016-11-29 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL,'110000'),('JT02','101','JT','JATI UNIT','U','0','2016-11-29 00:00:00','','100000','0','0','1','0','0','110000'),('KL01','101','KL','KELAPA LOG','L',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL,'110000'),('KL02','101','KL','KELAPA UNIT','U','0','0000-00-00 00:00:00','','100000','0','0','1','0','0','110000'),('KM01','101','KM','KAMPER LOG','L',NULL,'2016-11-30 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL,'110000'),('KM02','101','KM','KAMPER UNIT','U','0','2016-11-30 00:00:00','','100000','0','0','1','0','0','110000'),('MB01','101','MB','MERBAU LOG','L',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL,'110000'),('MB02','101','MB','MERBAU UNIT','U','0','0000-00-00 00:00:00','','100000','0','0','1','0','0','110000'),('MH01','101','MH','MAHONI LOG','L',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL,'110000'),('MH02','101','MH','MAHONI UNIT','U',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,NULL,NULL,NULL,'110000'),('MR01','101','MR','MERANTI LOG','L',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL,'110000'),('MR02','101','MR','MERANTI UNIT','U',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,NULL,NULL,NULL,'110000'),('PN01','101','PN','PINUS LOG','L',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL,'110000'),('PN02','101','PN','PINUS UNIT','U',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,NULL,NULL,NULL,'110000'),('SK01','101','SK','SUNGKAI LOG','L',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL,'110000'),('SK02','101','SK','SUNGKAI UNIT','U',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,NULL,NULL,NULL,'110000'),('SN01','101','SN','SONOKELING LOG','L',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL,'110000'),('SN02','101','SN','SONOKELING UNIT','U',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,NULL,NULL,NULL,'110000'),('TR01','101','TR','TREMBESI LOG','L',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL,'110000'),('TR02','101','TR','TREMBESI UNIT','U',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,NULL,NULL,NULL,'110000'),('UL01','101','UL','ULIN LOG','L',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,'1',NULL,NULL,'110000'),('UL02','101','UL','ULIN UNIT','U',NULL,'0000-00-00 00:00:00',NULL,'100000',NULL,NULL,NULL,NULL,NULL,'110000');

/*Table structure for table `m_profile` */

DROP TABLE IF EXISTS `m_profile`;

CREATE TABLE `m_profile` (
  `PROFCD` varchar(5) NOT NULL,
  `PROFNAME` varchar(20) DEFAULT NULL,
  `PROFADD` varchar(25) DEFAULT NULL,
  `PROFNPWP` varchar(20) DEFAULT NULL,
  `PROFCITY` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`PROFCD`),
  UNIQUE KEY `M_PROFILE_PK` (`PROFCD`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_profile` */

/*Table structure for table `m_promo` */

DROP TABLE IF EXISTS `m_promo`;

CREATE TABLE `m_promo` (
  `IDPROMO` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(100) DEFAULT NULL,
  `DESCRIPTION` text,
  `STARTDATE` date DEFAULT NULL,
  `DUEDATE` date DEFAULT NULL,
  `ISITEMDISC` char(1) DEFAULT NULL,
  `TOTALDISC` decimal(3,0) DEFAULT NULL,
  `MINAMOUNT` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`IDPROMO`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `m_promo` */

insert  into `m_promo`(`IDPROMO`,`TITLE`,`DESCRIPTION`,`STARTDATE`,`DUEDATE`,`ISITEMDISC`,`TOTALDISC`,`MINAMOUNT`) values (1,'Customer Proyek','Diskon untuk customer proyek','2016-12-25','2017-12-25','','5','0'),(2,'Customer Galangan','Diskon untuk customer dengan jenis galangan','2016-12-25','2017-01-09','','10','0'),(3,'Customer Retail','Diskon untuk pelanggan retail','2016-12-25','2017-12-25','','12','0');

/*Table structure for table `m_role` */

DROP TABLE IF EXISTS `m_role`;

CREATE TABLE `m_role` (
  `IDROLE` int(11) NOT NULL AUTO_INCREMENT,
  `NAMA` varchar(20) DEFAULT NULL,
  `ISACTIVE` int(11) DEFAULT '1',
  PRIMARY KEY (`IDROLE`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `m_role` */

insert  into `m_role`(`IDROLE`,`NAMA`,`ISACTIVE`) values (1,'Admin',1),(2,'Sales',1);

/*Table structure for table `m_sales` */

DROP TABLE IF EXISTS `m_sales`;

CREATE TABLE `m_sales` (
  `SLSNO` varchar(5) NOT NULL,
  `VANCODE` varchar(3) DEFAULT NULL,
  `STCODE` varchar(5) DEFAULT NULL,
  `SLSNAME` varchar(32) DEFAULT NULL,
  `SLSADD` varchar(50) DEFAULT NULL,
  `SLSCITY` varchar(25) DEFAULT NULL,
  `WORKDATE` timestamp NULL DEFAULT NULL,
  `TRANSDATE` timestamp NULL DEFAULT NULL,
  `BIRTHDATE` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`SLSNO`),
  UNIQUE KEY `M_SALES_PK` (`SLSNO`),
  KEY `SALES_TYPE_FK` (`STCODE`),
  KEY `VANCODE` (`VANCODE`),
  CONSTRAINT `FK_M_SALES_SALES_TYP_M_SALES_` FOREIGN KEY (`STCODE`) REFERENCES `m_sales_type` (`STCODE`),
  CONSTRAINT `m_sales_ibfk_2` FOREIGN KEY (`VANCODE`) REFERENCES `m_van` (`VANCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_sales` */

insert  into `m_sales`(`SLSNO`,`VANCODE`,`STCODE`,`SLSNAME`,`SLSADD`,`SLSCITY`,`WORKDATE`,`TRANSDATE`,`BIRTHDATE`) values ('00001',NULL,'TO','ANANG PRIBADI','PETUNG','PENAJAM','2015-02-01 21:59:36','2015-02-01 21:59:36','1994-06-05 23:59:42'),('00002','01','CVS','YOGA','PETUNG','PENAJAM','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00');

/*Table structure for table `m_sales_type` */

DROP TABLE IF EXISTS `m_sales_type`;

CREATE TABLE `m_sales_type` (
  `STCODE` varchar(5) NOT NULL,
  `STNAME` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`STCODE`),
  UNIQUE KEY `M_SALES_TYPE_PK` (`STCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_sales_type` */

insert  into `m_sales_type`(`STCODE`,`STNAME`) values ('CVS','Canvas'),('TO','Taking Order');

/*Table structure for table `m_setting` */

DROP TABLE IF EXISTS `m_setting`;

CREATE TABLE `m_setting` (
  `SKEY` varchar(20) NOT NULL DEFAULT '',
  `VALUE` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`SKEY`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_setting` */

insert  into `m_setting`(`SKEY`,`VALUE`) values ('ADDRESS','Jl.Propinsi Km 1'),('CORPORATE_NAME','UD. Bangkit Jaya'),('EMAIL_CONTACT',''),('MOBILE',''),('PHONE','0542-7200168'),('VANDATE','2015-12-25'),('WRDATE','2017-01-20');

/*Table structure for table `m_supplier` */

DROP TABLE IF EXISTS `m_supplier`;

CREATE TABLE `m_supplier` (
  `SUPPNO` varchar(5) NOT NULL,
  `SUPPNAME` varchar(50) DEFAULT NULL,
  `SUPPADD` varchar(50) DEFAULT NULL,
  `SUPPPHONE` varchar(20) DEFAULT NULL,
  `SUPPFAX` varchar(20) DEFAULT NULL,
  `SUPPCONTACT` varchar(25) DEFAULT NULL,
  `SUPPTYPE` varchar(2) DEFAULT NULL,
  `ACCNO` decimal(20,0) DEFAULT NULL,
  PRIMARY KEY (`SUPPNO`),
  UNIQUE KEY `M_SUPPLIER_PK` (`SUPPNO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_supplier` */

insert  into `m_supplier`(`SUPPNO`,`SUPPNAME`,`SUPPADD`,`SUPPPHONE`,`SUPPFAX`,`SUPPCONTACT`,`SUPPTYPE`,`ACCNO`) values ('101','ALPUKATSOFT','Surabaya','0812345678','','','','0');

/*Table structure for table `m_user` */

DROP TABLE IF EXISTS `m_user`;

CREATE TABLE `m_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USCODE` varchar(25) DEFAULT NULL,
  `USNAME` varchar(25) DEFAULT NULL,
  `FULLNAME` varchar(100) DEFAULT NULL,
  `CDLEVEL` decimal(1,0) DEFAULT NULL,
  `CODE` varchar(5) DEFAULT NULL,
  `USPASSWORD` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `M_USER_PK` (`USCODE`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `m_user` */

insert  into `m_user`(`ID`,`USCODE`,`USNAME`,`FULLNAME`,`CDLEVEL`,`CODE`,`USPASSWORD`) values (1,NULL,'admin','Administrator',NULL,NULL,'21232f297a57a5a743894a0e4a801fc3'),(2,NULL,'sales','Salesman',NULL,NULL,'9ed083b1436e5f40ef984b28255eef18'),(3,NULL,'yoga','Azmi Yoga',NULL,NULL,'807659cd883fc0a63f6ce615893b3558');

/*Table structure for table `m_userrole` */

DROP TABLE IF EXISTS `m_userrole`;

CREATE TABLE `m_userrole` (
  `IDROLE` int(11) NOT NULL,
  `IDUSER` int(11) NOT NULL,
  KEY `IDROLE` (`IDROLE`),
  KEY `IDUSER` (`IDUSER`),
  CONSTRAINT `m_userrole_ibfk_1` FOREIGN KEY (`IDROLE`) REFERENCES `m_role` (`IDROLE`),
  CONSTRAINT `m_userrole_ibfk_2` FOREIGN KEY (`IDUSER`) REFERENCES `m_user` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_userrole` */

insert  into `m_userrole`(`IDROLE`,`IDUSER`) values (1,1),(2,2),(1,3);

/*Table structure for table `m_van` */

DROP TABLE IF EXISTS `m_van`;

CREATE TABLE `m_van` (
  `VANCODE` varchar(5) NOT NULL,
  `VANNAME` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`VANCODE`),
  UNIQUE KEY `M_VAN_TYPE_PK` (`VANCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_van` */

insert  into `m_van`(`VANCODE`,`VANNAME`) values ('01','VAN 1'),('02','VAN 2');

/*Table structure for table `m_warehouse` */

DROP TABLE IF EXISTS `m_warehouse`;

CREATE TABLE `m_warehouse` (
  `WRCODE` varchar(2) NOT NULL,
  `PCODE` varchar(5) DEFAULT NULL,
  `WRNAME` varchar(25) DEFAULT NULL,
  `WRDATE` timestamp NULL DEFAULT NULL,
  `WRSTOCK` decimal(20,0) DEFAULT NULL,
  `WRTYPE` char(1) DEFAULT 'L',
  PRIMARY KEY (`WRCODE`),
  UNIQUE KEY `M_WAREHOUSE_PK` (`WRCODE`),
  KEY `PRODUCT_STOCK_FK` (`PCODE`),
  CONSTRAINT `FK_M_WAREHO_PRODUCT_S_M_PRODUC` FOREIGN KEY (`PCODE`) REFERENCES `m_product` (`PCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_warehouse` */

insert  into `m_warehouse`(`WRCODE`,`PCODE`,`WRNAME`,`WRDATE`,`WRSTOCK`,`WRTYPE`) values ('01',NULL,'GUDANG LOG',NULL,NULL,'L'),('02',NULL,'GUDANG UNIT',NULL,NULL,'U');

/*Table structure for table `t_cart` */

DROP TABLE IF EXISTS `t_cart`;

CREATE TABLE `t_cart` (
  `SESSIONID` varchar(100) NOT NULL,
  `TRXCODE` char(1) NOT NULL COMMENT 'R:Receiving; S:Sales',
  `PCODE` varchar(10) NOT NULL,
  `SUPPNO` varchar(5) DEFAULT NULL,
  `OUTNO` varchar(5) DEFAULT NULL,
  `PNAME` varchar(30) DEFAULT NULL,
  `PPRICE` decimal(17,0) DEFAULT NULL,
  `QTY` decimal(5,0) DEFAULT NULL,
  `DISC` decimal(3,0) DEFAULT NULL,
  `TOTAL` decimal(17,0) DEFAULT NULL,
  PRIMARY KEY (`SESSIONID`,`TRXCODE`,`PCODE`),
  KEY `PCODE` (`PCODE`),
  KEY `SUPPNO` (`SUPPNO`),
  KEY `OUTNO` (`OUTNO`),
  CONSTRAINT `t_cart_ibfk_1` FOREIGN KEY (`PCODE`) REFERENCES `m_product` (`PCODE`),
  CONSTRAINT `t_cart_ibfk_2` FOREIGN KEY (`SUPPNO`) REFERENCES `m_supplier` (`SUPPNO`),
  CONSTRAINT `t_cart_ibfk_3` FOREIGN KEY (`OUTNO`) REFERENCES `m_outlet` (`OUTNO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_cart` */

/*Table structure for table `t_dbeli` */

DROP TABLE IF EXISTS `t_dbeli`;

CREATE TABLE `t_dbeli` (
  `IDDBELI` int(11) NOT NULL AUTO_INCREMENT,
  `IDBELI` int(11) NOT NULL,
  `PCODE` varchar(10) NOT NULL,
  `QTYBELI` decimal(10,0) DEFAULT NULL,
  `AMOUNTITEM` decimal(10,0) DEFAULT NULL,
  `JMLBELI` decimal(10,0) DEFAULT NULL,
  `RETDATE` datetime DEFAULT NULL,
  `QTYRET` decimal(10,0) DEFAULT NULL,
  `AMOUNTRET` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`IDDBELI`),
  KEY `FK_T_DBELI_IDBELI_S_T_HBELI` (`IDBELI`),
  KEY `FK_T_DBELI_PCODE_S_M_PRODUCT` (`PCODE`),
  CONSTRAINT `FK_T_DBELI_IDBELI_S_T_HBELI` FOREIGN KEY (`IDBELI`) REFERENCES `t_hbeli` (`IDBELI`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_T_DBELI_PCODE_S_M_PRODUCT` FOREIGN KEY (`PCODE`) REFERENCES `m_product` (`PCODE`)
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=latin1;

/*Data for the table `t_dbeli` */

insert  into `t_dbeli`(`IDDBELI`,`IDBELI`,`PCODE`,`QTYBELI`,`AMOUNTITEM`,`JMLBELI`,`RETDATE`,`QTYRET`,`AMOUNTRET`) values (102,44,'AB01','72','100000','7200000',NULL,NULL,NULL),(103,44,'AR01','56','100000','5600000',NULL,NULL,NULL),(104,44,'KL01','76','100000','7600000',NULL,NULL,NULL),(105,44,'MH01','87','100000','8700000',NULL,NULL,NULL),(106,45,'BK01','31','100000','3100000',NULL,NULL,NULL),(107,45,'MH01','76','100000','7600000',NULL,NULL,NULL),(108,45,'PN01','99','100000','9900000',NULL,NULL,NULL),(109,46,'AR01','71','100000','7100000','2017-01-18 00:00:00','5','500000'),(110,46,'MH01','254','100000','25400000','2017-01-18 00:00:00','4','400000'),(111,46,'TR01','98','100000','9800000','2017-01-18 00:00:00','8','800000'),(112,47,'AR01','1','100000','100000',NULL,NULL,NULL),(113,48,'KL02','1','100000','100000',NULL,NULL,NULL),(114,48,'SN02','1','100000','100000',NULL,NULL,NULL),(115,49,'KL01','1','100000','100000',NULL,NULL,NULL),(116,49,'SN01','1','100000','100000',NULL,NULL,NULL),(117,49,'TR01','1','100000','100000',NULL,NULL,NULL),(118,50,'AB02','1','100000','100000',NULL,NULL,NULL),(119,50,'KL02','1','100000','100000',NULL,NULL,NULL),(120,50,'MH02','1','100000','100000',NULL,NULL,NULL),(122,52,'AR02','1','100000','100000',NULL,NULL,NULL),(123,52,'KL02','1','100000','100000',NULL,NULL,NULL),(127,56,'AB02','1','100000','100000',NULL,NULL,NULL),(128,57,'KL01','1','100000','100000',NULL,NULL,NULL),(129,57,'SN01','1','100000','100000',NULL,NULL,NULL),(130,58,'AB01','1','100000','100000',NULL,NULL,NULL),(131,58,'AB02','1','100000','100000',NULL,NULL,NULL),(132,58,'KL02','2','100000','200000',NULL,NULL,NULL),(133,58,'MH02','1','100000','100000',NULL,NULL,NULL),(134,58,'TR02','1','100000','100000',NULL,NULL,NULL),(135,62,'KL02','1','100000','100000',NULL,NULL,NULL),(136,62,'SN02','1','100000','100000',NULL,NULL,NULL),(137,62,'TR02','1','100000','100000',NULL,NULL,NULL),(138,63,'AR02','5','100000','500000',NULL,NULL,NULL);

/*Table structure for table `t_debt` */

DROP TABLE IF EXISTS `t_debt`;

CREATE TABLE `t_debt` (
  `DEBTCODE` varchar(8) NOT NULL,
  `ORDERNO` varchar(5) DEFAULT NULL,
  `OUTNO` varchar(5) DEFAULT NULL,
  `DEBTREASONCODE` varchar(2) DEFAULT NULL,
  `COLCODE` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`DEBTCODE`),
  UNIQUE KEY `T_DEBT_PK` (`DEBTCODE`),
  KEY `OUTLET_DEBT_FK` (`OUTNO`),
  KEY `ORDER_DEBT_FK` (`ORDERNO`),
  KEY `DEBT_REASON_FK` (`DEBTREASONCODE`),
  KEY `DEBT_COLLECTOR_FK` (`COLCODE`),
  CONSTRAINT `FK_T_DEBT_DEBT_COLL_M_COLLEC` FOREIGN KEY (`COLCODE`) REFERENCES `m_collector` (`COLCODE`),
  CONSTRAINT `FK_T_DEBT_DEBT_REAS_M_DEBT_R` FOREIGN KEY (`DEBTREASONCODE`) REFERENCES `m_debt_reason` (`DEBTREASONCODE`),
  CONSTRAINT `FK_T_DEBT_ORDER_DEB_T_HORDER` FOREIGN KEY (`ORDERNO`) REFERENCES `t_horder` (`ORDERNO`),
  CONSTRAINT `FK_T_DEBT_OUTLET_DE_M_OUTLET` FOREIGN KEY (`OUTNO`) REFERENCES `m_outlet` (`OUTNO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_debt` */

/*Table structure for table `t_dorder` */

DROP TABLE IF EXISTS `t_dorder`;

CREATE TABLE `t_dorder` (
  `DORDERNO` int(11) NOT NULL AUTO_INCREMENT,
  `ORDERNO` varchar(15) DEFAULT NULL,
  `PCODE` varchar(10) DEFAULT NULL,
  `QTY` decimal(10,0) DEFAULT NULL,
  `AMOUNTITEM` decimal(10,0) DEFAULT NULL,
  `DISC` decimal(10,0) DEFAULT NULL,
  `AMOUNTDISC` decimal(10,0) DEFAULT NULL,
  `AMOUNT` decimal(10,0) DEFAULT NULL,
  `COSTVALUE` decimal(10,0) DEFAULT NULL,
  `RETTYPE` char(1) DEFAULT NULL,
  `RETDATE` datetime DEFAULT NULL,
  `QTYRET` decimal(10,0) DEFAULT NULL,
  `AMOUNTRET` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`DORDERNO`),
  KEY `DETAIL_ORDER_FK` (`ORDERNO`),
  KEY `FK_T_DORDER_DETAIL_OR_M_PRODUCT` (`PCODE`),
  CONSTRAINT `FK_T_DORDER_DETAIL_OR_M_PRODUCT` FOREIGN KEY (`PCODE`) REFERENCES `m_product` (`PCODE`),
  CONSTRAINT `FK_T_DORDER_DETAIL_OR_T_HORDER` FOREIGN KEY (`ORDERNO`) REFERENCES `t_horder` (`ORDERNO`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

/*Data for the table `t_dorder` */

insert  into `t_dorder`(`DORDERNO`,`ORDERNO`,`PCODE`,`QTY`,`AMOUNTITEM`,`DISC`,`AMOUNTDISC`,`AMOUNT`,`COSTVALUE`,`RETTYPE`,`RETDATE`,`QTYRET`,`AMOUNTRET`) values (33,'OR.1612.001','AR02','71','100000','10','710000','6390000',NULL,NULL,NULL,NULL,NULL),(34,'OR.1701.001','MH02','12','100000','0','0','1200000',NULL,NULL,NULL,NULL,NULL),(35,'OR.1701.001','TR02','14','100000','0','0','1400000',NULL,NULL,NULL,NULL,NULL),(36,'OR.1701.002','KL02','1','100000','0','0','100000',NULL,NULL,NULL,NULL,NULL),(37,'OR.1701.002','MH02','1','100000','0','0','100000',NULL,NULL,NULL,NULL,NULL),(38,'OR.1701.002','SN02','1','100000','0','0','100000',NULL,NULL,NULL,NULL,NULL),(39,'OR.1701.004','KL02','1','100000','10','10000','90000',NULL,'T','2017-01-18 01:15:40','1','99000'),(40,'OR.1701.004','MH02','1','100000','10','10000','90000',NULL,'T','2017-01-18 01:15:40','1','99000'),(41,'OR.1701.004','SN02','1','100000','10','10000','90000',NULL,'T','2017-01-18 01:15:40','1','99000'),(42,'OR.1701.004','TR02','1','100000','10','10000','90000',NULL,'T','2017-01-18 01:15:40','1','99000'),(43,'OR.1701.005','AB02','1','110000','5','5500','104500',NULL,NULL,NULL,NULL,NULL),(44,'OR.1701.005','KM02','1','110000','5','5500','104500',NULL,NULL,NULL,NULL,NULL),(45,'OR.1701.005','MR02','45','110000','5','247500','4702500',NULL,'R','2017-01-18 00:00:00','5','522500'),(46,'OR.1701.006','SN02','5','110000','5','27500','522500',NULL,'R','2017-01-18 00:00:00','2','209000'),(47,'OR.1701.006','TR02','7','110000','5','38500','731500',NULL,'T','2017-01-18 00:00:00','2','209000'),(48,'OR.1701.007','TR02','1','110000','0','0','110000',NULL,NULL,NULL,NULL,NULL),(49,'OR.1701.008','AB02','2','110000','0','0','220000',NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `t_dvan` */

DROP TABLE IF EXISTS `t_dvan`;

CREATE TABLE `t_dvan` (
  `IDDTVAN` int(11) NOT NULL AUTO_INCREMENT,
  `IDTVAN` int(11) NOT NULL,
  `PCODE` varchar(10) NOT NULL,
  `QTY` decimal(20,0) DEFAULT NULL,
  `AMOUNT` decimal(20,0) DEFAULT NULL,
  PRIMARY KEY (`IDDTVAN`),
  KEY `PCODE` (`PCODE`),
  KEY `IDTVAN` (`IDTVAN`),
  CONSTRAINT `t_dvan_ibfk_2` FOREIGN KEY (`PCODE`) REFERENCES `m_product` (`PCODE`),
  CONSTRAINT `t_dvan_ibfk_3` FOREIGN KEY (`IDTVAN`) REFERENCES `t_hvan` (`IDTVAN`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_dvan` */

/*Table structure for table `t_hbeli` */

DROP TABLE IF EXISTS `t_hbeli`;

CREATE TABLE `t_hbeli` (
  `IDBELI` int(11) NOT NULL AUTO_INCREMENT,
  `WRCODE` varchar(2) NOT NULL,
  `TOTALBELI` decimal(10,0) DEFAULT NULL,
  `TGLBELI` timestamp NULL DEFAULT NULL,
  `POSTSTATUS` decimal(10,0) DEFAULT '0',
  `DOCNO` varchar(20) DEFAULT NULL,
  `REFDOCNO` varchar(20) DEFAULT NULL,
  `SUPPNO` varchar(5) DEFAULT NULL,
  `VANREGISTER` varchar(10) DEFAULT NULL,
  `FAKTURNO` varchar(10) DEFAULT NULL,
  `RETLOCK` char(1) DEFAULT NULL,
  `NOTE` text,
  PRIMARY KEY (`IDBELI`),
  KEY `FK_T_HBELI_WAREHOU_S_M_WAREHOUSE` (`WRCODE`),
  KEY `SUPPNO` (`SUPPNO`),
  KEY `VANID` (`VANREGISTER`),
  CONSTRAINT `FK_T_HBELI_WAREHOU_S_M_WAREHOUSE` FOREIGN KEY (`WRCODE`) REFERENCES `m_warehouse` (`WRCODE`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;

/*Data for the table `t_hbeli` */

insert  into `t_hbeli`(`IDBELI`,`WRCODE`,`TOTALBELI`,`TGLBELI`,`POSTSTATUS`,`DOCNO`,`REFDOCNO`,`SUPPNO`,`VANREGISTER`,`FAKTURNO`,`RETLOCK`,`NOTE`) values (44,'01','29100000','2016-12-26 00:00:00','1','RCV.1612.001',NULL,'',NULL,NULL,NULL,NULL),(45,'01','20600000','2016-12-26 00:00:00','1','RCV.1612.002',NULL,'101',NULL,NULL,NULL,NULL),(46,'01','42300000','2016-12-26 00:00:00','1','RCV.1612.003',NULL,'101',NULL,NULL,'1',NULL),(47,'01','100000','2016-12-26 00:00:00','1','RCV.1612.004',NULL,'',NULL,NULL,NULL,'tes'),(48,'01','200000','2017-01-18 01:23:24','1','RCV.1701.001',NULL,'',NULL,NULL,NULL,''),(49,'01','300000','2017-01-18 01:24:51','1','RCV.1701.002',NULL,'',NULL,NULL,NULL,''),(50,'02','300000','2017-01-18 01:28:53','1','RCV.1701.003',NULL,'',NULL,NULL,NULL,''),(52,'01','200000','2017-01-18 01:47:19','1','RCV.1701.005',NULL,'',NULL,NULL,NULL,''),(56,'02','100000','2017-01-18 01:52:24','1','RCV.1701.009',NULL,'',NULL,NULL,NULL,''),(57,'01','200000','2017-01-18 21:44:22','1','RCV.1701.010',NULL,'',NULL,NULL,NULL,''),(58,'01','100000','2017-01-18 21:45:25','1','RCV.1701.011',NULL,'',NULL,NULL,NULL,''),(62,'02','300000','2017-01-22 00:22:09','1','RCV.1701.012','RCV.1612.004','',NULL,NULL,NULL,''),(63,'02','500000','2017-01-22 09:14:01','1','RCV.1701.013','','',NULL,NULL,NULL,'');

/*Table structure for table `t_horder` */

DROP TABLE IF EXISTS `t_horder`;

CREATE TABLE `t_horder` (
  `ORDERNO` varchar(15) NOT NULL,
  `WRCODE` varchar(2) DEFAULT NULL,
  `VANCODE` varchar(2) DEFAULT NULL,
  `USCODE` varchar(25) DEFAULT NULL,
  `SLSNO` varchar(5) DEFAULT NULL,
  `OUTNO` varchar(5) DEFAULT NULL,
  `CDISC` varchar(10) DEFAULT NULL,
  `TTYPECODE` char(1) DEFAULT NULL,
  `OSTATUSCODE` char(1) DEFAULT NULL,
  `DOUTNO` varchar(5) DEFAULT NULL,
  `ORDERDATE` timestamp NULL DEFAULT NULL,
  `DUEDATE` timestamp NULL DEFAULT NULL,
  `SUBTOTAL` decimal(10,0) DEFAULT NULL,
  `TOTDISC` decimal(10,0) DEFAULT NULL,
  `PPN` decimal(10,0) DEFAULT NULL,
  `INVNO` varchar(15) DEFAULT NULL,
  `PRINTINV` char(1) DEFAULT '0',
  `RETLOCK` char(1) DEFAULT NULL,
  `PRINTNUM` decimal(1,0) DEFAULT NULL,
  PRIMARY KEY (`ORDERNO`),
  UNIQUE KEY `T_HORDER_PK` (`ORDERNO`),
  KEY `TRANSACTION_TYPE_FK` (`TTYPECODE`),
  KEY `ORDER_STATUS_FK` (`OSTATUSCODE`),
  KEY `USER_ORDER_FK` (`USCODE`),
  KEY `INVOICE_DISCOUNT_FK` (`CDISC`),
  KEY `INVOICE_OUTLET_FK` (`OUTNO`),
  KEY `ORDER_STOCK_FK` (`WRCODE`),
  KEY `SALES_ORDER_FK` (`SLSNO`),
  KEY `VANCODE` (`VANCODE`),
  CONSTRAINT `FK_T_HORDER_INVOICE_D_M_DISCOU` FOREIGN KEY (`CDISC`) REFERENCES `m_discount` (`CDISC`),
  CONSTRAINT `FK_T_HORDER_ORDER_STA_M_OSTATU` FOREIGN KEY (`OSTATUSCODE`) REFERENCES `m_ostatus` (`OSTATUSCODE`),
  CONSTRAINT `FK_T_HORDER_ORDER_STO_M_WAREHO` FOREIGN KEY (`WRCODE`) REFERENCES `m_warehouse` (`WRCODE`),
  CONSTRAINT `FK_T_HORDER_SALES_ORD_M_SALES` FOREIGN KEY (`SLSNO`) REFERENCES `m_sales` (`SLSNO`),
  CONSTRAINT `FK_T_HORDER_TRANSACTI_T_TRANST` FOREIGN KEY (`TTYPECODE`) REFERENCES `t_transtype` (`TTYPECODE`),
  CONSTRAINT `FK_T_HORDER_USER_ORDE_M_USER` FOREIGN KEY (`USCODE`) REFERENCES `m_user` (`USCODE`),
  CONSTRAINT `t_horder_ibfk_1` FOREIGN KEY (`VANCODE`) REFERENCES `m_van` (`VANCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_horder` */

insert  into `t_horder`(`ORDERNO`,`WRCODE`,`VANCODE`,`USCODE`,`SLSNO`,`OUTNO`,`CDISC`,`TTYPECODE`,`OSTATUSCODE`,`DOUTNO`,`ORDERDATE`,`DUEDATE`,`SUBTOTAL`,`TOTDISC`,`PPN`,`INVNO`,`PRINTINV`,`RETLOCK`,`PRINTNUM`) values ('OR.1612.001','02',NULL,NULL,NULL,'',NULL,NULL,'3',NULL,'2016-12-28 00:00:00',NULL,'6390000',NULL,NULL,'INV.1612.003','1',NULL,'5'),('OR.1701.001','02',NULL,NULL,NULL,'',NULL,NULL,'3',NULL,'2017-01-05 00:00:00',NULL,'2600000',NULL,NULL,'INV.1701.001','1',NULL,'1'),('OR.1701.002','02',NULL,NULL,NULL,'101',NULL,NULL,'3',NULL,'2017-01-08 00:00:00',NULL,'300000',NULL,NULL,'INV.1701.002','1',NULL,NULL),('OR.1701.003','02',NULL,NULL,NULL,'',NULL,NULL,'3',NULL,'2017-01-08 00:00:00',NULL,'0',NULL,NULL,'INV.1701.003','1',NULL,NULL),('OR.1701.004','02',NULL,NULL,NULL,'101',NULL,NULL,'3',NULL,'2017-01-08 00:00:00',NULL,'360000',NULL,NULL,'INV.1701.004','1','1','1'),('OR.1701.005','02',NULL,NULL,NULL,'102',NULL,NULL,'3',NULL,'2017-01-17 00:00:00',NULL,'4911500',NULL,NULL,'INV.1701.005','1','1',NULL),('OR.1701.006','02',NULL,NULL,NULL,'102',NULL,NULL,'3',NULL,'2017-01-17 00:00:00',NULL,'1254000',NULL,NULL,'INV.1701.006','1','1','9'),('OR.1701.007','02',NULL,NULL,NULL,'',NULL,NULL,'3',NULL,'2017-01-18 21:17:53',NULL,'110000',NULL,NULL,'INV.1701.007','1',NULL,NULL),('OR.1701.008','02',NULL,NULL,NULL,'',NULL,NULL,'3',NULL,'2017-01-22 09:11:17',NULL,'220000',NULL,NULL,'INV.1701.008','1',NULL,NULL);

/*Table structure for table `t_hvan` */

DROP TABLE IF EXISTS `t_hvan`;

CREATE TABLE `t_hvan` (
  `IDTVAN` int(11) NOT NULL AUTO_INCREMENT,
  `VANCODE` varchar(2) DEFAULT NULL,
  `WRCODE` varchar(2) DEFAULT NULL,
  `SLSNO` varchar(5) DEFAULT NULL,
  `OSTATUSCODE` char(1) DEFAULT '2',
  `DAYIN` timestamp NULL DEFAULT NULL,
  `DAYOUT` timestamp NULL DEFAULT NULL,
  `SUBTOTAL` decimal(20,0) DEFAULT NULL,
  PRIMARY KEY (`IDTVAN`),
  KEY `FK_T_VAN_TRANS_VAN_M_WAREHO` (`WRCODE`),
  CONSTRAINT `FK_T_VAN_TRANS_VAN_M_WAREHO` FOREIGN KEY (`WRCODE`) REFERENCES `m_warehouse` (`WRCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_hvan` */

/*Table structure for table `t_invoice` */

DROP TABLE IF EXISTS `t_invoice`;

CREATE TABLE `t_invoice` (
  `INVNO` varchar(15) NOT NULL,
  `INVDATE` date DEFAULT NULL,
  PRIMARY KEY (`INVNO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_invoice` */

insert  into `t_invoice`(`INVNO`,`INVDATE`) values ('INV.0000.001','0000-00-00'),('INV.0000.002','0000-00-00'),('INV.1612.001','2016-12-28'),('INV.1612.002','2016-12-28'),('INV.1612.003','2016-12-28'),('INV.1701.001','2017-01-05'),('INV.1701.002','2017-01-08'),('INV.1701.004','2017-01-08'),('INV.1701.005','2017-01-17'),('INV.1701.006','2017-01-17'),('INV.1701.007','2017-01-18'),('INV.1701.008','2017-01-22');

/*Table structure for table `t_outlet_promo` */

DROP TABLE IF EXISTS `t_outlet_promo`;

CREATE TABLE `t_outlet_promo` (
  `ATTCODE` varchar(2) NOT NULL,
  `IDPROMO` int(11) NOT NULL,
  PRIMARY KEY (`ATTCODE`,`IDPROMO`),
  KEY `IDPROMO` (`IDPROMO`),
  CONSTRAINT `t_outlet_promo_ibfk_1` FOREIGN KEY (`ATTCODE`) REFERENCES `m_outlet_attribute` (`ATTCODE`) ON UPDATE CASCADE,
  CONSTRAINT `t_outlet_promo_ibfk_2` FOREIGN KEY (`IDPROMO`) REFERENCES `m_promo` (`IDPROMO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_outlet_promo` */

insert  into `t_outlet_promo`(`ATTCODE`,`IDPROMO`) values ('P',1),('G',2),('R',3);

/*Table structure for table `t_payment` */

DROP TABLE IF EXISTS `t_payment`;

CREATE TABLE `t_payment` (
  `IDPAYMENT` int(11) NOT NULL AUTO_INCREMENT,
  `PAYNO` varchar(15) DEFAULT NULL,
  `INVNO` varchar(15) DEFAULT NULL,
  `PAYDATE` date DEFAULT NULL,
  `AMOUNT` decimal(20,0) DEFAULT NULL,
  PRIMARY KEY (`IDPAYMENT`),
  KEY `INVNO` (`INVNO`),
  CONSTRAINT `t_payment_ibfk_1` FOREIGN KEY (`INVNO`) REFERENCES `t_invoice` (`INVNO`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

/*Data for the table `t_payment` */

insert  into `t_payment`(`IDPAYMENT`,`PAYNO`,`INVNO`,`PAYDATE`,`AMOUNT`) values (24,'PY.1612.001','INV.1612.001','2016-12-28','1000'),(25,'PY.1612.002','INV.1612.002','2016-12-28','1000'),(26,'PY.1612.003','INV.1612.001','2016-12-28','147689000'),(27,'PY.1612.004','INV.1612.002','2016-12-28','13169000'),(28,'PY.1612.005','INV.1612.003','2016-12-28','1000'),(29,'PY.1701.001','INV.1612.003','2017-01-05','6389000'),(30,'PY.1701.002','INV.1701.001','2017-01-05','2600000'),(31,'PY.1701.003','INV.1701.002','2017-01-08','300000'),(33,'PY.1701.005','INV.1701.004','2017-01-08','360000'),(34,'PY.1701.006','INV.1701.005','2017-01-17','4911500'),(35,'PY.1701.007','INV.1701.006','2017-01-17','1000000'),(36,'PY.1701.008','INV.1701.006','2017-01-17','254000'),(37,'PY.1701.009','INV.1701.007','2017-01-18','110000'),(38,'PY.1701.010','INV.1701.008','2017-01-22','220000');

/*Table structure for table `t_price` */

DROP TABLE IF EXISTS `t_price`;

CREATE TABLE `t_price` (
  `IDPRICE` int(11) NOT NULL AUTO_INCREMENT,
  `PCODE` varchar(11) DEFAULT NULL,
  `DATEPRICE` datetime DEFAULT NULL,
  `RECPRICE` decimal(10,0) DEFAULT NULL,
  `SELLPRICE` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`IDPRICE`),
  KEY `PCODE` (`PCODE`),
  CONSTRAINT `t_price_ibfk_1` FOREIGN KEY (`PCODE`) REFERENCES `m_product` (`PCODE`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

/*Data for the table `t_price` */

insert  into `t_price`(`IDPRICE`,`PCODE`,`DATEPRICE`,`RECPRICE`,`SELLPRICE`) values (1,'AR01','2017-01-17 19:48:17','100000','120'),(2,'AR01','2017-01-17 19:48:28','100000','120000'),(3,'BK02','2017-01-17 19:49:14','100000','120'),(4,'BK02','2017-01-17 19:49:24','100000','120000'),(5,'AB01','2017-01-17 19:49:55','100000','0'),(6,'AB02','2017-01-17 19:49:55','100000','0'),(7,'AR01','2017-01-17 19:49:55','100000','0'),(8,'AR02','2017-01-17 19:49:55','100000','0'),(9,'BK01','2017-01-17 19:49:55','100000','0'),(10,'BK02','2017-01-17 19:49:55','100000','0'),(11,'AB01','2017-01-17 19:50:30','100000','110000'),(12,'AB02','2017-01-17 19:50:30','100000','110000'),(13,'AR01','2017-01-17 19:50:30','100000','110000'),(14,'AR02','2017-01-17 19:50:30','100000','110000'),(15,'BK01','2017-01-17 19:50:30','100000','110000'),(16,'BK02','2017-01-17 19:50:30','100000','110000'),(17,'CD01','2017-01-17 19:50:30','100000','110000'),(18,'CD02','2017-01-17 19:50:30','100000','110000'),(19,'EB01','2017-01-17 19:50:30','100000','110000'),(20,'EB02','2017-01-17 19:50:30','100000','110000'),(21,'JT01','2017-01-17 19:50:30','100000','110000'),(22,'JT02','2017-01-17 19:50:30','100000','110000'),(23,'KL01','2017-01-17 19:50:30','100000','110000'),(24,'KL02','2017-01-17 19:50:30','100000','110000'),(25,'KM01','2017-01-17 19:50:30','100000','110000'),(26,'KM02','2017-01-17 19:50:30','100000','110000'),(27,'MB01','2017-01-17 19:50:30','100000','110000'),(28,'MB02','2017-01-17 19:50:30','100000','110000'),(29,'MH01','2017-01-17 19:50:30','100000','110000'),(30,'MH02','2017-01-17 19:50:30','100000','110000'),(31,'MR01','2017-01-17 19:50:30','100000','110000'),(32,'MR02','2017-01-17 19:50:30','100000','110000'),(33,'PN01','2017-01-17 19:50:30','100000','110000'),(34,'PN02','2017-01-17 19:50:30','100000','110000'),(35,'SK01','2017-01-17 19:50:30','100000','110000'),(36,'SK02','2017-01-17 19:50:30','100000','110000'),(37,'SN01','2017-01-17 19:50:30','100000','110000'),(38,'SN02','2017-01-17 19:50:30','100000','110000'),(39,'TR01','2017-01-17 19:50:30','100000','110000'),(40,'TR02','2017-01-17 19:50:30','100000','110000'),(41,'UL01','2017-01-17 19:50:30','100000','110000'),(42,'UL02','2017-01-17 19:50:30','100000','110000'),(43,'AB01','2017-01-17 20:51:12','100000','120'),(44,'AB01','2017-01-17 20:51:18','100000','120000'),(45,'AB01','2017-01-17 20:54:01','100000','120001'),(46,'AB01','2017-01-17 20:54:18','100000','120000');

/*Table structure for table `t_recap` */

DROP TABLE IF EXISTS `t_recap`;

CREATE TABLE `t_recap` (
  `RECAPCODE` varchar(8) NOT NULL,
  `RECAPDETAIL` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`RECAPCODE`),
  UNIQUE KEY `T_RECAP_PK` (`RECAPCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_recap` */

/*Table structure for table `t_retorder` */

DROP TABLE IF EXISTS `t_retorder`;

CREATE TABLE `t_retorder` (
  `DORDERNO` int(11) DEFAULT NULL,
  `RETTYPE` char(1) DEFAULT NULL,
  `RETDATE` date DEFAULT NULL,
  `QTYRET` decimal(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_retorder` */

/*Table structure for table `t_retsupp` */

DROP TABLE IF EXISTS `t_retsupp`;

CREATE TABLE `t_retsupp` (
  `IDDBELI` int(11) DEFAULT NULL,
  `QTYRET` decimal(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_retsupp` */

/*Table structure for table `t_stock` */

DROP TABLE IF EXISTS `t_stock`;

CREATE TABLE `t_stock` (
  `STOCKDATE` date NOT NULL,
  `PCODE` varchar(10) NOT NULL,
  `WRCODE` varchar(2) NOT NULL,
  `QTYSTART` decimal(10,0) DEFAULT NULL,
  `QTYIN` decimal(10,0) DEFAULT NULL,
  `QTYOUT` decimal(10,0) DEFAULT NULL,
  `QTYSTOCK` decimal(10,0) DEFAULT NULL,
  `SALDOSTART` decimal(10,0) DEFAULT NULL,
  `DEBIT` decimal(10,0) DEFAULT NULL,
  `KREDIT` decimal(10,0) DEFAULT NULL,
  `SALDOEND` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`STOCKDATE`,`PCODE`,`WRCODE`),
  KEY `FK_T_STOCK_PCODE_M_PRODUCT` (`PCODE`),
  KEY `FK_T_STOCK_WRCODE_M_WAREHOUSE` (`WRCODE`),
  CONSTRAINT `FK_T_STOCK_PCODE_M_PRODUCT` FOREIGN KEY (`PCODE`) REFERENCES `m_product` (`PCODE`),
  CONSTRAINT `FK_T_STOCK_WRCODE_M_WAREHOUSE` FOREIGN KEY (`WRCODE`) REFERENCES `m_warehouse` (`WRCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_stock` */

insert  into `t_stock`(`STOCKDATE`,`PCODE`,`WRCODE`,`QTYSTART`,`QTYIN`,`QTYOUT`,`QTYSTOCK`,`SALDOSTART`,`DEBIT`,`KREDIT`,`SALDOEND`) values ('2017-01-18','AB01','01','10','72','23','59','1000000','0','7200000','-6200000'),('2017-01-18','AB02','02','10','5','1','14','1000000','110000','105500','1004500'),('2017-01-18','AR01','01','10','128','48','90','1000000','1500000','12800000','-10300000'),('2017-01-18','AR02','02','10','0','947','-937','1000000','94700000','710000','94990000'),('2017-01-18','BK01','01','10','31','11','30','1000000','1100000','3100000','-1000000'),('2017-01-18','BK02','02','10','0','63','-53','1000000','6300000','630000','6670000'),('2017-01-18','CD01','01','10','0','0','10','1000000','0','0','1000000'),('2017-01-18','CD02','02','10','0','0','10','1000000','0','0','1000000'),('2017-01-18','EB01','01','10','0','0','10','1000000','0','0','1000000'),('2017-01-18','EB02','02','10','0','0','10','1000000','0','0','1000000'),('2017-01-18','JT01','01','10','0','0','10','1000000','0','0','1000000'),('2017-01-18','JT02','02','10','0','0','10','1000000','0','0','1000000'),('2017-01-18','KL01','01','10','77','0','87','1000000','0','7700000','-6700000'),('2017-01-18','KL02','02','10','1','78','-67','1000000','7700000','209000','8491000'),('2017-01-18','KM01','01','10','0','0','10','1000000','0','0','1000000'),('2017-01-18','KM02','02','10','0','1','9','1000000','110000','5500','1104500'),('2017-01-18','MB01','01','10','0','0','10','1000000','0','0','1000000'),('2017-01-18','MB02','02','10','0','0','10','1000000','0','0','1000000'),('2017-01-18','MH01','01','10','417','4','423','1000000','400000','41700000','-40300000'),('2017-01-18','MH02','02','10','1','15','-4','1000000','1400000','209000','2191000'),('2017-01-18','MR01','01','10','0','0','10','1000000','0','0','1000000'),('2017-01-18','MR02','02','10',NULL,NULL,'10','1000000',NULL,NULL,'1000000'),('2017-01-18','PN01','01','10','0','0','10','1000000','0','0','1000000'),('2017-01-18','PN02','02','10',NULL,NULL,'10','1000000',NULL,NULL,'1000000'),('2017-01-18','SK01','01','10','0','0','10','1000000','0','0','1000000'),('2017-01-18','SK02','02','10',NULL,NULL,'10','1000000',NULL,NULL,'1000000'),('2017-01-18','SN01','01','10','0','0','10','1000000','0','0','1000000'),('2017-01-18','SN02','02','10',NULL,NULL,'10','1000000',NULL,NULL,'1000000'),('2017-01-18','TR01','01','10','0','0','10','1000000','0','0','1000000'),('2017-01-18','TR02','02','10',NULL,NULL,'10','1000000',NULL,NULL,'1000000'),('2017-01-18','UL01','01','10','0','0','10','1000000','0','0','1000000'),('2017-01-18','UL02','02','10',NULL,NULL,'10','1000000',NULL,NULL,'1000000'),('2017-01-19','AB01','01','59','21','10','70','-6200000','2000000','1100000','-5300000'),('2017-01-19','AB02','02','14','0','0','14','1004500','0','0','1004500'),('2017-01-19','AR01','01','90','0','0','90','-10300000','0','0','-10300000'),('2017-01-19','AR02','02','-937','947','0','10','94990000','94700000','0','189690000'),('2017-01-19','BK01','01','30','0','0','30','-1000000','0','0','-1000000'),('2017-01-19','BK02','02','-53','63','0','10','6670000','6300000','0','12970000'),('2017-01-19','CD01','01','10','0','0','10','1000000','0','0','1000000'),('2017-01-19','CD02','02','10','0','0','10','1000000','0','0','1000000'),('2017-01-19','EB01','01','10','0','0','10','1000000','0','0','1000000'),('2017-01-19','EB02','02','10','0','0','10','1000000','0','0','1000000'),('2017-01-19','JT01','01','10','0','0','10','1000000','0','0','1000000'),('2017-01-19','JT02','02','10','0','0','10','1000000','0','0','1000000'),('2017-01-19','KL01','01','87','1','0','88','-6700000','0','0','-6700000'),('2017-01-19','KL02','02','-67','77','0','10','8491000','7600000','0','16091000'),('2017-01-19','KM01','01','10','0','0','10','1000000','0','0','1000000'),('2017-01-19','KM02','02','9','0','0','9','1104500','0','0','1104500'),('2017-01-19','MB01','01','10','0','0','10','1000000','0','0','1000000'),('2017-01-19','MB02','02','10','0','0','10','1000000','0','0','1000000'),('2017-01-19','MH01','01','423','0','0','423','-40300000','0','0','-40300000'),('2017-01-19','MH02','02','-4','14','0','10','2191000','1400000','0','3591000'),('2017-01-19','MR01','01','10',NULL,NULL,'10','1000000',NULL,NULL,'1000000'),('2017-01-19','MR02','02','10',NULL,NULL,'10','1000000',NULL,NULL,'1000000'),('2017-01-19','PN01','01','10',NULL,NULL,'10','1000000',NULL,NULL,'1000000'),('2017-01-19','PN02','02','10',NULL,NULL,'10','1000000',NULL,NULL,'1000000'),('2017-01-19','SK01','01','10',NULL,NULL,'10','1000000',NULL,NULL,'1000000'),('2017-01-19','SK02','02','10',NULL,NULL,'10','1000000',NULL,NULL,'1000000'),('2017-01-19','SN01','01','10',NULL,NULL,'10','1000000',NULL,NULL,'1000000'),('2017-01-19','SN02','02','10',NULL,NULL,'10','1000000',NULL,NULL,'1000000'),('2017-01-19','TR01','01','10',NULL,NULL,'10','1000000',NULL,NULL,'1000000'),('2017-01-19','TR02','02','10',NULL,NULL,'10','1000000',NULL,NULL,'1000000'),('2017-01-19','UL01','01','10',NULL,NULL,'10','1000000',NULL,NULL,'1000000'),('2017-01-19','UL02','02','10',NULL,NULL,'10','1000000',NULL,NULL,'1000000'),('2017-01-20','AB01','01','70',NULL,NULL,NULL,'-5300000',NULL,NULL,NULL),('2017-01-20','AB02','02','14',NULL,NULL,NULL,'1004500',NULL,NULL,NULL),('2017-01-20','AR01','01','90',NULL,NULL,NULL,'-10300000',NULL,NULL,NULL),('2017-01-20','AR02','02','10',NULL,NULL,NULL,'189690000',NULL,NULL,NULL),('2017-01-20','BK01','01','30',NULL,NULL,NULL,'-1000000',NULL,NULL,NULL),('2017-01-20','BK02','02','10',NULL,NULL,NULL,'12970000',NULL,NULL,NULL),('2017-01-20','CD01','01','10',NULL,NULL,NULL,'1000000',NULL,NULL,NULL),('2017-01-20','CD02','02','10',NULL,NULL,NULL,'1000000',NULL,NULL,NULL),('2017-01-20','EB01','01','10',NULL,NULL,NULL,'1000000',NULL,NULL,NULL),('2017-01-20','EB02','02','10',NULL,NULL,NULL,'1000000',NULL,NULL,NULL),('2017-01-20','JT01','01','10',NULL,NULL,NULL,'1000000',NULL,NULL,NULL),('2017-01-20','JT02','02','10',NULL,NULL,NULL,'1000000',NULL,NULL,NULL),('2017-01-20','KL01','01','88',NULL,NULL,NULL,'-6700000',NULL,NULL,NULL),('2017-01-20','KL02','02','10',NULL,NULL,NULL,'16091000',NULL,NULL,NULL),('2017-01-20','KM01','01','10',NULL,NULL,NULL,'1000000',NULL,NULL,NULL),('2017-01-20','KM02','02','9',NULL,NULL,NULL,'1104500',NULL,NULL,NULL),('2017-01-20','MB01','01','10',NULL,NULL,NULL,'1000000',NULL,NULL,NULL),('2017-01-20','MB02','02','10',NULL,NULL,NULL,'1000000',NULL,NULL,NULL),('2017-01-20','MH01','01','423',NULL,NULL,NULL,'-40300000',NULL,NULL,NULL),('2017-01-20','MH02','02','10',NULL,NULL,NULL,'3591000',NULL,NULL,NULL),('2017-01-20','MR01','01','10',NULL,NULL,NULL,'1000000',NULL,NULL,NULL),('2017-01-20','MR02','02','10',NULL,NULL,NULL,'1000000',NULL,NULL,NULL),('2017-01-20','PN01','01','10',NULL,NULL,NULL,'1000000',NULL,NULL,NULL),('2017-01-20','PN02','02','10',NULL,NULL,NULL,'1000000',NULL,NULL,NULL),('2017-01-20','SK01','01','10',NULL,NULL,NULL,'1000000',NULL,NULL,NULL),('2017-01-20','SK02','02','10',NULL,NULL,NULL,'1000000',NULL,NULL,NULL),('2017-01-20','SN01','01','10',NULL,NULL,NULL,'1000000',NULL,NULL,NULL),('2017-01-20','SN02','02','10',NULL,NULL,NULL,'1000000',NULL,NULL,NULL),('2017-01-20','TR01','01','10',NULL,NULL,NULL,'1000000',NULL,NULL,NULL),('2017-01-20','TR02','02','10',NULL,NULL,NULL,'1000000',NULL,NULL,NULL),('2017-01-20','UL01','01','10',NULL,NULL,NULL,'1000000',NULL,NULL,NULL),('2017-01-20','UL02','02','10',NULL,NULL,NULL,'1000000',NULL,NULL,NULL);

/*Table structure for table `t_stockmove` */

DROP TABLE IF EXISTS `t_stockmove`;

CREATE TABLE `t_stockmove` (
  `IDSTMV` int(11) NOT NULL AUTO_INCREMENT,
  `STMVCODE` char(2) DEFAULT NULL COMMENT 'I: Opname In; O:Opname Out; S:Sales; R:Receiving; D:Discount; T:Return Supplier; P:Return Penjualan; M:Mutasi',
  `TRANSDATE` timestamp NULL DEFAULT NULL,
  `WRCODE` varchar(2) DEFAULT NULL,
  `PCODE` varchar(10) DEFAULT NULL,
  `STOCKNUM` decimal(10,0) DEFAULT NULL,
  `BALANCE` decimal(17,0) DEFAULT NULL,
  PRIMARY KEY (`IDSTMV`),
  KEY `WAREHOUSE_MOVE_FK` (`WRCODE`),
  KEY `PCODE` (`PCODE`),
  CONSTRAINT `FK_T_STOCKM_WAREHOUSE_M_WAREHO` FOREIGN KEY (`WRCODE`) REFERENCES `m_warehouse` (`WRCODE`),
  CONSTRAINT `t_stockmove_ibfk_1` FOREIGN KEY (`PCODE`) REFERENCES `m_product` (`PCODE`)
) ENGINE=InnoDB AUTO_INCREMENT=210 DEFAULT CHARSET=latin1;

/*Data for the table `t_stockmove` */

insert  into `t_stockmove`(`IDSTMV`,`STMVCODE`,`TRANSDATE`,`WRCODE`,`PCODE`,`STOCKNUM`,`BALANCE`) values (72,'R','2017-01-18 20:41:19','01','AB01','72','-7200000'),(73,'R','2017-01-18 20:41:19','01','AR01','56','-5600000'),(74,'R','2017-01-18 20:41:19','01','KL01','76','-7600000'),(75,'R','2017-01-18 20:41:19','01','MH01','87','-8700000'),(76,'R','2017-01-18 20:41:19','01','BK01','31','-3100000'),(77,'R','2017-01-18 20:41:19','01','MH01','76','-7600000'),(78,'R','2017-01-18 20:41:19','01','PN01','99','-9900000'),(79,'R','2017-01-18 20:41:19','01','AR01','71','-7100000'),(80,'R','2017-01-18 20:41:19','01','MH01','254','-25400000'),(81,'R','2017-01-18 20:41:19','01','TR01','98','-9800000'),(82,'R','2017-01-18 20:41:19','01','AR01','1','-100000'),(106,'S','2017-01-18 20:41:19','02','AR02','-876','87600000'),(107,'S','2017-01-18 20:41:19','02','TR02','-765','76500000'),(108,'S','2017-01-18 20:41:19','02','BK02','-63','6300000'),(109,'D','2017-01-18 20:41:19','02','BK02','0','-630000'),(110,'S','2017-01-18 20:41:19','02','KL02','-75','7500000'),(111,'S','2017-01-18 20:41:19','02','AR02','-71','7100000'),(112,'D','2017-01-18 20:41:19','02','AR02','0','-710000'),(113,'S','2017-01-18 20:41:19','02','MH02','-12','1200000'),(114,'S','2017-01-18 20:41:19','02','TR02','-14','1400000'),(115,'S','2017-01-18 20:41:19','02','KL02','-1','100000'),(116,'S','2017-01-18 20:41:19','02','MH02','-1','100000'),(117,'S','2017-01-18 20:41:19','02','SN02','-1','100000'),(118,'S','2017-01-18 20:41:19','02','KL02','-1','100000'),(119,'D','2017-01-18 20:41:19','02','KL02','0','-10000'),(120,'S','2017-01-18 20:41:19','02','MH02','-1','100000'),(121,'D','2017-01-18 20:41:19','02','MH02','0','-10000'),(122,'S','2017-01-18 20:41:19','02','SN02','-1','100000'),(123,'D','2017-01-18 20:41:19','02','SN02','0','-10000'),(124,'S','2017-01-18 20:41:19','02','TR02','-1','100000'),(125,'D','2017-01-18 20:41:19','02','TR02','0','-10000'),(126,'S','2017-01-18 20:41:19','02','AB02','-1','110000'),(127,'D','2017-01-18 20:41:19','02','AB02','0','-5500'),(128,'S','2017-01-18 20:41:19','02','KM02','-1','110000'),(129,'D','2017-01-18 20:41:19','02','KM02','0','-5500'),(130,'S','2017-01-18 20:41:19','02','MR02','-45','4950000'),(131,'D','2017-01-18 20:41:19','02','MR02','0','-247500'),(132,'S','2017-01-18 20:41:19','02','SN02','-5','550000'),(133,'D','2017-01-18 20:41:19','02','SN02','0','-27500'),(134,'S','2017-01-18 20:41:19','02','TR02','-7','770000'),(135,'D','2017-01-18 20:41:19','02','TR02','0','-38500'),(151,'T','2017-01-18 20:41:19','01','AR01','-5','500000'),(152,'T','2017-01-18 20:41:19','01','MH01','-4','400000'),(153,'T','2017-01-18 20:41:19','01','TR01','-8','800000'),(154,'P','2017-01-18 20:41:19','02','SN02','2','-209000'),(155,'P','2017-01-18 20:41:19','02','TR02','-2','-209000'),(158,'P','2017-01-18 20:41:19','02','MR02','5','-522500'),(159,'P','2017-01-18 20:41:19','02','KL02','-1','-99000'),(160,'P','2017-01-18 20:41:19','02','MH02','-1','-99000'),(161,'P','2017-01-18 20:41:19','02','SN02','-1','-99000'),(162,'P','2017-01-18 20:41:19','02','TR02','-1','-99000'),(163,'R','2017-01-18 20:41:19','01','KL02','1','-100000'),(164,'R','2017-01-18 20:41:19','01','SN02','1','-100000'),(165,'R','2017-01-18 20:41:19','01','KL01','1','-100000'),(166,'R','2017-01-18 20:41:19','01','SN01','1','-100000'),(167,'R','2017-01-18 20:41:19','01','TR01','1','-100000'),(168,'R','2017-01-18 20:41:19','02','AB02','1','-100000'),(169,'R','2017-01-18 20:41:19','02','KL02','1','-100000'),(170,'R','2017-01-18 20:41:19','02','MH02','1','-100000'),(177,'R','2017-01-18 20:41:19','02','AB02','1','0'),(178,'R','2017-01-18 20:41:19','02','AB02','1','0'),(179,'R','2017-01-18 20:41:19','02','AB02','1','0'),(180,'R','2017-01-18 20:41:19','02','AB02','1','0'),(181,'M','2017-01-18 20:41:19','01','AB01','-1','0'),(182,'S','2017-01-18 00:00:00','02','TR02','-1','110000'),(183,'R','2017-01-19 00:00:00','01','KL01','1','0'),(184,'R','2017-01-19 00:00:00','01','SN01','1','0'),(185,'R','2017-01-19 00:00:00','01','AB01','1','-100000'),(190,'R','2017-01-19 00:00:00','02','KL02','1','0'),(191,'R','2017-01-19 00:00:00','02','SN02','1','0'),(192,'R','2017-01-19 00:00:00','02','TR02','1','0'),(193,'O','2017-01-18 00:00:00','01','AB01','-12','0'),(194,'O','2017-01-18 00:00:00','01','AB01','-10','0'),(195,'O','2017-01-18 00:00:00','01','AR01','-33','0'),(196,'O','2017-01-18 00:00:00','01','AR01','-10','1000000'),(197,'O','2017-01-18 00:00:00','01','BK01','-1','100000'),(198,'O','2017-01-18 00:00:00','01','BK01','-10','1000000'),(199,'I','2017-01-19 00:00:00','01','AB01','-10','-1000000'),(200,'I','2017-01-19 00:00:00','01','AB01','20','2000000'),(201,'I','2017-01-19 00:00:00','02','AR02','947','94700000'),(202,'I','2017-01-19 00:00:00','02','BK02','63','6300000'),(203,'I','2017-01-19 00:00:00','02','KL02','76','7600000'),(204,'I','2017-01-19 00:00:00','02','MH02','14','1400000'),(205,'S','2017-01-20 00:00:00','02','AB02','-2','220000'),(206,'O','2017-01-20 00:00:00','02','KM02','-8','800000'),(207,'I','2017-01-20 00:00:00','02','KM02','9','900000'),(208,'R','2017-01-21 00:00:00','02','AR02','5','-100000'),(209,'O','2017-01-22 00:00:00','01','MH01','-23','2300000');

/*Table structure for table `t_transtype` */

DROP TABLE IF EXISTS `t_transtype`;

CREATE TABLE `t_transtype` (
  `TTYPECODE` char(1) NOT NULL,
  `TRANSNAME` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`TTYPECODE`),
  UNIQUE KEY `T_TRANSTYPE_PK` (`TTYPECODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_transtype` */

/*Table structure for table `t_van` */

DROP TABLE IF EXISTS `t_van`;

CREATE TABLE `t_van` (
  `IDSVAN` int(11) NOT NULL AUTO_INCREMENT,
  `VANCODE` varchar(2) DEFAULT NULL,
  `PCODE` varchar(5) DEFAULT NULL,
  `DAYIN` timestamp NULL DEFAULT NULL,
  `DAYOUT` timestamp NULL DEFAULT NULL,
  `QTYSTOCK` decimal(20,0) DEFAULT NULL,
  `TOTALBAYAR` decimal(20,0) DEFAULT NULL,
  `DAYBEG` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`IDSVAN`),
  KEY `VANID` (`VANCODE`),
  KEY `PCODE` (`PCODE`),
  CONSTRAINT `t_van_ibfk_1` FOREIGN KEY (`VANCODE`) REFERENCES `m_van` (`VANCODE`),
  CONSTRAINT `t_van_ibfk_3` FOREIGN KEY (`PCODE`) REFERENCES `m_product` (`PCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_van` */

/* Trigger structure for table `m_cmenu` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `tib_cmenu` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `tib_cmenu` BEFORE INSERT ON `m_cmenu` FOR EACH ROW BEGIN
	SET @iterate = (SELECT COALESCE(MAX(CMNCODE), 0) FROM m_cmenu WHERE MNCODE = NEW.MNCODE);
	
	SET NEW.CMNCODE := @iterate + 1;
    END */$$


DELIMITER ;

/* Trigger structure for table `m_menu` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `tib_menu` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `tib_menu` BEFORE INSERT ON `m_menu` FOR EACH ROW BEGIN
	SET @iterate = (SELECT COALESCE(MAX(MNCODE), 0) FROM m_menu);
		
	SET NEW.MNCODE := @iterate + 100;
    END */$$


DELIMITER ;

/* Trigger structure for table `m_product` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `tgr_tuaproduct` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `tgr_tuaproduct` AFTER UPDATE ON `m_product` FOR EACH ROW BEGIN
	IF NEW.PPRICE1 <> OLD.PPRICE1 OR NEW.SPRICE1 <> OLD.SPRICE1
	THEN
		INSERT INTO t_price(PCODE, DATEPRICE, RECPRICE, SELLPRICE)
		VALUES(OLD.PCODE, now(), NEW.PPRICE1, NEW.SPRICE1);
	END IF;
    END */$$


DELIMITER ;

/* Trigger structure for table `t_hbeli` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `tib_hbelidocno` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `tib_hbelidocno` BEFORE INSERT ON `t_hbeli` FOR EACH ROW BEGIN
	SET @prefix = DATE_FORMAT(NEW.TGLBELI, '%y%m');
	SET @iterate = (SELECT COALESCE(MAX(SUBSTR(DOCNO, 10, 3)), 0) FROM t_hbeli WHERE SUBSTR(DOCNO, 5, 4) = @prefix);
	
	SET NEW.DOCNO := CONCAT('RCV.',@prefix,'.',LPAD(COALESCE(@iterate,0) + 1, 3, '0'));
    END */$$


DELIMITER ;

/* Trigger structure for table `t_horder` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `tib_horder` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `tib_horder` BEFORE INSERT ON `t_horder` FOR EACH ROW BEGIN
	SET @prefix = DATE_FORMAT(NEW.ORDERDATE, '%y%m');
	SET @iterate = (SELECT COALESCE(MAX(SUBSTR(ORDERNO, 9, 3)), 0) FROM t_horder WHERE SUBSTR(ORDERNO, 4, 4) = @prefix);
	
	SET NEW.ORDERNO := CONCAT('OR.',@prefix,'.',LPAD(@iterate + 1, 3, '0'));
    END */$$


DELIMITER ;

/* Trigger structure for table `t_invoice` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `tib_invoice` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `tib_invoice` BEFORE INSERT ON `t_invoice` FOR EACH ROW BEGIN
	SET @prefix = DATE_FORMAT(NEW.INVDATE, '%y%m');
	SET @iterate = (SELECT COALESCE(MAX(SUBSTR(INVNO, 10, 3)), 0) FROM t_invoice WHERE SUBSTR(INVNO, 5, 4) = @prefix);
	
	SET NEW.INVNO := CONCAT('INV.',@prefix,'.',LPAD(@iterate + 1, 3, '0'));
    END */$$


DELIMITER ;

/* Trigger structure for table `t_payment` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `tib_payment` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `tib_payment` BEFORE INSERT ON `t_payment` FOR EACH ROW BEGIN
	SET @prefix = DATE_FORMAT(NEW.PAYDATE, '%y%m');
	SET @iterate = (SELECT COALESCE(MAX(SUBSTR(PAYNO, 9, 3)), 0) FROM t_payment WHERE SUBSTR(PAYNO, 4, 4) = @prefix);
	
	SET NEW.PAYNO := CONCAT('PY.',@prefix,'.',LPAD(@iterate + 1, 3, '0'));
    END */$$


DELIMITER ;

/* Trigger structure for table `t_stockmove` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `tgr_tuastockmove` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `tgr_tuastockmove` BEFORE INSERT ON `t_stockmove` FOR EACH ROW BEGIN
	SET @setting_date = (SELECT VALUE FROM m_setting WHERE skey = 'WRDATE');
	
	set NEW.TRANSDATE := @setting_date;
    END */$$


DELIMITER ;

/* Procedure structure for procedure `f_totalbeli` */

/*!50003 DROP PROCEDURE IF EXISTS  `f_totalbeli` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `f_totalbeli`(IN id INT)
BEGIN
	SET @t_total = 0;
	
	SELECT SUM(JMLBELI) INTO @t_total 
	FROM t_dbeli
	WHERE IDBELI = id
	GROUP BY IDBELI;
	
	UPDATE t_hbeli SET TOTALBELI = @t_total WHERE IDBELI = id;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `f_totalorder` */

/*!50003 DROP PROCEDURE IF EXISTS  `f_totalorder` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `f_totalorder`(IN id INT)
BEGIN
	SET @t_total = 0;
	
	SELECT SUM(AMOUNT) INTO @t_total 
	FROM t_dorder
	WHERE ORDERNO = id
	GROUP BY ORDERNO;
	
	UPDATE t_horder SET SUBTOTAL = @t_total WHERE ORDERNO = id;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `get_last_custom_error` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_last_custom_error` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `get_last_custom_error`()
    DETERMINISTIC
    SQL SECURITY INVOKER
BEGIN
  SELECT @error_code, @error_message;
END */$$
DELIMITER ;

/* Procedure structure for procedure `raise_application_error` */

/*!50003 DROP PROCEDURE IF EXISTS  `raise_application_error` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`%` PROCEDURE `raise_application_error`(IN CODE INTEGER, IN MESSAGE VARCHAR(255))
    DETERMINISTIC
    SQL SECURITY INVOKER
BEGIN
  CREATE TEMPORARY TABLE IF NOT EXISTS RAISE_ERROR(F1 INT NOT NULL);
  SELECT CODE, MESSAGE INTO @error_code, @error_message;
  INSERT INTO RAISE_ERROR VALUES(NULL);
END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
