/*
 * stickyfloat - jQuery plugin for verticaly floating anything in a constrained area
 * 
 * Example: jQuery('#menu').stickyfloat({duration: 400});
 * parameters:
 * 		duration 	- the duration of the animation
 *		startOffset - the amount of scroll offset after it the animations kicks in
 *		offsetY		- the offset from the top when the object is animated
 *		lockBottom	- 'true' by default, set to false if you don't want your floating box to stop at parent's bottom
 * $Version: 05.16.2009 r1
 * Copyright (c) 2009 Yair Even-Or
 * vsync.design@gmail.com

 * Mods by: Christopher Haupt, Webvanta Inc. 
 * http://www.webvanta.com/, http://twitter.com/chaupt, http://github.com/chaupt
 * Honor options set by user.
 */
  $.fn.stickyfloat = function(options, lockBottom) {
          var $obj        = this;
          var parentPaddingTop  = parseInt($obj.parent().css('padding-top'));
          var startOffset     = $obj.parent().offset().top;
          var opts        = $.extend({ startOffset: startOffset, offsetY: parentPaddingTop, duration: 200, lockBottom:false }, options);

          $obj.css({ position: 'absolute' });

          if(opts.lockBottom){
            var bottomPos = $obj.parent().height() - $obj.height() + opts.offsetY; //get the maximum scrollTop value
            if( bottomPos < 0 )
              bottomPos = 0;
          }

          $(window).scroll(function () { 
            $obj.stop(); // stop all calculations on scroll event

            var pastStartOffset     = $(document).scrollTop() > opts.startOffset; // check if the window was scrolled down more than the start offset declared.
            var objFartherThanTopPos  = $obj.offset().top > opts.startOffset; // check if the object is at it's top position (starting point)
            var objBiggerThanWindow   = $obj.outerHeight() < $(window).height();  // if the window size is smaller than the Obj size, then do not animate.

            // if window scrolled down more than startOffset OR obj position is greater than
            // the top position possible (+ offsetY) AND window size must be bigger than Obj size
            if( (pastStartOffset || objFartherThanTopPos) && objBiggerThanWindow ){ 
              var newpos = ($(document).scrollTop() - opts.startOffset + opts.offsetY );
              if ( newpos > bottomPos )
                newpos = bottomPos;
              if ( $(document).scrollTop() < opts.startOffset ) // if window scrolled < starting offset, then reset Obj position (opts.offsetY);
                newpos = opts.offsetY;

              $obj.animate({ top: newpos }, opts.duration );
            }
          });
        };
jQuery.extend( jQuery.easing,{
				def: 'easeOutQuad',
				swing: function (x, t, b, c, d) {
					//alert(jQuery.easing.default);
					return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
				},
				easeInQuad: function (x, t, b, c, d) {
					return c*(t/=d)*t + b;
				},
				easeOutQuad: function (x, t, b, c, d) {
					return -c *(t/=d)*(t-2) + b;
				},
				easeInOutQuad: function (x, t, b, c, d) {
					if ((t/=d/2) < 1) return c/2*t*t + b;
					return -c/2 * ((--t)*(t-2) - 1) + b;
				},
				easeOutElastic: function (x, t, b, c, d) {
					var s=1.70158;var p=0;var a=c;
					if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
					if (a < Math.abs(c)) { a=c; var s=p/4; }
					else var s = p/(2*Math.PI) * Math.asin (c/a);
					return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
				},
				easeInOutElastic: function (x, t, b, c, d) {
					var s=1.70158;var p=0;var a=c;
					if (t==0) return b;  if ((t/=d/2)==2) return b+c;  if (!p) p=d*(.3*1.5);
					if (a < Math.abs(c)) { a=c; var s=p/4; }
					else var s = p/(2*Math.PI) * Math.asin (c/a);
					if (t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
					return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*.5 + c + b;
				},
				easeInBack: function (x, t, b, c, d, s) {
					if (s == undefined) s = 1.70158;
					return c*(t/=d)*t*((s+1)*t - s) + b;
				},
				easeOutBack: function (x, t, b, c, d, s) {
					if (s == undefined) s = 1.70158;
					return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
				},
				easeInOutBack: function (x, t, b, c, d, s) {
					if (s == undefined) s = 1.70158; 
					if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
					return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
				}
			});