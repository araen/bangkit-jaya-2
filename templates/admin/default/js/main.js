jQuery(function(){
    jQuery('.number').keyup(function(){
            this.value = this.value.replace(/[^0-9\.]/,''); 
    });
    jQuery('.currency').keyup(function(){
        money(jQuery(this).get(0));
    });
});
      
function money(objNum){
    var num = objNum.value
    var ent, dec;

    if (num != '' && num != objNum.oldvalue){
        num = MoneyToNumber(num);
        if (isNaN(num)){
            objNum.value = (objNum.oldvalue)?objNum.oldvalue:'';
        }else{
            var ev = (navigator.appName.indexOf('Netscape') != -1)?Event:event;
            if (ev.keyCode == 190 || !isNaN(num.split('.')[1])){
                objNum.value = AddCommas(num.split('.')[0])+'.'+num.split('.')[1];
            }
            else{
                objNum.value = AddCommas(num.split('.')[0]);
            }
            objNum.oldvalue = objNum.value;
        }
    }
}

function MoneyToNumber(num){
    return (num.replace(/,/g, ''));
}

function AddCommas(num){
    numArr=new String(num).split('').reverse();
    for (i=3;i<numArr.length;i+=3){
        numArr[i]+=',';
    }
    return numArr.reverse().join('');
}

function autocomplete(obj,url)
{
    var data;
    jQuery.ajax({
        url : url,
        type: 'get',
        data: {term:jQuery(obj).val()},
        success:function(respon){
            data = jQuery.parseJSON(respon);

        }   
    });
}

function autocompletBind(obj,data){
    jQuery(obj).smartAutoComplete({source: data, maxResults: 5, delay: 200 } );
    jQuery(obj).bind({

       keyIn: function(ev){
         var tag_list = ev.smartAutocompleteData.query.split(","); 
         //pass the modified query to default event
         ev.smartAutocompleteData.query = jQuery.trim(tag_list[tag_list.length - 1]);
       },

        itemSelect: function(ev, selected_item){ 
        var options = jQuery(this).smartAutoComplete();

        //get the text from selected item
        var selected_value = jQuery(selected_item).text();
        var cur_list = jQuery(this).val().split(","); 
        cur_list[cur_list.length - 1] = selected_value;
        jQuery(this).val(cur_list.join(",") + ","); 

        //set item selected property
        options.setItemSelected(true);

        //hide results container
        jQuery(this).trigger('lostFocus');
          
        //prevent default event handler from executing
        ev.preventDefault();
      },

    });
}